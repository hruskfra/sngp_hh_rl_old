package heuristics;

import gp.Population;
import gp.tasks.bpp.BPP;
import gp.tasks.Instance;
import gp.tasks.Problem;
import gp.tasks.bpp.BPPInstance;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import sngp.MethodName;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 * Created by Fanda on 05-Feb-16.
 */
public abstract class Heuristic {

    /**
     * Name of the heuristic.
     */
    public String name = "";

    /**
     * Fitness values on problem instances.
     */
    public double[] instancesFitness;

    /**
     * Average fitness of the best llh across all instances.
     */
    public static double AVG_BEST_LLH_FITNESS = 0.0;

    /**
     * Applies one iteration of the heuristic.
     * @param inst actual instance ID
     * @return integer value that determines result of the application step
     */
    public abstract int applyHeuristic(Instance inst);

    /**
     * Returns name of the heuristic.
     * @return name of the heuristic
     */
    public String getName() {
        return this.getClass().getSimpleName();
    }

    /**
     * Restarts values related to the particular heuristic.
     */
    public void restartValues() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for (int i = 0; i < this.instancesFitness.length; i++) {
            if (Settings.FITNESS_MINIMIZATION) {
                this.instancesFitness[i] = Double.MAX_VALUE;
            } else {
                this.instancesFitness[i] = -Double.MAX_VALUE;
            }
        }
    }

    public static void writeBestPerformanceIntoCSV() {
        try {
            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_BEST.csv");

            //--- Header line.
            pout.write("-;");
            for (int i = 0; i < BPP.llhs.length; i++) {
                pout.write(BPP.llhs[i].getName());
                if (i < BPP.llhs.length - 1) {
                    pout.write(";");
                }
            }
            if (BPP.evolvedHeuristics.size() > 0) {
                pout.write(";");
            }
            for (int i = 0; i < BPP.evolvedHeuristics.size(); i++) {
                pout.write("GHH_" + i);
                if (i < BPP.evolvedHeuristics.size() - 1) {
                    pout.write(";");
                }
            }
            pout.write("\r\n");

            //--- Lines with LLHS.
            for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
                double bestValueLLH = Integer.MAX_VALUE;
                for (int h = 0; h < BPP.llhs.length; h++) {
                    if (BPP.llhs[h].instancesFitness[i] < bestValueLLH) {
                        bestValueLLH = BPP.llhs[h].instancesFitness[i];
                    }
                }
                for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                    if ((int) (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i]) < bestValueLLH) {
                        bestValueLLH = (int) (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i]);
                    }
                }

                pout.write(BPP.INSTANCES[i].name + ";");
                for (int h = 0; h < BPP.llhs.length; h++) {

                    if (BPP.llhs[h].instancesFitness[i] == bestValueLLH) {
                        if (bestValueLLH == BPP.INSTANCES[i].theoreticalOptimumFitness) {
                            pout.write("1");
                        } else if (bestValueLLH == BPP.INSTANCES[i].theoreticalOptimumFitness + 1) {
                            pout.write("2");
                        } else {
                            pout.write("3");
                        }
                    } else {
                        pout.write("");
                    }
                    if (h < BPP.llhs.length - 1) {
                        pout.write(";");
                    }
                }

                for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                    //System.out.println(MethodName.getCurrentMethodName() + " " + h + "(" + BPP.evolvedHeuristics.get(h).getBestNodeID() + ")) " + BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i] + " VS " + bestValueLLH);
                    pout.write(";");
                    if ((int) (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i]) == bestValueLLH) {
                        if (bestValueLLH == BPP.INSTANCES[i].theoreticalOptimumFitness) {
                            pout.write("1");
                        } else if (bestValueLLH == BPP.INSTANCES[i].theoreticalOptimumFitness + 1) {
                            pout.write("2");
                        } else {
                            pout.write("3");
                        }
                    } else {
                        pout.write("");
                    }
                }
                pout.write("\r\n");
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {

        }
    }

    public static void writeBestPerformanceIntoHTML() {

        int LLH_Best_Counter[] = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
        Arrays.fill(LLH_Best_Counter, 0);

        try {
            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "LLH_FITNESS.html");

            pout.write("<HEAD>\r\n");
            pout.write("<TITLE>LLH Results</TITLE>\r\n");
            pout.write("</HEAD>\r\n");
            pout.write("<BODY>\r\n");
            pout.write("<TABLE border=\"1\" bordercolor=\"black\">\r\n");

            pout.write("<TR><TD>Instance</TD>");
            for (int i = 0; i < BPP.llhs.length; i++) {
                pout.write("<TD>" + BPP.llhs[i].getName() + "</TD>");
            }
            for (int i = 0; i < BPP.evolvedHeuristics.size(); i++) {
                pout.write("<TD>" + "GHH_" + i + "</TD>");
            }
            pout.write("<TD>SHH</TD>");
            pout.write("</TR>\r\n");

            //--- Lines with LLHS.
            for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {

                double bestValueLLH = Double.MAX_VALUE;
                for (int h = 0; h < BPP.llhs.length; h++) {
                    if (BPP.llhs[h].instancesFitness[i] < bestValueLLH) {
                        bestValueLLH = BPP.llhs[h].instancesFitness[i];
                    }
                }
                for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                    if (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i] < bestValueLLH) {
                        bestValueLLH = BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i];
                    }
                }
                if (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i] < bestValueLLH) {
                    bestValueLLH = Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i];
                }

                pout.write("<TR><TD>" + BPP.INSTANCES[i].name + "</TD>");
                for (int h = 0; h < BPP.llhs.length; h++) {
                    if (BPP.llhs[h].instancesFitness[i] == bestValueLLH) {
                        LLH_Best_Counter[h]++;
                        pout.write("<TD bgcolor=\"green\">" + BPP.llhs[h].instancesFitness[i] + "</TD>");
                    } else {
                        pout.write("<TD>" + BPP.llhs[h].instancesFitness[i] + "</TD>");
                    }
                }

                for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                    if ((BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i]) == bestValueLLH) {
                        LLH_Best_Counter[BPP.actions.length + h]++;
                        pout.write("<TD bgcolor=\"green\">" + BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i] + "</TD>");
                    } else {
                        pout.write("<TD>" + BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i] + "</TD>");
                    }
                }
                if (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i] == bestValueLLH) {
                    LLH_Best_Counter[LLH_Best_Counter.length - 1]++;
                    pout.write("<TD bgcolor=\"green\">" + Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i] + "</TD>");
                } else {
                    pout.write("<TD>" + Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i] + "</TD>");
                }
                pout.write("</TR>\r\n");
            }
            pout.write("</TABLE>\r\n");

            pout.write("<TABLE border=\"1\" bordercolor=\"black\">\r\n");
            pout.write("<TR>");
            for (int i = 0; i < BPP.llhs.length; i++) {
                pout.write("<TD>" + BPP.llhs[i].getName() + "</TD>");
            }
            for (int i = 0; i < BPP.evolvedHeuristics.size(); i++) {
                pout.write("<TD>" + "GHH_" + i + "</TD>");
            }
            pout.write("<TD>SHH</TD></TR>\r\n");

            int best = 0;
            for (int i = 0; i < LLH_Best_Counter.length; i++) {
                if (LLH_Best_Counter[i] > best) {
                    best = LLH_Best_Counter[i];
                }

            }
            pout.write("<TR>");
            for (int i = 0; i < LLH_Best_Counter.length; i++) {
                if (LLH_Best_Counter[i] == best) {
                    pout.write("<TD bgcolor=\"green\">" + LLH_Best_Counter[i] + "</TD>");
                } else {
                    pout.write("<TD>" + LLH_Best_Counter[i] + "</TD>");
                }

            }
            pout.write("</TR>\r\n");

            pout.write("</TABLE>\r\n");
            pout.write("</BODY>");
            pout.close();
        } catch (FileNotFoundException FNFE) {

        }

    }

    public static void writeBestPerformanceIntoHTML2() {

        int LLH_Best_Counter[] = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
        Arrays.fill(LLH_Best_Counter, 0);
        int LLH_Bin_Counter[] = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
        Arrays.fill(LLH_Bin_Counter, 0);
        try {
            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "LLH_BINS.html");

            pout.write("<HEAD>\r\n");
            pout.write("<TITLE>LLH Results</TITLE>\r\n");
            pout.write("</HEAD>\r\n");
            pout.write("<BODY>\r\n");
            pout.write("<TABLE border=\"1\" bordercolor=\"black\">\r\n");

            pout.write("<TR><TD>Instance</TD>");
            for (int i = 0; i < BPP.llhs.length; i++) {
                pout.write("<TD>" + BPP.llhs[i].getName() + "</TD>");
            }
            for (int i = 0; i < BPP.evolvedHeuristics.size(); i++) {
                pout.write("<TD>" + "GHH_" + i + "</TD>");
            }
            pout.write("<TD>SHH</TD>");
            pout.write("</TR>\r\n");

            //--- Lines with LLHS.
            for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {

                int bestValueLLH = Integer.MAX_VALUE;
                for (int h = 0; h < BPP.llhs.length; h++) {
                    if ((int) BPP.llhs[h].instancesFitness[i] < bestValueLLH) {
                        bestValueLLH = (int) BPP.llhs[h].instancesFitness[i];
                    }
                }
                for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                    if ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i] < bestValueLLH) {
                        bestValueLLH = (int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i];
                    }
                }
                if ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i] < bestValueLLH) {
                    bestValueLLH = (int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i];
                }

                pout.write("<TR><TD>" + BPP.INSTANCES[i].name + "</TD>");
                for (int h = 0; h < BPP.llhs.length; h++) {
                    LLH_Bin_Counter[h] += (int) BPP.llhs[h].instancesFitness[i];
                    if ((int) BPP.llhs[h].instancesFitness[i] == bestValueLLH) {
                        LLH_Best_Counter[h]++;
                        pout.write("<TD bgcolor=\"green\">" + ((int) BPP.llhs[h].instancesFitness[i]) + "</TD>");
                    } else {
                        pout.write("<TD>" + ((int) BPP.llhs[h].instancesFitness[i]) + "</TD>");
                    }
                }

                for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                    LLH_Bin_Counter[BPP.actions.length + h] += (int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i];
                    if ((int) (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i]) == bestValueLLH) {
                        LLH_Best_Counter[BPP.actions.length + h]++;
                        pout.write("<TD bgcolor=\"green\">" + ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i]) + "</TD>");
                    } else {
                        pout.write("<TD>" + ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[i]) + "</TD>");
                    }
                }
                LLH_Bin_Counter[LLH_Bin_Counter.length - 1] += (int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i];
                if ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i] == bestValueLLH) {
                    LLH_Best_Counter[LLH_Best_Counter.length - 1]++;
                    pout.write("<TD bgcolor=\"green\">" + ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i]) + "</TD>");
                } else {
                    pout.write("<TD>" + ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[i]) + "</TD>");
                }
                pout.write("</TR>\r\n");
            }
            pout.write("</TABLE>\r\n");

            pout.write("<TABLE border=\"1\" bordercolor=\"black\">\r\n");
            pout.write("<TR><TD></TD>");
            for (int i = 0; i < BPP.llhs.length; i++) {
                pout.write("<TD>" + BPP.llhs[i].getName() + "</TD>");
            }
            for (int i = 0; i < BPP.evolvedHeuristics.size(); i++) {
                pout.write("<TD>" + "GHH_" + i + "</TD>");
            }
            pout.write("<TD>SHH</TD></TR>\r\n");

            int bestCounter = 0;
            for (int i = 0; i < LLH_Best_Counter.length; i++) {
                if (LLH_Best_Counter[i] > bestCounter) {
                    bestCounter = LLH_Best_Counter[i];
                }
            }
            int bestBin = Integer.MAX_VALUE;
            for (int i = 0; i < LLH_Bin_Counter.length; i++) {
                if (LLH_Bin_Counter[i] < bestBin) {
                    bestBin = LLH_Bin_Counter[i];
                }
            }
            pout.write("<TR><TD>#Best solutions</TD>");
            for (int i = 0; i < LLH_Best_Counter.length; i++) {
                if (LLH_Best_Counter[i] == bestCounter) {
                    pout.write("<TD bgcolor=\"green\">" + LLH_Best_Counter[i] + "</TD>");
                } else {
                    pout.write("<TD>" + LLH_Best_Counter[i] + "</TD>");
                }

            }
            pout.write("</TR>\r\n");
            pout.write("<TR><TD>#Bins used</TD>");
            for (int i = 0; i < LLH_Bin_Counter.length; i++) {
                if (LLH_Bin_Counter[i] == bestBin) {
                    pout.write("<TD bgcolor=\"green\">" + LLH_Bin_Counter[i] + "</TD>");
                } else {
                    pout.write("<TD>" + LLH_Bin_Counter[i] + "</TD>");
                }

            }
            pout.write("</TR>\r\n");

            pout.write("</TABLE>\r\n");
            pout.write("</BODY>");
            pout.close();
        } catch (FileNotFoundException FNFE) {

        }

    }

    /**
     * Write HTML tables with result divided by specific subsets.
     */
    public static void writeGroupedBestPerformanceIntoHTML() {


        int LLH_Best_Bins_Counter[];
        double LLH_Best_Fitness_Counter[];
        int LLH_Bins_Counter[];
        double LLH_Fitness_Counter[];
        int LLH_Unique_Wins_Counter[];

        //--- StringBuilder for different tables.
        StringBuilder tableHeaderSB = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        //--- StringBuilders for bar graph.
        StringBuilder binsSB = new StringBuilder();
        StringBuilder barSB = new StringBuilder();

        try {
            PrintWriter pout2 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_GROUPED_BINS.html");
            PrintWriter pout1 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_GROUPED_FITNESS.html");
            //--- Head of the HTML file.
            tableHeaderSB.append("<HEAD>\r\n");
            tableHeaderSB.append("<TITLE>LLH Results</TITLE>\r\n");
            tableHeaderSB.append("</HEAD>\r\n");
            tableHeaderSB.append("<BODY>\r\n");

            pout1.write(tableHeaderSB.toString());
            pout2.write(tableHeaderSB.toString());

            //--- Head of the tables.
            tableHeaderSB = new StringBuilder();
            tableHeaderSB.append("<TABLE border=\"1\" bordercolor=\"black\">\r\n");
            tableHeaderSB.append("<TR><TD>Instance</TD>");
            for (int i = 0; i < BPP.llhs.length; i++) {
                tableHeaderSB.append("<TD>" + BPP.llhs[i].getName() + "</TD>");
            }
            for (int i = 0; i < BPP.evolvedHeuristics.size(); i++) {
                tableHeaderSB.append("<TD>" + "GHH_" + i + "</TD>");
            }
            tableHeaderSB.append("<TD>SHH</TD>");
            tableHeaderSB.append("</TR>\r\n");

            //--- Create new Matlab arrays.
            binsSB.append("wins = [");

            //--- Go through all instance subsets stored in the hashmap.
            for (int k = 0; k < Problem.instanceSubgroups.length; k++)  {
                String key = Problem.instanceSubgroups[k];
                //--- Title.
                pout1.write("<H2>" + key + "</H2>\r\n");
                pout2.write("<H2>" + key + "</H2>\r\n");
                //--- New table.
                pout1.write(tableHeaderSB.toString());
                pout2.write(tableHeaderSB.toString());

                sb2 = new StringBuilder(); //--- New table body.
                sb1 = new StringBuilder();

                //--- Reset all counters.
                LLH_Best_Bins_Counter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Best_Bins_Counter, 0);
                LLH_Bins_Counter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Bins_Counter, 0);
                LLH_Best_Fitness_Counter = new double[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Best_Fitness_Counter, 0);
                LLH_Fitness_Counter = new double[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Fitness_Counter, 0);
                LLH_Unique_Wins_Counter = new int[Problem.llhs.length + Problem.evolvedHeuristics.size() +1];
                Arrays.fill(LLH_Unique_Wins_Counter, 0);

                String color = "";
                double aHH_FITNESS;
                for (int i = 0; i < Problem.INSTANCES.length; i++) {

                    if (Problem.INSTANCES[i].instanceSubgroup.equals(key)) {
                        int inst = i;

                        //--- Get best value among all heuristics.
                        int bestBinValueLLH = Integer.MAX_VALUE;
                        int bestBinID = -1; //--- Index for getting heuristic with unique best result.
                        int nBest = 0;
                        double bestFitnessValueLLH = Double.MAX_VALUE;
                        //--- LLHs.
                        for (int h = 0; h < BPP.llhs.length; h++) {
                            aHH_FITNESS = Problem.llhs[h].instancesFitness[inst];
                            //--- Get best values.
                            if ((int) aHH_FITNESS < bestBinValueLLH) {
                                bestBinValueLLH = (int) aHH_FITNESS;
                                bestBinID = h;
                            }
                            if (aHH_FITNESS < bestFitnessValueLLH) {
                                bestFitnessValueLLH = aHH_FITNESS;
                            }
                        }
                        //--- Evolved heuristics.
                        for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                            aHH_FITNESS = BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                            //--- Get best values.
                            if ((int) aHH_FITNESS < bestBinValueLLH) {
                                bestBinValueLLH = (int) aHH_FITNESS;
                                bestBinID = h + Problem.llhs.length;
                            }
                            if (aHH_FITNESS < bestFitnessValueLLH) {
                                bestFitnessValueLLH = aHH_FITNESS;
                            }
                        }
                        //--- Selection HH result.
                        //--- Get best values.
                        aHH_FITNESS = Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                        if ((int) aHH_FITNESS < bestBinValueLLH) {
                            bestBinValueLLH = (int) aHH_FITNESS;
                            bestBinID = Problem.llhs.length + Problem.evolvedHeuristics.size();
                        }
                        if (aHH_FITNESS < bestFitnessValueLLH) {
                            bestFitnessValueLLH = aHH_FITNESS;
                        }

                        sb1.append("<TR><TD>" + BPP.INSTANCES[inst].name + "</TD>");
                        sb2.append("<TR><TD>" + BPP.INSTANCES[inst].name + "</TD>");

                        //--- Write output on actual instance into the table.
                        for (int h = 0; h < BPP.llhs.length; h++) {
                            aHH_FITNESS = Problem.llhs[h].instancesFitness[inst];
                            //--- Bins.
                            LLH_Bins_Counter[h] += (int) aHH_FITNESS;

                            //--- Get proper color.
                            if ((int) aHH_FITNESS == bestBinValueLLH) {
                                LLH_Best_Bins_Counter[h]++;
                                nBest++;
                                sb2.append("<TD bgcolor=\"green\">" + ((int) aHH_FITNESS) + "</TD>");
                            } else {
                                sb2.append("<TD>" + ((int) aHH_FITNESS) + "</TD>");
                            }
                            //--- Fitness.
                            LLH_Fitness_Counter[h] += aHH_FITNESS;
                            if (aHH_FITNESS == bestFitnessValueLLH) {
                                LLH_Best_Fitness_Counter[h]++;
                                sb1.append("<TD bgcolor=\"green\">" + (aHH_FITNESS) + "</TD>");
                            } else {
                                sb1.append("<TD>" + (aHH_FITNESS) + "</TD>");
                            }
                        }

                        for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                            aHH_FITNESS = BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                            //--- Bins.
                            LLH_Bins_Counter[BPP.actions.length + h] += (int) aHH_FITNESS;
                            if ((int) (aHH_FITNESS) == bestBinValueLLH) {
                                LLH_Best_Bins_Counter[BPP.actions.length + h]++;
                                nBest++;
                                sb2.append("<TD bgcolor=\"green\">" + ((int) aHH_FITNESS) + "</TD>");
                            } else {
                                sb2.append("<TD>" + ((int) aHH_FITNESS) + "</TD>");
                            }

                            //--- Fitness.
                            LLH_Fitness_Counter[BPP.actions.length + h] += aHH_FITNESS;
                            if (aHH_FITNESS == bestFitnessValueLLH) {
                                LLH_Best_Fitness_Counter[BPP.actions.length + h]++;
                                sb1.append("<TD bgcolor=\"green\">" + (aHH_FITNESS) + "</TD>");
                            } else {
                                sb1.append("<TD>" + (aHH_FITNESS) + "</TD>");
                            }
                        }

                        //--- Bins.
                        aHH_FITNESS = Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                        LLH_Bins_Counter[LLH_Bins_Counter.length - 1] += (int) aHH_FITNESS;
                        if ((int) aHH_FITNESS == bestBinValueLLH) {
                            LLH_Best_Bins_Counter[LLH_Best_Bins_Counter.length - 1]++;
                            nBest++;
                            sb2.append("<TD bgcolor=\"green\">" + ((int) aHH_FITNESS) + "</TD>");
                        } else {
                            sb2.append("<TD>" + ((int) aHH_FITNESS) + "</TD>");
                        }
                        sb2.append("</TR>\r\n");
                        //--- Fitness
                        LLH_Fitness_Counter[LLH_Bins_Counter.length - 1] += aHH_FITNESS;
                        if (aHH_FITNESS == bestFitnessValueLLH) {
                            LLH_Best_Fitness_Counter[LLH_Best_Fitness_Counter.length - 1]++;
                            sb1.append("<TD bgcolor=\"green\">" + (aHH_FITNESS) + "</TD>");
                        } else {
                            sb1.append("<TD>" + (aHH_FITNESS) + "</TD>");
                        }
                        sb1.append("</TR>\r\n");

                        if (nBest == 1) {
                            LLH_Unique_Wins_Counter[bestBinID]++;
                        }
                    }
                }


                sb1.append("</TABLE>\r\n");
                sb1.append("<BR />\r\n");
                sb2.append("</TABLE>\r\n");
                sb2.append("<BR />\r\n");

                pout1.write(sb1.toString());
                pout2.write(sb2.toString());

                pout1.write(tableHeaderSB.toString());
                double bestFitnessCounter = 0;
                for (int i = 0; i < LLH_Best_Fitness_Counter.length; i++) {
                    if (LLH_Best_Fitness_Counter[i] > bestFitnessCounter) {
                        bestFitnessCounter = LLH_Best_Fitness_Counter[i];
                    }
                }
                double bestFitness = Double.MAX_VALUE;
                for (int i = 0; i < LLH_Fitness_Counter.length; i++) {
                    if (LLH_Fitness_Counter[i] < bestFitness) {
                        bestFitness = LLH_Fitness_Counter[i];
                    }
                }
                pout1.write("<TR><TD>Fitness Sum</TD>");
                for (int i = 0; i < LLH_Fitness_Counter.length; i++) {
                    if (LLH_Fitness_Counter[i] == bestFitness) {
                        pout1.write("<TD bgcolor=\"green\">" + LLH_Fitness_Counter[i] + "</TD>");
                    } else {
                        pout1.write("<TD>" + LLH_Fitness_Counter[i] + "</TD>");
                    }
                }
                pout1.write("<TR><TD>#Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Fitness_Counter.length; i++) {
                    if (LLH_Best_Fitness_Counter[i] == bestFitnessCounter) {
                        pout1.write("<TD bgcolor=\"green\">" + LLH_Best_Fitness_Counter[i] + "</TD>");
                    } else {
                        pout1.write("<TD>" + LLH_Best_Fitness_Counter[i] + "</TD>");
                    }
                }

                pout1.write("</TR>\r\n");
                pout1.write("</TABLE>\r\n");
                pout1.write("<BR />\r\n");
                pout1.write("<HR>\r\n");
                pout1.write("</TR>\r\n");

                pout2.write(tableHeaderSB.toString());
                int bestCounter = 0;
                for (int i = 0; i < LLH_Best_Bins_Counter.length; i++) {
                    if (LLH_Best_Bins_Counter[i] > bestCounter) {
                        bestCounter = LLH_Best_Bins_Counter[i];
                    }
                }
                int bestBin = Integer.MAX_VALUE;
                for (int i = 0; i < LLH_Bins_Counter.length; i++) {
                    if (LLH_Bins_Counter[i] < bestBin) {
                        bestBin = LLH_Bins_Counter[i];
                    }
                }
                pout2.write("<TR><TD>#Bins used</TD>");
                for (int i = 0; i < LLH_Bins_Counter.length; i++) {
                    if (LLH_Bins_Counter[i] == bestBin) {
                        pout2.write("<TD bgcolor=\"green\">" + LLH_Bins_Counter[i] + "</TD>");
                    } else {
                        pout2.write("<TD>" + LLH_Bins_Counter[i] + "</TD>");
                    }
                }
                pout2.write("</TR>\r\n");
                pout2.write("<TR><TD>#Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Bins_Counter.length; i++) {
                    if (LLH_Best_Bins_Counter[i] == bestCounter) {
                        pout2.write("<TD bgcolor=\"green\">" + LLH_Best_Bins_Counter[i] + "</TD>");
                    } else {
                        pout2.write("<TD>" + LLH_Best_Bins_Counter[i] + "</TD>");
                    }
                }
                pout2.write("</TR>\r\n");

                pout2.write("<TR><TD>#Unique solutions</TD>");
                for (int i = 0; i < LLH_Unique_Wins_Counter.length; i++) {
                        pout2.write("<TD>" +  LLH_Unique_Wins_Counter[i] + "</TD>");
                }
                pout2.write("</TR>\r\n");
                pout2.write("</TR>\r\n");
                pout2.write("</TABLE>\r\n");
                pout2.write("<BR />\r\n");
                pout2.write("<HR>\r\n");

                //--- Fill the graph with data for the matlab file.
                if(k > 0) binsSB.append(";");
                for (int llh = 0; llh < LLH_Bins_Counter.length; llh++) {
                    //--- Array with the numbers of wins.
                    binsSB.append(LLH_Bins_Counter[llh]);
                    if(llh < LLH_Bins_Counter.length -1) {
                        binsSB.append(" ");
                    }

                }

            }

            pout1.close();
            pout2.close();
            System.out.println(MethodName.getCurrentMethodName() + ": RESULTS WRITER - Table with numbers of used bins divided into instance subgroups " + SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_GROUPED_BINS ... SAVED");
            System.out.println(MethodName.getCurrentMethodName() + ": RESULTS WRITER - Table with fitness values divided into instance subgroups " + SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_GROUPED_FITNESS ... SAVED");

            //--- End of the bar graph.
            binsSB.append("];\r\n");

            barSB.append("figure\r\nhold on\r\n");
            barSB.append("b = bar(wins);\r\n");

            PrintWriter pout3 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_GROUPED_BINS_BAR_GRAPH.m");
            //--- Write StringBuilders into the file.
            pout3.write(binsSB.toString());
            pout3.write(barSB.toString());
            pout3.close();
            System.out.println(MethodName.getCurrentMethodName() + ": RESULTS WRITER - Data for the grouped bar graphs of llh wins in the file " + SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_GROUPED_BINS_BAR_GRAPH.m ... SAVED");

        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
            System.exit(1);
        }

    }

    public static void writeNumberOfItemsBestPerformanceIntoHTML() {
        HashMap<String, ArrayList<Integer>> groups = new HashMap();
        for (int inst = 0; inst < Problem.NUMBER_OF_INSTANCES; inst++) {
            if (!groups.containsKey(Problem.INSTANCES[inst].size + "")) {
                groups.put(Problem.INSTANCES[inst].size + "", new ArrayList());
            }
            groups.get(Problem.INSTANCES[inst].size + "").add(inst);
        }

        /*for(String key: groups.keySet()) {
            System.out.println(key + " " + groups.get(key).toString());
        }/**/
        int LLH_Best_Bins_Counter[];
        double LLH_Best_Fitness_Counter[];
        int LLH_Bin_Counter[];
        double LLH_Fitness_Counter[];

        //--- StringBuilder for different tables.
        StringBuilder tableHeaderSB = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        try {
            PrintWriter pout2 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_NUMBER_OF_ITEMS_BINS.html");
            PrintWriter pout1 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_NUMBER_OF_ITEMS_FITNESS.html");

            //--- Head of the HTML file.
            tableHeaderSB.append("<HEAD>\r\n");
            tableHeaderSB.append("<TITLE>LLH Results</TITLE>\r\n");
            tableHeaderSB.append("</HEAD>\r\n");
            tableHeaderSB.append("<BODY>\r\n");

            pout1.write(tableHeaderSB.toString());
            pout2.write(tableHeaderSB.toString());

            //--- Head of the tables.
            tableHeaderSB = new StringBuilder();
            tableHeaderSB.append("<TABLE border=\"1\" bordercolor=\"black\">\r\n");
            tableHeaderSB.append("<TR><TD>Instance</TD>");
            for (int i = 0; i < BPP.llhs.length; i++) {
                tableHeaderSB.append("<TD>" + BPP.llhs[i].getName() + "</TD>");
            }
            for (int i = 0; i < BPP.evolvedHeuristics.size(); i++) {
                tableHeaderSB.append("<TD>" + "GHH_" + i + "</TD>");
            }
            tableHeaderSB.append("<TD>SHH</TD>");
            tableHeaderSB.append("</TR>\r\n");

            //--- Go through all instance subsets stored in the hashmap.
            for (String key : groups.keySet()) {
                //--- Title.
                pout1.write("<H2>" + key + "</H2>\r\n");
                pout2.write("<H2>" + key + "</H2>\r\n");
                //--- New table.
                pout1.write(tableHeaderSB.toString());
                pout2.write(tableHeaderSB.toString());

                sb2 = new StringBuilder(); //--- New table body.
                sb1 = new StringBuilder();
                //--- Reset all counters.
                LLH_Best_Bins_Counter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Best_Bins_Counter, 0);
                LLH_Bin_Counter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Bin_Counter, 0);
                LLH_Best_Fitness_Counter = new double[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Best_Fitness_Counter, 0);
                LLH_Fitness_Counter = new double[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Fitness_Counter, 0);

                for (int p = 0; p < groups.get(key).size(); p++) {
                    int inst = groups.get(key).get(p);

                    //--- Get best value among all heuristics.
                    int bestBinValueLLH = Integer.MAX_VALUE;
                    double bestFitnessValueLLH = Double.MAX_VALUE;
                    for (int h = 0; h < BPP.llhs.length; h++) {
                        if ((int) BPP.llhs[h].instancesFitness[inst] < bestBinValueLLH) {
                            bestBinValueLLH = (int) BPP.llhs[h].instancesFitness[inst];
                        }
                        if (BPP.llhs[h].instancesFitness[inst] < bestFitnessValueLLH) {
                            bestFitnessValueLLH = BPP.llhs[h].instancesFitness[inst];
                        }
                    }
                    for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                        if ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst] < bestBinValueLLH) {
                            bestBinValueLLH = (int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        }
                        if (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst] < bestFitnessValueLLH) {
                            bestFitnessValueLLH = BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        }
                    }
                    if ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] < bestBinValueLLH) {
                        bestBinValueLLH = (int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    }
                    if (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] < bestFitnessValueLLH) {
                        bestFitnessValueLLH = Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    }

                    sb1.append("<TR><TD>" + BPP.INSTANCES[inst].name + "</TD>");
                    sb2.append("<TR><TD>" + BPP.INSTANCES[inst].name + "</TD>");
                    for (int h = 0; h < BPP.llhs.length; h++) {
                        //--- Bins.
                        LLH_Bin_Counter[h] += (int) BPP.llhs[h].instancesFitness[inst];
                        if ((int) BPP.llhs[h].instancesFitness[inst] == bestBinValueLLH) {
                            LLH_Best_Bins_Counter[h]++;
                            sb2.append("<TD bgcolor=\"green\">" + ((int) BPP.llhs[h].instancesFitness[inst]) + "</TD>");
                        } else {
                            sb2.append("<TD>" + ((int) BPP.llhs[h].instancesFitness[inst]) + "</TD>");
                        }
                        //--- Fitness.
                        LLH_Fitness_Counter[h] += BPP.llhs[h].instancesFitness[inst];
                        if (BPP.llhs[h].instancesFitness[inst] == bestFitnessValueLLH) {
                            LLH_Best_Fitness_Counter[h]++;
                            sb1.append("<TD bgcolor=\"green\">" + (BPP.llhs[h].instancesFitness[inst]) + "</TD>");
                        } else {
                            sb1.append("<TD>" + (BPP.llhs[h].instancesFitness[inst]) + "</TD>");
                        }
                    }

                    for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                        //--- Bins.
                        LLH_Bin_Counter[BPP.actions.length + h] += (int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        if ((int) (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) == bestBinValueLLH) {
                            LLH_Best_Bins_Counter[BPP.actions.length + h]++;
                            sb2.append("<TD bgcolor=\"green\">" + ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) + "</TD>");
                        } else {
                            sb2.append("<TD>" + ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) + "</TD>");
                        }

                        //--- Fitness.
                        LLH_Fitness_Counter[BPP.actions.length + h] += BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        if ((BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) == bestFitnessValueLLH) {
                            LLH_Best_Fitness_Counter[BPP.actions.length + h]++;
                            sb1.append("<TD bgcolor=\"green\">" + (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) + "</TD>");
                        } else {
                            sb1.append("<TD>" + (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) + "</TD>");
                        }
                    }

                    //--- Bins.
                    LLH_Bin_Counter[LLH_Bin_Counter.length - 1] += (int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    if ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] == bestBinValueLLH) {
                        LLH_Best_Bins_Counter[LLH_Best_Bins_Counter.length - 1]++;
                        sb2.append("<TD bgcolor=\"green\">" + ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst]) + "</TD>");
                    } else {
                        sb2.append("<TD>" + ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst]) + "</TD>");
                    }
                    sb2.append("</TR>\r\n");
                    //--- Fitness
                    LLH_Fitness_Counter[LLH_Bin_Counter.length - 1] += Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    if (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] == bestFitnessValueLLH) {
                        LLH_Best_Fitness_Counter[LLH_Best_Fitness_Counter.length - 1]++;
                        sb1.append("<TD bgcolor=\"green\">" + (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst]) + "</TD>");
                    } else {
                        sb1.append("<TD>" + (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst]) + "</TD>");
                    }
                    sb1.append("</TR>\r\n");
                }

                sb1.append("</TABLE>\r\n");
                sb1.append("<BR />\r\n");
                sb2.append("</TABLE>\r\n");
                sb2.append("<BR />\r\n");

                pout1.write(sb1.toString());
                pout2.write(sb2.toString());

                pout1.write(tableHeaderSB.toString());
                double bestFitnessCounter = 0;
                for (int i = 0; i < LLH_Best_Fitness_Counter.length; i++) {
                    if (LLH_Best_Fitness_Counter[i] > bestFitnessCounter) {
                        bestFitnessCounter = LLH_Best_Fitness_Counter[i];
                    }
                }
                double bestFitness = Double.MAX_VALUE;
                for (int i = 0; i < LLH_Fitness_Counter.length; i++) {
                    if (LLH_Fitness_Counter[i] < bestFitness) {
                        bestFitness = LLH_Fitness_Counter[i];
                    }
                }
                pout1.write("<TR><TD>Fitness Sum</TD>");
                for (int i = 0; i < LLH_Fitness_Counter.length; i++) {
                    if (LLH_Fitness_Counter[i] == bestFitness) {
                        pout1.write("<TD bgcolor=\"green\">" + LLH_Fitness_Counter[i] + "</TD>");
                    } else {
                        pout1.write("<TD>" + LLH_Fitness_Counter[i] + "</TD>");
                    }
                }
                pout1.write("<TR><TD>#Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Fitness_Counter.length; i++) {
                    if (LLH_Best_Fitness_Counter[i] == bestFitnessCounter) {
                        pout1.write("<TD bgcolor=\"green\">" + LLH_Best_Fitness_Counter[i] + "</TD>");
                    } else {
                        pout1.write("<TD>" + LLH_Best_Fitness_Counter[i] + "</TD>");
                    }
                }
                pout1.write("<TR><TD>%Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Fitness_Counter.length; i++) {
                    if (LLH_Best_Fitness_Counter[i] == bestFitnessCounter) {
                        pout1.write("<TD bgcolor=\"green\">" + (LLH_Best_Fitness_Counter[i] / (double) groups.get(key).size() * 100) + "</TD>");
                    } else {
                        pout1.write("<TD>" + (LLH_Best_Fitness_Counter[i] / (double) groups.get(key).size() * 100) + "</TD>");
                    }
                }
                pout1.write("</TR>\r\n");
                pout1.write("</TABLE>\r\n");
                pout1.write("<BR />\r\n");
                pout1.write("<HR>\r\n");
                pout1.write("</TR>\r\n");

                pout2.write(tableHeaderSB.toString());
                int bestCounter = 0;
                for (int i = 0; i < LLH_Best_Bins_Counter.length; i++) {
                    if (LLH_Best_Bins_Counter[i] > bestCounter) {
                        bestCounter = LLH_Best_Bins_Counter[i];
                    }
                }
                int bestBin = Integer.MAX_VALUE;
                for (int i = 0; i < LLH_Bin_Counter.length; i++) {
                    if (LLH_Bin_Counter[i] < bestBin) {
                        bestBin = LLH_Bin_Counter[i];
                    }
                }
                pout2.write("<TR><TD>#Bins used</TD>");
                for (int i = 0; i < LLH_Bin_Counter.length; i++) {
                    if (LLH_Bin_Counter[i] == bestBin) {
                        pout2.write("<TD bgcolor=\"green\">" + LLH_Bin_Counter[i] + "</TD>");
                    } else {
                        pout2.write("<TD>" + LLH_Bin_Counter[i] + "</TD>");
                    }
                }
                pout2.write("<TR><TD>#Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Bins_Counter.length; i++) {
                    if (LLH_Best_Bins_Counter[i] == bestCounter) {
                        pout2.write("<TD bgcolor=\"green\">" + LLH_Best_Bins_Counter[i] + "</TD>");
                    } else {
                        pout2.write("<TD>" + LLH_Best_Bins_Counter[i] + "</TD>");
                    }
                }
                pout2.write("<TR><TD>%Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Bins_Counter.length; i++) {
                    if (LLH_Best_Bins_Counter[i] == bestCounter) {
                        pout2.write("<TD bgcolor=\"green\">" + ((double) LLH_Best_Bins_Counter[i] / (double) groups.get(key).size() * 100) + "</TD>");
                    } else {
                        pout2.write("<TD>" + ((double) LLH_Best_Bins_Counter[i] / (double) groups.get(key).size() * 100) + "</TD>");
                    }
                }
                pout2.write("</TR>\r\n");
                pout2.write("</TR>\r\n");
                pout2.write("</TABLE>\r\n");
                pout2.write("<BR />\r\n");
                pout2.write("<HR>\r\n");
                // pout2.write("<BR />\r\n");
            }

            pout1.close();
            pout2.close();
        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
            System.exit(1);
        }

    }

    public static void writeBinCapacityBestPerformanceIntoHTML() {
        HashMap<String, ArrayList<Integer>> groups = new HashMap();
        for (int inst = 0; inst < Problem.NUMBER_OF_INSTANCES; inst++) {
            BPPInstance instance = (BPPInstance) BPP.INSTANCES[inst].clone();
            if (!groups.containsKey(instance.binCapacity + "")) {
                groups.put(instance.binCapacity + "", new ArrayList());
            }
            groups.get(instance.binCapacity + "").add(inst);
        }

        /*for(String key: groups.keySet()) {
            System.out.println(key + " " + groups.get(key).toString());
        }/**/
        int LLH_Best_Bins_Counter[];
        double LLH_Best_Fitness_Counter[];
        int LLH_Bin_Counter[];
        double LLH_Fitness_Counter[];

        //--- StringBuilder for different tables.
        StringBuilder tableHeaderSB = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        try {
            PrintWriter pout2 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_BIN_CAPACITY_BINS.html");
            PrintWriter pout1 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_BIN_CAPACITY_FITNESS.html");

            //--- Head of the HTML file.
            tableHeaderSB.append("<HEAD>\r\n");
            tableHeaderSB.append("<TITLE>LLH Results</TITLE>\r\n");
            tableHeaderSB.append("</HEAD>\r\n");
            tableHeaderSB.append("<BODY>\r\n");

            pout1.write(tableHeaderSB.toString());
            pout2.write(tableHeaderSB.toString());

            //--- Head of the tables.
            tableHeaderSB = new StringBuilder();
            tableHeaderSB.append("<TABLE border=\"1\" bordercolor=\"black\">\r\n");
            tableHeaderSB.append("<TR><TD>Instance</TD>");
            for (int i = 0; i < BPP.llhs.length; i++) {
                tableHeaderSB.append("<TD>" + BPP.llhs[i].getName() + "</TD>");
            }
            for (int i = 0; i < BPP.evolvedHeuristics.size(); i++) {
                tableHeaderSB.append("<TD>" + "GHH_" + i + "</TD>");
            }
            tableHeaderSB.append("<TD>SHH</TD>");
            tableHeaderSB.append("</TR>\r\n");

            //--- Go through all instance subsets stored in the hashmap.
            for (String key : groups.keySet()) {
                //--- Title.
                pout1.write("<H2>" + key + "</H2>\r\n");
                pout2.write("<H2>" + key + "</H2>\r\n");
                //--- New table.
                pout1.write(tableHeaderSB.toString());
                pout2.write(tableHeaderSB.toString());

                sb2 = new StringBuilder(); //--- New table body.
                sb1 = new StringBuilder();
                //--- Reset all counters.
                LLH_Best_Bins_Counter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Best_Bins_Counter, 0);
                LLH_Bin_Counter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Bin_Counter, 0);
                LLH_Best_Fitness_Counter = new double[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Best_Fitness_Counter, 0);
                LLH_Fitness_Counter = new double[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Fitness_Counter, 0);

                for (int p = 0; p < groups.get(key).size(); p++) {
                    int inst = groups.get(key).get(p);

                    //--- Get best value among all heuristics.
                    int bestBinValueLLH = Integer.MAX_VALUE;
                    double bestFitnessValueLLH = Double.MAX_VALUE;
                    for (int h = 0; h < BPP.llhs.length; h++) {
                        if ((int) BPP.llhs[h].instancesFitness[inst] < bestBinValueLLH) {
                            bestBinValueLLH = (int) BPP.llhs[h].instancesFitness[inst];
                        }
                        if (BPP.llhs[h].instancesFitness[inst] < bestFitnessValueLLH) {
                            bestFitnessValueLLH = BPP.llhs[h].instancesFitness[inst];
                        }
                    }
                    for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                        if ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst] < bestBinValueLLH) {
                            bestBinValueLLH = (int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        }
                        if (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst] < bestFitnessValueLLH) {
                            bestFitnessValueLLH = BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        }
                    }
                    if ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] < bestBinValueLLH) {
                        bestBinValueLLH = (int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    }
                    if (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] < bestFitnessValueLLH) {
                        bestFitnessValueLLH = Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    }

                    sb1.append("<TR><TD>" + BPP.INSTANCES[inst].name + "</TD>");
                    sb2.append("<TR><TD>" + BPP.INSTANCES[inst].name + "</TD>");
                    for (int h = 0; h < BPP.llhs.length; h++) {
                        //--- Bins.
                        LLH_Bin_Counter[h] += (int) BPP.llhs[h].instancesFitness[inst];
                        if ((int) BPP.llhs[h].instancesFitness[inst] == bestBinValueLLH) {
                            LLH_Best_Bins_Counter[h]++;
                            sb2.append("<TD bgcolor=\"green\">" + ((int) BPP.llhs[h].instancesFitness[inst]) + "</TD>");
                        } else {
                            sb2.append("<TD>" + ((int) BPP.llhs[h].instancesFitness[inst]) + "</TD>");
                        }
                        //--- Fitness.
                        LLH_Fitness_Counter[h] += BPP.llhs[h].instancesFitness[inst];
                        if (BPP.llhs[h].instancesFitness[inst] == bestFitnessValueLLH) {
                            LLH_Best_Fitness_Counter[h]++;
                            sb1.append("<TD bgcolor=\"green\">" + (BPP.llhs[h].instancesFitness[inst]) + "</TD>");
                        } else {
                            sb1.append("<TD>" + (BPP.llhs[h].instancesFitness[inst]) + "</TD>");
                        }
                    }

                    for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                        //--- Bins.
                        LLH_Bin_Counter[BPP.actions.length + h] += (int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        if ((int) (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) == bestBinValueLLH) {
                            LLH_Best_Bins_Counter[BPP.actions.length + h]++;
                            sb2.append("<TD bgcolor=\"green\">" + ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) + "</TD>");
                        } else {
                            sb2.append("<TD>" + ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) + "</TD>");
                        }

                        //--- Fitness.
                        LLH_Fitness_Counter[BPP.actions.length + h] += BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        if ((BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) == bestFitnessValueLLH) {
                            LLH_Best_Fitness_Counter[BPP.actions.length + h]++;
                            sb1.append("<TD bgcolor=\"green\">" + (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) + "</TD>");
                        } else {
                            sb1.append("<TD>" + (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst]) + "</TD>");
                        }
                    }

                    //--- Bins.
                    LLH_Bin_Counter[LLH_Bin_Counter.length - 1] += (int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    if ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] == bestBinValueLLH) {
                        LLH_Best_Bins_Counter[LLH_Best_Bins_Counter.length - 1]++;
                        sb2.append("<TD bgcolor=\"green\">" + ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst]) + "</TD>");
                    } else {
                        sb2.append("<TD>" + ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst]) + "</TD>");
                    }
                    sb2.append("</TR>\r\n");
                    //--- Fitness
                    LLH_Fitness_Counter[LLH_Bin_Counter.length - 1] += Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    if (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] == bestFitnessValueLLH) {
                        LLH_Best_Fitness_Counter[LLH_Best_Fitness_Counter.length - 1]++;
                        sb1.append("<TD bgcolor=\"green\">" + (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst]) + "</TD>");
                    } else {
                        sb1.append("<TD>" + (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst]) + "</TD>");
                    }
                    sb1.append("</TR>\r\n");
                }

                sb1.append("</TABLE>\r\n");
                sb1.append("<BR />\r\n");
                sb2.append("</TABLE>\r\n");
                sb2.append("<BR />\r\n");

                pout1.write(sb1.toString());
                pout2.write(sb2.toString());

                pout1.write(tableHeaderSB.toString());
                double bestFitnessCounter = 0;
                for (int i = 0; i < LLH_Best_Fitness_Counter.length; i++) {
                    if (LLH_Best_Fitness_Counter[i] > bestFitnessCounter) {
                        bestFitnessCounter = LLH_Best_Fitness_Counter[i];
                    }
                }
                double bestFitness = Double.MAX_VALUE;
                for (int i = 0; i < LLH_Fitness_Counter.length; i++) {
                    if (LLH_Fitness_Counter[i] < bestFitness) {
                        bestFitness = LLH_Fitness_Counter[i];
                    }
                }
                pout1.write("<TR><TD>Fitness Sum</TD>");
                for (int i = 0; i < LLH_Fitness_Counter.length; i++) {
                    if (LLH_Fitness_Counter[i] == bestFitness) {
                        pout1.write("<TD bgcolor=\"green\">" + LLH_Fitness_Counter[i] + "</TD>");
                    } else {
                        pout1.write("<TD>" + LLH_Fitness_Counter[i] + "</TD>");
                    }
                }
                pout1.write("<TR><TD>#Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Fitness_Counter.length; i++) {
                    if (LLH_Best_Fitness_Counter[i] == bestFitnessCounter) {
                        pout1.write("<TD bgcolor=\"green\">" + LLH_Best_Fitness_Counter[i] + "</TD>");
                    } else {
                        pout1.write("<TD>" + LLH_Best_Fitness_Counter[i] + "</TD>");
                    }
                }
                pout1.write("<TR><TD>%Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Fitness_Counter.length; i++) {
                    if (LLH_Best_Fitness_Counter[i] == bestFitnessCounter) {
                        pout1.write("<TD bgcolor=\"green\">" + (LLH_Best_Fitness_Counter[i] / (double) groups.get(key).size() * 100) + "</TD>");
                    } else {
                        pout1.write("<TD>" + (LLH_Best_Fitness_Counter[i] / (double) groups.get(key).size() * 100) + "</TD>");
                    }
                }
                pout1.write("</TR>\r\n");
                pout1.write("</TABLE>\r\n");
                pout1.write("<BR />\r\n");
                pout1.write("<HR>\r\n");
                pout1.write("</TR>\r\n");

                pout2.write(tableHeaderSB.toString());
                int bestCounter = 0;
                for (int i = 0; i < LLH_Best_Bins_Counter.length; i++) {
                    if (LLH_Best_Bins_Counter[i] > bestCounter) {
                        bestCounter = LLH_Best_Bins_Counter[i];
                    }
                }
                int bestBin = Integer.MAX_VALUE;
                for (int i = 0; i < LLH_Bin_Counter.length; i++) {
                    if (LLH_Bin_Counter[i] < bestBin) {
                        bestBin = LLH_Bin_Counter[i];
                    }
                }
                pout2.write("<TR><TD>#Bins used</TD>");
                for (int i = 0; i < LLH_Bin_Counter.length; i++) {
                    if (LLH_Bin_Counter[i] == bestBin) {
                        pout2.write("<TD bgcolor=\"green\">" + LLH_Bin_Counter[i] + "</TD>");
                    } else {
                        pout2.write("<TD>" + LLH_Bin_Counter[i] + "</TD>");
                    }
                }
                pout2.write("<TR><TD>#Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Bins_Counter.length; i++) {
                    if (LLH_Best_Bins_Counter[i] == bestCounter) {
                        pout2.write("<TD bgcolor=\"green\">" + LLH_Best_Bins_Counter[i] + "</TD>");
                    } else {
                        pout2.write("<TD>" + LLH_Best_Bins_Counter[i] + "</TD>");
                    }
                }
                pout2.write("<TR><TD>%Best solutions</TD>");
                for (int i = 0; i < LLH_Best_Bins_Counter.length; i++) {
                    if (LLH_Best_Bins_Counter[i] == bestCounter) {
                        pout2.write("<TD bgcolor=\"green\">" + ((double) LLH_Best_Bins_Counter[i] / (double) groups.get(key).size() * 100) + "</TD>");
                    } else {
                        pout2.write("<TD>" + ((double) LLH_Best_Bins_Counter[i] / (double) groups.get(key).size() * 100) + "</TD>");
                    }
                }
                pout2.write("</TR>\r\n");
                pout2.write("</TR>\r\n");
                pout2.write("</TABLE>\r\n");
                pout2.write("<BR />\r\n");
                pout2.write("<HR>\r\n");
                // pout2.write("<BR />\r\n");
            }

            pout1.close();
            pout2.close();
        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
            System.exit(1);
        }

    }

    public static void writeGroupedHistogramsIntoMatlabAndHTML() {
        int MAX_DIFF = 10;
        int HISTOGRAM[][];// = new int[MAX_DIFF + 1];
        int TOTAL_HISTOGRAM[][] = new int[BPP.INSTANCES.length][BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
        int TOTAL_HISTOGRAM_POINTER = 0;
        int LLH_HISTOGRAM_VALUES[] = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
        for (int i = 0; i < TOTAL_HISTOGRAM.length; i++) {
            for (int j = 0; j < TOTAL_HISTOGRAM[i].length; j++) {
                TOTAL_HISTOGRAM[i][j] = 0;
            }
        }


        int LLH_Best_Bins_Counter[];
        double LLH_Best_Fitness_Counter[];
        int LLH_Bin_Counter[];
        double LLH_Fitness_Counter[];

        //--- StringBuilder for different tables.
        StringBuilder tableHeaderSB = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        try {
            PrintWriter pout1 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_DEATILED_HISTOGRAM_BINS.html");
            PrintWriter pout2 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_HISTOGRAM_BINS.html");
            PrintWriter pout1m = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_DEATILED_HISTOGRAM_BINS.m");
            PrintWriter pout2m = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_HISTOGRAM_BINS.m");
            pout1m.write("clear all\r\nclose all\r\n\r\n");
            pout2m.write("clear all\r\nclose all\r\n\r\n");

            //--- Head of the HTML file.
            tableHeaderSB.append("<HEAD>\r\n");
            tableHeaderSB.append("<TITLE>Grouped Histograms</TITLE>\r\n");
            tableHeaderSB.append("</HEAD>\r\n");
            tableHeaderSB.append("<BODY>\r\n");

            pout1.write(tableHeaderSB.toString());
            pout2.write(tableHeaderSB.toString());

            //--- Head of the tables.
            tableHeaderSB = new StringBuilder();
            tableHeaderSB.append("<TABLE border=\"1\" bordercolor=\"black\">\r\n");
            tableHeaderSB.append("<TR><TD>Difference</TD>");
            for (int i = 0; i < MAX_DIFF; i++) {
                tableHeaderSB.append("<TD> - " + i + " - </TD>");
            }
            tableHeaderSB.append("<TD> - " + MAX_DIFF + " - </TD>");
            tableHeaderSB.append("</TR>\r\n");
            //--- New table.
            pout2.write(tableHeaderSB.toString());

            int counter[] = new int[MAX_DIFF + 1];

            //--- Go through all instance subsets stored in the hashmap.
            for(int k =0; k < Problem.instanceSubgroups.length; k++) {

                String key = Problem.instanceSubgroups[k];
                int groupSize = 0;
                for(Instance inst : Problem.INSTANCES) {
                    if(inst.instanceSubgroup == key) {
                        groupSize++;
                    }
                }
                //--- Restart counter.
                HISTOGRAM = new int[groupSize][BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                for (int i = 0; i < HISTOGRAM.length; i++) {
                    for (int j = 0; j < HISTOGRAM[i].length; j++) {
                        HISTOGRAM[i][j] = 0;
                    }
                }

                //--- Title.
                pout1.write("<H2>" + key + "</H2>\r\n");
                //--- New table.
                pout1.write(tableHeaderSB.toString());

                int p = -1;
                for(int i = 0; i < Problem.INSTANCES.length; i++) {
                    if(Problem.INSTANCES[i].instanceSubgroup != key) {
                        continue;
                    }

                    p++;
                    int inst = i;

                    //--- Get best value among all heuristics.
                    int bestBinValueLLH = Integer.MAX_VALUE;
                    //--- LLHs.
                    for (int h = 0; h < BPP.llhs.length; h++) {
                        //--- Get best values.
                        if ((int) BPP.llhs[h].instancesFitness[inst] < bestBinValueLLH) {
                            bestBinValueLLH = (int) BPP.llhs[h].instancesFitness[inst];
                        }
                    }
                    //--- Evolved heuristics.
                    for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                        //--- Get best values.
                        if ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst] < bestBinValueLLH) {
                            bestBinValueLLH = (int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        }
                    }
                    //--- Selection HH result.
                    //--- Get best values.
                    if ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] < bestBinValueLLH) {
                        bestBinValueLLH = (int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    }

                    //--- Write output on actual instance into the table.
                    Arrays.fill(LLH_HISTOGRAM_VALUES, -1);
                    for (int h = 0; h < BPP.llhs.length; h++) {
                        //--- Bins.
                        int actualLLHBins = (int) BPP.llhs[h].instancesFitness[inst];
                        HISTOGRAM[p][h] = actualLLHBins - bestBinValueLLH;
                        TOTAL_HISTOGRAM[TOTAL_HISTOGRAM_POINTER][h] = actualLLHBins - bestBinValueLLH;
                    }

                    for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                        //--- Bins.
                        int actualLLHBins = (int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        HISTOGRAM[p][h + BPP.llhs.length] = actualLLHBins - bestBinValueLLH;
                        TOTAL_HISTOGRAM[TOTAL_HISTOGRAM_POINTER][h + BPP.llhs.length] = actualLLHBins - bestBinValueLLH;
                    }

                    //--- Bins.
                    int actualLLHBins = (int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    HISTOGRAM[p][LLH_HISTOGRAM_VALUES.length - 1] = actualLLHBins - bestBinValueLLH;
                    TOTAL_HISTOGRAM[TOTAL_HISTOGRAM_POINTER][LLH_HISTOGRAM_VALUES.length - 1] = actualLLHBins - bestBinValueLLH;
                    TOTAL_HISTOGRAM_POINTER++;
                }

                //--- Write detailed table.
                for (int i = 0; i < HISTOGRAM[0].length; i++) {

                    Arrays.fill(counter, 0);
                    if (i < BPP.llhs.length) {
                        pout1.write("<TR><TD>LLH_" + i + "</TD>");
                        pout1m.write("LLH_" + key + "_" + i + " = [");
                        for (int j = 0; j < HISTOGRAM.length; j++) {
                            if (HISTOGRAM[j][i] > MAX_DIFF) {
                                HISTOGRAM[j][i] = MAX_DIFF;
                            }
                            counter[HISTOGRAM[j][i]]++;
                        }
                    } else if (i == HISTOGRAM[0].length - 1) {
                        pout1.write("<TR><TD>SHH</TD>");
                        pout1m.write("SHH_" + key + " = [");
                        for (int j = 0; j < HISTOGRAM.length; j++) {
                            if (HISTOGRAM[j][i] > MAX_DIFF) {
                                HISTOGRAM[j][i] = MAX_DIFF;
                            }
                            counter[HISTOGRAM[j][i]]++;
                        }
                    } else {
                        pout1.write("<TR><TD>GHH_" + (i - BPP.llhs.length) + "</TD>");
                        pout1m.write("GHH_" + key + "_" + (i - BPP.llhs.length) + " = [");
                        for (int j = 0; j < HISTOGRAM.length; j++) {
                            if (HISTOGRAM[j][i] > MAX_DIFF) {
                                HISTOGRAM[j][i] = MAX_DIFF;
                            }
                            counter[HISTOGRAM[j][i]]++;
                        }
                    }

                    for (int c = 0; c < counter.length; c++) {
                        pout1.write("<TD>" + counter[c] + "</TD>");
                        pout1m.write(counter[c] + " ");
                    }
                    pout1.write("</TR>\r\n");
                    pout1m.write("];\r\n");

                }

                pout1.write("</TABLE>\r\n");
                pout1.write("<BR />\r\n");

                pout1m.write("figure\r\nhold on\r\n");
                pout1m.write("bar(");
                for (int ii = 0; ii < BPP.llhs.length; ii++) {
                    pout1m.write("LLH_" + key + "_" + ii + ",");
                }
                for (int ii = 0; ii < BPP.evolvedHeuristics.size(); ii++) {
                    pout1m.write("GHH_" + key + "_" + ii + ",");
                }
                pout1m.write("SHH_" + key + ")\r\n\r\n");
            }

            //--- Create table with all instances and their histograms.
            for (int i = 0; i < TOTAL_HISTOGRAM[0].length; i++) {

                Arrays.fill(counter, 0);
                if (i < BPP.llhs.length) {
                    pout2.write("<TR><TD>LLH_" + i + "</TD>");
                    pout2m.write("LLH_" + i + " = [");
                    for (int j = 0; j < TOTAL_HISTOGRAM.length; j++) {
                        if (TOTAL_HISTOGRAM[j][i] > MAX_DIFF) {
                            TOTAL_HISTOGRAM[j][i] = MAX_DIFF;
                        }
                        counter[TOTAL_HISTOGRAM[j][i]]++;
                    }
                } else if (i == TOTAL_HISTOGRAM[0].length - 1) {
                    pout2.write("<TR><TD>SHH</TD>");
                    pout2m.write("SHH = [");
                    for (int j = 0; j < TOTAL_HISTOGRAM.length; j++) {
                        if (TOTAL_HISTOGRAM[j][i] > MAX_DIFF) {
                            TOTAL_HISTOGRAM[j][i] = MAX_DIFF;
                        }
                        counter[TOTAL_HISTOGRAM[j][i]]++;
                    }
                } else {
                    pout2.write("<TR><TD>GHH_" + (i - BPP.llhs.length) + "</TD>");
                    pout2m.write("GHH_" + (i - BPP.llhs.length) + " = [");
                    for (int j = 0; j < TOTAL_HISTOGRAM.length; j++) {
                        if (TOTAL_HISTOGRAM[j][i] > MAX_DIFF) {
                            TOTAL_HISTOGRAM[j][i] = MAX_DIFF;
                        }
                        counter[TOTAL_HISTOGRAM[j][i]]++;
                    }
                }
                for (int c = 0; c < counter.length; c++) {
                    pout2.write("<TD>" + counter[c] + "</TD>");
                    pout2m.write(counter[c] + " ");
                }
                pout2m.write("];\r\n");
            }

            pout2m.write("figure\r\nhold on\r\n");
            pout2m.write("bar(");
            for (int ii = 0; ii < BPP.llhs.length; ii++) {
                pout2m.write("LLH_" + ii + ",");
            }
            for (int ii = 0; ii < BPP.evolvedHeuristics.size(); ii++) {
                pout2m.write("GHH_" + ii + ",");
            }
            pout2m.write("SHH)\r\n\r\n");

            pout2.write("</TABLE>\r\n");
            pout2.write("<BR />\r\n");

            pout1.close();
            pout1m.close();
            pout2.close();
            pout2m.close();

        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
            System.exit(1);
        }

    }

    public static void writeGroupedBinDifferenceOnWorseInstanceIntoMatlab() {
        // TO DO - delete pout2.

        HashMap<String, ArrayList<Integer>> groups = new HashMap();
        for (int inst = 0; inst < Problem.NUMBER_OF_INSTANCES; inst++) {
            //--- NXCYWZ instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'N'
                    && Problem.INSTANCES[inst].name.charAt(2) == 'C'
                    && Problem.INSTANCES[inst].name.charAt(4) == 'W') {
                if (!groups.containsKey(Problem.INSTANCES[inst].name.substring(0, 6))) {
                    groups.put(Problem.INSTANCES[inst].name.substring(0, 6), new ArrayList());
                }
                groups.get(Problem.INSTANCES[inst].name.substring(0, 6)).add(inst);
            }
            //--- Hard instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'H') {
                if (!groups.containsKey("HARD")) {
                    groups.put("HARD", new ArrayList());
                }
                groups.get("HARD").add(inst);

            }
            //--- NXWYBZRA instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'N'
                    && Problem.INSTANCES[inst].name.charAt(2) == 'W'
                    && Problem.INSTANCES[inst].name.charAt(4) == 'B') {
                if (!groups.containsKey(Problem.INSTANCES[inst].name.substring(0, 6))) {
                    groups.put(Problem.INSTANCES[inst].name.substring(0, 6), new ArrayList());
                }
                groups.get(Problem.INSTANCES[inst].name.substring(0, 6)).add(inst);
            }
            //--- FalkenauerU instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'F'
                    && Problem.INSTANCES[inst].name.charAt(11) == 'u') {
                if (!groups.containsKey("FalkU_" + Problem.INSTANCES[inst].size)) {
                    groups.put("FalkU_" + Problem.INSTANCES[inst].size, new ArrayList());
                }
                groups.get("FalkU_" + Problem.INSTANCES[inst].size).add(inst);
            }
            //--- FalkenauerT instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'F'
                    && Problem.INSTANCES[inst].name.charAt(11) == 't') {
                if (!groups.containsKey("FalkT_" + Problem.INSTANCES[inst].size)) {
                    groups.put("FalkT_" + Problem.INSTANCES[inst].size, new ArrayList());
                }
                groups.get("FalkT_" + Problem.INSTANCES[inst].size).add(inst);
            }
        }

        int LLH_Best_Bins_Counter[];
        double LLH_Best_Fitness_Counter[];
        int LLH_Bin_Counter[];
        double LLH_Fitness_Counter[];

        //--- StringBuilder for different tables.
        StringBuilder headerSB = new StringBuilder();
        try {
            PrintWriter pout2 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_GROUPED_DIFFERENCE_BINS.html");
            PrintWriter pout1 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_GROUPED_DIFFERENCE_FITNESS.html");

            //--- Head of the HTML file.
            headerSB.append("clear all\r\n");
            headerSB.append("close all\r\n");
            headerSB.append("\r\n");

            pout1.write(headerSB.toString());
            pout2.write(headerSB.toString());

            //--- StringBuilders for each heuristic.
            StringBuilder bestSB = new StringBuilder();
            StringBuilder namesSB = new StringBuilder();
            StringBuilder[] binsSB = new StringBuilder[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
            for (int p = 0; p < binsSB.length; p++) {
                binsSB[p] = new StringBuilder();
            }

            //--- Go through all instance subsets stored in the hashmap.
            for (String key : groups.keySet()) {
                bestSB.append("BEST_" + key + " = [");
                namesSB.append("NAMES_" + key + " = [");
                for (int p = 0; p < binsSB.length; p++) {
                    if (p < BPP.llhs.length) {
                        binsSB[p].append("LLH" + p + "_" + key + " = [");
                    }
                    if (p == binsSB.length - 1) {
                        binsSB[p].append("SHH_" + key + " = [");
                    } else {
                        binsSB[p].append("GHH" + (p - BPP.llhs.length) + "_" + key + " = [");
                    }
                }

                //--- Reset all counters.
                LLH_Best_Bins_Counter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Best_Bins_Counter, 0);
                LLH_Bin_Counter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Bin_Counter, 0);
                LLH_Best_Fitness_Counter = new double[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Best_Fitness_Counter, 0);
                LLH_Fitness_Counter = new double[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                Arrays.fill(LLH_Fitness_Counter, 0);

                String color = "";
                //--- For all instances in the current group.
                for (int p = 0; p < groups.get(key).size(); p++) {
                    int inst = groups.get(key).get(p);

                    //--- Get best value among all heuristics.
                    int bestBinValueLLH = Integer.MAX_VALUE;
                    double bestFitnessValueLLH = Double.MAX_VALUE;
                    //--- LLHs.
                    for (int h = 0; h < BPP.llhs.length; h++) {
                        //--- Get best values.
                        if ((int) BPP.llhs[h].instancesFitness[inst] < bestBinValueLLH) {
                            bestBinValueLLH = (int) BPP.llhs[h].instancesFitness[inst];
                        }
                        if (BPP.llhs[h].instancesFitness[inst] < bestFitnessValueLLH) {
                            bestFitnessValueLLH = BPP.llhs[h].instancesFitness[inst];
                        }
                        //--- Store value in StringBulder.
                        binsSB[h].append((int) BPP.llhs[h].instancesFitness[inst] + " ");
                    }
                    //--- Evolved heuristics.
                    for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                        //--- Get best values.
                        if ((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst] < bestBinValueLLH) {
                            bestBinValueLLH = (int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        }
                        if (BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst] < bestFitnessValueLLH) {
                            bestFitnessValueLLH = BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst];
                        }

                        //--- Store value in StringBulder.
                        binsSB[h + BPP.llhs.length].append((int) BPP.evolvedHeuristics.get(h).nodes[BPP.evolvedHeuristics.get(h).getBestNodeID()].nodeInstanceFitness[inst] + " ");
                    }
                    //--- Selection HH result.
                    //--- Get best values.
                    if ((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] < bestBinValueLLH) {
                        bestBinValueLLH = (int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    }
                    if (Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] < bestFitnessValueLLH) {
                        bestFitnessValueLLH = Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst];
                    }
                    //--- Store value in StringBulder.
                    binsSB[binsSB.length - 1].append((int) Population.bestIndividual.nodes[Population.bestIndividual.bestNodeID].nodeInstanceFitness[inst] + " ");
                    //--- Store best values per instance and instance names into the StringBuilders.
                    bestSB.append(bestBinValueLLH + " ");
                    namesSB.append(BPP.INSTANCES[inst].name + " ");
                }

                for (StringBuilder ss : binsSB) {
                    ss.append("];\r\n");
                }
                bestSB.append("];\r\n");
                namesSB.append("];\r\n");

                // TO DO - graphs and figures
                namesSB.append("\r\n");
                namesSB.append("figure\r\n");
                namesSB.append("hold on\r\n");
                namesSB.append("bar(");
                for (int h = 0; h < BPP.llhs.length; h++) {
                    namesSB.append("LLH" + h + "_" + key + ", ");
                }
                for (int h = 0; h < BPP.evolvedHeuristics.size(); h++) {
                    namesSB.append("GHH" + h + "_" + key + ", ");
                }
                namesSB.append("SHH_" + key + ");");
            }

            for (StringBuilder ss : binsSB) {
                pout1.write(ss.toString());
            }
            pout1.write(bestSB.toString());
            pout1.write(namesSB.toString());

            pout1.close();
            pout2.close();
        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
            System.exit(1);
        }

    }
}
