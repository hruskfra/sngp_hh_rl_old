package heuristics.cvrp;

import gp.tasks.Instance;
import gp.tasks.cvrp.*;
import gp.tasks.Problem;
import heuristics.Heuristic;


/**
 * Created by Fanda on 05-Feb-16.
 *
 * BestFit heuristic places selected item into the selected bin, where will
 * remain least free space.
 */
public class Kilby extends Heuristic {

    /**
     * Constructor.
     */
    public Kilby() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for (int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    /**
     * Applies one iteration of the heuristic.
     * @param _inst actual instance object
     * @return integer value that determines result of the application step
     */
    @Override
    public int applyHeuristic(Instance _inst) {
        CVRPInstance inst = (CVRPInstance) _inst;

        //--- Test it only for one node.
        int node = 0;

        //--- Execute Kilby for exactly one iteration.
        int[] freeCities = inst.getUnassignedCities();
        //--- If there is no free city, break the cycle.
        if (freeCities.length == 0) {
            return -1;
        }

        //--- If there is not any route, create a new route and assign best fit city.
        if (inst.routes.isEmpty()) {
            int id = inst.getFurthestUnassignedCityFromDepot(freeCities);
            inst.createRoute(0, freeCities[id]);
            return 1;
        }

        //--- Apply Kilby heuristic.
        int firstCityIndex = -1;
        int addedCity = -1;
        double highestPrice = Double.MAX_VALUE;  //--- Lowest found price
        double price;   // Actual price
        int bestRouteID = -1;
        int i, j, k;
        //--- For every unclassified city...
        for (int freeCity : freeCities) {
            //--- For all routes...
            for (int r = 0; r < inst.routes.size(); r++) {
                double totalCap = inst.getRouteCapacity(inst.routes.get(r));
                //--- If city does not match due to its capacity.
                if (totalCap + inst.demands[freeCity] > inst.vehicleCapacity) {
                    continue;
                }
                //--- Try insert city between two cities in the route.
                for (int cityIter = 0; cityIter < inst.routes.get(r).size() - 1; cityIter++) {

                    //--- Calculate score of this position.
                    i = inst.routes.get(r).get(cityIter);
                    j = inst.routes.get(r).get(cityIter + 1);
                    k = freeCity;
                    price = inst.distances[i][k] + inst.distances[j][k] - inst.distances[i][j];

                    //--- Store bvest price found among all possible cities and positions.
                    if (price < highestPrice || firstCityIndex == -1) {
                        bestRouteID = r;
                        firstCityIndex = cityIter;

                        highestPrice = price;
                        addedCity = freeCity;
                    }

                }
            }
        }

        //--- If capacity does not match, create new route.
        if (addedCity == -1) {
            int id = inst.getFurthestUnassignedCityFromDepot(freeCities);
            inst.createRoute(inst.routes.size(), freeCities[id]);
        } else {
            //--- Else place the city into the best position.
            inst.routes.get(bestRouteID).add(firstCityIndex + 1, addedCity);
            inst.cityOnRoute[addedCity] = bestRouteID;
        }

        return 1;
    }

}
