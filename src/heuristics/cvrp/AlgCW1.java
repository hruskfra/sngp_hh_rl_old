/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package heuristics.cvrp;

import gp.tasks.Instance;
import gp.tasks.Problem;
import gp.functions.ProtDiv;
import gp.tasks.cvrp.CVRPInstance;
import gp.tasks.cvrp.Savings;
import heuristics.Heuristic;
import sngp.MethodName;

import java.util.Arrays;

/**
 *
 * @author Fanda
 */
public class AlgCW1 extends Heuristic {

    /**
     * Pointer on the savings list.
     */
    public int savingsPointer = 0;

    /**
     * Savings list.
     */
    private Savings[] savingsList;


    /**
     * Constructor.
     */
    public AlgCW1() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for (int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    /**
     * Applies one iteration of the heuristic.
     * @param _inst actual instance object
     * @return integer value that determines result of the application step
     */
    @Override
    public int applyHeuristic(Instance _inst) {
        CVRPInstance inst = (CVRPInstance) _inst;

        //--- Calculates savings.
        createSavingsList(inst);

        boolean cwApplied = false;

        //--- Execute one iteration of CW.
        while (!cwApplied) {

            //--- No more savings in the list, end the execution.
            if (this.savingsPointer == this.savingsList.length) {
                break;
            }

            //--- Get actual savings.
            Savings s = this.getActualSavingsPair();

            //--- Try to join route with two cities from actual savings.
            if (inst.joinRoutes(s.firstCity, s.secondCity) > 0) {
                inst.setCitiesOnRoutes();
                cwApplied = true;
            }
        }

        //--- Return integer value according to the result of application of CW algorithm.
        if(cwApplied) return 1;
        else {
            //--- Restart pointer at the end of execution.
            this.savingsPointer = 0;
            return -1;
        }
    }

    /**
     * Restart values for this heuristic.
     */
    @Override
    public void restartValues() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for (int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    /**
     * Creates savings list for actual instance
     * @param inst object of the isntace
     */
    public void createSavingsList(CVRPInstance inst) {
        //--- Count all possible pairs in the instance.
        int counter = 0;
        for (int i = 1; i < inst.size; i++) {
            for (int j = i + 1; j < inst.size; j++) {
                counter++;
            }
        }
        //--- Init array.
        this.savingsList = new Savings[counter];
        //--- Calculate savings.
        double val = 0;
        counter = 0;
        for (int i = 1; i < inst.size; i++) {
            for (int j = i + 1; j < inst.size; j++) {
                val = (((-(inst.distances[i][j])) + ((((-(inst.distances[i][j])) + (((-(inst.distances[i][0])) +
                        inst.distances[0][j]) + (Math.abs(inst.distances[i][0]) * 0.7786658814239372))) + inst.distances[0][j]) +
                        ((ProtDiv.execute_function((Math.abs(((((ProtDiv.execute_function((Math.abs(inst.distances[i][0]) -
                                (-(inst.distances[i][0]))), inst.distances[0][j]) + inst.distances[i][0]) * 0.7786658814239372) *
                                0.7786658814239372) - (Math.abs(inst.distances[i][0]) - (-((inst.distances[i][0] *
                                0.7786658814239372)))))) - (-(Math.abs(inst.distances[i][0])))), inst.distances[0][j]) +
                                inst.distances[i][0]) * 0.7786658814239372))) + Math.abs(inst.distances[i][0]));

                this.savingsList[counter++] = new Savings(i, j, val);
            }
        }

        //--- Sort savings.
        Arrays.sort(this.savingsList);

    }

    /**
     * Gets next savings pair.
     *
     * @return Saving object of the actual savings in the list
     */
    public Savings getActualSavingsPair() {
        try{
            return this.savingsList[this.savingsPointer++];
        }
        catch(ArrayIndexOutOfBoundsException AIOOB) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Index: " + (this.savingsPointer-1) + " | Array length: " + this.savingsList.length +"");
            AIOOB.printStackTrace();
            System.exit(-1);
        }
        return null;
    }
}
