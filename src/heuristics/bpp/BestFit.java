package heuristics.bpp;

import gp.tasks.bpp.*;
import gp.tasks.Instance;

import gp.tasks.Problem;
import heuristics.Heuristic;

import java.util.ArrayList;

/**
 * Created by Fanda on 05-Feb-16.
 *
 * BestFit heuristic places selected item into the selected bin, where will remain least free space.
 */
public class BestFit extends Heuristic {

    public BestFit() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;
        BPPItem placedItem;
        ArrayList<BPPBin> bins = inst.bins;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();

        if(fi.isEmpty()) {
            return -1;
        }
        double lowestRemaining = Double.MAX_VALUE;
        int binID = -1;
        int itemID = -1;

        boolean placed = false;
        for(int i = 0; i < fi.size(); i++) {
            placedItem = fi.get(i);
            for(int b = 0; b < bins.size(); b++) {
                if (bins.get(b).canBePlaced(placedItem)) {
                    if (lowestRemaining > bins.get(b).freeSpace - placedItem.weight) {
                        lowestRemaining = bins.get(b).freeSpace - placedItem.weight;
                        binID = b;
                        itemID = i;
                        placed = true;
                    }
                }
            }
        }


        if(!placed) {
            return -1;
        }
        else {
            inst.bins.get(binID).addItem(fi.get(itemID));
            return 1;
        }

    }
}
