package heuristics.bpp;

import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;
import sngp.MethodName;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 05-Feb-16.
 */
public class SumOfSquares extends Heuristic {

    public SumOfSquares() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;
        
        BPPItem placedItem;
        ArrayList<BPPBin> bins = inst.bins;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        ArrayList<BPPItem> tempfi;
        double minGapValue = Double.MAX_VALUE;
        double actGapValue;
        BPPInstance tempInstance;

        //--- Sort items as it is in Sim & Hart paper.
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        if(fi.isEmpty()) {
            return -1;
        }
        double lowestRemaining = Double.MAX_VALUE;
        int binID = -1;
        int itemID = -1;

        boolean placed = false;
        //--- Take first item.
        placedItem = fi.get(0);
        for(int b = 0; b < bins.size(); b++) {
            if (bins.get(b).canBePlaced(placedItem)) {
                tempInstance = inst.clone(); //--- Clone instance.
                tempfi = tempInstance.getUnpackedItems();
                BPP.ITEMS_SORTING = BPP.sorting.decreasing;
                Collections.sort(tempfi);
                tempInstance.bins.get(b).addItem(tempfi.get(0)); //--- Place item into actual bin b.


                if(fi.get(0).itemID != tempfi.get(0).itemID) {
                    System.out.println(MethodName.getCurrentMethodName() + " ERROR - Not the same items!");
                    System.exit(1);
                }

                actGapValue = getGapValue(tempInstance); //--- Get new gap value.
                if (minGapValue > actGapValue) {
                    minGapValue = actGapValue;
                    binID = b;
                    placed = true;
                }
            }
        }

        if(!placed) {
            return -1;
        }
        else {
            inst.bins.get(binID).addItem(fi.get(0));
            return 1;
        }

    }

    public double getGapValue(BPPInstance inst) {
        double LAST_MIN = 0.0;
        double ACT_MIN;
        int counter;
        boolean found;
        double gapValue = 0.0;
        while(true) {
            found = false;
            // Found next minimal value.
            ACT_MIN = inst.binCapacity;
            for(int b = 0; b < inst.bins.size(); b++) {
                if(inst.bins.get(b).freeSpace > LAST_MIN && inst.bins.get(b).freeSpace < ACT_MIN) {
                    ACT_MIN = inst.bins.get(b).freeSpace;
                    found = true;
                }
            }
            if(!found) {
                break;
            }

            LAST_MIN = ACT_MIN;
            //--- Count the number of bins with this capacity.
            counter = 0;
            for(int b = 0; b < inst.bins.size(); b++) {
                if(inst.bins.get(b).freeSpace == LAST_MIN) {
                    counter++;
                }
            }
            gapValue += counter*counter;
            //System.out.println(MethodName.getCurrentMethodName() + " LEVEL: " + ACT_MIN + " | COUNTER: " +  counter + " | GAP: " + gapValue) ;
            if(!found) {
                break;
            }
        }

        return gapValue;
    }
}
