package heuristics.bpp;

import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 21. 3. 2016.
 */
public class PackUpTo3LargestItems extends Heuristic {
    public PackUpTo3LargestItems() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;
        
        ArrayList<BPPItem> fi = inst.getUnpackedItems();

        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        double lowestRemainingSpace = Double.MAX_VALUE;
        int itemsID[] = {-1, -1, -1};
        boolean placed = false;

        /*if (fi.size() < 2) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There are not enough free items in the instance!");
            return -1;
        }/**/

        //System.out.println(inst.currentBin + " " + inst.bins.get(inst.currentBin).freeSpace);
        for (int i = 0; i < fi.size(); i++) {
            //--- Even first item is larger than remaining capacity in the current bin.
            if(inst.bins.get(inst.currentBin).freeSpace < fi.get(i).weight) {
                continue;
            }
            //--- It is not possible to fill the bin better due to ordering in descend order.
            if(lowestRemainingSpace < inst.bins.get(inst.currentBin).freeSpace - 3*fi.get(i).weight) {
                break;
            }
            if(lowestRemainingSpace > inst.bins.get(inst.currentBin).freeSpace - fi.get(i).weight) {
                lowestRemainingSpace = inst.bins.get(inst.currentBin).freeSpace - fi.get(i).weight;
                itemsID[0] = i;
                itemsID[1] = -1;
                itemsID[2] = -1;
                placed = true;
            }

            for (int j = i + 1; j < fi.size(); j++) {
                //--- It is not possible to fill the bin better due to ordering in descend order.
                if(lowestRemainingSpace < inst.bins.get(inst.currentBin).freeSpace - fi.get(i).weight - 2*fi.get(j).weight) {
                    break;
                }
                //--- Enough space in the bin.
                if (inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight) >= 0.0) {
                    if(inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight) < lowestRemainingSpace) {
                        lowestRemainingSpace = inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight);
                        itemsID[0] = i;
                        itemsID[1] = j;
                        itemsID[2] = -1;
                        placed = true;
                    }
                }

                for(int k = j + 1; k < fi.size(); k++) {
                    if (inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight + fi.get(k).weight) >= 0.0) {
                        if(inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight + fi.get(k).weight) < lowestRemainingSpace) {
                            lowestRemainingSpace = inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight + fi.get(k).weight);
                            itemsID[0] = i;
                            itemsID[1] = j;
                            itemsID[2] = k;
                            placed = true;
                        }
                        else {
                            k = fi.size();
                            continue;
                        }
                    }
                }
            }
        }

        if (!placed) {
            //--- Do nothing.
            return -1;
        } else {
            inst.bins.get(inst.currentBin).addItem(fi.get(itemsID[0]));
            if(itemsID[1] != -1) {
                inst.bins.get(inst.currentBin).addItem(fi.get(itemsID[1]));
            }
            if(itemsID[2] != -1) {
                inst.bins.get(inst.currentBin).addItem(fi.get(itemsID[2]));
            }
            return 1;
        }
    }
}
