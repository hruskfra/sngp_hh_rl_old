package heuristics.bpp;

import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 18. 2. 2016.
 * <p/>
 * Packs each item into the first bin in which item fits. All opened bins are open during the optimization run.
 */
public class FirstFitDecreasing extends Heuristic {

    public FirstFitDecreasing() {
        instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for (int i = 0; i < instancesFitness.length; i++) {
            instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;
        
        BPPItem placedItem;
        ArrayList<BPPBin> bins = inst.bins;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        //--- Sort items.
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);


        boolean placed = false;
        int b = inst.bins.size() - 1;
        for (int i = 0; i < fi.size(); i++) {
            placedItem = fi.get(i);
            //for(int b = 0; b < inst.bins.size(); b++) {

            if (inst.bins.get(b).canBePlaced(placedItem)) {
                inst.bins.get(b).addItem(placedItem);
                placed = true;
                break;
            }
        }
        //}

        if (!placed) {
            return -1;
        }
        return 1;
    }

    public int applyHeuristic2(BPPInstance inst) {
        BPPItem placedItem;
        ArrayList<BPPBin> bins = inst.bins;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        //--- Sort items.
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);


        boolean placed = false;
        //int b = inst.bins.size()-1;
        for (int i = 0; i < fi.size(); i++) {
            placedItem = fi.get(0);
            for (int b = 0; b < inst.bins.size(); b++) {

                if (inst.bins.get(b).canBePlaced(placedItem)) {
                    inst.bins.get(b).addItem(placedItem);
                    placed = true;
                    break;
                }
            }
        }

        if (!placed) {
            return -1;
        }
        return 1;
    }

}