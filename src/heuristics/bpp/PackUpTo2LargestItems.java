package heuristics.bpp;

import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 18. 2. 2016.
 */
public class PackUpTo2LargestItems extends Heuristic {

    public PackUpTo2LargestItems() {
        instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < instancesFitness.length; i++) {
            instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;

        //System.out.println(MethodName.getCurrentMethodName() + " ERROR - Missing implementation to operate only on the last bin!");
        //System.exit(1);

        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        ArrayList<BPPBin> bins = inst.bins;

        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        double lowestRemainingSpace = Double.MAX_VALUE;
        int itemsID[] = {-1, -1};
        boolean placed = false;

        //System.out.println(inst.currentBin + " " + inst.bins.get(inst.currentBin).freeSpace);
        for (int i = 0; i < fi.size(); i++) {
            //--- First item is larger than remaining capacity in the current bin.
            if(inst.bins.get(inst.currentBin).freeSpace < fi.get(i).weight) {
                continue;
            }
            //--- Lower cardinality preference, but probably it is unused due to sorting of the items.
            if(lowestRemainingSpace == inst.bins.get(inst.currentBin).freeSpace - fi.get(i).weight && itemsID[1] != -1) {
                itemsID[0] = i;
                itemsID[1] = -1;
                placed = true;
            }
            if(lowestRemainingSpace > inst.bins.get(inst.currentBin).freeSpace - fi.get(i).weight) {
                lowestRemainingSpace = inst.bins.get(inst.currentBin).freeSpace - fi.get(i).weight;
                itemsID[0] = i;
                itemsID[1] = -1;
                placed = true;
            }
            for (int j = i + 1; j < fi.size(); j++) {
                if (inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight) >= 0.0) {
                    if(inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight) < lowestRemainingSpace) {
                        lowestRemainingSpace = inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight);
                        itemsID[0] = i;
                        itemsID[1] = j;
                        placed = true;
                    }
                }
            }
        }

        if (!placed) {
            //--- Do nothing.
            return -1;
        } else {
            inst.bins.get(inst.currentBin).addItem(fi.get(itemsID[0]));
            if(itemsID[1] != -1) {
                inst.bins.get(inst.currentBin).addItem(fi.get(itemsID[1]));
            }
            return 1;
        }

    }
}

