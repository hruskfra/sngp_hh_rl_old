package heuristics.bpp;

import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;
import sngp.*;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 18. 2. 2016.
 */
public class PackSmallestItem extends Heuristic {

    public PackSmallestItem() {
        instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < instancesFitness.length; i++) {
            instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;

        //System.out.println(MethodName.getCurrentMethodName() + " ERROR - Missing implementation to operate only on the last bin!");
        //System.exit(1);

        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        ArrayList<BPPBin> bins = inst.bins;

        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        boolean placed = false;
        if (fi.isEmpty()) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There are not any free items in the instance!");
            return -1;
        }
        if (inst.bins.get(inst.currentBin).freeSpace - fi.get(fi.size()-1).weight >= 0.0) {
            placed = true;
        }

        if (!placed) {
            //--- Do nothing
            return -1;
        } else {
            inst.bins.get(inst.currentBin).addItem(fi.get(0));
            return 1;
        }

    }
}