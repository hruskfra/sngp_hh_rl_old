package heuristics.bpp;

import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 18. 2. 2016.
 *
 * This class equals to Pack2LargestItems.java class but this class cannot be used as single heuristic when one item is still unpacked.
 */
public class B2 extends Heuristic {

    public B2() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;

        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        ArrayList<BPPBin> bins = inst.bins;

        //--- There is only one item left, so heuristic cannot be applied.
        if (fi.size() < 2) {
            return -1;
        }

        //--- Sort items in decreasing order.
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        double lowestRemainingSpace = Double.MAX_VALUE;
        int itemsID[] = {-1, -1};
        boolean placed = false;

        double sum = 0.0;
        for (int i = 0; i < fi.size(); i++) {

            //--- Even first item is larger than remaining capacity in the current bin.
            if(inst.bins.get(inst.currentBin).freeSpace < fi.get(i).weight) {
                continue;
            }
            for (int j = i + 1; j < fi.size(); j++) {
                if (inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight) >= 0.0) {
                    if(inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight) < lowestRemainingSpace) {
                        lowestRemainingSpace = inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight);
                        itemsID[0] = i;
                        itemsID[1] = j;
                        placed = true;
                    }
                }
            }
        }

        if (!placed) {
            //--- Do nothing.
            return -1;
        } else {
            inst.bins.get(inst.currentBin).addItem(fi.get(itemsID[0]));
            if(itemsID[1] != -1) {
                inst.bins.get(inst.currentBin).addItem(fi.get(itemsID[1]));
            }
            return 1;
        }

    }
}
