package heuristics.bpp;


import gp.tasks.Instance;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import gp.tasks.Problem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Fanda on 05-Feb-16.
 *
 * Pacsk items into a current bin until it is at least one third full. Then run B3A.
 */
public class ADJD extends Heuristic {

    public ADJD() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

   public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;
        
        BPPItem placedItem;
        ArrayList<BPPBin> bins = inst.bins;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
         BPP.ITEMS_SORTING = BPP.sorting.decreasing;
       Collections.sort(fi);
        if(fi.isEmpty()) {
            return -1;
        }

        boolean placed = false;
        int b = inst.bins.size()-1;
        for(int i = 0; i < fi.size(); i++) {
            placedItem = fi.get(i);
            if(bins.get(b).canBePlaced(placedItem)) {
                bins.get(b).addItem(placedItem);
                placed = true;
                if(bins.get(b).freeSpace < getAverageFreeItemSize(inst.getUnpackedItems())) {
                    break;
                }
            }
        }


        PackUpTo3LargestItems B3A = new PackUpTo3LargestItems();
        int res = B3A.applyHeuristic(inst);

        //--- Return result of the heuristic.
        if(placed) res = 1;
        return res;

    }

    /**
     * Calculates average free items weight.
     * @param fi list of free items
     * @return average weight of free items
     */
    public double getAverageFreeItemSize(ArrayList<BPPItem> fi) {
        double sum = 0.0;
        for(int i = 0; i < fi.size(); i++) {
            sum += fi.get(i).weight;
        }
        sum /= (double)(fi.size());
        return sum;
    }

}
