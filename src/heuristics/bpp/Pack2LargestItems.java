package heuristics.bpp;

import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 18. 2. 2016.
 */
public class Pack2LargestItems extends Heuristic {

    public Pack2LargestItems() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;

        //System.out.println(MethodName.getCurrentMethodName() + " ERROR - Missing implementation to operate only on the last bin!");
        //System.exit(1);
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        ArrayList<BPPBin> bins = inst.bins;

        //System.out.println(MethodName.getCurrentMethodName() + " " + inst.bins.get(inst.currentBin).freeSpace);
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);


        double lowestRemainingSpace = Double.MAX_VALUE;
        int itemsID[] = {-1, -1};
        boolean placed = false;

        /*if (fi.size() < 2) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There are not enough free items in the instance!");
            return -1;
        }/**/

        //System.out.println(inst.currentBin + " " + inst.bins.get(inst.currentBin).freeSpace);
        double sum = 0.0;
        for (int i = 0; i < fi.size(); i++) {
            //--- Even first item is larger than remaining capacity in the current bin.
            if(inst.bins.get(inst.currentBin).freeSpace < fi.get(i).weight) {
                continue;
            }
            for (int j = i + 1; j < fi.size(); j++) {
                if (inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight) >= 0.0) {
                    if(inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight) < lowestRemainingSpace) {
                        lowestRemainingSpace = inst.bins.get(inst.currentBin).freeSpace - (fi.get(i).weight + fi.get(j).weight);
                        itemsID[0] = i;
                        itemsID[1] = j;
                        placed = true;
                    }
                }
            }
        }

        if(!placed) {
            for (int i = 0; i < fi.size(); i++) {
                //--- Even first item is larger than remaining capacity in the current bin.
                if(inst.bins.get(inst.currentBin).freeSpace < fi.get(i).weight) {
                    continue;
                }
                if (lowestRemainingSpace > inst.bins.get(inst.currentBin).freeSpace - fi.get(i).weight) {
                    lowestRemainingSpace = inst.bins.get(inst.currentBin).freeSpace - fi.get(i).weight;
                    itemsID[0] = i;
                    itemsID[1] = -1;
                    placed = true;
                    break;
                }
            }
        }
        if (!placed) {
            //--- Do nothing.
            return -1;
        } else {
            inst.bins.get(inst.currentBin).addItem(fi.get(itemsID[0]));
            if(itemsID[1] != -1) {
                inst.bins.get(inst.currentBin).addItem(fi.get(itemsID[1]));
            }
            return 1;
        }

    }
}
