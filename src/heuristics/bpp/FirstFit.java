package heuristics.bpp;


import gp.tasks.Instance;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import gp.tasks.Problem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 05-Feb-16.
 */
public class FirstFit extends Heuristic {

    public FirstFit() {
        instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < instancesFitness.length; i++) {
            instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;
        
        BPPItem placedItem;
        ArrayList<BPPBin> bins = inst.bins;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        if(fi.isEmpty()) {
            return -1;
        }

        boolean placed = false;
        int b = inst.bins.size()-1;
        for(int i = 0; i < fi.size(); i++) {
            placedItem = fi.get(i);
            if(bins.get(b).canBePlaced(placedItem)) {
                bins.get(b).addItem(placedItem);
                placed = true;
                break;
            }
        }

        if(!placed) {
            //inst.createNewBin(placedItem);
            return -1;
        }

        return 1;

    }

    public int applyHeuristic(BPPInstance inst) {
        BPPItem placedItem;
        ArrayList<BPPBin> bins = inst.bins;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();

        boolean placed = false;

        placedItem = fi.get(0);
        for (int b = 0; b < inst.bins.size(); b++) {

            if (inst.bins.get(b).canBePlaced(placedItem)) {
                inst.bins.get(b).addItem(placedItem);
                placed = true;
                break;
            }
        }

        if (!placed) {
            return -1;
        }
        return 1;
    }
}
