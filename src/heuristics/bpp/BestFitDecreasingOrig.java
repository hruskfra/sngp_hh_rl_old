package heuristics.bpp;

import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 05-Feb-16.
 */
public class BestFitDecreasingOrig extends Heuristic {

    public BestFitDecreasingOrig() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for (int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance) _inst;

        BPPItem placedItem;
        ArrayList<BPPBin> bins = inst.bins;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        //--- Sort items.
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        if (fi.isEmpty()) {
            return -1;
        }
        double lowestRemaining = Double.MAX_VALUE;
        int binID = -1;
        int itemID = -1;

        boolean placed = false;
        for (int i = 0; i < fi.size(); i++) {
            placedItem = fi.get(i);
            for (int b = 0; b < bins.size(); b++) {
                if (bins.get(b).canBePlaced(placedItem)) {
                    if (lowestRemaining > bins.get(b).freeSpace - placedItem.weight) {
                        lowestRemaining = bins.get(b).freeSpace - placedItem.weight;
                        binID = b;
                        placed = true;
                        itemID = i;
                    }
                }
            }
        }


        if (!placed) {
            return -1;
        } else {
            inst.bins.get(binID).addItem(fi.get(itemID));
            return 1;
        }

    }
}
