package heuristics.bpp;


import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 05-Feb-16.
 */
public class WorstFit extends Heuristic {

    public WorstFit() {
        instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < instancesFitness.length; i++) {
            instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;
        
        BPPItem placedItem;
        ArrayList<BPPBin> bins = inst.bins;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);
        if(fi.isEmpty()) {
            return -1;
        }

        boolean placed = false;
        int b = inst.bins.size()-1;
        for(int i = fi.size()-1; i>= 0; i--) {
            placedItem = fi.get(i);
            if(bins.get(b).canBePlaced(placedItem)) {
                bins.get(b).addItem(placedItem);
                placed = true;
                break;
            }
        }

        if(!placed) {
            //inst.createNewBin(placedItem);
            return -1;
        }

        return 1;

    }

}
