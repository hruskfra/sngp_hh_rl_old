package heuristics.bpp;


import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import heuristics.Heuristic;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 05-Feb-16.
 *
 * Pacsk items into a current bin until it is at least one third full. Then run B3A.
 */
public class DJD extends Heuristic {

    public DJD() {
        this.instancesFitness = new double[Problem.NUMBER_OF_INSTANCES];
        for(int i = 0; i < this.instancesFitness.length; i++) {
            this.instancesFitness[i] = Double.MAX_VALUE;
        }
    }

    public int applyHeuristic(Instance _inst) {
        BPPInstance inst = (BPPInstance)_inst;
        
        BPPItem placedItem;
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        if(fi.isEmpty()) {
            return -1;
        }

        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        boolean placed = false;

        int b = inst.bins.size()-1;
        for(int i = 0; i < fi.size(); i++) {
            placedItem = fi.get(i);
            if(inst.bins.get(b).canBePlaced(placedItem)) {
                inst.bins.get(b).addItem(placedItem);
                placed = true;
                if(inst.bins.get(b).freeSpace < (2.0*inst.binCapacity)/3.0) {
                    break;
                }
                fi = inst.getUnpackedItems();
            }
        }


        PackUpTo3LargestItems B3A = new PackUpTo3LargestItems();
        int res = B3A.applyHeuristic(inst);

        //--- Return result of the heuristic.
        if(placed) res = 1;
        return res;

    }

}
