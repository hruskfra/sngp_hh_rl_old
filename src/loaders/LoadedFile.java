package loaders;

import sngp.MethodName;

import java.io.File;

/**
 * Created by fhruska on 09.03.2017.
 */
public class LoadedFile {

    /**
     * Gets file from input path.
     * @param path path to the file
     * @return object of the file
     */
    public static File getFile(String path) {

        boolean exists = new File(path).exists();

        File file;
        // File exists?
        if (exists) {
            // Then prepare for loading.
            file = new File(path);
            if (file.isDirectory()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Input file path " + path + " leads only to directory.");
                System.exit(1);

            }
        } else {
            // Else make a new populations.
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Cannot find file " + path + ".");
            System.exit(1);
            return new File("abc");
        }

        return file;
    }
}
