/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package results.tables;

import sngp.MethodName;

/**
 *
 * @author fhruska
 */
public class TableHTML extends Table{

    /**
     * Constructor.
     *
     * @param c number of columns
     */
    public TableHTML(int c) {

        this.columns = c;
        this.fileExtension = ".html";
        this.footer = "</TABLE>\r\n";
    }

    /**
     * Constructor.
     *
     * @param c number of columns
     * @param titles column's titles
     */
    public TableHTML(int c, String[] titles) {
        //--- Check if input parameters are correct.
        if (this.columns != titles.length) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Number of columns doesn't match with titles array length (" + this.columns + " != " + titles.length + ")! ");
            System.exit(-1);
        }

        this.columns = c;
        this.fileExtension = ".html";
        this.footer = "</TABLE>\r\n";
        this.createHeader(titles);
    }

    /**
     * Creates header of the HTML table with titles written in bold font.
     *
     * @param titles titles of the table
     */
    @Override
    public void createHeader(String[] titles) {
        this.header = "<TABLE border=\"1\" bordercolor=\"black\">\r\n";
        this.header += "<TR>";
        for (String t : titles) {
            this.header += "<TD><STRONG>" + t + "</STRONG></TD>";
        }
        this.header += "</TR>\r\n";
    }

    /**
     * Inserts exactly one row to the body table.
     *
     * @param row array of the row elements
     */
    @Override
    public void insertRow(String[] row) {
        //--- Check if input parameters are correct.
        if (row.length != this.columns) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Number of columns doesn't match with row array length (" + this.columns + " != " + row.length + ")! ");
            System.exit(-1);
        }

        body.append("<TR>");
        for (int c = 0; c < this.columns; c++) {
            body.append("<TD>" + row[c] + "</TD>");
        }
        body.append("</TR>\r\n");
    }

    /**
     * Inserts exactly one row to the body table.
     *
     * @param row array of the row elements
     * @param properties array of properties for actual cell of the table in the row
     */
    @Override
    public void insertRow(String[] row, String[] properties) {
        //--- Check if input parameters are correct.
        if (row.length != this.columns) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Number of columns doesn't match with row array length (" + this.columns + " != " + row.length + ")! ");
            System.exit(-1);
        }

        body.append("<TR>");
        for (int c = 0; c < this.columns; c++) {
            body.append("<TD " + properties[c] + ">" + row[c] + "</TD>");
        }
        body.append("</TR>\r\n");
    }

    /**
     * Inserts several rows into the table body.
     *
     * @param rows matrix of the rows
     */
    @Override
    public void insertRows(String[][] rows) {
        //--- Check if input parameters are correct.
        if (rows[0].length != this.columns) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Number of columns doesn't match with row array length (" + this.columns + " != " + rows[0].length + ")! ");
            System.exit(-1);
        }

        for (int r = 0; r < rows.length; r++) {
            body.append("<TR>");
            for (int c = 0; c < this.columns; c++) {
                body.append("<TD>" + rows[r][c] + "</TD>");
            }
            body.append("</TR>\r\n");
        }
    }

}
