package results.tables;

import sngp.MethodName;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by Fanda on 21.1.2017.
 */
public abstract class Table {

    /**
     * Number of columns in the table.
     */
    public int columns = -1;

    /**
     * String of the header of the table.
     */
    public String header = "";

    /**
     * Rows of the table.
     */
    public StringBuilder body = new StringBuilder();

    /**
     * String of the footer of the table.
     */
    public String footer = "";

    /**
     * Extension of the file with table.
     */
    public String fileExtension = "";

    /**
     * Creates header of the HTML table with titles written in bold font.
     *
     * @param titles titles of the table
     */
    public void createHeader(String[] titles) {
        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Missing implementation!");
        System.exit(-1);
    }

    /**
     * Inserts exactly one row to the body table.
     *
     * @param row array of the row elements
     */
    public void insertRow(String[] row) {
        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Missing implementation!");
        System.exit(-1);
    }

    /**
     * Inserts exactly one row to the body table.
     *
     * @param row array of the row elements
     * @param properties array of properties for actual cell of the table in the row
     */
    public void insertRow(String[] row, String[] properties) {
        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Missing implementation!");
        System.exit(-1);
    }

    /**
     * Inserts several rows into the table body.
     *
     * @param rows matrix of the rows
     */
    public void insertRows(String[][] rows) {
        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Missing implementation!");
        System.exit(-1);
    }

    /**
     * Writes table into the corresponding file.
     * @param fileName name of the file without extension
     */
    public void writeTableIntoFile(String fileName) {
        try {
            PrintWriter pout = new PrintWriter(fileName + this.fileExtension);

            pout.write(this.header);
            pout.write(this.body.toString());
            pout.write(this.footer);

            pout.close();
        } catch(FileNotFoundException FNFE) {
            FNFE.printStackTrace();
        }
    }

}
