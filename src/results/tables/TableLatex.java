/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package results.tables;

import sngp.MethodName;

/**
 * @author fhruska
 */
public class TableLatex extends Table {

    /**
     * Constructor.
     *
     * @param c number of columns
     */
    public TableLatex(int c) {

        this.columns = c;
        this.fileExtension = ".tex";
        this.footer = "\\end{tabular}\r\n\\end{table}\r\n";
    }

    /**
     * Constructor.
     *
     * @param c      number of columns
     * @param titles column's titles
     */
    public TableLatex(int c, String[] titles) {
        //--- Check if input parameters are correct.
        if (this.columns != titles.length) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Number of columns doesn't match with titles array length (" + this.columns + " != " + titles.length + ")! ");
            System.exit(-1);
        }

        this.fileExtension = ".tex";
        this.footer = "\\end{table}\r\n";

        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Missing implementation!");
        System.exit(-1);
    }

    /**
     * Creates header of the HTML table with titles written in bold font.
     *
     * @param titles titles of the table
     */
    @Override
    public void createHeader(String[] titles) {
        this.header += "\\begin{table}[h!]\r\n";
        this.header += "\\centering\r\n";
        this.header += "\\caption{Caption for the table.}\r\n";
        this.header += "\\label{tab:table1}\r\n";
        this.header += "\\begin{tabular}{";
        for (int i = 0; i < titles.length; i++) {
            this.header += " c ";
            if (i != titles.length - 1) {
                this.header += "|";
            }
        }
        this.header += "}\r\n";
        this.header += "\\hline\r\n";
        for (int i = 0; i < titles.length; i++) {
            this.header += titles[i] + " ";
            if (i != titles.length - 1) {
                this.header += "& ";
            }
        }
        this.header += "\\\\\r\n";
        this.header += "\\hline\r\n\\hline\r\n";
    }

    /**
     * Inserts exactly one row to the body table.
     *
     * @param row array of the row elements
     */
    @Override
    public void insertRow(String[] row) {
        //--- Check if input parameters are correct.
        if (row.length != this.columns) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Number of columns doesn't match with row array length (" + this.columns + " != " + row.length + ")! ");
            System.exit(-1);
        }
        for (int c = 0; c < row.length; c++) {
            this.body.append(row[c]);
            if (c != row.length - 1) {
                this.body.append(" & ");
            }
            this.body.append("\\\\\r\n");
        }
        this.body.append("\\hline\r\n");
    }

    /**
     * Inserts several rows into the table body.
     *
     * @param rows matrix of the rows
     */
    @Override
    public void insertRows(String[][] rows) {
        //--- Check if input parameters are correct.
        if (rows[0].length != this.columns) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Number of columns doesn't match with row array length (" + this.columns + " != " + rows[0].length + ")! ");
            System.exit(-1);
        }
        for (int r = 0; r < rows.length; r++) {
            for (int c = 0; c < rows[r].length; c++) {
                this.body.append(rows[r][c]);
                if (c != rows[r].length - 1) {
                    this.body.append(" & ");
                }
                this.body.append("\\\\\r\n");
            }
            this.body.append("\\hline\r\n");
        }
        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Missing implementation!");
        System.exit(-1);
    }

}
