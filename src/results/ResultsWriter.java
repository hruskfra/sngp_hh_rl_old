package results;

import gp.Population;
import gp.tasks.Problem;
import gp.terminals.Constant;
import results.tables.Table;
import results.tables.TableHTML;
import sngp.MethodName;
import sngp.SNGP_HH_RL;
import sngp.Settings;
import sngp.Vector;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Fanda on 21.1.2017.
 */
public class ResultsWriter {

    public static double bestValue;
    public static String[] properties;

    /**
     * Vectors of interest.
     */
    public static ArrayList<Vector> VOI = new ArrayList();

    /**
     * Writes input HashMap into the table.
     *
     * @param tableName name of the file with the table
     * @param t         object of the table
     * @param map       values stored in HashMap for writing to the file
     */
    public static void writeHashMapIntoTheTable(String tableName, Table t, HashMap<String, int[]> map) {
        //--- Initialize HashMap.
        HashMap<String, int[]> groups = new HashMap();
        for (String groupKey : Problem.instanceSubgroups) {
            groups.put(groupKey, new int[t.columns - 1]);
        }
        //--- Add every instance result to specific subgroup counter.
        for (int inst = 0; inst < Problem.INSTANCES.length; inst++) {
            int c[] = map.get(Problem.INSTANCES[inst].name);
            for (int i = 0; i < t.columns - 1; i++) {
                try {
                    groups.get(Problem.INSTANCES[inst].instanceSubgroup)[i] += c[i];
                } catch (NullPointerException NPE) {
                    System.out.println(MethodName.getCurrentMethodName() + ": Instance name: " + Problem.INSTANCES[inst].name + " | Instance subgroup: " + Problem.INSTANCES[inst].instanceSubgroup);
                    System.exit(-1);
                }
            }
        }

        String[] row = new String[t.columns];
        for (String key : groups.keySet()) {
            row[0] = key;
            for (int c = 0; c < t.columns - 1; c++) {
                row[c + 1] = groups.get(key)[c] + "";
            }
            t.insertRow(row);
        }

        //--- Store table into the file.
        t.writeTableIntoFile(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_" + tableName);
    }

    /**
     * Writes Integer fitness values for all instances into the table and stores it in the file.
     *
     * @param tableName name of the file with the table
     * @param t         object of the table
     */
    public static void writeIntegerFitnessIntoTable(String tableName, Table t) {

        double aHH_FITNESS;
        int aHH_ID;

        //--- Arrays for statistics and showing the best results on the instance.
        int LLH_Wins_Counter[] = new int[Problem.llhs.length + Problem.evolvedHeuristics.size() + 1];
        Arrays.fill(LLH_Wins_Counter, 0);
        int LLH_Unique_Wins_Counter[] = new int[Problem.llhs.length + Problem.evolvedHeuristics.size() + 1];
        Arrays.fill(LLH_Unique_Wins_Counter, 0);
        int LLH_Bins_Counter[] = new int[Problem.llhs.length + Problem.evolvedHeuristics.size() + 1];
        Arrays.fill(LLH_Bins_Counter, 0);

        //--- Set proper number od columns.
        t.columns = (1 + Problem.llhs.length + Problem.evolvedHeuristics.size() + 1);
        //--- Define titles for the table headers.
        String row[] = new String[t.columns];
        row[0] = "Instance";
        for (int i = 0; i < Problem.llhs.length; i++) {
            row[i + 1] = Problem.llhs[i].getName();
        }
        for (int i = 0; i < Problem.evolvedHeuristics.size(); i++) {
            row[i + 1 + Problem.llhs.length] = "GHH_" + i;
        }
        row[row.length - 1] = "SHH";

        //--- Create table header with titles.
        t.createHeader(row);

        //--- Prepare properties array.
        properties = new String[t.columns];
        Arrays.fill(properties, "");
        bestValue = Integer.MAX_VALUE;

        //--- Insert lines with the results for every instances.
        for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
            Arrays.fill(properties, ""); //--- Clear properties.
            bestValue = Integer.MAX_VALUE;
            //--- Create Strings for actual row.
            row[0] = Problem.INSTANCES[i].name;

            //--- LLHs fitness values.
            for (int f = 0; f < Problem.llhs.length; f++) {
                aHH_FITNESS = Problem.llhs[f].instancesFitness[i];

                row[f + 1] = (int) (aHH_FITNESS) + "";
                LLH_Bins_Counter[f] += (int) (aHH_FITNESS);
                //--- Store properties for the field with best value. New best result found.
                setProperties(f + 1, (int) (aHH_FITNESS));
            }

            //--- GHHs if any.
            for (int f = 0; f < Problem.evolvedHeuristics.size(); f++) {
                aHH_FITNESS = Problem.evolvedHeuristics.get(f).nodes[Problem.evolvedHeuristics.get(f).getBestNodeID()].nodeInstanceFitness[i];
                aHH_ID = f + 1 + Problem.llhs.length;

                row[aHH_ID] = (int) (aHH_FITNESS) + "";
                LLH_Bins_Counter[aHH_ID-1] += (int) (aHH_FITNESS);
                //--- Store properties for the field with best value.
                setProperties(aHH_ID, (int) (aHH_FITNESS));

            }
            //--- Selection LLH.
            aHH_FITNESS = Population.bestIndividual.nodes[Population.bestIndividual.getBestNodeID()].nodeInstanceFitness[i];
            aHH_ID = row.length - 1;
            row[aHH_ID] = (int) (aHH_FITNESS) + "";
            LLH_Bins_Counter[LLH_Bins_Counter.length - 1] += (int) (aHH_FITNESS);
            //--- Store properties for the field with best value.
            setProperties(aHH_ID, (int) (aHH_FITNESS));

            //--- Insert values and properties into the table.
            t.insertRow(row, properties);

            //--- Gets best results and increase their counters. Also get the index of the unique best result if it is any.
            int nBest = 0;
            int iBest = -1;
            for (int p = 1; p < properties.length; p++) {
                if (!properties[p].isEmpty()) {
                    LLH_Wins_Counter[p - 1]++;
                    nBest++;
                    iBest = p-1;
                }
            }
            if(nBest == 1) {
                LLH_Unique_Wins_Counter[iBest]++;
            }
        }

        //--- Foot of the table containing several sum values. Start with empty row.
        Arrays.fill(row, "");
        t.insertRow(row);
        //--- Prepare row with sum of the fitness used by particular LLH on all instances.
        bestValue = Integer.MAX_VALUE;
        row[0] = "SUM OF ELEMENTS";
        for (int c = 0; c < LLH_Bins_Counter.length; c++) {
            row[c + 1] = LLH_Bins_Counter[c] + "";
            //--- Store properties for the field with best value. TODO there is <= in method but originally there is only <
            setProperties(c+1, LLH_Bins_Counter[c]);
        }
        t.insertRow(row, properties);

        //--- Number of wins.
        bestValue = 0;
        row[0] = "#WINS";
        for (int c = 0; c < LLH_Wins_Counter.length; c++) {
            row[c + 1] = LLH_Wins_Counter[c] + "";
            if (LLH_Wins_Counter[c] > bestValue) {
                Arrays.fill(properties, "");
                properties[c+1] = "bgcolor=\"green\"";
                bestValue = LLH_Wins_Counter[c];
            } else if (LLH_Wins_Counter[c] == bestValue) {
                properties[c+1] = "bgcolor=\"green\"";
            }
        }
        t.insertRow(row, properties);

        //--- Number of unique wins.
        bestValue = 0;
        row[0] = "#UNIQUE WINS";
        for (int c = 0; c < LLH_Unique_Wins_Counter.length; c++) {
            row[c + 1] = LLH_Unique_Wins_Counter[c] + "";
            if (LLH_Unique_Wins_Counter[c] > bestValue) {
                Arrays.fill(properties, "");
                properties[c+1] = "bgcolor=\"green\"";
                bestValue = LLH_Unique_Wins_Counter[c];
            } else if (LLH_Unique_Wins_Counter[c] == bestValue) {
                properties[c+1] = "bgcolor=\"green\"";
            }
        }
        t.insertRow(row, properties);

        //--- Store table into the file.
        t.writeTableIntoFile(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_" + tableName);
        System.out.println(MethodName.getCurrentMethodName() + ": RESULTS WRITER - Table with numbers of used bins " + SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_" + tableName + " ... SAVED");

        //--- Write statistics for the bar graphs.
        try {
            StringBuilder binsSB = new StringBuilder();
            StringBuilder barSB = new StringBuilder();
            StringBuilder namesSB = new StringBuilder();

            PrintWriter pout3 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_BINS_BAR_GRAPH.m");

            binsSB.append("bins = [");

            for (int llh = 0; llh < LLH_Bins_Counter.length; llh++) {
                //--- Array with the numbers of wins.
                binsSB.append(LLH_Bins_Counter[llh] + " ");
            }
            binsSB.append("];\r\n");

            namesSB.append("c = categorical({");
            for(int i =0; i < Problem.llhs.length; i++) {
                namesSB.append("\'LLH_" + i + "\',");
            }
            for(int i = 0; i < Problem.evolvedHeuristics.size(); i++) {
                namesSB.append("\'GHH_" + (Problem.llhs.length) + "\',");
            }
            namesSB.append("\'SHH\'});\r\n");

            //--- Write StringBuilders into the file.
            pout3.write("close all\r\nclear all\r\n");
            pout3.write(namesSB.toString());
            pout3.write(binsSB.toString());
            pout3.write("bar(c, wins);\r\n");

            pout3.close();
            System.out.println(MethodName.getCurrentMethodName() + ": RESULTS WRITER - Data for the bar graphs of llh wins in the file " + SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_BAR_GRAPH.m ... SAVED");
        }
        catch(IOException IOE) {

        }

        //--- Write unique solutions count into the table
        try {
            StringBuilder binsSB = new StringBuilder();
            StringBuilder barSB = new StringBuilder();
            StringBuilder namesSB = new StringBuilder();

            PrintWriter pout4 = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_UNIQUE_SOLUTIONS.html");

            pout4.write("<TABLE>\r\n<TR>\r\n");
            //--- Define titles for the table headers.
            pout4.write("<TD>Instance</TD>");
            for (int i = 0; i < Problem.llhs.length; i++) {
                pout4.write("<TD>"+ Problem.llhs[i].getName() + "</TD>");
            }
            for (int i = 0; i < Problem.evolvedHeuristics.size(); i++) {
                pout4.write("<TD>GHH_" + i + "</TD>");
            }
            pout4.write("<TD>SHH</TD></TR>\r\n");
            //--- Line with unique counter.
            pout4.write("<TD>ALL</TD>");
            for (int i = 0; i < Problem.llhs.length; i++) {
                pout4.write("<TD>"+ LLH_Unique_Wins_Counter[i] + "</TD>");
            }
            for (int i = 0; i < Problem.evolvedHeuristics.size(); i++) {
                pout4.write("<TD>" + LLH_Unique_Wins_Counter[i+Problem.llhs.length] + "</TD>");
            }
            pout4.write("<TD>" + LLH_Unique_Wins_Counter[LLH_Unique_Wins_Counter.length-1] + "</TD>\r\n");
            pout4.write("</TR>\r\n</TABLE>");

            pout4.close();
            System.out.println(MethodName.getCurrentMethodName() + ": RESULTS WRITER - Table with numbers of unique best solutions " + SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_UNIQUE_SOLUTIONS.html ... SAVED");
        }
        catch(IOException IOE) {

        }


        /**
         * Second table with grouped instances.
         */
        TableHTML t2 = new TableHTML(t.columns * 2 - 1);
        row = new String[t.columns * 2 - 1];
        Arrays.fill(row, "");
        //--- Define titles.
        row[0] = "Instance Set";
        for (int i = 0; i < Problem.llhs.length; i++) {
            row[(i * 2) + 1] = Problem.llhs[i].getName();
        }
        for (int i = 0; i < Problem.evolvedHeuristics.size(); i++) {
            row[Problem.llhs.length * 2 + (i * 2) + 1] = "GHH_" + i;
        }
        row[row.length - 2] = "SHH";
        t2.createHeader(row);

        double tableFields[] = new double[row.length];

        double bestSolutionsSum;
        double actualBestSolution;

        for (String group : Problem.instanceSubgroups) {
            row[0] = group;

            bestSolutionsSum = 0.0;
            Arrays.fill(tableFields, 0.0);
            Arrays.fill(LLH_Unique_Wins_Counter, 0);
            for (int i = 0; i < Problem.INSTANCES.length; i++) {

                if (Problem.INSTANCES[i].instanceSubgroup.equals(group)) {
                    actualBestSolution = Double.MAX_VALUE;
                    //--- LLHs.
                    for (int llh = 0; llh < Problem.llhs.length; llh++) {
                        aHH_FITNESS = Problem.llhs[llh].instancesFitness[i];
                        tableFields[(llh * 2) + 1] += (int) (aHH_FITNESS);
                        if ((int) (aHH_FITNESS) < actualBestSolution) {
                            actualBestSolution = (int) (aHH_FITNESS);
                        }
                    }
                    //--- GHHs.
                    for (int ghh = 0; ghh < Problem.evolvedHeuristics.size(); ghh++) {
                        aHH_FITNESS = Problem.evolvedHeuristics.get(ghh).nodes[Problem.evolvedHeuristics.get(ghh).getBestNodeID()].nodeInstanceFitness[i];

                        tableFields[1 + (Problem.llhs.length * 2) + (ghh * 2)] += (int) (aHH_FITNESS);
                        if ((int) (aHH_FITNESS) < actualBestSolution) {
                            actualBestSolution = (int) (aHH_FITNESS);
                        }
                    }
                    //--- SHH.
                    aHH_FITNESS = Population.bestIndividual.nodes[Population.bestIndividual.getBestNodeID()].nodeInstanceFitness[i];
                    tableFields[tableFields.length - 2] += (int) (aHH_FITNESS);
                    if ((int) (aHH_FITNESS) < actualBestSolution) {
                        actualBestSolution = (int) (aHH_FITNESS);
                    }

                    bestSolutionsSum += actualBestSolution;
                }
            }

            properties = new String[row.length];
            Arrays.fill(properties, "");
            double min = Double.MAX_VALUE;
            for (int c = 1; c < tableFields.length; c += 2) {
                if (tableFields[c] < min) {
                    min = tableFields[c];

                }
            }
            for (int c = 2; c < tableFields.length; c += 2) {
                tableFields[c] = (tableFields[c - 1] - bestSolutionsSum) / (double) bestSolutionsSum;
                tableFields[c] *= 100.0;
                tableFields[c] = roundDouble(tableFields[c], 5);
                if (tableFields[c - 1] == min) {
                    properties[c] = "bgcolor=\"yellow\"";
                    properties[c - 1] = "bgcolor=\"yellow\"";
                }
                if (tableFields[c - 1] == bestSolutionsSum) {
                    properties[c] = "bgcolor=\"green\"";
                    properties[c - 1] = "bgcolor=\"green\"";
                }
            }
            for (int i = 1; i < row.length; i++) {
                row[i] = tableFields[i] + "";
            }


            t2.insertRow(row, properties);
        }
        t2.writeTableIntoFile(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_DETAILED_GROUPED_BINS");
        System.out.println(MethodName.getCurrentMethodName() + ": RESULTS WRITER - Table with numbers of used bins divided into instance subgroups " + SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_DETAILED_GROUPED_BINS ... SAVED");
    }

    /**
     * Writes Integer fitness values for all instances into the table and stores it in the file.
     *
     * @param tableName name of the file with the table
     * @param t         object of the table
     */
    public static void writeDoubleFitnessIntoTable(String tableName, Table t) {

        double aHH_FITNESS;
        int aHH_ID;

        //--- Arrays for statistics and showing the best results on the instance.
        int LLH_Wins_Counter[] = new int[Problem.llhs.length + Problem.evolvedHeuristics.size() + 1];
        Arrays.fill(LLH_Wins_Counter, 0);
        double LLH_Fitness_Counter[] = new double[Problem.llhs.length + Problem.evolvedHeuristics.size() + 1];
        Arrays.fill(LLH_Fitness_Counter, 0);

        //--- Set proper number od columns.
        t.columns = (1 + Problem.llhs.length + Problem.evolvedHeuristics.size() + 1);
        //--- Define titles for the table headers.
        String row[] = new String[t.columns];
        row[0] = "Instance";
        for (int i = 0; i < Problem.llhs.length; i++) {
            row[i + 1] = Problem.llhs[i].getName();
        }
        for (int i = 0; i < Problem.evolvedHeuristics.size(); i++) {
            row[i + 1 + Problem.llhs.length] = "GHH_" + i;
        }
        row[row.length - 1] = "SHH";

        //--- Create table header with titles.
        t.createHeader(row);

        //--- Prepare properties array.
        properties = new String[t.columns];
        Arrays.fill(properties, "");
        double bestValue = Double.MAX_VALUE;

        //--- Insert lines with the results for every instances.
        for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
            Arrays.fill(properties, "");
            bestValue = Double.MAX_VALUE;
            //--- Create Strings for actual row.
            row[0] = Problem.INSTANCES[i].name;
            //--- LLHs fitness values.
            for (int f = 0; f < Problem.llhs.length; f++) {
                aHH_FITNESS = Problem.llhs[f].instancesFitness[i];

                row[f + 1] = aHH_FITNESS + "";
                LLH_Fitness_Counter[f] += aHH_FITNESS; //--- There was conversion to int?!
                //--- Store properties for the field with best value.
                setProperties(f + 1, aHH_FITNESS);

            }
            //--- GHHs if any.
            for (int f = 0; f < Problem.evolvedHeuristics.size(); f++) {
                aHH_FITNESS = Problem.evolvedHeuristics.get(f).nodes[Problem.evolvedHeuristics.get(f).getBestNodeID()].nodeInstanceFitness[i];
                aHH_ID = f + 1 + Problem.llhs.length;

                row[aHH_ID] = aHH_FITNESS + "";
                LLH_Fitness_Counter[aHH_ID - 1] += aHH_FITNESS;
                //--- Store properties for the field with best value.
                setProperties(aHH_ID, aHH_FITNESS);
            }
            //--- Selection LLH.
            aHH_FITNESS = Population.bestIndividual.nodes[Population.bestIndividual.getBestNodeID()].nodeInstanceFitness[i];
            aHH_ID = row.length - 1;

            row[aHH_ID] = aHH_FITNESS + "";
            LLH_Fitness_Counter[LLH_Fitness_Counter.length - 1] += aHH_FITNESS;
            //--- Store properties for the field with best value.
            setProperties(aHH_ID, aHH_FITNESS);

            //--- Insert values and properties into the table.
            t.insertRow(row, properties);

            //--- Gets best results and increase their counters.
            for (int p = 1; p < properties.length; p++) {
                if (!properties[p].isEmpty()) LLH_Wins_Counter[p - 1]++;
            }
        }

        //--- Foot of the table containing several sum values. Start with empty line.
        Arrays.fill(row, "");
        t.insertRow(row);
        //--- Prepare row with sum of the fitness used by particular LLH on all instances.
        double bestFitnessValue = Double.MAX_VALUE;
        row[0] = "SUM OF ELEMENTS";
        for (int c = 0; c < LLH_Fitness_Counter.length; c++) {
            row[c + 1] = LLH_Fitness_Counter[c] + "";
            //--- Store properties for the field with best value. TODO there is <= in method but originally there is only <
            setProperties(c+1, LLH_Fitness_Counter[c]);
        }
        t.insertRow(row, properties);

        //--- Number of wins.

        bestValue = 0;
        row[0] = "#WINS";
        for (int c = 0; c < LLH_Wins_Counter.length; c++) {
            row[c + 1] = LLH_Wins_Counter[c] + "";
            if (LLH_Wins_Counter[c] > bestValue) {
                Arrays.fill(properties, "");
                properties[c+1] = "bgcolor=\"green\"";
                bestValue = LLH_Wins_Counter[c];
            } else if (LLH_Wins_Counter[c] == bestValue) {
                properties[c+1] = "bgcolor=\"green\"";
            }
        }
        t.insertRow(row, properties);

        //--- Store table into the file.
        t.writeTableIntoFile(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_" + tableName);
    }

    /**
     * Stores VOI in the file.
     *
     * @param fileName name of the file
     */
    public static void writeVOIIntoTheFile(String fileName) {
        //--- Write all VOI into the single file.
        try {

            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_" + fileName + ".txt");
            //--- Terminals names.
            pout.write("# ");
            for (int i = 0; i < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; i++) {
                pout.write(Population.doubleTerminals[i].name + " ");
            }
            pout.write("\r\n");
            //--- VOI.
            for (Vector voi : VOI) {
                pout.write(voi.toString() + "\r\n");
            }

            pout.close();
        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
        }
        //--- Write VOI according to instance groups into the several files.
        for (String group : Problem.instanceSubgroups) {
            try {
                //--- Check if directory exists and create new one if it is not true.
                if (!new File(SNGP_HH_RL.resultsDirPath + "GROUPS_TXT\\").isDirectory()) {
                    new File(SNGP_HH_RL.resultsDirPath + "GROUPS_TXT\\").mkdir();
                    System.out.println("RESULTS WRITER: Directory .\\" + SNGP_HH_RL.resultsDirPath + "GROUPS_TXT\\ created!");
                }

                PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + "GROUPS_TXT\\" + SNGP_HH_RL.timeStampString + "_" + fileName + "_" + group + ".txt");
                //--- Terminals names.
                pout.write("# ");
                for (int i = 0; i < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; i++) {
                    pout.write(Population.doubleTerminals[i].name + " ");
                }
                pout.write("\r\n");
                //--- VOI.
                for (int i = 0; i < Problem.INSTANCES.length; i++) {
                    //--- If instance is in actual group, write VOI into corresponding file.
                    if (Problem.INSTANCES[i].instanceSubgroup.equals(group)) {
                        for (int t = 0; t < Population.bestIndividual.instanceVOI[i].size(); t++) {
                            pout.write(Population.bestIndividual.instanceVOI[i].get(t).toString() + "\r\n");
                        }
                    }
                }
                pout.close();
            } catch (FileNotFoundException FNFE) {
                FNFE.printStackTrace();
            }
        }
        //--- Write VOI according to instance groups into the MatLab file and split the arrays into smaller one.
        try {

            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_" + fileName + "_split.m");
            //--- Header.
            pout.write("close all\r\nclear all\r\n\r\n");
            //--- Terminals names.
            pout.write("% ");
            for (int i = 0; i < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; i++) {
                pout.write(Population.doubleTerminals[i].name + " ");
            }
            pout.write("\r\n");

            //--- Initialize VOI StringBuilders.
            StringBuilder LLHsb[] = new StringBuilder[Population.doubleTerminals.length - Settings.CONSTANTS_SIZE];
            for (int i = 0; i < LLHsb.length; i++) {
                LLHsb[i] = new StringBuilder();
                LLHsb[i].append(Population.bestIndividual.nodes[i].name + "_0 = [");
            }
            //--- Spilt VOI into the several arrays.
            int sumVOI = 0;
            int counterVOI;
            int partVOI;
            for (int i = 0; i < Problem.INSTANCES.length; i++) {
                sumVOI += Population.bestIndividual.instanceVOI[i].size();
            }

            //--- All VOI coordinates.
            int SIZE_OF_PARTS = 10000;
            for (int llh = 0; llh < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; llh++) {
                counterVOI = 0;
                partVOI = 1;
                //--- Get VOI from all instances.
                for (int i = 0; i < Problem.INSTANCES.length; i++) {
                    for (int t = 0; t < Population.bestIndividual.instanceVOI[i].size(); t++) {
                        LLHsb[llh].append(Population.bestIndividual.instanceVOI[i].get(t).values[llh] + " ");
                        counterVOI++;
                        //--- End of the part of the array.
                        if (counterVOI >= SIZE_OF_PARTS) {
                            LLHsb[llh].append("];\r\n");
                            LLHsb[llh].append(Population.bestIndividual.nodes[llh].name + "_" + partVOI + " = [");
                            partVOI++;
                            counterVOI = 0;
                        }
                    }

                }

            }

            for (int i = 0; i < LLHsb.length; i++) {
                pout.write(LLHsb[i].toString() + "];\r\n");
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
        }

        //--- Write VOI according to instance groups into the MatLab file.
        try {
            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_" + fileName + ".m");
            //--- Header.
            pout.write("close all\r\nclear all\r\n\r\n");
            //--- Terminals names.
            pout.write("% ");
            for (int i = 0; i < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; i++) {
                pout.write(Population.doubleTerminals[i].name + " ");
            }
            pout.write("\r\n");

            //--- VOI.
            StringBuilder LLHsb[] = new StringBuilder[Population.doubleTerminals.length - Settings.CONSTANTS_SIZE];
            for (int i = 0; i < LLHsb.length; i++) {
                LLHsb[i] = new StringBuilder();
                LLHsb[i].append(Population.bestIndividual.nodes[i].name + " = [");
            }
            for (int i = 0; i < Problem.INSTANCES.length; i++) {

                for (int t = 0; t < Population.bestIndividual.instanceVOI[i].size(); t++) {
                    for (int llh = 0; llh < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; llh++) {
                        LLHsb[llh].append(Population.bestIndividual.instanceVOI[i].get(t).values[llh] + " ");
                    }
                }
            }
            for (int i = 0; i < LLHsb.length; i++) {
                pout.write(LLHsb[i].toString() + "];\r\n");
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
        }

        //--- Write VOI according to instance groups into the several MatLab files.
        for (String group : Problem.instanceSubgroups) {
            try {
                //--- Check if directory exists and create new one if it is not true.
                if (!new File(SNGP_HH_RL.resultsDirPath + "GROUPS_MATLAB\\").isDirectory()) {
                    new File(SNGP_HH_RL.resultsDirPath + "GROUPS_MATLAB\\").mkdir();
                    System.out.println("RESULTS WRITER: Directory .\\" + SNGP_HH_RL.resultsDirPath + "GROUPS_MATLAB\\ created!");
                }

                PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + "GROUPS_MATLAB\\" + SNGP_HH_RL.timeStampString + "_" + fileName + "_" + group + ".m");
                //--- Header.
                pout.write("close all\r\nclear all\r\n\r\n");
                //--- Terminals names.
                pout.write("% ");
                for (int i = 0; i < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; i++) {
                    pout.write(Population.doubleTerminals[i].name + " ");
                }
                pout.write("\r\n");

                //--- VOI.
                StringBuilder LLHsb[] = new StringBuilder[Population.doubleTerminals.length - Settings.CONSTANTS_SIZE];
                for (int i = 0; i < LLHsb.length; i++) {
                    LLHsb[i] = new StringBuilder();
                    LLHsb[i].append(Population.bestIndividual.nodes[i].name + " = [");
                }
                for (int i = 0; i < Problem.INSTANCES.length; i++) {
                    //--- If instance is in actual group, write VOI into corresponding file.
                    if (Problem.INSTANCES[i].instanceSubgroup.equals(group)) {
                        for (int t = 0; t < Population.bestIndividual.instanceVOI[i].size(); t++) {
                            for (int llh = 0; llh < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; llh++) {
                                LLHsb[llh].append(Population.bestIndividual.instanceVOI[i].get(t).values[llh] + " ");
                            }
                        }
                    }
                }
                for (int i = 0; i < LLHsb.length; i++) {
                    pout.write(LLHsb[i].toString() + "];\r\n");
                }
                pout.close();
            } catch (FileNotFoundException FNFE) {
                FNFE.printStackTrace();
            }
        }

    }

    /**
     * Writes names of instances used in actual run into the file.
     */
    public static void writeUsedInstancesIntoFile() {
        try {
            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_INSTANCES.txt");
            for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
                pout.write(Problem.INSTANCES[i].name + "\r\n");
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
        }
    }

    /**
     * Saves best individual in various forms into the several files.
     */
    public static void writeIndividualIntoFiles() {

        Population.bestIndividual.saveIndividual();
        Population.bestIndividual.saveBestIndividual();
        Population.bestIndividual.saveBestInfixIndividual();

    }

    /**
     * Saves actions used in the evolution in the same order.
     */
    public static void writeUsedActionsIntoFile() {
        try {
            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_ACTIONS.txt");
            for (int i = 0; i < Problem.NUMBER_OF_ACTIONS; i++) {
                pout.write(Problem.actions[i].getClass().getSimpleName() + "\r\n");
            }
            for (int i = 0; i < Problem.evolvedHeuristics.size(); i++) {
                pout.write(Problem.evolvedHeuristics.get(i).individualName + "\r\n");
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
        }
    }

    /**
     * Saves the list of all terminals and functions into the file.
     */
    public static void writeNodesIntoFile() {
        try {
            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_TERMINALS.txt");
            for (int i = 0; i < Population.doubleTerminals.length; i++) {
                    pout.write(Population.doubleTerminals[i].getClass().getSimpleName() + "\r\n");
                    if(Population.doubleTerminals[i].getClass() == Constant.class) {
                        break;
                    }
            }
            pout.close();
            pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_FUNCTIONS.txt");
            for (int i = 0; i < Population.doubleFunctions.length; i++) {
                pout.write(Population.doubleFunctions[i].getClass().getSimpleName() + "\r\n");
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {
            FNFE.printStackTrace();
        }
    }

    /**
     * Sets proper properties for table celss in actual row with index index.
     *
     * @param index index to set
     * @param value value to set
     * @return
     */
    public static void setProperties(int index, double value) {
        if (Problem.isBetterFitness(value, bestValue)) {
            Arrays.fill(properties, "");
            properties[index] = "bgcolor=\"green\"";
            bestValue = value;
        }
        //--- Another same best result found.
        else if (value == bestValue) {
            properties[index] = "bgcolor=\"green\"";
        }
    }

    /**
     * Rounbds double value on specific number of decimals
     *
     * @param val         input double value
     * @param numDecimals number of decimal for rounding
     * @return rounded double value
     */
    public static double roundDouble(double val, int numDecimals) {
        int coef = 1;
        for (int i = 0; i < numDecimals; i++) {
            coef *= 10;
        }

        val = Math.round(val * coef) / (double) (coef);
        return val;
    }
}
