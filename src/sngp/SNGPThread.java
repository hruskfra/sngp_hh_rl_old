/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sngp;

import gp.individuals.*;
import gp.tasks.Problem;

import java.util.ArrayList;

/**
 * @author Fanda
 */
public class SNGPThread extends Thread implements Runnable {

    /**
     * ID of current thread.
     */
    public int threadID;

    /**
     * Number of current instance for execution.
     */
    public int threadInstance;

    /**
     * Clone of the individual from the main thread.
     */
    public Individual individual;

    /**
     * Individual numbers.
     */
    public ArrayList<Integer> assignedInstances = new ArrayList();
    public ArrayList<Integer> executedInstances = new ArrayList();

    /**
     * If it can be redistributed
     */
    public static boolean CAN_BE_REDISTRIBUTED = true;
    public boolean WAIT_FOR_MUTEX = false;
    public boolean WAIT_FOR_THREADS = false;
    public static boolean CRITICAL_SECTION = false;

    public long startThTime = System.currentTimeMillis();
    public long actualThTime = System.currentTimeMillis();

    /**
     * States of the thread.
     */
    public enum STATE {

        WAIT, EXECUTE;
    }

    public volatile STATE THREAD_STATE = STATE.WAIT;

    /**
     * Constructor.
     */
    public SNGPThread(int id) {
        this.threadID = id;
    }

    public void restartThread(int id) {
        this.threadID = id;
    }

    @Override
    public String toString() {
        String s = "Thread: " + this.threadID + " | Assigned instances: ";
        for (int ind : assignedInstances) {
            s += ind + " ";
        }
        s+= "|";

        return s;
    }

    /**
     * Redistributes instances numbers into all allocated threads.
     *
     * @param t              Threads
     * @param newRun         indicates if this method should allocate whole population or reallocate remaining numbers.
     */
    public static synchronized void redistributeInstances(SNGPThread[] t, boolean newRun) {
        // New round, assign individuals to ArrayList.
        if (newRun) {
            for (int th = 0; th < t.length; th++) {
                t[th].assignedInstances = new ArrayList();
            }
            for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i += t.length) {
                // All instances reassigned.
                if (i >= Problem.NUMBER_OF_INSTANCES) {
                    break;
                }
                // Add instance to the actual thread.
                for (int th = 0; th < t.length; th++) {
                    if (i + th < Problem.NUMBER_OF_INSTANCES) {
                        t[th].assignedInstances.add(i + th);
                    }
                    //System.out.println(MethodName.getCurrentMethodName() + " " + t[th].threadID + " added " + (i+th));
                }
            }
            return;
        }
        // Redistribution of instances.
        if (!newRun) {
            SNGPThread.CRITICAL_SECTION = true; // Lock the critical section.

            int[] instances = new int[t.length];
            int[] newSizes = new int[t.length];
            int oldInstancesCount = 0;
            int newInstancesCount = 0;
            int perThread = 0;
            int rest = 0;
            ArrayList<Integer> freeInstances = new ArrayList();
            // Store number of instances.
            for (int th = 0; th < t.length; th++) {
                instances[th] = t[th].assignedInstances.size();
                oldInstancesCount += instances[th];
            }
            // Get number of instances per thread.
            perThread = oldInstancesCount / t.length;
            // Rest of the instances after integer division.
            rest = oldInstancesCount - (perThread * t.length);
            for (int i = 0; i < t.length; i++) {
                //System.out.println(MethodName.getCurrentMethodName() + "\r\nBefore\r\n" + t[i].toString());
                newSizes[i] = perThread;
                // If there is instances not placed into the thread by integer division, add one to the thread.
                if (i < rest) {
                    newSizes[i]++;
                }
            }
            /*System.out.println(MethodName.getCurrentMethodName());
            for(int pp : newSizes) {
            System.out.print(pp + " ");
            }
            System.out.println();/**/
            //System.out.println(MethodName.getCurrentMethodName() + " perThread " + perThread);

            // Do not redistribute if there are not enough instances to execute.
            if (perThread <= 1) {
                SNGPThread.CAN_BE_REDISTRIBUTED = false;
                SNGPThread.CRITICAL_SECTION = false; // Leave the critical section!
                return;
            }

            // Remove all instances in above average size threads and store them into the list of free instances.
            for (int i = 0; i < t.length; i++) {
                if (t[i].assignedInstances.size() > newSizes[i]) {
                    int listSize = t[i].assignedInstances.size();
                    //--- Mark extra instances in the thread as free instances.
                    for (int j = newSizes[i]; j < listSize; j++) {
                        freeInstances.add(t[i].assignedInstances.get(j));
                    }
                    //--- Remove free instances from this thread list.
                    for (int j = newSizes[i]; j < listSize; j++) {
                        t[i].assignedInstances.remove(newSizes[i]);
                    }
                }
            }
            //System.out.println(MethodName.getCurrentMethodName() + " " + freeInds.toString());
            // Assign new individuals to new threads.
            for (int i = 0; i < t.length; i++) {
                // Skip all full threads
                if (t[i].assignedInstances.size() >= newSizes[i]) {
                    System.out.println(MethodName.getCurrentMethodName() + "\r\nAfter\r\n" + t[i].toString());
                    newInstancesCount += t[i].assignedInstances.size();
                    continue;
                }
                // Fill the free threads.
                for (int j = t[i].assignedInstances.size(); j < newSizes[i]; j++) {
                    t[i].assignedInstances.add(freeInstances.get(0));
                    freeInstances.remove(0);
                }
                System.out.println(MethodName.getCurrentMethodName() + "\r\nAfter\r\n" + t[i].toString());
                newInstancesCount += t[i].assignedInstances.size();
            }

            if (!freeInstances.isEmpty()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There are some free instances after redistribution!");
                System.out.println(MethodName.getCurrentMethodName() + ": Free instances - " + freeInstances.toString());
                System.exit(1);
            }
            if (newInstancesCount != oldInstancesCount) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There is another count of instances in all threads after redistribution! (" + oldInstancesCount + " != " + newInstancesCount + ") ");
                System.exit(1);
            }

            SNGPThread.CRITICAL_SECTION = false; // Leave critical section.
        }
    }

    /**
     * Override method.
     *
     * ERRORS - Critical section while execution is done can cause problems.
     * - Fix it by locking all threads after execution of one instance is done and than redistribute instances.
     */
    public void run() {

        int instancesCount;
        while (true) {
            if (THREAD_STATE == STATE.WAIT) {
                // Waiting loop.
            } else if (THREAD_STATE == STATE.EXECUTE) {
                instancesCount = this.assignedInstances.size();

                //--- Evaluate instances.
                for (int i = 0; i < instancesCount; i++) {
                    // Assign instance.
                    int instanceID = individual.instanceExecutedID = this.assignedInstances.get(0);
                    this.executedInstances.add(instanceID);
                    this.assignedInstances.remove(0);

                    this.individual.executeIndividual1Thread(instanceID);

                    /*while (SNGPThread.CRITICAL_SECTION) {
                        // Waiting loop for execution of critical section.
                    }/**/
                }

                //System.out.println(MethodName.getCurrentMethodName() + " Thread " + this.threadID + " FINISHED!");

                //--- the thread is complete, it will wait for others to finish their jobs.
                SNGP_HH_RL.increaseThreadsDone();
                this.THREAD_STATE = STATE.WAIT;
                            }
        }
    }

    /**
     * Override method.
     *
     * ERRORS - Critical section while execution is done can cause problems.
     * - Fix it by locking all threads after execution of one instance is done and than redistribute instances.
     */
    public void run2() {

        int instancesCount;
        while (true) {
            /*if (THREAD_STATE == STATE.EXECUTE) {
                System.out.println(MethodName.getCurrentMethodName() + " +THR " + this.threadID + " " + THREAD_STATE.toString());
            }/**/

            /*if (System.currentTimeMillis() - actualThTime > 10E2) {
                System.out.println(MethodName.getCurrentMethodName() + " ++THR " + this.threadID + " " + THREAD_STATE.toString());
                actualThTime = System.currentTimeMillis();
            }/**/

            if (THREAD_STATE == STATE.WAIT) {
                // Waiting loop.
            } else if (THREAD_STATE == STATE.EXECUTE) {
                instancesCount = this.assignedInstances.size();


                //--- Assign instances.
                //redistributeInstances(SNGP_HH_RL.threads, true);


                for (int i = 0; i < instancesCount; i++) {
                    // Assign instance.
                    int instanceID = individual.instanceExecutedID = this.assignedInstances.get(0);
                    System.out.println("Thread " + this.threadID + " | Instance " + instanceID + " | To do " + this.assignedInstances.size());
                    this.executedInstances.add(instanceID);
                    this.assignedInstances.remove(0);
                    System.out.println("Thread " + this.threadID + " | Instance " + instanceID + " | To do do " + this.assignedInstances.size());
                    //System.out.println(MethodName.getCurrentMethodName() + " TH: " + this.threadID + " " + instanceID);
                    //for (int node = 0; node < Settings.POPULATION_SIZE; node++) {

                    //System.out.println(MethodName.getCurrentMethodName() + " MISSING IMPLEMENTATION!");
                    //System.exit(1);
                    this.individual.executeIndividual1Thread(instanceID);

                        while (SNGPThread.CRITICAL_SECTION) {
                            // Waiting loop for execution of critical section.
                        }/**/
                        // Check if thread has something to execute and if not end it.
                        /*if (this.assignedInstances.isEmpty()) {
                            //System.out.println(MethodName.getCurrentMethodName() + " Thread " + this.threadID + " FINISHED!");
                            break;
                        }/**/
                    //}
                }


                SNGP_HH_RL.increaseThreadsDone();
                if (CAN_BE_REDISTRIBUTED) {
                    redistributeInstances(SNGP_HH_RL.threads, false);
                } else {
                    //System.out.println(MethodName.getCurrentMethodName() + " Thread " + this.threadID + " FINISHED!");
                    SNGP_HH_RL.increaseThreadsDone();
                    this.THREAD_STATE = STATE.WAIT;
                }
                // Empty thread.
                if (this.assignedInstances.isEmpty() && CAN_BE_REDISTRIBUTED) {
                    //System.out.println(MethodName.getCurrentMethodName() + " Thread " + this.threadID + " FINISHED!");
                    SNGP_HH_RL.increaseThreadsDone();
                    this.THREAD_STATE = STATE.WAIT;
                }/**/
            }
        }
    }
}
