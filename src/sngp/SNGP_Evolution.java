package sngp;

import gp.Crossover;
import gp.Mutation;
import gp.Node;
import gp.Population;
import gp.tasks.bpp.BPP;
import gp.tasks.Problem;
import gp.tasks.bpp.BPP_GHH;
import gp.tasks.cvrp.CVRP;
import gp.terminals.Constant;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Fanda on 04-Dec-15.
 */
public class SNGP_Evolution {

    public static int BFS_NOT_FOUND_FOR = 0;

    public static boolean BEST_EXECUTION = false;
    public static int terminalsCountsInBestIndividual[];
    public static StringBuilder infixIndividualSB = new StringBuilder();
    public static StringBuilder bestAlgorithmExecutionProgressSB = new StringBuilder();
    public static StringBuilder bestExecutionNodeValuesSB = new StringBuilder();
    public static HashMap<String, Double> vectorsOfInterestsMap = new HashMap();
    public static StringBuilder allVectorsOfInterestsSB = new StringBuilder();
    public static StringBuilder algorithmStepsSB = new StringBuilder();
    public static boolean SAVE_BEST_VECTORS = false;
    public static ArrayList<StringBuilder> allBestVectorsOfInterestsSB = new ArrayList();

    /**
     * Evolves individuals for the one experiment with one thread.
     *
     * @param e number of experiment for the results outputs
     */
    public static void evolve1Thread(int e) {
        //--- First evaluation.
        //Population.newIndividual.markConstants();

        for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
            Population.newIndividual.executeIndividual1Thread(i);
        }

        //--- Calculate fitness value of all nodes.
        Population.newIndividual.calcNodesTotalFitness();
        Population.newIndividual.calcPopulationFitness();

        //--- Clone the results.
        Population.actualIndividual = Population.newIndividual.clone();
        Population.bestIndividual = Population.newIndividual.clone();
        Population.bestIndividual.saveIndividual();
        printEvolutionInformation(e, -1, true);

        //--- Evolution cycle.
        for (int ACTUAL_GENERATION = 0; ACTUAL_GENERATION < Settings.GENERATIONS; ACTUAL_GENERATION++) {

            //--- Mutate actual individual.
            performMutations();

            //--- Execute new individual.
            Population.newIndividual.executeIndividual1Thread();

            //--- Calculate its fitness.
            Population.newIndividual.calcNodesTotalFitness();
            Population.newIndividual.calcPopulationFitness();

            //--- Compare, accept and save best individual with the new one.
            compareIndividuals(e, ACTUAL_GENERATION);
        }
    }

    /**
     * Evolves individuals for the one experiment with more threads.
     *
     * @param e number of experiment for the results outputs
     */
    public static void evolveMultiThread(int e) {
        //--- Assign each instance to one thread.
        SNGPThread.redistributeInstances(SNGP_HH_RL.threads, true);

        //--- Assign new individual.
        for (SNGPThread t : SNGP_HH_RL.threads) {
            t.individual = Population.newIndividual.clone();
            t.individual.assignedThread = t;
            for (Node n : t.individual.nodes) {
                n.isFitnessActual = false;
            }
            t.executedInstances = new ArrayList(); //--- Restart the list of executed instances.
        }

        //--- Execute all threads.
        for (SNGPThread t : SNGP_HH_RL.threads) {
            t.THREAD_STATE = SNGPThread.STATE.EXECUTE;
        }

        while (SNGP_HH_RL.THREADS_DONE != SNGP_HH_RL.threads.length) {
            //--- Waiting loop.
        }

        //--- Restart running thread counter.
        SNGP_HH_RL.THREADS_DONE = 0;

        //--- Get actual fitness values from all threads.
        for (SNGPThread t : SNGP_HH_RL.threads) {
            int n;
            for (int i = 0; i < t.executedInstances.size(); i++) {

                n = t.executedInstances.get(i);
                for (int node = Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES; node < Settings.POPULATION_SIZE; node++) {
                    Population.newIndividual.nodes[node].nodeInstanceFitness[n] = t.individual.nodes[node].nodeInstanceFitness[n];
                }
            }
        }

        //--- Calculate fitness value.
        Population.newIndividual.calcNodesTotalFitness();
        Population.newIndividual.calcPopulationFitness();

        //--- Store actual individual from initialization process.
        Population.actualIndividual = Population.newIndividual.clone();
        Population.bestIndividual = Population.newIndividual.clone();

        //--- Print some information.
        printEvolutionInformation(e, -1, true);
        Population.bestIndividual.saveIndividual();
        //Population.bestIndividual.printIndividual();

        // System.out.println(MethodName.getCurrentMethodName() + " " + (timeStamp - System.currentTimeMillis()));
        //  timeStamp = System.currentTimeMillis();

        for (int ACTUAL_GENERATION = 0; ACTUAL_GENERATION < Settings.GENERATIONS; ACTUAL_GENERATION++) {

            //--- Perform several mutations.
            performMutations();

            //--- Redistribute instances into threads.
            SNGPThread.redistributeInstances(SNGP_HH_RL.threads, true);

            //--- Assign new individual to threads.
            for (SNGPThread t : SNGP_HH_RL.threads) {
                t.individual = Population.newIndividual.clone();
                t.individual.assignedThread = t;
                t.executedInstances = new ArrayList(); // Restart the list of executed instances.
            }

            //--- Execution part, where all threads are executed.
            for (SNGPThread t : SNGP_HH_RL.threads) {
                t.THREAD_STATE = SNGPThread.STATE.EXECUTE;
            }
            while (SNGP_HH_RL.THREADS_DONE != SNGP_HH_RL.threads.length) {
                //--- Waiting loop.
            }
            SNGP_HH_RL.THREADS_DONE = 0;

            //--- Get actual fitness values from all threads.
            for (SNGPThread t : SNGP_HH_RL.threads) {
                int n;
                for (int i = 0; i < t.executedInstances.size(); i++) {
                    n = t.executedInstances.get(i);
                    for (int node = 0; node < Settings.POPULATION_SIZE; node++) {
                        Population.newIndividual.nodes[node].nodeInstanceFitness[n] = t.individual.nodes[node].nodeInstanceFitness[n];
                    }
                }
            }

            //--- Calculate fitness.
            Population.newIndividual.calcNodesTotalFitness();
            Population.newIndividual.calcPopulationFitness();

            //--- Compare, accept and save best individual with the new one.
            compareIndividuals(e, ACTUAL_GENERATION);
        }   //--- End for(gen...)
    }

    /**
     * Prints information about evolution.
     *
     * @param gen
     * @param foundBetter
     */
    public static void printEvolutionInformation(int exp, int gen, boolean foundBetter) {


        if (foundBetter) {
            System.out.print("EXP: " + exp + " | GEN: " + gen + "\r\n");
            if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.CVRP) {
                for (int i = 0; i < CVRP.INSTANCES.length; i++) {

                    if (Population.bestIndividual.nodes[Population.bestIndividual.getBestNodeID()].nodeInstanceFitness[i] > CVRP.INSTANCES[i].bestHeuristicFitness) {
                        System.out.println("\033[31m" + "INST: " + CVRP.INSTANCES[i].name + " | HH Fitness: " + Population.bestIndividual.nodes[Population.bestIndividual.getBestNodeID()].nodeInstanceFitness[i] + " | LLH Fitness: " + CVRP.INSTANCES[i].bestHeuristicFitness);
                    }
                    if (Population.bestIndividual.nodes[Population.bestIndividual.getBestNodeID()].nodeInstanceFitness[i] == CVRP.INSTANCES[i].bestHeuristicFitness) {
                        System.out.println("\033[34m" + "INST: " + CVRP.INSTANCES[i].name + " | HH Fitness: " + Population.bestIndividual.nodes[Population.bestIndividual.getBestNodeID()].nodeInstanceFitness[i] + " | LLH Fitness: " + CVRP.INSTANCES[i].bestHeuristicFitness);
                    }
                    if (Population.bestIndividual.nodes[Population.bestIndividual.getBestNodeID()].nodeInstanceFitness[i] < CVRP.INSTANCES[i].bestHeuristicFitness) {
                        System.out.println("\033[32m" + "INST: " + CVRP.INSTANCES[i].name + " | HH Fitness: " + Population.bestIndividual.nodes[Population.bestIndividual.getBestNodeID()].nodeInstanceFitness[i] + " | LLH Fitness: " + CVRP.INSTANCES[i].bestHeuristicFitness);
                    }
                    System.out.print("\033[0m"); //--- Reset colors
                }
            } else if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP) {
                double sum = 0.0;
                for (int i = 0; i < BPP.INSTANCES.length; i++) {
                    sum += BPP.INSTANCES[i].bestHeuristicFitness;
                }
                if (sum < Population.bestIndividual.bestFitness) {
                    System.out.println("\033[31m" + "HH Fitness: " + Population.bestIndividual.bestFitness + " | LLH Fitness: " + sum);
                }
                if (sum == Population.bestIndividual.bestFitness) {
                    System.out.println("\033[34m" + "HH Fitness: " + Population.bestIndividual.bestFitness + " | LLH Fitness: " + sum);
                }
                if (sum > Population.bestIndividual.bestFitness) {
                    System.out.println("\033[32m" + "HH Fitness: " + Population.bestIndividual.bestFitness + " | LLH Fitness: " + sum);
                }
                System.out.print("\033[0m"); // Reset colors

            } else if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP_GHH) {
                double sum = 0.0;
                for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
                    //--- Only FFD heuristic.
                    sum += BPP.llhs[0].instancesFitness[i];
                }

                if (sum < Population.bestIndividual.bestFitness) {
                    System.out.println("\033[31m" + "HH Fitness: " + Population.bestIndividual.bestFitness + " | LLH Fitness: " + sum);
                }
                if (sum == Population.bestIndividual.bestFitness) {
                    System.out.println("\033[34m" + "HH Fitness: " + Population.bestIndividual.bestFitness + " | LLH Fitness: " + sum);
                }
                if (sum > Population.bestIndividual.bestFitness) {
                    System.out.println("\033[32m" + "HH Fitness: " + Population.bestIndividual.bestFitness + " | LLH Fitness: " + sum);
                }
                System.out.print("\033[0m"); // Reset colors

            } else if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.IMG) {
                System.out.println("\033[34m" + " Fitness: " + Population.bestIndividual.bestFitness);
            } else {
                System.out.println(MethodName.getCurrentMethodName() + " ERROR - Missing implementation for the " + Settings.OPTIMIZATION_PROBLEM.toString() + "!");
                System.exit(1);
            }
        } else {
            double sum = 0.0;
            for (int i = 0; i < BPP.INSTANCES.length; i++) {
                sum += BPP.INSTANCES[i].bestHeuristicFitness;
            }
            System.out.println("EXP: " + exp + " | GEN: " + gen + " | HH Fitness: " + Population.bestIndividual.bestFitness + " (gen: " + Population.bestIndividual.startingGeneration + ") | LLH Fitness: " + sum);
        }

    }

    /**
     * Mutation procedure.
     */
    public static void performMutations() {
        int numMut = (int) (Math.random() * (Settings.NUMBER_OF_MUTATIONS + (BFS_NOT_FOUND_FOR / Settings.MUTATION_RATE_INCREASED_AFTER) + 1));
        //int numMut = Settings.NUMBER_OF_MUTATIONS + (BFS_NOT_FOUND_FOR / Settings.MUTATION_RATE_INCREASED_AFTER) + 1;
        //System.out.println(MethodName.getCurrentMethodName() + " 3 - " + numMut);
        for (int m = 0; m < numMut; m++) {
            Mutation.linkChangeMutation(Population.newIndividual, false);
        }
        if (Math.random() < Settings.MUT_CONST_PROB) {
            Mutation.constantMutation(Population.newIndividual);
        }
    }

    /**
     * Compares new individual with the best one and accept if the new individual is at least the same quality as the best one.
     */
    public static void compareIndividuals(int e, int ACTUAL_GENERATION) {
        //--- Indicates if best individual was found in actual generation.
        boolean BEST_FOUND = false;

        //--- Best individual found.
        if (Settings.FITNESS_MINIMIZATION) {
            if (Population.newIndividual.bestFitness < Population.bestIndividual.bestFitness) {
                Population.bestIndividual = Population.newIndividual.clone();
                Population.bestIndividual.startingGeneration = ACTUAL_GENERATION;
                printEvolutionInformation(e, ACTUAL_GENERATION, true);
                Population.bestIndividual.saveIndividual();
                Population.bestIndividual.saveBestIndividual();
                Population.bestIndividual.saveBestInfixIndividual();

                BFS_NOT_FOUND_FOR = 0;
                BEST_FOUND = true;
                Population.actualIndividual = Population.newIndividual.clone();
            }
        } else {
            if (Population.newIndividual.bestFitness > Population.bestIndividual.bestFitness) {
                Population.bestIndividual = Population.newIndividual.clone();
                Population.bestIndividual.startingGeneration = ACTUAL_GENERATION;
                printEvolutionInformation(e, ACTUAL_GENERATION, true);
                Population.bestIndividual.saveIndividual();
                Population.bestIndividual.saveBestIndividual();
                Population.bestIndividual.saveBestInfixIndividual();

                BFS_NOT_FOUND_FOR = 0;
                BEST_FOUND = true;
                Population.actualIndividual = Population.newIndividual.clone();
            }
        }

        if(!BEST_FOUND) {
            if (Population.newIndividual.bestFitness == Population.bestIndividual.bestFitness) {
                if (ACTUAL_GENERATION % Settings.PRINT_EVOLUTION_INFORMATION_FREQUENCY == 0 && ACTUAL_GENERATION > 0) {
                    printEvolutionInformation(e, ACTUAL_GENERATION, false);
                }
                //--- Accept the individual with the same fitness.
                Population.actualIndividual = Population.newIndividual.clone();
                if (BFS_NOT_FOUND_FOR / Settings.MUTATION_RATE_INCREASED_AFTER < Settings.MAX_MUTATIONS - Settings.NUMBER_OF_MUTATIONS) {
                    BFS_NOT_FOUND_FOR++;
                }
            } else {
                if (ACTUAL_GENERATION % Settings.PRINT_EVOLUTION_INFORMATION_FREQUENCY == 0 && ACTUAL_GENERATION > 0) {
                    printEvolutionInformation(e, ACTUAL_GENERATION, false);
                }
                //--- Return original individual.
                Population.newIndividual = Population.actualIndividual.clone();

                if (BFS_NOT_FOUND_FOR / Settings.MUTATION_RATE_INCREASED_AFTER < Settings.MAX_MUTATIONS - Settings.NUMBER_OF_MUTATIONS) {
                    BFS_NOT_FOUND_FOR++;
                }
            }
        }
    }

    /**
     * Restarts and clears StringBuilders.
     */
    public static void initStatistics() {
        // TO DO for more problems.
        //--- Restart StringBuilders.
        infixIndividualSB = new StringBuilder();
        bestAlgorithmExecutionProgressSB = new StringBuilder();
        bestExecutionNodeValuesSB = new StringBuilder();
        vectorsOfInterestsMap = new HashMap();
        algorithmStepsSB = new StringBuilder();
        terminalsCountsInBestIndividual = new int[Population.doubleTerminals.length - Settings.CONSTANTS_SIZE];
        for (int t : terminalsCountsInBestIndividual) t = 0;
    }

    /**
     * Writes statistics from experiments into the several files.
     */
    public static void writeStatisticsIntoFiles() {
        for (int n = 0; n < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; n++) {
            terminalsCountsInBestIndividual[n] += Population.bestIndividual.nodes[n].nodeOccurences;
        }
        writeAlgorithmValuesIntoFile();
        writeIndividualsIntoFile();
        writeTerminalOccurenceIntoFile();
        writeVectorsOfInterestsIntoFile();
        writeNodesValuesIntoFile();
        writeAlgorithmsStepsIntoFile();
        writeAllVectorsOfInterestsIntoFile();
        writeAllBestVectorsOfInterestsIntoFile();
        writeSettingsIntoFile();
    }

    /**
     * Stores vectors of interest in this instance.
     */
    public static void writeVectorsOfInterestsIntoFile() {
        try {
            // Remove - chars from the name of the instnace.
            String name = "";
            for (int i = 0; i < CVRP.INSTANCES[0].name.length(); i++) {
                if (CVRP.INSTANCES[0].name.charAt(i) == '-') {
                    continue;
                }
                if (CVRP.INSTANCES[0].name.charAt(i) == '.') {
                    break;
                }
                name += CVRP.INSTANCES[0].name.charAt(i);
            }

            PrintWriter pout = new PrintWriter("VECTORS_" + name + "_" + Population.bestIndividual.nodes[Settings.POPULATION_SIZE - 1].name + ".txt");
            for (String k : vectorsOfInterestsMap.keySet()) {
                pout.write(k + " " + vectorsOfInterestsMap.get(k) + "\r\n");
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }

    /**
     * Stores vectors of interest in this instance.
     */
    public static void writeAllVectorsOfInterestsIntoFile() {
        try {
            // Remove - chars from the name of the instance.
            String name = "";
            for (int i = 0; i < CVRP.INSTANCES[0].name.length(); i++) {
                if (CVRP.INSTANCES[0].name.charAt(i) == '-') {
                    continue;
                }
                if (CVRP.INSTANCES[0].name.charAt(i) == '.') {
                    break;
                }
                name += CVRP.INSTANCES[0].name.charAt(i);
            }

            PrintWriter pout = new PrintWriter("ALL_VECTORS_" + name + "_" + Population.bestIndividual.nodes[Settings.POPULATION_SIZE - 1].name + ".txt");
            pout.write(allVectorsOfInterestsSB.toString());
            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }

    /**
     * Stores vectors of interest in this instance.
     */
    public static void writeAllBestVectorsOfInterestsIntoFile() {
        try {
            // Remove - chars from the name of the instance.
            String name = "";
            for (int i = 0; i < CVRP.INSTANCES[0].name.length(); i++) {
                if (CVRP.INSTANCES[0].name.charAt(i) == '-') {
                    continue;
                }
                if (CVRP.INSTANCES[0].name.charAt(i) == '.') {
                    break;
                }
                name += CVRP.INSTANCES[0].name.charAt(i);
            }

            PrintWriter pout = new PrintWriter("ALL_BEST_ONLY_VECTORS_" + name + "_" + Population.bestIndividual.nodes[Settings.POPULATION_SIZE - 1].name + ".txt");
            for (int i = allBestVectorsOfInterestsSB.size() - 1; i >= 0; i--) {
                pout.write(allBestVectorsOfInterestsSB.get(i).toString());
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }

    /**
     * Saves terminal occurence during the training phase.
     */
    public static void writeTerminalOccurenceIntoFile() {
        try {
            // Remove - chars from the name of the instnace.
            String name = "";
            for (int i = 0; i < CVRP.INSTANCES[0].name.length(); i++) {
                if (CVRP.INSTANCES[0].name.charAt(i) == '-') {
                    continue;
                }
                if (CVRP.INSTANCES[0].name.charAt(i) == '.') {
                    break;
                }
                name += CVRP.INSTANCES[0].name.charAt(i);
            }
            PrintWriter pout = new PrintWriter("TERMINALS_" + name + "_" + Population.bestIndividual.nodes[Settings.POPULATION_SIZE - 1].name + ".txt");
            for (int n = 0; n < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; n++) {
                pout.write(Population.bestIndividual.nodes[n].getClass().getSimpleName() + " " + terminalsCountsInBestIndividual[n] + "\r\n");
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }

    /**
     * Writes individuals in infix form into the file.
     */
    public static void writeIndividualsIntoFile() {
        try {
            // Remove - chars from the name of the instance.
            String name = "";
            for (int i = 0; i < CVRP.INSTANCES[0].name.length(); i++) {
                if (CVRP.INSTANCES[0].name.charAt(i) == '-') {
                    continue;
                }
                if (CVRP.INSTANCES[0].name.charAt(i) == '.') {
                    break;
                }
                name += CVRP.INSTANCES[0].name.charAt(i);
            }
            PrintWriter pout = new PrintWriter("INDIVIDUALS_" + name + "_" + Population.bestIndividual.nodes[Settings.POPULATION_SIZE - 1].name + ".txt");
            pout.write(infixIndividualSB.toString());
            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }

    /**
     * Writes values from non-fixed algorithm from evolution.
     */
    public static void writeAlgorithmValuesIntoFile() {
        try {
            // Remove - chars from the name of the instnace.
            String name = "";
            for (int i = 0; i < CVRP.INSTANCES[0].name.length(); i++) {
                if (CVRP.INSTANCES[0].name.charAt(i) == '-') {
                    continue;
                }
                if (CVRP.INSTANCES[0].name.charAt(i) == '.') {
                    break;
                }
                name += CVRP.INSTANCES[0].name.charAt(i);
            }
            PrintWriter pout = new PrintWriter("ALGORITHM_" + name + "_" + Population.bestIndividual.nodes[Settings.POPULATION_SIZE - 1].name + ".m");
            pout.write(bestAlgorithmExecutionProgressSB.toString());
            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }

    /**
     * Writes node values for all algorithm trees into the matlab file.
     */
    public static void writeNodesValuesIntoFile() {
        try {
            // Remove - chars from the name of the instnace.
            String name = "";
            for (int i = 0; i < CVRP.INSTANCES[0].name.length(); i++) {
                if (CVRP.INSTANCES[0].name.charAt(i) == '-') {
                    continue;
                }
                if (CVRP.INSTANCES[0].name.charAt(i) == '.') {
                    break;
                }
                name += CVRP.INSTANCES[0].name.charAt(i);
            }
            PrintWriter pout = new PrintWriter("VALUES_" + name + "_" + Population.bestIndividual.nodes[Settings.POPULATION_SIZE - 1].name + ".m");
            pout.write(bestExecutionNodeValuesSB.toString());
            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }

    /**
     * Writes node values for all algorithm trees into the matlab file.
     */
    public static void writeAlgorithmsStepsIntoFile() {
        try {
            // Remove - chars from the name of the instnace.
            String name = "";
            for (int i = 0; i < CVRP.INSTANCES[0].name.length(); i++) {
                if (CVRP.INSTANCES[0].name.charAt(i) == '-') {
                    continue;
                }
                if (CVRP.INSTANCES[0].name.charAt(i) == '.') {
                    break;
                }
                name += CVRP.INSTANCES[0].name.charAt(i);
            }
            PrintWriter pout = new PrintWriter("STEPS_" + name + "_" + Population.bestIndividual.nodes[Settings.POPULATION_SIZE - 1].name + ".txt");
            pout.write(algorithmStepsSB.toString());
            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }

    /**
     * Writes node values for all algorithm trees into the matlab file.
     */
    public static void writeSettingsIntoFile() {
        try {
            // Remove - chars from the name of the instnace.
            /*String name = "";
            for(int i = 0; i < TSP.INSTANCES[0].name.length(); i++) {
                if(TSP.INSTANCES[0].name.charAt(i) == '-') {
                    continue;
                }
                if(TSP.INSTANCES[0].name.charAt(i) == '.') {
                    break;
                }
                name += TSP.INSTANCES[0].name.charAt(i);
            }/**/
            PrintWriter pout = new PrintWriter("SETTINGS.txt");

            pout.write("GENERATIONS " + Settings.GENERATIONS + "\r\n");
            pout.write("EXPERIMENTS " + Settings.NUMBER_OF_EXPERIMENTS + "\r\n");
            pout.write("POPULATION_SIZE " + Settings.POPULATION_SIZE + "\r\n");
            pout.write("CONSTANTS " + Settings.CONSTANTS_SIZE + "\r\n");
            pout.write("TERMINALS " + Population.doubleTerminals.length + "\r\n");
            pout.write("TERMINAL_CLASSES: ");
            for (int n = 0; n < Population.doubleTerminals.length; n++) {
                pout.write(Population.doubleTerminals[n].getClass().getSimpleName());
                if (n != Population.doubleTerminals.length - 1) {
                    pout.write(" ");
                }
                pout.write("\r\n");
            }
            pout.write("FUNCTION_CLASSES: ");
            for (int n = 0; n < Population.doubleFunctions.length; n++) {
                pout.write(Population.doubleFunctions[n].getClass().getSimpleName());
                if (n != Population.doubleFunctions.length - 1) {
                    pout.write(" ");
                }
                pout.write("\r\n");
            }


            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }
}
