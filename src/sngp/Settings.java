/**
 * Created by Fanda on 3. 9. 2015.
 */
package sngp;

public class Settings {

    /**
     * Optimization problem.
     */
    public enum OptimizationProblem {
        BPP,
        BPP_GHH,
        TSP,
        TSPTW,
        CVRP,
        IMG
    };
    public static OptimizationProblem OPTIMIZATION_PROBLEM = OptimizationProblem.BPP;

    /**
     * Population size.
     */
    public static int POPULATION_SIZE = 300;

    /**
     * Constants.
     */
    public static int CONSTANTS_SIZE = 20;
    public static double CONSTNANT_MIN_VALUE = 0.0;
    public static double CONSTNANT_MAX_VALUE = 1.0;

    /**
     * Is actual fitness minimization? 
     */
    public static boolean FITNESS_MINIMIZATION = true;
    
    /**
     * Number of individuals represented by set of algorithm node at the end of the genome.
     */
    public static int INDIVIDUALS_ROOTED_AT_THE_END = 1;
    public static int NUMBER_OF_EXECUTED_NODES = 32; // POPULATION_SIZE for all nodes, POPULATION_SIZE - TERMINALS for all function nodes.
    public static int TOTAL_NUMBER_OF_ROOTS;

    /**
     * Number of generations.
     */
    public static int GENERATIONS = 300000;

    /**
     * Number of experiments.
     */
    public static int NUMBER_OF_EXPERIMENTS = 1;

    /**
     * Information about evolution will be printed every X generations.
     */
    public static int PRINT_EVOLUTION_INFORMATION_FREQUENCY = 100;

    /**
     * Random seed for the clustering algorithms.
     */
    public static long RANDOM_SEED = 124;

    /**
     * Number of fixed nodes.
     */
    public static int NUMBER_OF_FIXED_NODES = 1;

    /**
     * Type of selection.
     */
    public enum Selection {
        depthwise,
        random
    }
    public static Selection SELECTION = Selection.depthwise;

    /**
     * Type of move operator.
     */
    public enum Move {
        noMove,
        moveRight,
        moveLeft
    }
    public static Move MOVE = Move.noMove;

    /**
     * Size of the tournament selection.
     */
    public static int TOURNAMENT_SIZE = 2;

    /**
     * Number of mutations
     */
    public static int NUMBER_OF_MUTATIONS = 10;
    public static int MUTATION_RATE_INCREASED_AFTER = 20; // Number of mutations will be increased after each x trials.
    public static int MAX_MUTATIONS = 100;
    public static int NUMBER_OF_LLH_TREE_MUTATIONS = 0;

    /**
     * Probability of constant mutation.
     */
    public static double MUT_CONST_PROB = 0.3;
    public static double LINK_TO_TERMINAL_PROB = 0.7;
    public static double LINK_TO_VARIABLE_PROB = 0.3;

    /**
     * Number of crossovers.
     */
    public static boolean CROSSOVER_ALLOWED = false;
    public static int NUMBER_OF_CROSSOVERS = 5;

    /**
     * Coeficient for multiplication of terminals.
     */
    public static double TERMINAL_MULTIPLICATION_COEFICIENT = 1.0;

    /**
     * Determine if there will provide training or testing
     */
    public enum Action2 {
        Training,
        Testing
    }
    public enum Action {
        SNGP_Train,
        SNGP_Test,
        SNGP_Function_Test,
        SNGP_SA,
        Data_Transformation,
        DT_Test,
        DTs_Test,
        PDTs_Test,
        RF_Test,
        PRF_Test
    }
    public static Action ACTION = Action.SNGP_Train;
    public static Action2 ACTION2 = Action2.Training;
    public static String TEST_INDIVIDUAL_NAME = "Ind_400_3_5_random_noMove_1445422287843.txt";

    /**
     * Discount factor for evaluating actions with the score.
     */
    public static double DISCOUNT_ALPHA = 0.5;

    /**
     * Clustering variables.
     */
    public static int MAX_CLUSTER_ITERATIONS = 100;
    public static double MIN_CLUSTER_MOVE = 0.01;

    /**
     * Printing information.
     */
    public static boolean PRINT_EVOLUTION_INFORMATION = true;
    public static boolean PRINT_WARNINGS = true;
}