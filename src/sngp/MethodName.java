package sngp;

/**
 * Created by Fanda on 3. 9. 2015.
 */
public class MethodName {

    /**
     * Finds and returns name of current method.
     * @return name of the current method.
     */
    public static String getCurrentMethodName() {
        StackTraceElement stackTraceElements[] = (new Throwable()).getStackTrace();
        return stackTraceElements[1].toString();
    }

    /**
     * Finds and returns name of current method.
     * @return name of the current method.
     */
    public static String getCurrentMethodStack() {
        StackTraceElement stackTraceElements[] = (new Throwable()).getStackTrace();
        String s = "";
        // Index 0 is the name of this method.
        for(int i = 1; i < stackTraceElements.length; i++) {
            s += stackTraceElements[i].toString () + "\r\n";
        }
        return s;
    }

    /**
     * Returns name of current class and current line number.
     * @return name of current class and current line number.
     */
    public static String getCurrentMethodAndLine() {
        StackTraceElement stackTraceElements[] = (new Throwable()).getStackTrace();
        return stackTraceElements[1].getMethodName() + ".("+stackTraceElements[1].getFileName().toString() + ":" + stackTraceElements[1].getLineNumber() + ")";
    }



}
