package sngp;

import gp.Population;
import gp.tasks.Problem;
import gp.tasks.cvrp.CVRP;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by Fanda on 04-Dec-15.
 */
public class SNGP_Testing {

    /**
     * Tests the evolved individual loaded from the file.
     */
    public static void SNGP_Testing() {
        for (int i = 0; i < CVRP.INSTANCES.length; i++) {
            Population.newIndividual.executeIndividual1Thread(i);
        }
        for (int i = Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES; i < Settings.POPULATION_SIZE; i++) {
            Population.newIndividual.calcNodeTotalFitness(i);
        }
        Population.newIndividual.calcPopulationFitness();

        writeResultsIntoTheLatexFile(Settings.POPULATION_SIZE - 1);
    }


    /**
     * Creates LaTeX file with the results.
     *
     * @param bestNode
     */
    public static void writeResultsIntoTheLatexFile(int bestNode) {
        try {
            PrintWriter pout = new PrintWriter("Results_" + Settings.TEST_INDIVIDUAL_NAME + ".tex");
            pout.write("\\begin{document}\r\n");
            pout.write("\\section{Results}\r\n");
            pout.write("\\begin{itemize}\r\n");
            pout.write("\\item LLHs used: \r\n");
            pout.write("\\begin{itemize}\r\n");
            for (int a = 0; a < Problem.llhs.length; a++) {
                pout.write("\\item " + Problem.llhs[a].getClass().getSimpleName() + "\r\n");
            }
            pout.write("\\end{itemize}\r\n");
            pout.write("\r\n");
            pout.write("\\item Function nodes:\r\n");
            pout.write("\\begin{itemize}\r\n");
            for (int a = 0; a < Population.doubleFunctions.length; a++) {
                pout.write("\\item " + Population.doubleFunctions[a].getClass().getSimpleName() + "\r\n");
            }
            pout.write("\\end{itemize}\r\n");
            pout.write("\r\n");
            pout.write("\\item Terminal nodes:\r\n");
            pout.write("\\begin{itemize}\r\n");
            for (int a = 0; a < Population.doubleTerminals.length; a++) {
                pout.write("\\item " + Population.doubleTerminals[a].getClass().getSimpleName() + "\r\n");
                if (Population.doubleTerminals[a].getClass().getSimpleName().equals("Constant")) {
                    break;
                }
            }
            pout.write("\\end{itemize}\r\n");
            pout.write("\r\n");
            pout.write("\\item Fitness value: " + Population.newIndividual.nodes[Settings.POPULATION_SIZE - 1].nodeFitness + "\r\n");
            pout.write("\\item Routes length difference (sub zero means that HH created shorter sum of distances: ");

            double distDiff = 0.0;
            for (int i = 0; i < CVRP.INSTANCES.length; i++) {
                distDiff += Population.newIndividual.nodes[Settings.POPULATION_SIZE - 1].nodeInstanceFitness[i] - CVRP.INSTANCES[i].bestHeuristicFitness;
            }
            pout.write(distDiff + "\r\n\r\n");
            pout.write("\\end{itemize}\r\n");

            pout.write("\\begin{table*}[hp]\r\n");
            pout.write("\\centering\r\n");
            pout.write("\\begin{tabular}{c | c | c}\r\n");
            pout.write("Instance & LLH fitness & HH fitness \\\\\r\n");
            double bestLLHFitness;
            String LLHString = "";
            String HHString = "";
            for (int i = 0; i < CVRP.INSTANCES.length; i++) {

                if (i > 0 && i % 50 == 0) {
                    pout.write("\\end{tabular}\r\n");
                    pout.write("\\caption{Results.}\r\n");
                    pout.write("\\label{tab:Results}\r\n");
                    pout.write("\\end{table*}\r\n");

                    pout.write("\\begin{table*}[hp]\r\n");
                    pout.write("\\centering\r\n");
                    pout.write("\\begin{tabular}{c | c | c}\r\n");
                    pout.write("Instance & LLH fitness & HH fitness \\\\\r\n");
                }

                bestLLHFitness = Double.MAX_VALUE;
                for (int a = 0; a < Problem.llhs.length; a++) {
                    if (bestLLHFitness > Problem.llhs[a].instancesFitness[i]) {
                        bestLLHFitness = Problem.llhs[a].instancesFitness[i];
                    }
                }

                LLHString = "" + bestLLHFitness;
                HHString = "" + Population.newIndividual.nodes[bestNode].nodeInstanceFitness[i];
                if (bestLLHFitness < Population.newIndividual.nodes[bestNode].nodeInstanceFitness[i]) {
                    LLHString = "\\textbf{" + bestLLHFitness + "} ";
                }
                if (bestLLHFitness > Population.newIndividual.nodes[bestNode].nodeInstanceFitness[i]) {
                    HHString = "\\textbf{" + Population.newIndividual.nodes[bestNode].nodeInstanceFitness[i] + "} ";
                }
                System.out.println(CVRP.instancesPaths[i] + " & " + LLHString + " & " + HHString);
                pout.write(CVRP.instancesPaths[i] + " & " + LLHString + " & " + HHString + " \\\\\r\n");
            }

            pout.write("\\end{tabular}\r\n");
            pout.write("\\caption{Results.}\r\n");
            pout.write("\\label{tab:Results}\r\n");
            pout.write("\\end{table*}\r\n");
            pout.write("\\end{document}\r\n");
            pout.close();
        } catch (FileNotFoundException FNFE) {
        }
    }
}
