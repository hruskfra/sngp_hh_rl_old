/**
 * Created by Fanda on 3. 9. 2015.
 * <p/>
 * TO DO Smoother and smaller methods for Individual classes.
 */
package sngp;

import gp.Population;
import gp.individuals.BPPIndividual;
import gp.tasks.Problem;
import gp.tasks.bpp.BPP;
import heuristics.Heuristic;
import results.ResultsWriter;
import results.tables.TableHTML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Main class.
 */
public class SNGP_HH_RL {

    /**
     * Opimization problem object.
     */
    public static Problem optimizationProblem = Problem.createProblem();

    /**
     * Threads allowed in the SNGP run.
     */
    public static SNGPThread[] threads;

    /**
     * Number of threads.
     */
    public static int threadsCount = 1;

    /**
     * Number of threads done and waiting for the rest of threads.
     */
    public static volatile int THREADS_DONE = 0;

    /**
     * Name of the file for saving the individual.
     */
    public static String indFileName = "Ind_" + Settings.POPULATION_SIZE + "_" + Settings.NUMBER_OF_MUTATIONS + "_" + Settings.SELECTION.toString() + "_" + Settings.MOVE.toString() + "_" + System.currentTimeMillis() + ".txt";
    public static long timeStamp = System.currentTimeMillis();
    public static String timeStampString;
    public static String resultsDirPath;
    /**
     * Actual number of generations without finding BFS.
     */
    public static int BFS_NOT_FOUND_FOR = 0;
    public static int ACTUAL_ITERATION = 0;

    /**
     * Indicates when algorithm tries to find constant trees. TODO move it to the Settings.java
     */
    public static boolean FIND_CONSTANT_TREES = false;

    /**
     * Original population size for temporally purposes mainly in testing procedures.
     */
    public static int ORIGINAL_POPULATION_SIZE = Settings.POPULATION_SIZE;

    /**
     * Name of the leaded individual. TODO This is only for BPP now.
     */
    public static String LOADED_INDIVIDUAL_NAME = "";

    /**
     * @param args
     */
    public static void main(String[] args) {

        //--- Create time stamp for every evolution run.
        DateFormat df = new SimpleDateFormat("yyddMMHHmmss");
        Date today = Calendar.getInstance().getTime();
        timeStampString = df.format(today);

        //--- Check if directory for results exists and create new one if it is not true.
        if (!new File("RESULTS\\_new\\" + Settings.OPTIMIZATION_PROBLEM.toString() + "_" + timeStampString + "\\").isDirectory()) {
            new File("RESULTS\\_new\\" + Settings.OPTIMIZATION_PROBLEM.toString() + "_" + timeStampString + "\\").mkdir();
            System.out.println("SNGP HH RL: Directory ./RESULTS/_new/" + Settings.OPTIMIZATION_PROBLEM.toString() + "_" + timeStampString + "/ created!");
        }
        resultsDirPath = "RESULTS\\_new\\" + Settings.OPTIMIZATION_PROBLEM.toString() + "_" + timeStampString + "\\";

        /*try {
            LoadedInstance tspinst;

            //tspinst = new LoadedInstance(new File("INSTANCES//TSP//Training//vrp_421_41_1.vrp"));
            tspinst = new LoadedInstance(new File("INSTANCES//TSP//Training//a-n32-k5.vrp"));
            System.out.println(tspinst.getDistanceTable().toString() + "\r\n");
            for(int i = 0; i < tspinst.getDistanceTable().getDistanceMatrix().length; i++) {
                for(int j = 0; j < tspinst.getDistanceTable().getDistanceMatrix().length; j++) {
                    System.out.print(tspinst.getDistanceTable().getDistanceMatrix()[i][j] + " ");
                }
                System.out.println();
            }
            System.out.println();
            System.out.println(Arrays.toString(tspinst.getVehicleRoutingTable().getDemenadsArray()));
        }
        catch(IOException IOE) {
            IOE.printStackTrace();
        }
        System.exit(1); /**/

        /*for(int a = 0; a < args.length; a++) {
            System.out.println("SNGP: Input argument " + a + ": " + args[a]);
        }
        //--- Parse the arguments.
        if(args.length > 0) {
            threadsCount = Integer.parseInt(args[0]);
        }
        if(args.length > 1) {
            LOADED_INDIVIDUAL_NAME = args[1];
        }/**/
        //System.out.println("SNGP: Input arguments: " + Arrays.toString(args));

        //--- Number of threads.
        threadsCount = 2;

        //--- Information output.
        System.out.println("SNGP: Number of processor cores: " + Runtime.getRuntime().availableProcessors());
        System.out.println("SNGP: Number of allocated threads: " + threadsCount);
        System.out.println("SNGP: Population size: " + Settings.POPULATION_SIZE);
        System.out.println("SNGP: Selection: " + Settings.SELECTION.toString());
        System.out.println("SNGP: Move operator: " + Settings.MOVE.toString());

        //--- If testing, reduce the number of experiments to one.
        if (Settings.ACTION2 == Settings.Action2.Testing) {
            Settings.NUMBER_OF_EXPERIMENTS = 1;
        }

        //--- Get the list of all instances and load them.
        optimizationProblem.loadInstances(optimizationProblem.instancesTrainingDir);

        //--- Init genotype.
        Population.initGenotype(false);

        //--- Restart StringBuilder if training phase is enabled.
        // TODO - for what? Move it elsewhere.
        if (Settings.ACTION == Settings.Action.SNGP_Train) {
            SNGP_Evolution.initStatistics();
        }

        //--- Perform several experiments for actual instance.
        for (int e = 0; e < Settings.NUMBER_OF_EXPERIMENTS; e++) {

            //--- Init heuristics and actions, if necessary.
            Problem.llhs = optimizationProblem.initHeuristics();
            Problem.actions = optimizationProblem.initActions();

            //--- Create population.
            Population.initPopulation();

            //--- Execute low level-heuristics.
            Population.newIndividual.executeLowLevelHeuristics(false);

            //--- Load evolved heuristics for BPP problem if it is set.
            if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP) {
                //--- Executes and store results of the low-level heuristics.
                Settings.OPTIMIZATION_PROBLEM = Settings.OptimizationProblem.BPP_GHH;
                Population.initGenotype(false);

                //--- Load and execute test individuals.
                //BPP.loadEvolvedHeuristics();

                //--- Return the optimization problem to BPP problem.
                Settings.OPTIMIZATION_PROBLEM = Settings.OptimizationProblem.BPP;
                //--- Reinit genotype.
                Population.initGenotype(true);



                /*int counter[][] = new int[BPP.evolvedHeuristics.size()][3];
                for (int ind = 0; ind < BPP.evolvedHeuristics.size(); ind++) {
                    counter[ind][0] = counter[ind][1] = counter[ind][2] = 0;
                }
                for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
                    System.out.println("Inst: " + i + " | Inst Name: " + Problem.INSTANCES[i].name + " | Fitness: " + BPP.llhs[0].instancesFitness[i]);
                    for (int ind = 0; ind < BPP.evolvedHeuristics.size(); ind++) {
                        BPP.evolvedHeuristics.get(ind).executeIndividual1Thread(i, BPP.evolvedHeuristics.get(ind).getBestNodeID());
                        System.out.println("Ind: " + ind + " | Inst: " + i + " | Node: " + BPP.evolvedHeuristics.get(ind).getBestNodeID() + " | Fitness: " + BPP.evolvedHeuristics.get(ind).nodes[BPP.evolvedHeuristics.get(ind).getBestNodeID()].nodeInstanceFitness[i]);
                        if (BPP.evolvedHeuristics.get(ind).nodes[BPP.evolvedHeuristics.get(ind).getBestNodeID()].nodeInstanceFitness[i] < BPP.llhs[0].instancesFitness[i]) {
                            counter[ind][0]++;
                        } else if (BPP.evolvedHeuristics.get(ind).nodes[BPP.evolvedHeuristics.get(ind).getBestNodeID()].nodeInstanceFitness[i] == BPP.llhs[0].instancesFitness[i]) {
                            counter[ind][1]++;
                        } else {
                            counter[ind][2]++;
                        }
                    }
                    System.out.println("Instance " + i + " - DONE");

                }
                for (int ind = 0; ind < BPP.evolvedHeuristics.size(); ind++) {
                    System.out.println("Ind " + ind + ": " + counter[ind][0] + " | " + counter[ind][1] + " | " + counter[ind][2]);
                }

                for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
                    for (int ind = 0; ind < BPP.evolvedHeuristics.size(); ind++) {



                        BPP.evolvedHeuristics.get(ind).executeIndividual1Thread(i, BPP.evolvedHeuristics.get(ind).getBestNodeID());
                    }
                }

                //--- Load back BPP individual.
                //Population.bestIndividual.loadIndividual("RESULTS/BPP/Ind_Meta_3B.txt");
                //Population.bestIndividual.printInfixTree(Population.bestIndividual.getBestNode());
                //Population.bestIndividual.saveBestInfixIndividual();

                //--- Create HashMap with instance name as a key and LLH usage counter as value.
                HashMap<String, int[]> llhGroupedUsage = new HashMap();
                int[] llhTotalUsage = new int[Problem.NUMBER_OF_LLHS + Problem.evolvedHeuristics.size()];
                Arrays.fill(llhTotalUsage, 0);
                //--- Execute this individual only on best node.
                for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
                    Population.bestIndividual.executeIndividual1Thread(i, Population.bestIndividual.getBestNodeID());
                    BPPIndividual bppInd = (BPPIndividual) Population.bestIndividual;
                    llhGroupedUsage.put(Problem.INSTANCES[i].name, Arrays.copyOf(bppInd.llhUsageCounter, bppInd.llhUsageCounter.length));
                    ResultsWriter.VOI.addAll(Population.bestIndividual.instanceVOI[i]); //--- Store actual VOI.
                    for(int u = 0; u < llhTotalUsage.length; u++) {
                        llhTotalUsage[u] += bppInd.llhUsageCounter[u];
                    }
                }
                //--- Write total usage of llhs into the file.
                try{
                    PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_LLH_TOTAL_USAGE.txt");
                    pout.write(Arrays.toString(llhTotalUsage));
                    pout.close();
                }catch (FileNotFoundException FNFE) {

                }

                Heuristic.writeGroupedBestPerformanceIntoHTML();
                //Heuristic.writeColoredGroupedBestPerformanceIntoHTML();
                Heuristic.writeNumberOfItemsBestPerformanceIntoHTML();
                Heuristic.writeBinCapacityBestPerformanceIntoHTML();
                Heuristic.writeGroupedHistogramsIntoMatlabAndHTML();

                //--- Write LLH usages into the tables.
                TableHTML tHTML = new TableHTML(BPP.llhs.length + BPP.evolvedHeuristics.size() + 1);
                String titles[] = new String[BPP.llhs.length + BPP.evolvedHeuristics.size() + 1];
                titles[0] = " Instance type";
                for(int llh = 0; llh < BPP.llhs.length; llh++) {
                    titles[llh+1] = BPP.llhs[llh].getName();
                }
                for(int ghh = 0; ghh < BPP.evolvedHeuristics.size(); ghh++) {
                    titles[ghh + BPP.llhs.length+1]  = "GHH_" + ghh;
                }
                tHTML.createHeader(titles);
                ResultsWriter.writeHashMapIntoTheTable("LLH_USAGE", tHTML, llhGroupedUsage);
                ResultsWriter.writeHashMapIntoTheTable("LLH_GROUPED_USAGE", tHTML, llhGroupedUsage);
                ResultsWriter.writeVOIIntoTheFile("LLH_VOI");
                tHTML = new TableHTML(BPP.llhs.length + BPP.evolvedHeuristics.size() + 1);
                tHTML.createHeader(titles);
                ResultsWriter.writeIntegerFitnessIntoTable("LLH_BINS", tHTML);
                tHTML = new TableHTML(BPP.llhs.length + BPP.evolvedHeuristics.size() + 1);
                tHTML.createHeader(titles);
                ResultsWriter.writeDoubleFitnessIntoTable("LLH_FITNESS", tHTML);
                ResultsWriter.writeUsedInstancesIntoFile();
                ResultsWriter.writeIndividualIntoFiles();
                ResultsWriter.writeUsedActionsIntoFile();
                ResultsWriter.writeNodesIntoFile();
                System.exit(1);/**/
            }

            //--- Get best LLH fitness value on all instances.
            optimizationProblem.getBestLowLevelHeuristicFitnessValues();

            //--- Restart population for the new experiment.
            Population.initPopulation();
            //--- Generate new random constants for the new experiment.
            Population.reinitializeConstants();

            //--- One thread version.
            if (threadsCount == 1) {

                //--- Evolution result testing.
                if (Settings.ACTION == Settings.Action.SNGP_Test) {
                    SNGP_Testing.SNGP_Testing();
                }
                //--- Evolution training. TODO loadIndividual for all problems and in better structure.
                if (Settings.ACTION == Settings.Action.SNGP_Train) {
                    if (LOADED_INDIVIDUAL_NAME != "") {
                        Population.bestIndividual.loadIndividual("RESULTS/BPP/" + LOADED_INDIVIDUAL_NAME);
                        Population.newIndividual.loadIndividual("RESULTS/BPP/" + LOADED_INDIVIDUAL_NAME);
                        Population.actualIndividual.loadIndividual("RESULTS/BPP/" + LOADED_INDIVIDUAL_NAME);/**/
                    }
                    SNGP_Evolution.evolve1Thread(e);
                }
            } //--- Multi-thread version. TODO loadIndividual for all problems and in better structure.
            else {
                initThreads();
                if (Settings.ACTION == Settings.Action.SNGP_Train) {
                    if (LOADED_INDIVIDUAL_NAME != "") {
                        /*Population.bestIndividual.loadIndividual("RESULTS/BPP/Ind_Meta_6.txt");
                        Population.newIndividual.loadIndividual("RESULTS/BPP/Ind_Meta_6.txt");
                        Population.actualIndividual.loadIndividual("RESULTS/BPP/Ind_Meta_6.txt");/**/
                        Population.bestIndividual.loadIndividual("RESULTS/BPP/" + LOADED_INDIVIDUAL_NAME);
                        Population.newIndividual.loadIndividual("RESULTS/BPP/" + LOADED_INDIVIDUAL_NAME);
                        Population.actualIndividual.loadIndividual("RESULTS/BPP/" + LOADED_INDIVIDUAL_NAME);/**/
                    }
                    SNGP_Evolution.evolveMultiThread(e);
                }
            }

            //ProgressWriter.writeStatisticsIntoCSVFile();
            //--- Write StringBuilder from training phase into files.
            // TODO even for other problems.
            if (Settings.ACTION == Settings.Action.SNGP_Train) {
                if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.CVRP) {
                    SNGP_Evolution.writeStatisticsIntoFiles();
                }
            }
        }
        System.exit(1);
    }

    /**
     * Synchronized method for increasing value of semaphore after thread is
     * done.
     */
    public static synchronized void increaseThreadsDone() {
        THREADS_DONE++;
    }

    /**
     * Creates array of threads for multi-thread optimization.
     */
    public static void initThreads() {
        threads = new SNGPThread[threadsCount];
        for (int i = 0; i < threadsCount; i++) {
            threads[i] = new SNGPThread(i);
            threads[i].start();
        }
    }
}
