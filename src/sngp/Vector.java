/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sngp;

/**
 *
 * @author Fanda
 */
public class Vector {

    /**
     * Array of vector values.
     */
    public double values[];

    /**
     * Constructor of Vector.
     * @param size size of vector
     */
    public Vector(int size) {
        this.values = new double[size];
        for(int i = 0; i < size; i++) values[i] = 0.0;
    }

    /**
     * Constructor of Vector.
     * @param v values of the vector
     */
    public Vector(double[] v) {
        //values = new double[v.length];
        values = v;
        //for(int i = 0; i < v.length; i++) values[i] = v[i];
    }
    
    /**
     * Constructor of Vector.
     * @param v another Vector object
     */
    public Vector(Vector v) {
        //values = new double[v.length];
        this.values = new double[v.values.length];
        for(int i = 0; i < this.values.length; i++) this.values[i] = v.values[i]; 
        //for(int i = 0; i < v.length; i++) values[i] = v[i];
    }

    /**
     * Clonning method.
     * @return deep clone of the vector
     */
    public Vector clone() {
        Vector v = new Vector(this.values.length);
        v.values = new double[v.values.length];
        for(int i = 0; i < this.values.length; i++) {
            v.values[i] = this.values[i];
        }
        return v;
    }
    /**
     * Sets new values of the vector.
     * @param v new values
     */
    public synchronized void setValues(double[] v) {
        values = v;
    }

    /**
     * Returns actual values of the vector.
     * @return values array
     */
    public synchronized double[] getValues() {
        return values;
    }

    /**
     * Returns this instance.
     * @return this object
     */
    public synchronized Vector getVector() { return this; }

    /**
     * Copy values from Vector3d v to this Vector3d.
     * @param v old vector
     */
    public synchronized void setVector(Vector v) {
        this.values = v.values;
    }

    /**
     * Returns certain value of the vector.
     * @param i index of value
     * @return value of the vector in index i
     */
    public synchronized double get(int i) { 
        // Check array size. 
        try{
            return this.values[i]; 
        }
        catch(IndexOutOfBoundsException IOOBE) {
            IOOBE.printStackTrace();
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Index (" + i + ") out of bounds of array (array length is " + this.values.length +")!");
            System.exit(1);
        }
        
        return this.values[i];
    }


    /**
     * Sets value of the i'th position of the vector.
     * @param i index which should be replaced
     * @param v new value
     */
    public synchronized void set(int i, double v) { 
        // Check array size.
        if(i < 0 || i >= this.values.length) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Index (" + i + ") out of bounds of array (array length is " + this.values.length +")!");
            System.exit(1);
        }
        this.values[i] = v; 
    }

    /**
     * Returns sum of the vector.
     * @return sum of the vector.
     */
    public synchronized double getVectorSum() { 
        double sum = 0.0;
        for(int i = 0; i < this.values.length; i++) {
            sum += this.values[i];
        }
        return sum; 
    }
    
    /**
     * Returns distance between this Vector and Vector v.
     * @param v other Vector
     * @return distance between two vectors.
     */
    public synchronized double getDistance(Vector v) { 
        // Check if vectors have the same length.
        if(v.values.length != this.values.length) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Length of vetors is not same (" + this.values.length + " != " + v.values.length + ")!");
            System.exit(1);
        }
        
        double distance = 0.0;
        for(int i = 0; i < this.values.length; i++) {
            distance += Math.pow(this.values[i] - v.values[i], 2);
        }
        return Math.sqrt(distance);
    }

    /**
     * Returns sum of two vectors.
     * @param v second vector
     * @return addition of two vectors v and w represented in whole new vector object
     */
    public synchronized static Vector addVectors(Vector v, Vector w) {
        // Check if vectors have the same length.
        if(v.values.length != w.values.length) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Length of vetors is not same (" + v.values.length + " != " + w.values.length + ")!");
            System.exit(1);
        }
        
        Vector x = new Vector(v.values.length);
        for(int i = 0; i < v.values.length; i++) {
            x.set(i, v.values[i] + w.values[i]);
        }
        return x;
    }

    /**
     * Returns difference between two vectors (first - second).
     * @param v first vector
     * @param w second vector
     * @return difference of two vectors v and w represented by whole new vector object
     */
    public synchronized static Vector subVectors(Vector v, Vector w) {
        // Check if vectors have the same length.
        if(v.values.length != w.values.length) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Length of vetors is not same (" + v.values.length + " != " + w.values.length + ")!");
            System.exit(1);
        }
        
        Vector x = new Vector(v.values.length);
        for(int i = 0; i < v.values.length; i++) {
            x.set(i, v.values[i] - w.values[i]);
        }
        return x;
    }

    /**
     * Multiplies two vectors of the same size.
     * @param v first vector
     * @param w second vector
     * @return multiplication of two vectors v and w
     */
    public synchronized static double mulVectors(Vector v, Vector w) {
        
        // Check if vectors have the same length.
        if(v.values.length != w.values.length) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Length of vetors is not same (" + v.values.length + " != " + w.values.length + ")!");
            System.exit(1);
        }
        
        double mul = 0.0;
        for(int i = 0; i < v.values.length; i++) {
            mul += v.values[i] * w.values[i];
        }
        return mul;
    }
    
    /**
     * Divides vector by some constant.
     * @param n scalar value
     * @return Vector divided by a scalar value
     */
    public synchronized static Vector divVector(Vector a, double n) {
        Vector b = new Vector(a.values.length);
        
        for(int i = 0; i < a.values.length; i++) {
            b.values[i] = a.values[i] / n;
        }
        return b;
    }

    /**
     * Scalar multiplication.
     * @param a input vector.
     * @param s scalar value.
     * @return Vector s multiplied by scalar value a.
     */
    public synchronized static Vector scalarMultiplication(Vector a, double s) {
        Vector b = new Vector(a.values.length);
        
        for(int i = 0; i < a.values.length; i++) {
            b.values[i] = a.values[i] * s;
        }
        return b;
    }

    /**
     * Returns String representation of the vector.
     * @return String representation of the vector
     */
    @Override
    public String toString() {
        String s = "";
        for(int i = 0; i < this.values.length; i++) {
            s += this.values[i] + " ";
        }
        return s;
    }

    /**
     * True if vector is null vector.
     * @return true if all values are zero
     */
    public synchronized boolean isNullVector() {
        for(int i = 0; i < this.values.length; i++) {
            if(this.values[i] != 0.0) return false;
        }
        return true;
    }

    /**
     * Compares two vectors.
     * @return true if vectors a and b are the same.
     */
    public synchronized static boolean equals(Vector v, Vector w) {
        // Check if vectors have the same length.
        if(v.values.length != w.values.length) {
            return false;
        }
        
        for(int i = 0; i < v.values.length; i++) {
            if(v.values[i] != w.values[i]) return false;
        }
        return true;
    }

    /**
     * Transforms vector to one vector but stores original vector.
     * @param v vector
     * @return one vector of vector v
     */
    public synchronized static Vector getOneVector(Vector v) {
        Vector w = new Vector(v.values.length);
        double sum = v.getVectorSum();
        
        for(int i = 0; i < v.values.length; i++) {
            w.values[i] = v.values[i] / sum;
        }
        return w;
    }

    /**
     * Transforms vector to one vector. Original values are forgotten.
     * @param vec input vector
     * @return one vector
     */
    public synchronized static Vector normalise(Vector vec) {
        
        double sum = vec.getVectorSum();
        for(int i = 0; i < vec.values.length; i++) {
            vec.values[i] = vec.values[i] / sum;
        }
        return vec;
    }

    /**
     * Dot product of two vectors.
     * @param v1 first vector
     * @param v2 second vector
     * @return dot product of vectors v1 and v2
     */
    public synchronized static double dotProduct(Vector v1, Vector v2) {
        
        // Check if vectors have the same length.
        if(v1.values.length != v2.values.length) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Length of vetors is not same (" + v1.values.length + " != " + v2.values.length + ")!");
            System.exit(1);
        }
        
        double dot = 0.0;
        for(int i = 0; i < v1.values.length; i++) {
            dot += v1.values[i] * v2.values[i];
        }
        
        return dot; 
    }

    /**
     * Rounds up all values in the Vector to the specific number of decimal places.
     * @param numDecimals number of decimals after rounding
     */
    public synchronized void roundValues(int numDecimals) {
        int coef = 1;
        for(int i = 0; i < numDecimals; i++) {
            coef *= 10;
        }
        for(int v = 0; v < this.values.length; v++) {
            this.values[v] = Math.round(this.values[v]*coef) / (double)(coef);
        }
    }


}
