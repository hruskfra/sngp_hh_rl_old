package gp;

import sngp.MethodName;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;

/**
 * Created by Fanda on 5. 11. 2015.
 */
public class DataSamples {
    public static int FEATURES = 9;
    public static Sample[] samples;
    public static SampleNearMiss[] samplesNearMiss;
    public static TrainSample[] trainSamples;

    public enum UNDERSAMPLING {
        NearMiss2,
        NearestMajority,
        Random
    }
    public static UNDERSAMPLING UnderSampling = UNDERSAMPLING.NearestMajority;

    public static double minorityResult = -1.0;
    public static double majorityResult = -1.0;
    public static int majoritySamplesCount = 0;
    public static int minoritySamplesCount = 0;
    public static int REFERENCE_SAMPLE = -1;
    public static final int K_NEAR_MISS_COEF = 3;

    public static void loadSamples(String path) {

        File file;
        String line;
        int lineCounter = -1;    // Var for better exporting of bugs in file format.
        String nodes[];
        String s, s0, s1;

        boolean exists = new File(path).exists();

        // File exists?
        if (exists) {
            // Then prepare for loading.
            file = new File(path);
            if (file.isDirectory()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Input file path " + path + " leads only to directory.");
                System.exit(1);

            }

            System.out.println("SAMPLES: Found file " + path + ". Loading samples...");
        } else {
            // Else make a new populations.
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Cannot find file " + path + ".");
            System.exit(1);
            return;
        }


        // Start loading evolution.
        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);

            int numSamples;

            try {
                try {

                    do {
                        lineCounter++;
                        line = br.readLine();

                    } while (!line.isEmpty());
                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        samples = new Sample[lineCounter];

        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);

            int numSamples = lineCounter;
            try {
                try {
                    for (int i = 0; i < lineCounter; i++) {
                        line = br.readLine();
                        StringTokenizer st = new StringTokenizer(line);
                        //s0 = st.nextToken();
                        //if(!s0.equals("0.0")) {
                        samples[i] = new Sample();
                        for (int f = 0; f < FEATURES; f++) {
                            s = st.nextToken();
                            samples[i].features[f] = Double.parseDouble(s);
                        }
                        s = st.nextToken();
                        samples[i].result = Double.parseDouble(s);
                        //}
                    }

                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        System.out.println("SAMPLES: " + "Samples successfully loaded!" + "\033[0m");
    }

    public static void loadRoundedSamples(String path) {

        File file;
        String line;
        int lineCounter = -1;    // Var for better exporting of bugs in file format.
        String nodes[];
        String s, s0, s1;

        boolean exists = new File(path).exists();

        // File exists?
        if (exists) {
            // Then prepare for loading.
            file = new File(path);
            if (file.isDirectory()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Input file path " + path + " leads only to directory.");
                System.exit(1);

            }

            System.out.println("SAMPLES: Found file " + path + ". Loading samples...");
        } else {
            // Else make a new populations.
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Cannot find file " + path + ".");
            System.exit(1);
            return;
        }


        // Start loading evolution.
        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);

            int numSamples;

            try {
                try {

                    do {
                        lineCounter++;
                        line = br.readLine();

                    } while (!line.isEmpty());
                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        samples = new Sample[lineCounter];

        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);

            int numSamples = lineCounter;
            try {
                try {
                    for (int i = 0; i < lineCounter; i++) {
                        line = br.readLine();
                        StringTokenizer st = new StringTokenizer(line);
                        //s0 = st.nextToken();
                        //if(!s0.equals("0.0")) {
                        samples[i] = new Sample();
                        for (int f = 0; f < FEATURES; f++) {
                            s = st.nextToken();
                            samples[i].features[f] = Math.round(Double.parseDouble(s) * 100.0) / 100.0;;
                        }
                        s = st.nextToken();
                        samples[i].result = Double.parseDouble(s);
                        //}
                    }

                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        System.out.println("SAMPLES: " + "Samples successfully loaded!" + "\033[0m");
    }

    public static void reduceSamples(double treshold) {
        Collections.sort(Arrays.asList(samples));
        ArrayList<Sample> shortSamples = new ArrayList();
        StringBuilder parallelCoordsSB = new StringBuilder();
        parallelCoordsSB.append("clear all\r\nclose all\r\n");
        parallelCoordsSB.append("Xaxis = [1 2 3 4 5 6 7 8 9];\r\n");
        parallelCoordsSB.append("figure\r\n");
        StringBuilder featuresSB = new StringBuilder();
        /*featuresSB.append("fNames = [");
        for(int i = 0; i < Population.doubleTerminals.length- Settings.CONSTANTS_SIZE; i++) {
            featuresSB.append("'" + Population.doubleTerminals[i].getClass().getSimpleName() + "' ");
        }
        featuresSB.append("];\r\n");/**/

        boolean sameSamples = false;
        int samplesCount = 0;
        int samplesOnesCount = 0;
        for (int i = 0; i < samples.length; i++) {

            // New sample.
            if (!sameSamples) {
                samplesCount = 1;
                samplesOnesCount = 0;
                Sample temp = new Sample();
                for (int f = 0; f < FEATURES; f++) {
                    temp.features[f] = samples[i].features[f];
                }
                temp.result = samples[i].result;
                shortSamples.add(0, temp);

                if (samples[i].result == 1.0) {
                    samplesOnesCount++;
                }
            } else {
                samplesCount++;
                if (samples[i].result == 1.0) {
                    samplesOnesCount++;
                }
            }
            // Last sample.
            if (i == samples.length - 1) {
                if ((double) samplesOnesCount / (double) samplesCount >= treshold) {
                    shortSamples.get(0).result = 1.0;
                } else {
                    shortSamples.get(0).result = 0.0;
                }
                break;
            }
            // Check if the next sample is the same.
            sameSamples = true;
            for (int f = 0; f < FEATURES; f++) {
                if (samples[i].features[f] != samples[i + 1].features[f]) {
                    sameSamples = false;
                    if ((double) samplesOnesCount / (double) samplesCount >= treshold) {
                        shortSamples.get(0).result = 1.0;
                        parallelCoordsSB.append("f" + shortSamples.size() + " = [");
                        for (int ff = 0; ff < FEATURES; ff++) {
                            parallelCoordsSB.append(shortSamples.get(0).features[ff] + " ");
                        }
                        parallelCoordsSB.append("];\r\n");
                        parallelCoordsSB.append("plot(Xaxis, f" + shortSamples.size() + ", 'b')\r\n");
                        parallelCoordsSB.append("hold on\r\n");
                    } else {
                        shortSamples.get(0).result = 0.0;

                        parallelCoordsSB.append("f" + shortSamples.size() + " = [");
                        for (int ff = 0; ff < FEATURES; ff++) {
                            parallelCoordsSB.append(shortSamples.get(0).features[ff] + " ");
                        }
                        parallelCoordsSB.append("];\r\n");
                        parallelCoordsSB.append("plot(Xaxis, f" + shortSamples.size() + ", 'r')\r\n");
                        parallelCoordsSB.append("hold on\r\n");
                    }
                    break;
                }
            }
        }

        // Scatter plot in matlab.
        for (int f = 0; f < FEATURES; f++) {
            featuresSB.append("f" + f + "true = [");
            for (int s = 0; s < shortSamples.size(); s++) {
                if (shortSamples.get(s).result == 1.0) {
                    featuresSB.append(shortSamples.get(s).features[f] + " ");
                }
            }
            featuresSB.append("];\r\n");
            featuresSB.append("f" + f + "false = [");
            for (int s = 0; s < shortSamples.size(); s++) {
                if (shortSamples.get(s).result == 0.0) {
                    featuresSB.append(shortSamples.get(s).features[f] + " ");
                }
            }
            featuresSB.append("];\r\n");
        }
        for (int f1 = 0; f1 < FEATURES; f1++) {
            for (int f2 = f1 + 1; f2 < FEATURES; f2++) {
                featuresSB.append("figure\r\n");
                featuresSB.append("plot(f" + f1 + "true, f" + f2 + "true,'bo');\r\n");
                featuresSB.append("hold on\r\n");
                featuresSB.append("plot(f" + f1 + "false, f" + f2 + "false,'rx');\r\n");
                featuresSB.append("xlabel('" + Population.doubleTerminals[f1].getClass().getSimpleName() + "')\r\n");
                featuresSB.append("ylabel('" + Population.doubleTerminals[f2].getClass().getSimpleName() + "')\r\n");
            }
        }

        saveSortedSamplesIntoFile("SORTED_VALUES" + (int) (treshold * 100));
        //saveSortedRoundedSamplesIntoFile("SORTED_ROUNDED_VALUES" + (int) (treshold * 100));


        try {
            PrintWriter pout = new PrintWriter("SORTED_SHORT_VALUES_" + (int) (treshold * 100) + ".csv");
            for (int i = shortSamples.size() - 1; i >= 0; i--) {
                if(shortSamples.get(i).features[0] != 0.0) {
                    for (int f = 0; f < FEATURES; f++) {
                        pout.write(shortSamples.get(i).features[f] + " ");
                    }
                    pout.write(shortSamples.get(i).result + "\r\n");
                }
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {

        }

        // Save MatLab parallel coordinates graph.
        try {
            PrintWriter pout = new PrintWriter("SORTED_SHORT_VALUES_PARALLEL_COORDS_" + (int) (treshold * 100) + ".m");
            pout.write(parallelCoordsSB.toString());
            pout.close();
        } catch (FileNotFoundException FNFE) {

        }
        // Save MatLab scatter plots grpahs.
        try {
            PrintWriter pout = new PrintWriter("SORTED_SHORT_VALUES_SCATTER_PLOT_" + (int) (treshold * 100) + ".m");
            pout.write(featuresSB.toString());
            pout.close();
        } catch (FileNotFoundException FNFE) {

        }

        //-- Delete initial samples.
        for(int i = 0; i < shortSamples.size(); i++) {
            if(shortSamples.get(i).features[0] == 0.0) {
                shortSamples.remove(i);
                i--;
            }
        }
        //-- Copy data samples to the slightly different objects.
        samplesNearMiss = new SampleNearMiss[shortSamples.size()];
        int onesCounter = 0;
        int zerosCounter = 0;
        for (int i = 0; i < samplesNearMiss.length; i++) {
            samplesNearMiss[i] = new SampleNearMiss();
            for (int f = 0; f < FEATURES; f++) {
                samplesNearMiss[i].features[f] = shortSamples.get(i).features[f];
            }
            samplesNearMiss[i].result = shortSamples.get(i).result;
            if (samplesNearMiss[i].result == 1.0) onesCounter++;
            if (samplesNearMiss[i].result == 0.0) zerosCounter++;

        }

        if (onesCounter > zerosCounter) {
            majorityResult = 1.0;
            minorityResult = 0.0;
            majoritySamplesCount = onesCounter;
            minoritySamplesCount = zerosCounter;
        }
        if (zerosCounter > onesCounter) {
            majorityResult = 0.0;
            minorityResult = 1.0;
            majoritySamplesCount = zerosCounter;
            minoritySamplesCount = onesCounter;
        }
    }

    public static void saveSortedSamplesIntoFile(String fileName) {
        try {
            PrintWriter pout = new PrintWriter(fileName + ".csv");
            for (int i = 0; i < samples.length; i++) {
                if (samples[i].features[0] != 0.0) {
                    for (int f = 0; f < FEATURES; f++) {
                        pout.write(samples[i].features[f] + " ");
                    }
                    pout.write(samples[i].result + "\r\n");
                }
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {

        }
    }

    public static void saveSortedRoundedSamplesIntoFile(String fileName) {
        try {
            PrintWriter pout = new PrintWriter(fileName + ".csv");
            for (int i = 0; i < samples.length; i++) {
                if (samples[i].features[0] != 0.0) {
                    for (int f = 0; f < FEATURES; f++) {
                        pout.write(Math.round(samples[i].features[f] * 100.0) / 100.0 + " ");
                    }
                    pout.write(samples[i].result + "\r\n");
                }
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {

        }
    }

    public static void createTrainingSet(double treshold, String namePostFix) {

        if(UnderSampling != UNDERSAMPLING.Random) {
            for (int s = 0; s < samplesNearMiss.length; s++) {
                if (samplesNearMiss[s].result == minorityResult) {
                    samplesNearMiss[s].distanceValue = -2.0;
                } else {
                    if (UnderSampling == UNDERSAMPLING.NearMiss2) {
                        samplesNearMiss[s].NearMiss2();
                    }
                    if (UnderSampling == UNDERSAMPLING.NearestMajority) {
                        samplesNearMiss[s].NearestMajority();
                    }
                    System.out.println(MethodName.getCurrentMethodName() + " " + s + "/" + samplesNearMiss.length + " = " + samplesNearMiss[s].distanceValue + "(" + samplesNearMiss[s].result + ")");
                }

            }
        }
        if (UnderSampling == UNDERSAMPLING.Random) {
            SampleNearMiss.randomSelection();
        }

        System.out.println("Majority class: " + DataSamples.majorityResult + " (count: " + DataSamples.majoritySamplesCount + ")");
        System.out.println("Minority class: " + DataSamples.minorityResult + " (count: " + DataSamples.minoritySamplesCount + ")");

        if (DataSamples.majoritySamplesCount == DataSamples.minoritySamplesCount) {
            System.out.println("Data are balanced!");
            return;
        }

        // Create data samples.
        trainSamples = new TrainSample[samplesNearMiss.length];
        for (int s = 0; s < trainSamples.length; s++) {
            trainSamples[s] = new TrainSample();
            for (int f = 0; f < FEATURES; f++) {
                trainSamples[s].features[f] = samplesNearMiss[s].features[f];
            }
            trainSamples[s].result = samplesNearMiss[s].result;
            trainSamples[s].distanceValue = samplesNearMiss[s].distanceValue;
            if (trainSamples[s].result == minorityResult) {
                trainSamples[s].isInTraininingSet = true;
            } else {
                trainSamples[s].isInTraininingSet = false;
            }
        }

        Collections.sort(Arrays.asList(trainSamples));
        int trainSamplesCount = 0;
        for (int s = 0; s < trainSamples.length; s++) {
            if (trainSamples[s].result == minorityResult) {
                continue;
            }
            trainSamples[s].isInTraininingSet = true;
            System.out.println(MethodName.getCurrentMethodName() + " Sample " + s + "(" + trainSamplesCount + "th) added with value " + trainSamples[s].distanceValue);
            trainSamplesCount++;
            if (trainSamplesCount == minoritySamplesCount) {
                break;
            }
        }

        System.out.println(MethodName.getCurrentMethodName() + " " + "Breakpoint1.");
        samples = new Sample[minoritySamplesCount * 2];
        int samplePointer = 0;
        for (int s = 0; s < trainSamples.length; s++) {
            if (trainSamples[s].isInTraininingSet) {
                samples[samplePointer] = new Sample();
                samples[samplePointer].result = trainSamples[s].result;
                for (int f = 0; f < FEATURES; f++) {
                    samples[samplePointer].features[f] = trainSamples[s].features[f];
                }
                samplePointer++;
            }
        }
        System.out.println(MethodName.getCurrentMethodName() + " " + samplePointer);
        Collections.sort(Arrays.asList(samples));
        System.out.println(MethodName.getCurrentMethodName() + " " + "Balanced saving.");
        saveSortedSamplesIntoFile("SORTED_BALANCED_VALUES_" + (int) (treshold * 100) + "_" + UnderSampling.toString() + "_" + namePostFix) ;
        //saveSortedRoundedSamplesIntoFile("SORTED_ROUNDED_BALANCED_VALUES_" + (int) (treshold * 100) + "_" + UnderSampling.toString() + "_" + namePostFix) ;
    }
}

class Sample implements Comparable {

    public double features[] = new double[DataSamples.FEATURES];
    public double result;


    public int compareTo(Object o) {
        Sample s = (Sample) o;
        return compareFeatures(this, s, 0);
    }

    public static int compareFeatures(Sample s1, Sample s2, int index) {
        if (s1.features[index] < s2.features[index]) return -1;
        else if (s1.features[index] > s2.features[index]) return 1;
        else {
            if (index < DataSamples.FEATURES - 1) {
                return Sample.compareFeatures(s1, s2, index + 1);
            } else {
                return 0;
            }
        }
    }
}

/**
 * Data sample which is compared according to the distance form the reference sample.
 */
class SampleNearMiss implements Comparable {

    public double features[] = new double[DataSamples.FEATURES];
    public double result;
    public double distanceFromReferenceSample = -1.0;
    public double distanceValue = -1.0;

    public int compareTo(Object o) {
        SampleNearMiss s = (SampleNearMiss) o;
        if (this.distanceFromReferenceSample < s.distanceFromReferenceSample) return -1;
        if (this.distanceFromReferenceSample > s.distanceFromReferenceSample) return 1;
        return 0;
    }

    /**
     * Selects majority class samples which are nearest to some of the minority samples. Also can be reffered as NearMiss1.
     */
    public void NearestMajority() {
        // Skip majority class samples. Calculate distance only between this (majority) and minority samples.
        double distance;
        for (int i = 0; i < DataSamples.samplesNearMiss.length; i++) {
            // Skip minority class samples.
            if (DataSamples.samplesNearMiss[i].result == DataSamples.majorityResult) {
                continue;
            }
            distance = 0.0;
            for (int f = 0; f < DataSamples.FEATURES; f++) {
                distance += Math.pow(this.features[f] - DataSamples.samplesNearMiss[i].features[f], 2.0);
            }
            distance = Math.sqrt(distance);
            DataSamples.samplesNearMiss[i].distanceFromReferenceSample = distance;
        }
        // Sort samples according to the distance.
        Collections.sort(Arrays.asList(DataSamples.samplesNearMiss));

        //System.out.println(MethodName.getCurrentMethodName() + " " + DataSamples.samplesNearMiss[0].distanceFromReferenceSample + " < " + DataSamples.samplesNearMiss[DataSamples.samplesNearMiss.length - 1].distanceFromReferenceSample);

        // Near-Miss 2
        this.distanceValue = 0.0;
        int samplesPointer = 0;
        int samplesCounter = 0;
        for (int s = 0; s < DataSamples.K_NEAR_MISS_COEF; s++) {
            for (int i = samplesPointer; i < DataSamples.samplesNearMiss.length; i++) {
                // Skip majority class samples.
                if (DataSamples.samplesNearMiss[i].result != DataSamples.minorityResult) {
                    continue;
                } else {
                    this.distanceValue += DataSamples.samplesNearMiss[i].distanceFromReferenceSample;
                    samplesPointer = i + 1;
                    samplesCounter++;
                    break;
                }
            }
        }

        if (samplesCounter != DataSamples.K_NEAR_MISS_COEF) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR: Could not find sufficient samples! (" + samplesCounter + ", " + DataSamples.K_NEAR_MISS_COEF + ")");
            System.exit(1);
        }
        this.distanceValue /= (double) (DataSamples.K_NEAR_MISS_COEF);
    }

    /**
     * NearMiss2 method selects majority samples which have the lowest average distance from the three furthest minority samples.
     */
    public void NearMiss2() {
        // For all samples, calculate distance from this sample.
        double distance;
        for (int i = 0; i < DataSamples.samplesNearMiss.length; i++) {
            // Skip majority class samples. Calculate distance only between this (majority) and minority samples.
            if (DataSamples.samplesNearMiss[i].result == DataSamples.majorityResult) {
                continue;
            }
            distance = 0.0;
            for (int f = 0; f < DataSamples.FEATURES; f++) {
                distance += Math.pow(this.features[f] - DataSamples.samplesNearMiss[i].features[f], 2.0);
            }
            distance = Math.sqrt(distance);
            DataSamples.samplesNearMiss[i].distanceFromReferenceSample = distance;
        }
        // Sort samples according to the distance.
        Collections.sort(Arrays.asList(DataSamples.samplesNearMiss));

        //System.out.println(MethodName.getCurrentMethodName() + " " + DataSamples.samplesNearMiss[0].distanceFromReferenceSample + " < " + DataSamples.samplesNearMiss[DataSamples.samplesNearMiss.length - 1].distanceFromReferenceSample);

        // Near-Miss 2
        this.distanceValue = 0.0;
        int samplesPointer = DataSamples.samplesNearMiss.length - 1;
        int samplesCounter = 0;
        for (int s = 0; s < DataSamples.K_NEAR_MISS_COEF; s++) {
            for (int i = samplesPointer; i >= 0; i--) {
                if (DataSamples.samplesNearMiss[i].result != DataSamples.minorityResult) {
                    continue;
                } else {
                    this.distanceValue += DataSamples.samplesNearMiss[i].distanceFromReferenceSample;
                    samplesPointer = i - 1;
                    samplesCounter++;
                    break;
                }
            }
        }

        if (samplesCounter != DataSamples.K_NEAR_MISS_COEF) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR: Could not find sufficient samples!");
            System.exit(1);
        }
        this.distanceValue /= (double) (DataSamples.K_NEAR_MISS_COEF);
    }

    /**
     * NearMiss3 selects for each minority sample its nearest neighbour.
     */
    public static void randomSelection() {
        for(int i = 0; i < DataSamples.minoritySamplesCount; i++) {
            if(DataSamples.samplesNearMiss[i].result == DataSamples.majorityResult) {
                DataSamples.samplesNearMiss[i].distanceValue = Double.MAX_VALUE;
            }
            else {
                DataSamples.samplesNearMiss[i].distanceValue = 0.0;
            }
        }

        int rnd;
        for(int i = 0; i < DataSamples.minoritySamplesCount; i++) {
            rnd = (int)(Math.random() * DataSamples.samplesNearMiss.length);
            // Skip minority results.
            if(DataSamples.samplesNearMiss[rnd].result == DataSamples.minorityResult) {
                i--;
                continue;
            }
            // Skip already selected results.
            if(DataSamples.samplesNearMiss[rnd].distanceValue < 10.0) {
                i--;
                continue;
            }
            DataSamples.samplesNearMiss[rnd].distanceValue = 1.0;
        }
    }

}

/**
 * Data sample which is compared according to the distance form the reference sample.
 */
class TrainSample implements Comparable {

    public double features[] = new double[DataSamples.FEATURES];
    public double result;
    public double distanceValue = -1.0;
    public boolean isInTraininingSet = true;

    public int compareTo(Object o) {
        TrainSample s = (TrainSample) o;
        if (this.distanceValue < s.distanceValue) return -1;
        if (this.distanceValue > s.distanceValue) return 1;
        return 0;
    }
}
