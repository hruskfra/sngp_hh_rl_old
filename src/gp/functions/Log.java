/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;

import gp.individuals.Individual;
import gp.tasks.Instance;
import sngp.MethodName;
import gp.*;

/**
 * @author Fanda
 */
public class Log extends Node {

    /**
     * Constructor of node.
     */
    public Log() {
        this.name = this.getClass().getSimpleName();
        this.arity = 1;
        this.childrenNodes = new Node[this.arity];
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][1];
        this.argumentTypes[0][0] = Double.class;
    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    public Log clone() {
        return new Log();
    }

    /**
     * Execution method.
     *
     * @return double value of this node.
     */
    public double execute_node() {
        double executionResult = Math.log(childrenNodes[0].execute_node());
        if (Double.isNaN(executionResult)) return 0.0;
        if (Double.isInfinite(executionResult)) return 0.0;
        else return executionResult;
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Individual i) {
        this.nodeValue = Math.log(childrenNodes[0].execute_node(i));
        if (Double.isNaN(this.nodeValue)) this.nodeValue = 0.0;
        if (Double.isInfinite(this.nodeValue)) this.nodeValue = 0.0;
        return this.nodeValue;
    }

    /**
     * Execution method
     * @param i actual executed instance.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Instance i) {
        this.nodeValue = Math.log(childrenNodes[0].execute_node(i));
        if (Double.isNaN(this.nodeValue)) this.nodeValue = 0.0;
        if (Double.isInfinite(this.nodeValue)) this.nodeValue = 0.0;
        return this.nodeValue;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return "Log";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart1() {
        return "Log(";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart2() {
        return ")";
    }
}