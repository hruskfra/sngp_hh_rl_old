/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;

import gp.*;
import gp.individuals.Individual;
import gp.tasks.Instance;

/**
 *
 * @author Fanda
 */
public class Greater extends Node {

    /**
     * Constructor of node.
     */
    public Greater() {
        this.name = this.getClass().getSimpleName();
        this.arity = 2;
        this.childrenNodes = new Node[this.arity];
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][1];
        this.argumentTypes[0][0] = Double.class;
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    public Greater clone() {
        return new Greater();
    }

    /**
     * Execution method.
     * @return double value of this node.
     */
    public double execute_node() {
        if(childrenNodes[0].execute_node() > childrenNodes[1].execute_node()) {
            return 1;
        }
        else {
            return -1;
        }
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Individual i) {
        if(childrenNodes[0].execute_node(i)> childrenNodes[1].execute_node(i)) {
            return 1;
        }
        else {
            return -1;
        }
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Instance i) {
        if(childrenNodes[0].execute_node(i) > childrenNodes[1].execute_node(i)) {
            return 1;
        }
        else {
            return -1;
        }
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return ">";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart1() {
        return "";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart2() {
        return "";
    }
}
