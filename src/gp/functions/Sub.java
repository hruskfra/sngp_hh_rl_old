/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;

import gp.*;
import gp.individuals.Individual;
import gp.tasks.Instance;

/**
 *
 * @author Fanda
 */
public class Sub extends Node{

    /**
     * Constructor of node.
     */
    public Sub() {
        this.name = this.getClass().getSimpleName();
        this.arity = 2;
        this.childrenNodes = new Node[this.arity];
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][2];
        this.argumentTypes[0][0] = Double.class;
        this.argumentTypes[0][1] = Integer.class;
        this.argumentTypes[1][0] = Double.class;
        this.argumentTypes[1][1] = Integer.class;
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    public Sub clone() {
        return new Sub();
    }
    
    /**
     * Execution method.
     * @return double value of this node.
     */
    public double execute_node() {
        return childrenNodes[0].execute_node() - childrenNodes[1].execute_node();
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Individual i) {
        this.nodeValue = childrenNodes[0].execute_node(i) - childrenNodes[1].execute_node(i);
        return this.nodeValue;
    }

    /**
     * Execution method
     * @param i actual executed instance.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Instance i) {
        this.nodeValue = childrenNodes[0].execute_node(i) - childrenNodes[1].execute_node(i);
        return this.nodeValue;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return "-";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart1() {
        return "(";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart2() {
        return ")";
    }

    /**
     * @return middle part of the infix representation.
     */
    @Override
    public String toStringMiddlePart() { return ") - ("; }
}
