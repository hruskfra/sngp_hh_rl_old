/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;

import gp.individuals.Individual;
import gp.tasks.Instance;
import sngp.MethodName;
import gp.*;

/**
 *
 * @author Fanda
 */
public class Max extends Node {

    /**
     * Constructor of node.
     */
    public Max() {
        this.name = this.getClass().getSimpleName();
        this.arity = 2;
        this.childrenNodes = new Node[this.arity];
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][2];
        this.argumentTypes[0][0] = Double.class;
        this.argumentTypes[0][1] = Integer.class;
        this.argumentTypes[1][0] = Double.class;
        this.argumentTypes[1][1] = Integer.class;
    }


    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    public Max clone() {
        return new Max();
    }

    /**
     * Execution method.
     * @return double value of this node.
     */
    public double execute_node() {
        //System.out.println("ABS");
        return Math.max(childrenNodes[0].execute_node(), childrenNodes[1].execute_node());
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Individual i) {
        this.nodeValue = Math.max(childrenNodes[0].execute_node(i), childrenNodes[1].execute_node(i));
        return this.nodeValue;
    }

    /**
     * Execution method
     * @param i actual executed instance.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Instance i) {
        this.nodeValue = Math.max(childrenNodes[0].execute_node(i), childrenNodes[1].execute_node(i));
        return this.nodeValue;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return "Max";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart1() {
        return "Max(";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart2() {
        return ")";
    }

    /**
     * @return middle part of the infix representation.
     */
    @Override
    public String toStringMiddlePart() { return ","; }

}
