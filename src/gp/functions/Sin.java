/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;

import gp.Node;
import gp.individuals.Individual;
import gp.tasks.Instance;

/**
 *
 * @author Fanda
 */
public class Sin extends Node {

    /**
     * Constructor of node.
     */
    public Sin() {
        this.name = this.getClass().getSimpleName();
        this.arity = 1;
        this.childrenNodes = new Node[this.arity];
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][1];
        this.argumentTypes[0][0] = Double.class;
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    public Sin clone() {
        return new Sin();
    }

    /**
     * Execution method.
     * @return double value of this node.
     */
    public double execute_node() {
        return Math.sin(childrenNodes[0].execute_node());
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Individual i) {
        this.nodeValue = Math.sin(childrenNodes[0].execute_node(i));
        return this.nodeValue;
    }

    /**
     * Execution method
     * @param i actual executed instance.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Instance i) {
        this.nodeValue = Math.sin(childrenNodes[0].execute_node(i));
        return this.nodeValue;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return "Sin";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart1() {
        return "Sin(";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart2() {
        return ")";
    }
}
