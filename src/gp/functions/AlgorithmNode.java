/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;

import gp.*;
import gp.individuals.Individual;
import gp.tasks.Instance;

/**
 * @author Fanda
 */
public class AlgorithmNode extends Node {

    /**
     * Constructor of node.
     */
    public AlgorithmNode() {
        this.name = this.getClass().getSimpleName();
        this.arity = 1;
        this.childrenNodes = new Node[this.arity];
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][1];
        this.argumentTypes[0][0] = Double.class;
    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    public AlgorithmNode clone() {
        AlgorithmNode a = new  AlgorithmNode();
        a.name = this.name;
        return a;
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public double execute_node(Individual i) {
        this.nodeValue = this.childrenNodes[0].execute_node(i);
        return this.nodeValue;
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Instance i) {
        this.nodeValue = this.childrenNodes[0].execute_node(i);
        return this.nodeValue;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart1() {
        return this.name + ": ";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart2() {
        return "";
    }
}