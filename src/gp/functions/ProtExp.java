/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;


/**
 *
 * @author Fanda
 */
public class ProtExp {

    /**
     * Constructor of node.
     */
    public ProtExp() {}

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    public ProtExp clone() {
        return new ProtExp();
    }

    /**
     * Execution method.
     * @return double value of this node.
     */
    public static double execute_function(double param) {
        double executionResult = Math.exp(param);
        if(Double.isNaN(executionResult)) return 0.0;
        if(Double.isInfinite(executionResult)) return 0.0;
        else return executionResult;
    }

    /**
     * Execution method.
     * @param i executed individual.
     * @return double value of this node.
     */
    /*public double execute_node(TSPIndividual i) {
        double executionResult = Math.exp(childrenNodes[0].execute_node(i));
        if(Double.isNaN(executionResult)) return 0.0;
        if(Double.isInfinite(executionResult)) return 0.0;
        else return executionResult;
    }/*
    
    /**
     * Execution method.
     * @param i executed town.
     * @param j executed town.
     * @return double value of this node.
     */

    /*public double execute_node(int i, int j) {
        double executionResult = Math.exp(childrenNodes[0].execute_node(i, j));
        if(Double.isNaN(executionResult)) return 0.0;
        if(Double.isInfinite(executionResult)) return 0.0;
        else return executionResult;
    }*/ 
    
    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return "Exp";
    }

    /**
     * @return String used in infix representation.
     */
    public String toStringPart1() {
        return "Exp(";
    }

    /**
     * @return String used in infix representation.
     */
    public String toStringPart2() {
        return ")";
    }
}