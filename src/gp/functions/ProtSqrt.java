/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;

/**
 *
 * @author Fanda
 */
public class ProtSqrt {

    /**
     * Constructor of node.
     */
    public ProtSqrt() {}

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    public ProtSqrt clone() {
        return new ProtSqrt();
    }

    /**
     * Execution method.
     * @return double value of this node.
     */
    public static double execute_function(double param) {
        double result = Math.sqrt(param);
        if(Double.isNaN(result)) return 0.0;
        if(Double.isInfinite(result)) return 0.0;
        return result;
    }

    /**
     * Execution method.
     * @param i executed individual.
     * @return double value of this node.
     */
    /*public double execute_node(TSPIndividual i) {
        double result = Math.sqrt(childrenNodes[0].execute_node(i));
        if(Double.isNaN(result)) return 0.0;
        if(Double.isInfinite(result)) return 0.0;
        return result;
    }*/
    
    /**
     * Execution method.
     * @param i executed town.
     * @param j executed town.
     * @return double value of this node.
     */
    /*@Override
    public double execute_node(int i, int j) {
        //System.out.println("ABS");
        double result = Math.sqrt(childrenNodes[0].execute_node(i, j));
        if(Double.isNaN(result)) return 0.0;
        if(Double.isInfinite(result)) return 0.0;
        return result;
    }*/
    
    /**
     * @return String representation of this node.
     */
    public String toString() {
        return "Sqrt";
    }

    /**
     * @return String used in infix representation.
     */
    public String toStringPart1() {
        return "Sqrt(";
    }

    /**
     * @return String used in infix representation.
     */
    public String toStringPart2() {
        return ")";
    }
}