/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;

import gp.*;
import gp.individuals.Individual;
import gp.tasks.Instance;

/**
 * @author Fanda
 */
public class Div extends Node {

    /**
     * Constructor of node.
     */
    public Div() {
        this.name = this.getClass().getSimpleName();
        this.arity = 2;
        this.childrenNodes = new Node[this.arity];
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][2];
        this.argumentTypes[0][0] = Double.class;
        this.argumentTypes[0][1] = Integer.class;
        this.argumentTypes[1][0] = Double.class;
        this.argumentTypes[1][1] = Integer.class;

    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    public Div clone() {
        return new Div();
    }

    /**
     * Execution method.
     *
     * @return double value of this node.
     */
    public double execute_node() {
        //System.out.println("DIV");
        double result1 = childrenNodes[0].execute_node();
        double result2 = childrenNodes[1].execute_node();
        if (result2 == 0.0) return 1.0;
        return (double) result1 / (double) result2;
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Individual i) {
        double result1 = childrenNodes[0].execute_node(i);
        double result2 = childrenNodes[1].execute_node(i);
        if (result2 == 0.0) this.nodeValue = 1.0;
        else this.nodeValue = result1 / result2;
        return this.nodeValue;
    }

    /**
     * Execution method
     * @param i actual executed instance.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Instance i) {
        double result1 = childrenNodes[0].execute_node(i);
        double result2 = childrenNodes[1].execute_node(i);
        if (result2 == 0.0) this.nodeValue = 1.0;
        else this.nodeValue = result1 / result2;
        return this.nodeValue;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return "/";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart1() {
        return "(";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart2() {
        return ")";
    }

    /**
     * @return middle part of the infix representation.
     */
    @Override
    public String toStringMiddlePart() { return ") / ("; }
}