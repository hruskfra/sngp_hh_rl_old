/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;

import gp.*;
import gp.individuals.Individual;
import gp.tasks.Instance;

/**
 * @author Fanda
 */
public class Pow3 extends Node {

    /**
     * Constructor of node.
     */
    public Pow3() {
        this.name = this.getClass().getSimpleName();
        this.arity = 1;
        this.childrenNodes = new Node[this.arity];
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][1];
        this.argumentTypes[0][0] = Double.class;
    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    public Pow3 clone() {
        return new Pow3();
    }

    /**
     * Execution method.
     *
     * @return double value of this node.
     */
    public double execute_node() {
        double result = childrenNodes[0].execute_node();
        return result * result;
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Individual i) {
        this.nodeValue = Math.pow(childrenNodes[0].execute_node(i), 3.0);
        return this.nodeValue;
    }

    /**
     * Execution method
     * @param i actual executed instance.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Instance i) {
        this.nodeValue = Math.pow(childrenNodes[0].execute_node(i), 3.0);
        return this.nodeValue;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return "Pow3";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart1() {
        return "Pow3(";
    }

    /**
     * @return String used in infix representation.
     */
    @Override
    public String toStringPart2() {
        return ")";
    }
}