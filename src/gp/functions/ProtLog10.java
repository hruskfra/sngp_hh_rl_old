/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.functions;


/**
 *
 * @author Fanda
 */
public class ProtLog10 {

    /**
     * Constructor of node.
     */
    public ProtLog10() {}

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    public ProtLog10 clone() {
        return new ProtLog10();
    }

    /**
     * Execution method.
     * @return double value of this node.
     */
    public static double execute_function(double param) {
        double executionResult = Math.log10(param);
        if(Double.isNaN(executionResult)) return 0.0;
        if(Double.isInfinite(executionResult)) return 0.0;
        else return executionResult;
    }

    
    /**
     * @return String representation of this node.
     */
    public String toString() {
        return "Log10";
    }

    /**
     * @return String used in infix representation.
     */
    public String toStringPart1() {
        return "Log10(";
    }

    /**
     * @return String used in infix representation.
     */
    public String toStringPart2() {
        return ")";
    }
}