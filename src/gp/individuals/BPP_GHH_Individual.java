package gp.individuals;

import gp.Node;
import gp.Population;
import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import gp.terminals.Constant;
import heuristics.Heuristic;
import sngp.MethodName;
import sngp.Settings;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Fanda on 10.6.2016.
 * <p/>
 * Individual for the generative hyper heuristics approach. It contains functions, which evaluates all pairs bin-item and
 * adds that combination with the highest output of that function.
 */
public class BPP_GHH_Individual extends Individual implements Comparable {

    /**
     * Actual instance for execution.
     */
    public BPPInstance instance;

    /**
     * Constructor.
     */
    public BPP_GHH_Individual() {

        Random rnd = new Random();
        int irnd;
        // Create nodes.
        for (int i = 0; i < Population.doubleTerminals.length; i++) {
            this.nodes[i] = Population.doubleTerminals[i].clone();
            this.nodes[i].id = i;
        }
        for (int i = Population.doubleTerminals.length; i < Settings.POPULATION_SIZE; i++) {
            irnd = rnd.nextInt(Population.doubleFunctions.length);
            this.nodes[i] = Population.doubleFunctions[irnd].clone();
            this.nodes[i].id = i;
        }

        // Create references.
        for (int i = Settings.POPULATION_SIZE - 1; i >= 0; i--) {
            for (int a = 0; a < this.nodes[i].arity; a++) {
                irnd = rnd.nextInt(i);
                this.nodes[i].childrenNodes[a] = this.nodes[irnd];
                this.nodes[irnd].parents.add(this.nodes[i]);
            }
        }
        this.startingGeneration = Population.generationsCounter - 1;
    }

    /**
     * Empty constructor.
     *
     * @param b just parameter for different constructor.
     */
    public BPP_GHH_Individual(boolean b) {
        this.startingGeneration = Population.generationsCounter - 1;
    }

    /**
     * Constructor. Loads individual from file.
     *
     * @param path path to the file with individual to load
     */
    public BPP_GHH_Individual(String path) {
        //--- Load tree from the file.
        this.loadIndividual(path);
    }

    /**
     * Executes individual.
     * Packs one bin at a time. Others are closed. First item with the value greater than zero is packed.
     *
     * @param instanceID actual instance to execution
     */
    @Override
    public void executeIndividual1Thread(int instanceID, int node) {

        //--- Restart values.
        this.restartValues(instanceID);

        //--- Load instance and prepare instance.
        this.instance = (BPPInstance) BPP.INSTANCES[instanceID].clone();
        this.instance.resetInstance();
        this.instance.createNewBin(); //--- Allocate new bin in the new instance.

        //--- ArrayList of the free items.
        ArrayList<BPPItem> freeItems = new ArrayList();
        freeItems = this.instance.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(freeItems);

        //--- Add largest item into the first bin.
        this.instance.bins.get(0).addItem(freeItems.get(0));
        freeItems = this.instance.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(freeItems);

        int iteration = 0;
        while (!freeItems.isEmpty()) {
            iteration++;

            int bestItem = -1;
            int bestItem2 = -1;
            //int bestBin = -1;
            //int bestBin2 = -1;
            double bestPositiveValue = Double.POSITIVE_INFINITY;
            double bestNegativeValue = Double.NEGATIVE_INFINITY;
            double actValue;
            boolean placed = false;
            boolean placed2 = false;


            int b = this.instance.currentBin;
            //for(int b = 0; b < this.instance.bins.size(); b++) {
            //--- Find the best pair to be assigned.
            for (int it = 0; it < freeItems.size(); it++) {
                if (this.instance.bins.get(b).canBePlaced(freeItems.get(it))) {
                    this.instance.actualItem = freeItems.get(it);

                    actValue = this.nodes[node].execute_node(this.instance);

                    if (actValue >= 0) {
                        bestPositiveValue = actValue;
                        placed = true;
                        bestItem = it;

                        break;
                    }
                    if (actValue < 0 && bestItem2 == -1) {
                        bestNegativeValue = actValue;
                        placed2 = true;
                        bestItem2 = it;
                    }
                    if (actValue < 0 && actValue > bestNegativeValue) {
                        bestNegativeValue = actValue;
                        placed2 = true;
                        bestItem2 = it;

                    }/**/
                }
            }


            if (!placed && placed2) {
                placed = true;
                bestItem = bestItem2;
            }
            //--- Place the item into the new bin.
            if (!placed) {
                //--- Checking procedure.
                if (freeItems.get(freeItems.size() - 1).weight <= this.instance.bins.get(this.instance.currentBin).freeSpace) {
                    System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Opening bin while there is enough space for some item (item: " + freeItems.get(freeItems.size() - 1).itemID + ", weight: " + freeItems.get(freeItems.size() - 1).weight + ", free space: " + this.instance.bins.get(this.instance.currentBin).freeSpace + ")");
                    this.instance.printSolution();
                    System.out.println();
                    for (int it = 0; it < freeItems.size(); it++) {
                        this.instance.actualItem = freeItems.get(it);
                        actValue = this.nodes[node].execute_node(this.instance);
                        System.out.println("Free Item: " + freeItems.get(it).itemID + ", Weight: " + freeItems.get(it).weight + ", Result: " + actValue);
                    }
                    System.out.println();
                    System.out.println("Node: " + node);
                    this.printIndividual();
                    this.printInfixTree(this.nodes[node]);
                    System.exit(1);
                }
                this.instance.createNewBin();
                this.instance.bins.get(b + 1).addItem(freeItems.get(0));
            } else {
                this.instance.bins.get(b).addItem(freeItems.get(bestItem));
            }

            //--- Get new list of free cities for the next iteration.
            freeItems = this.instance.getUnpackedItems();
            BPP.ITEMS_SORTING = BPP.sorting.decreasing;
            Collections.sort(freeItems);
        }

        //--- Check created solution.
        this.instance.checkSolution();
        //--- Calculate instance fitness of actual node.
        this.calcNodeInstanceFitness(instanceID, node);

    }

    /**
     * Executes original heuristics for all training instances.
     *
     * @param print boolean flag for printing log information
     */
    @Override
    public void executeLowLevelHeuristics(boolean print) {
        //--- Unpacked items.
        ArrayList<BPPItem> fi;
        for (int llh = 0; llh < BPP.llhs.length; llh++) {
            BPP.llhs[llh].instancesFitness = new double[BPP.INSTANCES.length];
            for (int inst = 0; inst < BPP.INSTANCES.length; inst++) {
                BPP.llhs[llh].instancesFitness[inst] = Double.MAX_VALUE;
            }
        }

        //--- For all instances.
        for (int inst = 0; inst < BPP.INSTANCES.length; inst++) {
            //--- For all heuristics
            for (int llh = 0; llh < BPP.llhs.length; llh++) {
                this.instance = (BPPInstance) BPP.INSTANCES[inst].clone();
                this.instance.resetInstance();
                this.instance.createNewBin();
                //--- Execute each llh.
                Heuristic h = BPP.llhs[llh];
                fi = this.instance.getUnpackedItems();
                while (!fi.isEmpty()) {
                    if (h.applyHeuristic(this.instance) == -1) {
                        this.instance.createNewBin();
                    }
                    fi = this.instance.getUnpackedItems();
                }

                h.instancesFitness[inst] = this.instance.bins.size();
                for (int i = 0; i < this.instance.bins.size(); i++) {
                    //--- Check if the bin is not empty.
                    if (this.instance.bins.get(i).freeSpace - this.instance.binCapacity == 0) {
                        h.instancesFitness[inst]--;
                    }
                }
                h.instancesFitness[inst] += ((this.instance.binCapacity - this.instance.bins.get(this.instance.bins.size() - 1).freeSpace) / this.instance.binCapacity) / BPP.INSTANCES.length;

            }

            if (inst % 10.0 == 0) {
                System.out.println(MethodName.getCurrentMethodName() + " " + inst + "/" + BPP.INSTANCES.length + " instances DONE");
            }
        }

        for (int llh = 0; llh < BPP.llhs.length; llh++) {
            int counter = 0;
            int bins = 0;
            int difference = 0;
            for (int i = 0; i < BPP.INSTANCES.length; i += 1) {
                bins += BPP.llhs[llh].instancesFitness[i];
                if (Math.abs(BPP.llhs[llh].instancesFitness[i] - BPP.INSTANCES[i].theoreticalOptimumFitness) <= 1) {
                    counter++;
                } else {
                    difference += Math.abs(BPP.llhs[llh].instancesFitness[i] - (BPP.INSTANCES[i].theoreticalOptimumFitness + 1));
                }
                //System.out.println(MethodName.getCurrentMethodName() + " " + BPP.llhs[llh].instancesFitness[i] + " " +  BPP.INSTANCES[i].theoreticalOptimumFitness);
            }


            System.out.println(MethodName.getCurrentMethodName() + " LLH " + BPP.llhs[llh].getClass().getSimpleName() + " | #BINS: " + bins + " | OPT_SOL: " + counter + " | DIFF: " + difference);

        }/**/
    }

    /**
     * Calculates instance fitness value.
     *
     * @param instanceID actual instance ID
     * @return fitness of the solution obtained on instace
     */
    @Override
    public double calcInstanceFitness(int instanceID) {
        double instFitness = this.instance.bins.size();
        for (int bin = 0; bin < this.instance.bins.size(); bin++) {
            if (this.instance.bins.get(bin).allocatedItems.isEmpty()) {
                System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Instance: + " + instanceID + " | Empty bin " + bin + "created by evolved LLH!");
                instFitness--;
            }
        }
        instFitness += ((this.instance.binCapacity - this.instance.bins.get(this.instance.bins.size() - 1).freeSpace) / this.instance.binCapacity) / BPP.INSTANCES.length;

        return instFitness;
    }

    /**
     * Calculates instance fitness for actual node.
     *
     * @param instanceID actual instance ID
     * @param node       actual id of the node in the individual
     * @return fitness obtained in the actual instace
     */
    @Override
    public double calcNodeInstanceFitness(int instanceID, int node) {
        //--- Number of bins with the part of the last bin.
        this.nodes[node].nodeInstanceFitness[instanceID] = this.calcInstanceFitness(instanceID);
        return this.nodes[node].nodeInstanceFitness[instanceID];/**/
    }

    /**
     * Calc instance fitness for current node.
     *
     * @param node actual node id
     * @return total fitnes value
     */
    @Override
    public double calcNodeTotalFitness(int node) {

        this.nodes[node].nodeFitness = 0.0;
        for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
            this.nodes[node].nodeFitness += this.nodes[node].nodeInstanceFitness[i];
            //this.nodes[node].nodeFitness += (this.nodes[node].nodeInstanceFitness[i] - BPP_GHH.llhs[0].instancesFitness[i]);
            /*if(this.nodes[node].nodeInstanceFitness[i] == BPP.INSTANCES[i].theoreticalOptimumFitness) {
                this.nodes[node].nodeFitness--;
            }/**/
        }
        //System.out.println(MethodName.getCurrentMethodName() + " Node: " + node + " | Node_fitness: " + this.nodes[node].nodeFitness);
        return this.nodes[node].nodeFitness;
    }

    /**
     * Calculates fitness value across all instances for all nodes.
     */
    @Override
    public void calcNodesTotalFitness() {
        for (int node = Population.doubleTerminals.length; node < Settings.POPULATION_SIZE; node++) {
            this.nodes[node].nodeFitness = 0.0;
            for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
                //this.nodes[node].nodeFitness += (this.nodes[node].nodeInstanceFitness[i] - BPP_GHH.llhs[0].instancesFitness[i]);
                this.nodes[node].nodeFitness += this.nodes[node].nodeInstanceFitness[i];
            }
            //System.out.println(MethodName.getCurrentMethodName() + " Node " + node + " | Fitness " + this.nodes[node].nodeFitness);
        }
    }

    /**
     * Calculates and stores fitness value of individual. It is functional value
     * of the best point in simplex.
     *
     * @return fitness value at the end of execution.
     */
    @Override
    public double calcPopulationFitness() {

        this.averageFitness = 0.0;
        this.bestFitness = Double.MAX_VALUE;
        this.worstFitness = -Double.MAX_VALUE;

        for (int n = Population.doubleTerminals.length; n < Settings.POPULATION_SIZE; n++) {
            // System.out.println(MethodName.getCurrentMethodName() + " Node " + n + " | Fitness " + this.nodes[n].nodeFitness);
            this.averageFitness += this.nodes[n].nodeFitness;
            if (this.nodes[n].nodeFitness > this.worstFitness) {
                this.worstFitness = this.nodes[n].nodeFitness;
            }
            if (this.nodes[n].nodeFitness < this.bestFitness) {
                this.bestFitness = this.nodes[n].nodeFitness;
            }
        }

        this.averageFitness /= (double) (Settings.POPULATION_SIZE - Population.doubleTerminals.length);
        return this.averageFitness;
    }

    /**
     * Restarts some values to be fit for new instance.
     *
     * @param instanceID number of instance
     */
    @Override
    public void restartValues(int instanceID) {
        this.instance = (BPPInstance) BPP.INSTANCES[instanceID].clone();
        this.instance.resetInstance();
        this.instance.createNewBin();

        /*for (int i = 0; i < this.nodes.length; i++) {
            this.nodes[i].nodeInstanceFitness[instanceID] = Double.MAX_VALUE;
        }/**/
    }

    /**
     * Clones this individual.
     *
     * @return Deep copy of individual.
     */
    @Override
    public BPP_GHH_Individual clone() {
        // Create new object.
        BPP_GHH_Individual n = new BPP_GHH_Individual(false);
        // Copy graph.
        n.nodes = new Node[this.nodes.length];
        for (int i = 0; i < this.nodes.length; i++) {
            n.nodes[i] = this.nodes[i].clone();
            n.nodes[i].id = i;
            n.nodes[i].name = this.nodes[i].name;
            n.nodes[i].nodeFitness = this.nodes[i].nodeFitness;
            n.nodes[i].isFitnessActual = this.nodes[i].isFitnessActual;
            n.nodes[i].returnsConstant = this.nodes[i].returnsConstant;
            n.nodes[i].fixedNode = this.nodes[i].fixedNode;
            n.nodes[i].opened = this.nodes[i].opened;
            n.nodes[i].nodeInstanceFitness = new double[Problem.NUMBER_OF_INSTANCES];
            for (int inst = 0; inst < Problem.NUMBER_OF_INSTANCES; inst++) {
                n.nodes[i].nodeInstanceFitness[inst] = this.nodes[i].nodeInstanceFitness[inst];
            }
        }
        for (int i = 0; i < this.nodes.length; i++) {
            // Copy references.
            for (int a = 0; a < nodes[i].arity; a++) {
                n.nodes[i].childrenNodes[a] = n.nodes[this.nodes[i].childrenNodes[a].id];
            }
            for (int a = 0; a < nodes[i].parents.size(); a++) {
                n.nodes[i].parents.add(n.nodes[this.nodes[i].parents.get(a).id]);
            }
        }

        // Copy fitness values.
        n.averageFitness = this.averageFitness;
        n.bestFitness = this.bestFitness;
        n.worstFitness = this.worstFitness;

        // Copy other variables.
        n.startingGeneration = this.startingGeneration;
        n.bestNodeID = this.bestNodeID;
        n.EXECUTE_ONLY_BEST_NODE = this.EXECUTE_ONLY_BEST_NODE;
        n.setBestParents();

        n.comment = this.comment;
        n.individualName = this.individualName;

        return n;
    }

    /**
     * Compares two individuals.
     *
     * @param o second individual.
     * @return 1 if this is greater, -1 if second individual is greater, 0 when
     * both are equal.
     */
    @Override
    public int compareTo(Object o) {
        Individual n = (Individual) o;
        if (this.averageFitness < n.averageFitness) {
            return -1;
        }
        if (n.averageFitness < this.averageFitness) {
            return 1;
        }
        return 0;
    }
}
