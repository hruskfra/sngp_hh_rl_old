/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.individuals;

import gp.Node;
import gp.Population;
import gp.functions.Div;
import gp.functions.Sub;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import gp.tasks.Problem;
import gp.terminals.Constant;
import gp.terminals.HeuristicID;
import heuristics.Heuristic;
import sngp.*;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author fanda
 */
public class CVRPIndividual extends Individual implements Comparable {

    /**
     * Best tree of the individual.
     */
    public ArrayList<Node> bestTree = new ArrayList();

    /**
     * Vectors of interest.
     */
    public ArrayList<double[]> vectorsOfInterest = new ArrayList();

    /**
     * Constructor.
     */
    public CVRPIndividual() {

        Random rnd = new Random();
        int irnd;
        //--- Create nodes.
        for (int i = 0; i < Population.doubleTerminals.length; i++) {
            this.nodes[i] = Population.doubleTerminals[i].clone();
            this.nodes[i].id = i;
        }
        for (int i = Population.doubleTerminals.length; i < Settings.POPULATION_SIZE; i++) {
            irnd = rnd.nextInt(Population.doubleFunctions.length);
            this.nodes[i] = Population.doubleFunctions[irnd].clone();
            this.nodes[i].id = i;
        }

        //--- Create references.
        for (int i = Settings.POPULATION_SIZE - 1; i >= 0; i--) {
            for (int a = 0; a < this.nodes[i].arity; a++) {
                irnd = rnd.nextInt(i);
                this.nodes[i].childrenNodes[a] = this.nodes[irnd];
                this.nodes[irnd].parents.add(this.nodes[i]);
            }
        }
        //--- Score for the llhs.
        this.actionScores = new double[Problem.actions.length];
        for (int i = 0; i < this.actionScores.length; i++) {
            this.actionScores[i] = 0.0;
        }

        this.startingGeneration = Population.generationsCounter - 1;
    }

    /**
     * Empty constructor.
     *
     * @param b just parameter for different constructor.
     */
    public CVRPIndividual(boolean b) {

        this.startingGeneration = Population.generationsCounter - 1;

        // Score for the llhs.
        this.actionScores = new double[Problem.actions.length];
        for (int i = 0; i < this.actionScores.length; i++) {
            this.actionScores[i] = 0.0;
        }

    }

    /**
     * Constructor. Loads individual from file.
     *
     * @param path path to file
     */
    public CVRPIndividual(String path) {
        // Generate tree structure.
        this.loadIndividual(path);

        // Score for the llhs.
        this.actionScores = new double[Problem.actions.length];
        for (int i = 0; i < this.actionScores.length; i++) {
            this.actionScores[i] = 0.0;
        }
    }

    /**
     * Saves individual into a file.
     *
     * @param fileName name of the file
     */
    /*public void saveIndividual(String fileName) {
        try {
            PrintWriter pout = new PrintWriter(fileName);
            pout.write("PROBLEM : " + SNGP_HH_RL.optimizationProblem.getClass().getSimpleName() + "\r\n");
            pout.write("NAME : " + this.individualName + "\r\n");
            pout.write("FITNESS : " + this.bestFitness + "\r\n");
            pout.write("BEST_NODE : " + this.getBestNodeID() + "\r\n");
            pout.write("GENERATION : " + this.startingGeneration + "\r\n");
            pout.write("NODES : " + Settings.POPULATION_SIZE + "\r\n");
            pout.write("TERMINALS : " + Population.doubleTerminals.length + "\r\n");
            pout.write("CONSTANTS : " + Settings.CONSTANTS_SIZE + "\r\n");
            pout.write("HEURISTICS : " + Problem.llhs.length + " ");
            pout.write("NODES_SECTION\r\n");
            for (int i = 0; i < Problem.llhs.length; i++) {
                pout.write(CVRP.actions[i].getClass().getSimpleName());
                if (i < CVRP.actions.length - 1) {
                    pout.write(" ");
                }
            }
            pout.write("\r\n");
            pout.write("TRAINING_INSTANCES: " + CVRP.INSTANCES.length + " ");
            for (int i = 0; i < CVRP.INSTANCES.length; i++) {
                pout.write(CVRP.INSTANCES[i].name);
                if (i < CVRP.INSTANCES.length - 1) {
                    pout.write(" ");
                }
            }
            pout.write("\r\n");
            for (int i = 0; i < this.nodes.length; i++) {
                if (!this.nodes[i].name.equals("Constant")) {
                    pout.write(this.nodes[i].id + " | " + this.nodes[i].name + " | ");
                } else {
                    pout.write(this.nodes[i].id + " | " + this.nodes[i].name + "_" + this.nodes[i].value + " | ");
                }
                for (int a = 0; a < this.nodes[i].arity; a++) {
                    pout.write(this.nodes[i].childrenNodes[a].id + " ");
                }
                pout.write("| ");
                for (int a = 0; a < this.nodes[i].parents.size(); a++) {
                    pout.write(this.nodes[i].parents.get(a).id + " ");
                }
                pout.write("| " + this.nodes[i].nodeFitness + "\r\n");
            }
            pout.close();
        } catch (IOException IOE) {
            System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Error while saving individual into file.");
            System.exit(1);
        }
    }/**/

    /**
     * Saves individual into a file.
     *
     * @param fileName name of the file
     */
   /* public void saveBestInfixTree(String fileName) {
        try {
            PrintWriter pout = new PrintWriter(fileName);
            pout.write("FITNESS: " + this.bestFitness + "\r\n");
            pout.write("BEST_NODE: " + this.getBestNode().id + "\r\n");
            pout.write("GENERATION: " + this.startingGeneration + "\r\n");

            pout.write(this.infixTreeToString(this.getBestNode()) + "\r\n");
            pout.close();
        } catch (IOException IOE) {
            System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Error while saving individual into file.");
            System.exit(1);
        }
    }/**/

    /**
     * Loads individual from file.
     *
     * @param path String of file path
     */
    @Override
    public void loadIndividual(String path) {

        File file;
        String line;
        int lineCounter = 0;    // Var for better exporting of bugs in file format.

        boolean exists = new File(path).exists();

        //--- File exists?
        if (exists) {
            // Then prepare for loading.
            file = new File(path);
            if (file.isDirectory()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Input file path " + path + " leads only to directory.");
                System.exit(1);

            }

            System.out.println("INDIVIDUAL: Found file " + path + ". Loading individual...");
        } else {
            //--- Else make a new populations.
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Cannot find file " + path + ".");
            System.exit(1);
            return;
        }

        //--- Start loading evolution.
        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);
            try {
                try {

                    String nodes[];

                    //
                    line = br.readLine();
                    nodes = line.split(" ");
                    this.bestFitness = Double.parseDouble(nodes[1]);

                    line = br.readLine();
                    nodes = line.split(" ");
                    this.bestNodeID = Integer.parseInt(nodes[1]);

                    line = br.readLine();
                    nodes = line.split(" ");
                    this.startingGeneration = Integer.parseInt(nodes[1]);

                    line = br.readLine();
                    nodes = line.split(" ");
                    this.nodes = new Node[Integer.parseInt(nodes[1])];
                    Settings.POPULATION_SIZE = this.nodes.length;

                    line = br.readLine();
                    nodes = line.split(" ");
                    int TERMINALS = Integer.parseInt(nodes[1]);
                    Population.doubleTerminals = new Node[TERMINALS];

                    line = br.readLine();
                    nodes = line.split(" ");
                    Settings.CONSTANTS_SIZE = Integer.parseInt(nodes[1]);

                    line = br.readLine();
                    nodes = line.split(" ");
                    int LLHS = Integer.parseInt(nodes[1]);
                    List<Class<?>> llhClasses = ClassFinder.find("heuristics.cvrp");
                    ArrayList<Heuristic> temp = new ArrayList();
                    try {
                        try {
                            try {
                                try {
                                    for (int i = 0; i < LLHS; i++) {
                                        for (Class c : llhClasses) {
                                            if (c.getSimpleName().equals(nodes[2 + i])) {
                                                temp.add((Heuristic) c.getConstructor().newInstance());
                                                break;
                                            }
                                        }
                                    }
                                } catch (NoSuchMethodException NSME) {
                                }
                            } catch (InstantiationException IE) {
                            }
                        } catch (IllegalAccessException IAE) {
                        }
                    } catch (InvocationTargetException ITE) {
                    }/**/
                    Problem.llhs = new Heuristic[temp.size()];
                    for (int i = 0; i < temp.size(); i++) {
                        Problem.llhs[i] = temp.get(i);
                    }

                    line = br.readLine();

                    // Load classes.
                    List<Class<?>> terminalClasses = ClassFinder.find("gp.terminals");
                    List<Class<?>> functionClasses = ClassFinder.find("gp.functions");

                    //--- Get through all classes and find proper terminals.
                    for (int t = 0; t < TERMINALS; t++) {
                        this.nodes[t] = null;
                        line = br.readLine();
                        nodes = line.split(" ");
                        // Constants are easy to load.
                        if (t >= TERMINALS - Settings.CONSTANTS_SIZE) {
                            this.nodes[t] = new Constant();
                            this.nodes[t].id = t;
                            this.nodes[t].nodeValue = this.nodes[t].value = Double.parseDouble(nodes[2].split("_")[1]);
                            this.nodes[t].nodeFitness = Double.parseDouble(nodes[nodes.length - 1]);
                            Population.doubleTerminals[t] = this.nodes[t].clone();
                            continue;
                        }
                        //--- Non constant terminals.
                        for (Class c : terminalClasses) {
                            if (nodes[2].equals(c.getSimpleName())) {
                                try {
                                    try {
                                        try {
                                            try {
                                                this.nodes[t] = (Node) c.getConstructor().newInstance();
                                                this.nodes[t].id = t;
                                                this.nodes[t].nodeFitness = Double.parseDouble(nodes[nodes.length - 1]);
                                                Population.doubleTerminals[t] = this.nodes[t].clone();
                                            } catch (NoSuchMethodException NSME) {
                                            }
                                        } catch (InstantiationException IE) {
                                        }
                                    } catch (IllegalAccessException IAE) {
                                    }
                                } catch (InvocationTargetException ITE) {
                                }
                                break;
                            }
                        }
                        if (this.nodes[t] == null) {
                            System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Bad function node! (" + nodes[2] + ")");
                            System.exit(1);
                        }
                    }

                    //--- Load function nodes.
                    for (int f = TERMINALS; f < Settings.POPULATION_SIZE; f++) {
                        this.nodes[f] = null;
                        line = br.readLine();
                        nodes = line.split(" ");
                        for (Class c : functionClasses) {
                            if (nodes[2].equals(c.getSimpleName())) {
                                try {
                                    try {
                                        try {
                                            try {
                                                this.nodes[f] = (Node) c.getConstructor().newInstance();
                                                this.nodes[f].id = f;
                                                this.nodes[f].nodeFitness = Double.parseDouble(nodes[nodes.length - 1]);
                                            } catch (NoSuchMethodException NSME) {
                                            }
                                        } catch (InstantiationException IE) {
                                        }
                                    } catch (IllegalAccessException IAE) {
                                    }
                                } catch (InvocationTargetException ITE) {
                                }
                                break;
                            }
                        }
                        if (this.nodes[f] == null) {
                            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Bad function node! (" + nodes[0] + ")");
                            System.exit(1);
                        }
                        for (int ch = 0; ch < this.nodes[f].childrenNodes.length; ch++) {
                            this.nodes[f].childrenNodes[ch] = this.nodes[Integer.parseInt(nodes[4 + ch])];
                            this.nodes[f].childrenNodes[ch].parents.add(this.nodes[f]);
                        }
                    }
                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        this.nodes[this.bestNodeID].isFitnessActual = false;
        System.out.println("SNGP: " + "Population successfully loaded!" + "\033[0m");
    }

    /**
     * Executes tree.There is such different method, because we don't need to
     * know values of all node in the tree, but only one branch is enough for
     * updating simplex. Version with LLHs as actions.
     *
     * @param instanceID actual instance to execution
     * @return index of point which will be used to updating the simplex.
     */
    @Override
    public void executeIndividual1Thread(int instanceID, int node) {

        CVRP.INSTANCE_ID = this.instanceExecutedID = instanceID;
        CVRPInstance cinst = (CVRPInstance) CVRP.INSTANCES[instanceID];

        //--- Restart values.
        cinst.resetInstance();
        for (int i = 0; i < CVRP.actions.length; i++) {
            CVRP.actions[i].restartValues();
        }

        boolean[] algCanPerformStep = new boolean[CVRP.actions.length];
        Arrays.fill(algCanPerformStep, true);

        int impossibleLlhCounter = 0;
        int iteration = 0;

        while (true) {
            //--- None of the llhs can add any city or join the routes.
            if (impossibleLlhCounter == CVRP.actions.length) {
                break;
            }

            iteration++;
            int selectedLLH = -1;
            double bestResult = Double.NEGATIVE_INFINITY;
            double result = 0;

            //--- None of the llhs can add any city or join the routes.
            if (impossibleLlhCounter == CVRP.actions.length) {
                break;
            }

            //--- Evaluate all applicable heuristics.
            for (int llh = 0; llh < CVRP.actions.length; llh++) {
                //--- Skip LLHs that cannot perform any step.
                if (!algCanPerformStep[llh]) {
                    continue;
                }
                //--- Set HeuristicID parameter (indexed from 1!).
                this.nodes[0].value = llh + 1;
                if (this.nodes[0].getClass() != HeuristicID.class) {
                    System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Bad terminal class. expected HeuristicID terminal class!");
                    System.exit(1);
                }

                //--- Evaluate individual with correct HeuristicID parameter.
                result = this.nodes[node].execute_node(cinst);
                //--- Store best value found if it changed.
                if (result > bestResult) {
                    bestResult = result;
                    selectedLLH = llh;
                }
            }

            //--- Apply selected action.
            if (selectedLLH < CVRP.actions.length) {
                if (CVRP.actions[selectedLLH].applyHeuristic(cinst) <= -1) {
                    algCanPerformStep[selectedLLH] = false;
                    impossibleLlhCounter++;
                }
            }
        }

        if (!cinst.checkSolution()) {
            System.exit(-4);
        }

        //--- Calculate fitness of actual node on actual instance.
        this.calcNodeInstanceFitness(instanceID, node);

    }

    /**
     * Executes original heuristics for all training instances.
     *
     * @param print indicates if information will be printed
     */
    @Override
    public void executeLowLevelHeuristics(boolean print) {

        for (int llh = 0; llh < Problem.llhs.length; llh++) {
            //--- Restart heuristic.
            Problem.llhs[llh].restartValues();

            for (int inst = 0; inst < Problem.INSTANCES.length; inst++) {

                //--- Restart instance.
                Problem.INSTANCES[inst].resetInstance();

                //--- Execute LLH on the instance.
                while (Problem.llhs[llh].applyHeuristic(Problem.INSTANCES[inst]) > 0) {
                    continue;
                }
                //--- Calculate fitness value.
                Problem.llhs[llh].instancesFitness[inst] = this.calcInstanceFitness(inst);

                //--- Print information if allowed.
                if (print) {
                    System.out.println(MethodName.getCurrentMethodName() + " - LLH: " + Problem.llhs[llh].getClass().getSimpleName() + " | Fitness: " + Problem.llhs[llh].instancesFitness[inst]);
                }

                Problem.INSTANCES[inst].checkSolution();
            }
        }
    }

    /**
     * Calculates instance fitness value.
     *
     * @param instanceID actual instance ID
     * @return fitness of the solution obtained on instance
     */
    @Override
    public double calcInstanceFitness(int instanceID) {
        CVRPInstance cinst = (CVRPInstance) CVRP.INSTANCES[instanceID];
        double instFitness = 0.0;
        for (ArrayList<Integer> route : cinst.routes) {
            for (int j = 0; j < route.size() - 1; j++) {
                instFitness += cinst.distances[route.get(j)][route.get(j + 1)];
            }
        }

        return instFitness;
    }

    /**
     * Calculates instance fitness obtained by node
     *
     * @param instanceID actual instance ID
     * @param node       actual node ID
     * @return fitness of the node on actual instace
     */
    @Override
    public double calcNodeInstanceFitness(int instanceID, int node) {
        this.nodes[node].nodeInstanceFitness[instanceID] = this.calcInstanceFitness(instanceID);
        return this.nodes[node].nodeInstanceFitness[instanceID];
    }

    /**
     * Calculates instance fitness for current node.
     *
     * @param node actual node for calculating fitness value.
     * @return fitness value of actual node
     */
    @Override
    public double calcNodeTotalFitness(int node) {

        this.nodes[node].nodeFitness = 0.0;
        for (int i = 0; i < CVRP.INSTANCES.length; i++) {
            CVRPInstance cinst = (CVRPInstance) CVRP.INSTANCES[i];

            this.nodes[node].nodeFitness += this.nodes[node].nodeInstanceFitness[i];
        }
        this.nodes[node].nodeFitness /= (double) (CVRP.INSTANCES.length);

        return this.nodes[node].nodeFitness;
    }

    /**
     * Calculates total fitness of all nodes through all instances.
     */
    @Override
    public void calcNodesTotalFitness() {
        for (int node = Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES; node < Settings.POPULATION_SIZE; node++) {
            this.nodes[node].nodeFitness = 0.0;
            for (int i = 0; i < CVRP.INSTANCES.length; i++) {
                this.nodes[node].nodeFitness += this.nodes[node].nodeInstanceFitness[i];
            }
        }
    }

    /**
     * Calculates and stores fitness value of individual. It is functional value
     * of the best point in simplex.
     *
     * @return fitness value at the end of execution.
     */
    @Override
    public double calcPopulationFitness() {

        this.averageFitness = 0.0;
        this.bestFitness = Double.MAX_VALUE;
        this.worstFitness = -Double.MAX_VALUE;

        // for (int n = Population.doubleTerminals.length; n < Settings.POPULATION_SIZE; n++) {
        for (int n = Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES; n < Settings.POPULATION_SIZE; n++) {
            //System.out.println(MethodName.getCurrentMethodName() + " " + this.fitnessValues[i]);
            this.averageFitness += this.nodes[n].nodeFitness;
            if (this.nodes[n].nodeFitness > this.worstFitness) {
                this.worstFitness = this.nodes[n].nodeFitness;
            }
            if (this.nodes[n].nodeFitness < this.bestFitness) {
                this.bestFitness = this.nodes[n].nodeFitness;
            }
        }

        //this.averageFitness /= (double) (this.nodes.length - Population.doubleTerminals.length);
        this.averageFitness /= (double) (Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES);
        return this.averageFitness;
    }

    /**
     * Checks if tree rooted in node returns only constatnt.
     *
     * @param node actual node in the individual
     * @return true if it is constant tree
     */
    public boolean isConstantTree(int node) {

        //--- Constants.
        if (node >= Population.doubleTerminals.length - Settings.CONSTANTS_SIZE && node < Population.doubleTerminals.length) {
            this.nodes[node].returnsConstant = true;
            return true;
        }
        //--- Terminals.
        if (node < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE) {
            this.nodes[node].returnsConstant = false;
            return false;
        }
        //--- x-x and x/x.
        //--- For all functions, check if the subtree is not composed of constant-only subtrees.
        boolean sameTerminals;
        //--- Skip all nodes except Div and Sub.
        if (this.nodes[node].getClass() == Sub.class || this.nodes[node].getClass() == Div.class) {
            //--- Get this case only when the first argument is terminal.
            if (this.nodes[node].childrenNodes[0].arity == 0) {
                sameTerminals = true;
                for (int ch = 0; ch < this.nodes[node].arity - 1; ch++) {
                    if (this.nodes[node].childrenNodes[ch].getClass() != this.nodes[node].childrenNodes[ch + 1].getClass()) {
                        sameTerminals = false;
                    }
                }
                if (sameTerminals) {
                    this.nodes[node].returnsConstant = true;
                    return true;
                }
            }
        }

        //--- Other function nodes or other cases.
        boolean onlyConstants = true;
        for (Node ch : this.nodes[node].childrenNodes) {
            if (!ch.returnsConstant) {
                onlyConstants = false;
                break;
            }
        }

        //--- Set node state after checking it.
        if (onlyConstants) {
            this.nodes[node].returnsConstant = true;
            return true;
        } else {
            this.nodes[node].returnsConstant = false;
            return false;
        }
    }

    /**
     * Find and punish trees which return the same value over time, for example
     * x-x tree will be punished.
     */
    public void getConstantTrees() {

        for (Node n : this.nodes) {
            n.returnsConstant = false;
        }
        ArrayList<Node> constantOnlyTrees = new ArrayList();
        // Add contants into the array list.
        for (int i = Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; i < Population.doubleTerminals.length; i++) {
            constantOnlyTrees.add(this.nodes[i]);
        }
        // For all functions, check if the subtree is not composed of constant-only subtrees.
        boolean sameTerminals;
        for (int i = Population.doubleTerminals.length; i < Settings.POPULATION_SIZE; i++) {
            // Skip all nodes except Div and Sub.
            if (this.nodes[i].getClass() != Sub.class && this.nodes[i].getClass() != Div.class) {
                continue;
            }
            // Skip nodes which child is not a terminal.
            if (this.nodes[i].childrenNodes[0].arity > 0) {
                continue;
            }
            // Skip nodes with one argument.
            if (this.nodes[i].arity == 1) {
                continue;
            }

            sameTerminals = true;
            for (int ch = 0; ch < this.nodes[i].arity - 1; ch++) {
                if (this.nodes[i].childrenNodes[ch].getClass() != this.nodes[i].childrenNodes[ch + 1].getClass()) {
                    sameTerminals = false;
                }
            }
            if (sameTerminals) {
                constantOnlyTrees.add(this.nodes[i]);
            }
        }

        // For all functions, check if the subtree is not composed of constant-only subtrees.
        boolean onlyConstans;
        for (int i = Population.doubleTerminals.length; i < Settings.POPULATION_SIZE; i++) {
            onlyConstans = true;
            // Check if its children are not composed from only constant trees.
            for (Node ch : this.nodes[i].childrenNodes) {
                if (!constantOnlyTrees.contains(ch)) {
                    onlyConstans = false;
                    break;
                }
            }
            if (onlyConstans && !constantOnlyTrees.contains(this.nodes[i])) {
                constantOnlyTrees.add(this.nodes[i]);
            }
        }

        //System.out.println(MethodName.getCurrentMethodName());
        for (Node n : constantOnlyTrees) {
            n.returnsConstant = true;
            //System.out.print(n.id + " " + n.name + " | ");
        }
        //System.out.println();

    }

    /**
     * Restarts some values to be fit for new instance.
     *
     * @param instanceID number of instance
     */
    @Override
    public void restartValues(int instanceID) {
        //--- Restart fitness value.
        for (int node = Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES; node < Settings.POPULATION_SIZE; node++) {
            this.nodes[node].nodeInstanceFitness[instanceID] = Double.MAX_VALUE;
        }
    }

    /**
     * Clones this individual.
     *
     * @return Deep copy of individual.
     */
    @Override
    public CVRPIndividual clone() {
        // Create new object.
        CVRPIndividual n = new CVRPIndividual(false);
        // Copy graph.
        n.nodes = new Node[this.nodes.length];
        for (int i = 0; i < this.nodes.length; i++) {
            n.nodes[i] = this.nodes[i].clone();
            n.nodes[i].id = i;
            n.nodes[i].name = this.nodes[i].name;
            n.nodes[i].nodeFitness = this.nodes[i].nodeFitness;
            n.nodes[i].isFitnessActual = this.nodes[i].isFitnessActual;
            n.nodes[i].returnsConstant = this.nodes[i].returnsConstant;
            n.nodes[i].fixedNode = this.nodes[i].fixedNode;
            n.nodes[i].opened = this.nodes[i].opened;
            for (int inst = 0; inst < CVRP.INSTANCES.length; inst++) {
                n.nodes[i].nodeInstanceFitness[inst] = this.nodes[i].nodeInstanceFitness[inst];
            }
        }
        for (int i = 0; i < this.nodes.length; i++) {
            // Copy references.
            for (int a = 0; a < nodes[i].arity; a++) {
                n.nodes[i].childrenNodes[a] = n.nodes[this.nodes[i].childrenNodes[a].id];
            }
            for (int a = 0; a < nodes[i].parents.size(); a++) {
                n.nodes[i].parents.add(n.nodes[this.nodes[i].parents.get(a).id]);
            }
        }

        //--- Copy fitness values.
        n.averageFitness = this.averageFitness;
        n.bestFitness = this.bestFitness;
        n.worstFitness = this.worstFitness;

        //--- Copy other variables.
        n.startingGeneration = this.startingGeneration;
        n.bestNodeID = n.getBestNodeID();
        n.setBestParents();

        n.comment = this.comment;
        n.individualName = this.individualName;

        return n;
    }

    /**
     * Compares two individuals.
     *
     * @param o second individual.
     * @return 1 if this is greater, -1 if second individual is greater, 0 when
     * both are equal.
     */
    @Override
    public int compareTo(Object o) {
        CVRPIndividual n = (CVRPIndividual) o;
        if (this.averageFitness < n.averageFitness) {
            return -1;
        }
        if (n.averageFitness < this.averageFitness) {
            return 1;
        }
        return 0;
    }
}
