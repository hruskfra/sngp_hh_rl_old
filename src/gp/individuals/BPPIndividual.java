package gp.individuals;

import gp.Node;
import gp.Population;
import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import gp.terminals.Constant;
import gp.terminals.HeuristicID;
import heuristics.Heuristic;
import loaders.LoadedFile;
import sngp.*;
import sngp.Vector;

import java.io.*;
import java.util.*;

/**
 * Created by Fanda on 11. 3. 2016.
 */
public class BPPIndividual extends Individual implements Comparable {

    /**
     * Actual instance for execution.
     */
    public BPPInstance instance;

    /**
     * Counter of llh usage.
     */
    public int llhUsageCounter[];

    /**
     * Constructor.
     */
    public BPPIndividual() {

        Random rnd = new Random();
        int irnd;
        //--- Create nodes.
        for (int i = 0; i < Population.doubleTerminals.length; i++) {
            this.nodes[i] = Population.doubleTerminals[i].clone();
            this.nodes[i].id = i;
        }
        for (int i = Population.doubleTerminals.length; i < Settings.POPULATION_SIZE - BPP.actions.length - BPP.evolvedHeuristics.size(); i++) {
            irnd = rnd.nextInt(Population.doubleFunctions.length);
            this.nodes[i] = Population.doubleFunctions[irnd].clone();
            this.nodes[i].id = i;
        }
        //--- Last nodes will be for individuals.
        for (int i = Settings.POPULATION_SIZE - BPP.actions.length - BPP.evolvedHeuristics.size(); i < Settings.POPULATION_SIZE; i++) {
            irnd = rnd.nextInt(Population.doubleFunctions.length);
            this.nodes[i] = Population.doubleFunctions[irnd].clone();
            this.nodes[i].id = i;
        }

        //--- Create references.
        for (int i = Settings.POPULATION_SIZE - 1; i >= 0; i--) {
            for (int a = 0; a < this.nodes[i].arity; a++) {
                irnd = rnd.nextInt(i);
                this.nodes[i].childrenNodes[a] = this.nodes[irnd];
                this.nodes[irnd].parents.add(this.nodes[i]);
            }
        }
        //--- Score for the llhs.
        this.actionScores = new double[BPP.actions.length + BPP.evolvedHeuristics.size()];
        for (int i = 0; i < this.actionScores.length; i++) {
            this.actionScores[i] = 0.0;
        }

        this.startingGeneration = Population.generationsCounter - 1;
    }

    /**
     * Empty constructor.
     *
     * @param b just parameter for different constructor.
     */
    public BPPIndividual(boolean b) {
        this.startingGeneration = Population.generationsCounter - 1;

        // Score for the llhs.
        this.actionScores = new double[Problem.actions.length];
        for (int i = 0; i < this.actionScores.length; i++) {
            this.actionScores[i] = 0.0;
        }

    }

    /**
     * Constructor. Loads individual from file.
     *
     * @param path path to file
     */
    public BPPIndividual(String path) {
        // Generate tree structure.
        this.loadIndividual(path);

        // Score for the llhs.
        this.actionScores = new double[Problem.actions.length];
        for (int i = 0; i < this.actionScores.length; i++) {
            this.actionScores[i] = 0.0;
        }
    }

    /**
     * Execution method.
     *
     * @param instanceID ID of the actual instance
     */
    public void executeIndividual1Thread1LLH(int instanceID, int node) {

        //--- Restart arrays for statistics.
        llhUsageCounter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size()];
        Arrays.fill(llhUsageCounter, 0);
        this.instanceVOI[instanceID] = new ArrayList();

        //--- Restart individual.
        this.restartValues(instanceID);

        //--- Load instance.
        this.instance = (BPPInstance) BPP.INSTANCES[instanceID].clone();
        this.instance.resetInstance();
        this.instance.createNewBin();   //--- Allocate new bin in the new instance.
        //--- ArrayList of the free items.
        ArrayList<BPPItem> freeItems = new ArrayList();

        //--- Actual iteration.
        double result;
        double bestResult;
        int selectedLLH = -1;

        freeItems = this.instance.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(freeItems);
        this.instance.bins.get(0).addItem(freeItems.get(0));
        freeItems = this.instance.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(freeItems);

        //--- Solution construction cycle.
        while (!freeItems.isEmpty()) {

            //--- Heuristic selection part.
            bestResult = -Double.MAX_VALUE;
            for (int llh = 0; llh < BPP.actions.length + BPP.evolvedHeuristics.size(); llh++) {
                this.nodes[0].value = llh + 1;
                if (this.nodes[0].getClass() != HeuristicID.class) {
                    System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Bad terminal class. expected HeuristicID terminal class!");
                    System.exit(1);
                }

                result = this.nodes[node].execute_node(this.instance);
                if (result > bestResult) {
                    bestResult = result;
                    selectedLLH = llh;
                }
            }

            //--- Store vector of interest for this instance.
            this.nodes[0].value = selectedLLH + 1;
            int indexVOI = this.instanceVOI[instanceID].size();
            this.instanceVOI[instanceID].add(indexVOI, this.createVOI());

            //--- Increase counter of LLH used during execution.
            llhUsageCounter[selectedLLH]++;

            //--- If selected LLH is one from human-made LLHs...
            if (selectedLLH < BPP.actions.length) {


                if (BPP.actions[selectedLLH].applyHeuristic(this.instance) == -1) {
                    //--- Checking procedure.
                    if (freeItems.get(freeItems.size() - 1).weight <= this.instance.bins.get(this.instance.currentBin).freeSpace) {
                        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Opening bin while there is enough space for some item (weight: " + freeItems.get(freeItems.size() - 1).weight + ", free space: " + this.instance.bins.get(this.instance.currentBin).freeSpace + ")");
                        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Heuristic that caused problem has ID = " + selectedLLH + ".");

                        System.exit(1);
                    }
                    this.instance.createNewBin();
                    this.instance.bins.get(this.instance.currentBin).addItem(freeItems.get(0));

                    //--- Remove VOID from the list, we don't want VOI of manually created step.
                    this.instanceVOI[instanceID].remove(indexVOI);
                }
            } //--- Else selected LLH if the one from GHH process.
            else {
                //System.out.println(MethodName.getCurrentMethodName() + " " + results[0].llhID + ", actions  " + BPP.actions.length+ ", result " + (results[0].llhID - BPP.actions.length) + ", fi " + freeItems.size());
                int bestItem = -1;
                int bestItem2 = -1;
                double bestPositiveValue = Double.MAX_VALUE;
                double bestNegativeValue = -Double.MAX_VALUE;
                double actValue;
                boolean placed = false;
                boolean placed2 = false;

                int b = this.instance.currentBin;
                //--- Find the best pair to be assigned.
                for (int it = 0; it < freeItems.size(); it++) {
                    if (this.instance.bins.get(b).canBePlaced(freeItems.get(it))) {
                        this.instance.actualItem = freeItems.get(it);

                        actValue = BPP.evolvedHeuristics.get(selectedLLH - BPP.actions.length).nodes[BPP.evolvedHeuristics.get(selectedLLH - BPP.actions.length).getBestNodeID()].execute_node(this.instance);

                        //--- First-fit mechanism. If returned result is greater then zero, add item to the bin.
                        if (actValue >= 0) {
                            bestPositiveValue = actValue;
                            placed = true;
                            bestItem = it;
                            break;
                        }
                        //--- First below zero value found.
                        if (actValue < 0 && bestItem2 == -1) {
                            bestNegativeValue = actValue;
                            placed2 = true;
                            bestItem2 = it;
                        }
                        //--- Second and others values below zero.
                        if (actValue < 0 && actValue > bestNegativeValue) {
                            bestNegativeValue = actValue;
                            placed2 = true;
                            bestItem2 = it;
                        }/**/
                    }

                }

                if (!placed && placed2) {
                    placed = true;
                    bestItem = bestItem2;
                }
                if (placed) {
                    //--- Place the bin.
                    this.instance.bins.get(b).addItem(freeItems.get(bestItem));
                } //--- Place the item into the new bin.
                else {
                    //--- Checking procedure.
                    if (freeItems.get(freeItems.size() - 1).weight <= this.instance.bins.get(this.instance.currentBin).freeSpace) {
                        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Opening bin while there is enough space for some item (weight: " + freeItems.get(freeItems.size() - 1).weight + ", free space: " + this.instance.bins.get(this.instance.currentBin).freeSpace + ")");
                        System.out.println("Best Item 1 (" + placed + "): " + bestItem + "(" + freeItems.get(bestItem).weight + ") | Best Item 2 (" + placed2 + "): " + bestItem2 + "(" + freeItems.get(bestItem2).weight + ")");
                        this.instance.printSolution();
                        System.out.println();
                        for (int it = 0; it < freeItems.size(); it++) {
                            actValue = this.nodes[node].execute_node(this.instance);
                            System.out.println("Free Item: " + freeItems.get(it).itemID + ", Weight: " + freeItems.get(it).weight + ", Result: " + actValue);
                        }
                        System.exit(1);
                    }
                    this.instance.createNewBin();
                    this.instance.bins.get(b + 1).addItem(freeItems.get(0));

                    //--- Remove VOID from the list, we don't want VOI of manually created step.
                    this.instanceVOI[instanceID].remove(indexVOI);
                }
            }

            //--- Get new list of free cities for the next iteration.
            freeItems = this.instance.getUnpackedItems();
            BPP.ITEMS_SORTING = BPP.sorting.decreasing;
            Collections.sort(freeItems);
        }

        //--- Check created solution.
        this.instance.checkSolution();
        //--- Calc instance fitness for nodes.
        this.calcNodeInstanceFitness(instanceID, node);

    }

    /**
     * Execution method working with more bins.
     *
     * @param instanceID ID of the actual instance
     */
    @Override
    public void executeIndividual1Thread(int instanceID, int node) {

        //System.out.println("\r\n*****************");
        //--- Restart arrays for statistics.
        llhUsageCounter = new int[BPP.llhs.length + BPP.evolvedHeuristics.size()];
        Arrays.fill(llhUsageCounter, 0);
        this.instanceVOI[instanceID] = new ArrayList();

        //--- Restart individual.
        this.restartValues(instanceID);

        //--- Load instance.
        this.instance = (BPPInstance) BPP.INSTANCES[instanceID].clone();
        this.instance.resetInstance();
        this.instance.createNewBin();   //--- Allocate new bin in the new instance.
        //--- ArrayList of the free items.
        ArrayList<BPPItem> freeItems = new ArrayList();

        //--- Actual iteration.
        double result;
        double bestResult;
        int selectedLLH = -1;

        freeItems = this.instance.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(freeItems);
        this.instance.bins.get(0).addItem(freeItems.get(0));
        freeItems = this.instance.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(freeItems);

        //--- Solution construction cycle.
        while (!freeItems.isEmpty()) {

            //--- Heuristic selection part.
            bestResult = -Double.MAX_VALUE;
            for (int llh = 0; llh < BPP.actions.length + BPP.evolvedHeuristics.size(); llh++) {
                this.nodes[0].value = llh + 1;
                if (this.nodes[0].getClass() != HeuristicID.class) {
                    System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Bad terminal class. expected HeuristicID terminal class!");
                    System.exit(1);
                }

                result = this.nodes[node].execute_node(this.instance);
                if (result > bestResult) {
                    bestResult = result;
                    selectedLLH = llh;
                }
            }

            //--- Store vector of interest for this instance.
            this.nodes[0].value = selectedLLH + 1;
            int indexVOI = this.instanceVOI[instanceID].size();
            this.instanceVOI[instanceID].add(indexVOI, this.createVOI());

            //--- Increase counter of LLH used during execution.
            llhUsageCounter[selectedLLH]++;

            //--- If selected LLH is one from human-made LLHs...
            if (selectedLLH < BPP.actions.length) {

                //System.out.print(selectedLLH + " ");
                if (BPP.actions[selectedLLH].applyHeuristic(this.instance) == -1) {
                    //--- Checking procedure.
                    /*if (freeItems.get(freeItems.size() - 1).weight <= this.instance.bins.get(this.instance.currentBin).freeSpace) {
                        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Opening bin while there is enough space for some item (weight: " + freeItems.get(freeItems.size() - 1).weight + ", free space: " + this.instance.bins.get(this.instance.currentBin).freeSpace + ")");
                        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Heuristic that caused problem has ID = " + selectedLLH + ".");

                        System.exit(1);
                    }/**/
                    this.instance.createNewBin();
                    this.instance.bins.get(this.instance.currentBin).addItem(freeItems.get(0));

                    //--- Remove VOID from the list, we don't want VOI of manually created step.
                    this.instanceVOI[instanceID].remove(indexVOI);
                }
            } //--- Else selected LLH if the one from GHH process.
            else {
                //System.out.print(" ERR");
                //System.out.println(MethodName.getCurrentMethodName() + " " + results[0].llhID + ", actions  " + BPP.actions.length+ ", result " + (results[0].llhID - BPP.actions.length) + ", fi " + freeItems.size());
                int bestItem = -1;
                int bestItem2 = -1;
                double bestPositiveValue = Double.MAX_VALUE;
                double bestNegativeValue = -Double.MAX_VALUE;
                double actValue;
                boolean placed = false;
                boolean placed2 = false;

                int b = this.instance.currentBin;
                //--- Find the best pair to be assigned.
                for (int it = 0; it < freeItems.size(); it++) {
                    if (this.instance.bins.get(b).canBePlaced(freeItems.get(it))) {
                        this.instance.actualItem = freeItems.get(it);

                        actValue = BPP.evolvedHeuristics.get(selectedLLH - BPP.actions.length).nodes[BPP.evolvedHeuristics.get(selectedLLH - BPP.actions.length).getBestNodeID()].execute_node(this.instance);

                        //--- First-fit mechanism. If returned result is greater then zero, add item to the bin.
                        if (actValue >= 0) {
                            bestPositiveValue = actValue;
                            placed = true;
                            bestItem = it;
                            break;
                        }
                        //--- First below zero value found.
                        if (actValue < 0 && bestItem2 == -1) {
                            bestNegativeValue = actValue;
                            placed2 = true;
                            bestItem2 = it;
                        }
                        //--- Second and others values below zero.
                        if (actValue < 0 && actValue > bestNegativeValue) {
                            bestNegativeValue = actValue;
                            placed2 = true;
                            bestItem2 = it;
                        }/**/
                    }

                }

                if (!placed && placed2) {
                    placed = true;
                    bestItem = bestItem2;
                }
                if (placed) {
                    //--- Place the bin.
                    this.instance.bins.get(b).addItem(freeItems.get(bestItem));
                } //--- Place the item into the new bin.
                else {
                    //--- Checking procedure.
                    if (freeItems.get(freeItems.size() - 1).weight <= this.instance.bins.get(this.instance.currentBin).freeSpace) {
                        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Opening bin while there is enough space for some item (weight: " + freeItems.get(freeItems.size() - 1).weight + ", free space: " + this.instance.bins.get(this.instance.currentBin).freeSpace + ")");
                        System.out.println("Best Item 1 (" + placed + "): " + bestItem + "(" + freeItems.get(bestItem).weight + ") | Best Item 2 (" + placed2 + "): " + bestItem2 + "(" + freeItems.get(bestItem2).weight + ")");
                        this.instance.printSolution();
                        System.out.println();
                        for (int it = 0; it < freeItems.size(); it++) {
                            actValue = this.nodes[node].execute_node(this.instance);
                            System.out.println("Free Item: " + freeItems.get(it).itemID + ", Weight: " + freeItems.get(it).weight + ", Result: " + actValue);
                        }
                        System.exit(1);
                    }
                    this.instance.createNewBin();
                    this.instance.bins.get(b + 1).addItem(freeItems.get(0));

                    //--- Remove VOID from the list, we don't want VOI of manually created step.
                    this.instanceVOI[instanceID].remove(indexVOI);
                }
            }

            //--- Get new list of free cities for the next iteration.
            freeItems = this.instance.getUnpackedItems();
            BPP.ITEMS_SORTING = BPP.sorting.decreasing;
            Collections.sort(freeItems);
        }

        //--- Check created solution.
        this.instance.checkSolution();
        //--- Calc instance fitness for nodes.
        this.calcNodeInstanceFitness(instanceID, node);

    }

    /**
     * Executes original heuristics for all training instances.
     *
     * @param print indicates if information will be printed
     */
    @Override
    public void executeLowLevelHeuristics(boolean print) {
        //--- Unpacked intems.
        ArrayList<BPPItem> fi;
        for (int llh = 0; llh < BPP.llhs.length; llh++) {
            BPP.llhs[llh].instancesFitness = new double[BPP.INSTANCES.length];
            for (int inst = 0; inst < BPP.INSTANCES.length; inst++) {
                BPP.llhs[llh].instancesFitness[inst] = Double.MAX_VALUE;
            }
        }

        //--- For all instances.
        for (int inst = 0; inst < BPP.INSTANCES.length; inst++) {
            //--- For all heuristics
            for (int llh = 0; llh < BPP.llhs.length; llh++) {
                this.instance = (BPPInstance) BPP.INSTANCES[inst].clone();
                this.instance.resetInstance();
                this.instance.createNewBin();

                //--- Store actual LLH.
                Heuristic h = BPP.llhs[llh];

                //--- Execute actual LLH.
                fi = this.instance.getUnpackedItems();
                while (!fi.isEmpty()) {
                    if (h.applyHeuristic(this.instance) == -1) {
                        this.instance.createNewBin();
                    }
                    fi = this.instance.getUnpackedItems();
                }

                //--- Calculate heuristic fitness.
                h.instancesFitness[inst] = this.instance.bins.size();
                for (int i = 0; i < this.instance.bins.size(); i++) {
                    if (Math.abs(this.instance.bins.get(i).freeSpace - this.instance.binCapacity) <= 0) {
                        h.instancesFitness[inst]--;
                    }
                }
                h.instancesFitness[inst] += ((this.instance.binCapacity - this.instance.bins.get(this.instance.bins.size() - 1).freeSpace) / this.instance.binCapacity) / BPP.INSTANCES.length;

                //--- Print fitness information if allowed.
                if (print) {
                    System.out.println(h.getClass().getSimpleName() + " fitness: " + h.instancesFitness[inst]);
                }
            }
        }

        for (int llh = 0; llh < BPP.llhs.length; llh++) {
            int counter = 0;
            int difference = 0;
            for (int i = 0; i < BPP.INSTANCES.length; i += 1) {
                if (Math.abs(BPP.llhs[llh].instancesFitness[i] - BPP.INSTANCES[i].theoreticalOptimumFitness) <= 0) {
                    counter++;
                }
                difference += Math.abs(BPP.llhs[llh].instancesFitness[i] - BPP.INSTANCES[i].theoreticalOptimumFitness);
            }
        }/**/

    }

    /**
     * Calculates instance fitness value.
     *
     * @param instanceID actual instance ID
     * @return fitness of the solution obtained on instace
     */
    @Override
    public double calcInstanceFitness(int instanceID) {
        double instFitness = this.instance.bins.size();
        for (int bin = 0; bin < this.instance.bins.size(); bin++) {
            if (this.instance.bins.get(bin).allocatedItems.isEmpty()) {
                System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Instance: + " + instanceID + " | Empty bin " + bin + "created by evolved LLH!");
                instFitness--;
            }
        }
        instFitness += ((this.instance.binCapacity - this.instance.bins.get(this.instance.bins.size() - 1).freeSpace) / this.instance.binCapacity) / BPP.INSTANCES.length;

        return instFitness;
    }

    /**
     * Check if simplex has better optima.
     *
     * @param instanceID actual instance ID
     * @param node       index of actual node
     * @return fitness value for actual node and instnace
     */
    @Override
    public double calcNodeInstanceFitness(int instanceID, int node) {
        this.nodes[node].nodeInstanceFitness[instanceID] = this.calcInstanceFitness(instanceID);
        return this.nodes[node].nodeInstanceFitness[instanceID];
    }

    /**
     * Calc instance fitness for current node.
     *
     * @param node index of actual node
     * @return fitness of node across all instances
     */
    @Override
    public double calcNodeTotalFitness(int node) {

        this.nodes[node].nodeFitness = 0.0;
        for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
            this.nodes[node].nodeFitness += this.nodes[node].nodeInstanceFitness[i];
        }
        return this.nodes[node].nodeFitness;
    }

    /**
     * Calculates fitness value across all instances for all nodes.
     */
    @Override
    public void calcNodesTotalFitness() {
        for (int node = Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES; node < Settings.POPULATION_SIZE; node++) {
            this.nodes[node].nodeFitness = 0.0;
            for (int i = 0; i < Problem.NUMBER_OF_INSTANCES; i++) {
                this.nodes[node].nodeFitness += this.nodes[node].nodeInstanceFitness[i];
                /*if (this.nodes[node].nodeInstanceFitness[i] == BPP.INSTANCES[i].theoreticalOptimumFitness) {
                    this.nodes[node].nodeFitness--;
                }/**/

            }
            //System.out.println(MethodName.getCurrentMethodName() + " node " + node + " fitness " + this.nodes[node].nodeFitness);
        }
    }

    /**
     * Calculates and stores fitness value of individual. It is functional value
     * of the best point in simplex.
     *
     * @return fitness value at the end of execution.
     */
    @Override
    public double calcPopulationFitness() {

        this.averageFitness = 0.0;
        this.bestFitness = Double.MAX_VALUE;
        this.worstFitness = -Double.MAX_VALUE;

        // for (int n = Population.doubleTerminals.length; n < Settings.POPULATION_SIZE; n++) {
        for (int n = Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES; n < Settings.POPULATION_SIZE; n++) {
            //System.out.println(MethodName.getCurrentMethodName() + " node " + n + " fitness " + this.nodes[n].nodeFitness);
            this.averageFitness += this.nodes[n].nodeFitness;
            if (this.nodes[n].nodeFitness > this.worstFitness) {
                this.worstFitness = this.nodes[n].nodeFitness;
            }
            if (this.nodes[n].nodeFitness < this.bestFitness) {
                this.bestFitness = this.nodes[n].nodeFitness;
            }
        }

        //this.averageFitness /= (double) (this.nodes.length - Population.doubleTerminals.length);
        this.averageFitness /= (double) (BPP.actions.length + BPP.evolvedHeuristics.size());
        return this.averageFitness;
    }

    /**
     * Creates Vector object with all terminal values stored.
     *
     * @return Vector object
     */
    @Override
    public Vector createVOI() {
        Vector v = new Vector(Population.doubleTerminals.length - Settings.CONSTANTS_SIZE);
        for (int i = 0; i < v.values.length; i++) {
            v.values[i] = this.nodes[i].execute_node(this.instance);
        }
        v.roundValues(5);
        return v;
    }

    /**
     * Restarts some values to be fit for new instance.
     *
     * @param instanceID number of instance
     */
    @Override
    public void restartValues(int instanceID) {
        this.instance = (BPPInstance) BPP.INSTANCES[instanceID].clone();
        this.instance.resetInstance();
        this.instance.createNewBin();

        // Score for the llhs.
        this.actionScores = new double[Problem.actions.length];
        for (int i = 0; i < this.actionScores.length; i++) {
            this.actionScores[i] = 0.0;
        }

        /*for (int i = 0; i < this.nodes.length; i++) {
            this.nodes[i].nodeInstanceFitness[instanceID] = Double.MAX_VALUE;
        }/**/
    }

    /**
     * Prints individual's structure.
     */
    @Override
    public void printIndividual() {
        for (int n = 0; n < this.nodes.length; n++) {
            System.out.print(n + " | " + this.nodes[n].name + " | ");
            for (int ch = 0; ch < this.nodes[n].arity; ch++) {
                System.out.print(this.nodes[n].childrenNodes[ch].id + " ");
            }
            System.out.print("| ");
            for (int p = 0; p < this.nodes[n].parents.size(); p++) {
                System.out.print(this.nodes[n].parents.get(p).id + " ");
            }
            System.out.println("| " + this.nodes[n].nodeFitness);
        }
    }

    /**
     * Clones this individual.
     *
     * @return Deep copy of individual.
     */
    @Override
    public BPPIndividual clone() {
        // Create new object.
        BPPIndividual n = new BPPIndividual(false);
        // Copy graph.
        n.nodes = new Node[this.nodes.length];
        for (int i = 0; i < this.nodes.length; i++) {
            n.nodes[i] = this.nodes[i].clone();
            n.nodes[i].id = i;
            n.nodes[i].name = this.nodes[i].name;
            n.nodes[i].nodeFitness = this.nodes[i].nodeFitness;
            n.nodes[i].isFitnessActual = this.nodes[i].isFitnessActual;
            n.nodes[i].returnsConstant = this.nodes[i].returnsConstant;
            n.nodes[i].fixedNode = this.nodes[i].fixedNode;
            n.nodes[i].opened = this.nodes[i].opened;
            n.nodes[i].nodeInstanceFitness = new double[Problem.NUMBER_OF_INSTANCES];
            for (int inst = 0; inst < Problem.NUMBER_OF_INSTANCES; inst++) {
                n.nodes[i].nodeInstanceFitness[inst] = this.nodes[i].nodeInstanceFitness[inst];
            }
        }
        for (int i = 0; i < this.nodes.length; i++) {
            // Copy references.
            for (int a = 0; a < nodes[i].arity; a++) {
                n.nodes[i].childrenNodes[a] = n.nodes[this.nodes[i].childrenNodes[a].id];
            }
            for (int a = 0; a < nodes[i].parents.size(); a++) {
                n.nodes[i].parents.add(n.nodes[this.nodes[i].parents.get(a).id]);
            }
        }

        // Copy fitness values.
        n.averageFitness = this.averageFitness;
        n.bestFitness = this.bestFitness;
        n.worstFitness = this.worstFitness;

        // Copy other variables.
        n.startingGeneration = this.startingGeneration;
        n.setBestParents();

        n.comment = this.comment;
        n.individualName = this.individualName;

        return n;
    }

    /**
     * Compares two individuals.
     *
     * @param o second individual.
     * @return 1 if this is greater, -1 if second individual is greater, 0 when
     * both are equal.
     */
    @Override
    public int compareTo(Object o) {
        Individual n = (Individual) o;
        if (this.averageFitness < n.averageFitness) {
            return -1;
        }
        if (n.averageFitness < this.averageFitness) {
            return 1;
        }
        return 0;
    }

}

class LLHResult implements Comparable {

    public int llhID;
    public double value;
    public double actionScore;
    public int itemID;

    public LLHResult(int i, double v, double as, int it) {
        this.llhID = i;
        this.value = v;
        this.actionScore = as;
        this.itemID = it;
    }

    @Override
    public int compareTo(Object o) {
        LLHResult n = (LLHResult) o;
        if (this.value < n.value) {
            return -1;
        }
        if (this.value == n.value) {
            if (this.actionScore > n.actionScore) {
                return -1;
            }
            if (this.actionScore == n.actionScore) {
                return 0;
            }
            if (this.actionScore < n.actionScore) {
                return 1;
            }
        }
        if (this.value > n.value) {
            return 1;
        }
        return 0;
    }
}
