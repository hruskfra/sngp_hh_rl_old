package gp.individuals;

import gp.Node;
import gp.Population;
import gp.tasks.Instance;
import gp.tasks.Problem;
import gp.terminals.Constant;
import loaders.LoadedFile;
import sngp.*;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fanda on 14. 3. 2016.
 */
public abstract class Individual implements Cloneable, Comparable {

    /**
     * Individual name.
     */
    public String individualName = "";

    /**
     * Comments for this individual.
     */
    public String comment = "";

    /**
     * List of nodes in the individual.
     */
    public Node[] nodes = new Node[Settings.POPULATION_SIZE];

    /**
     * Final fitness value.
     */
    public double fitness = 0.0;
    public double averageFitness = Double.MAX_VALUE;
    public double bestFitness = Double.MAX_VALUE;
    public double worstFitness = Double.MAX_VALUE;

    /**
     * Starting generation.
     */
    public int startingGeneration = 0;

    /**
     * Scores for the LLHs.
     */
    public double actionScores[];

    /**
     * Index of the instance to execution.
     */
    public int instanceExecutedID = 0;

    /**
     * Actual executed instance.
     */
    public Instance instance;

    /**
     * Index of individual in archive. -1 if individual is not in archive.
     */
    public int bestNodeID = -1;

    /**
    * Vectors of interest.
    */
    public ArrayList<Vector>[] instanceVOI = new ArrayList[Problem.INSTANCES.length];

    /**
     * Number of improved instances.
     */
    @Deprecated
    public int improvedInstances = 0;

    /**
     * Indicates execution only of the best node.
     */
    @Deprecated
    public boolean EXECUTE_ONLY_BEST_NODE = false;

    /**
     * Assigned thread to this individual. If null, it runs in the main thread.
     */
    public SNGPThread assignedThread = null;

    /**
     * Creates new individuals for evolution.
     *
     * @return new individuals needed for population-based algorithms to run
     */
    public static Individual createNewInstance() {
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP) {
            return new BPPIndividual();
        }
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.CVRP) {
            return new CVRPIndividual();
        }
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP_GHH) {
            return new BPP_GHH_Individual();
        }
        /*if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.TSP) {
            return new TSPIndividual();
        }/**/
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.TSPTW) {
            return new TSPTWIndividual();
        }
        System.out.println(MethodName.getCurrentMethodName() + " ERROR - Bad optimization problem set!");
        System.exit(1);
        return null;
    }

    /**
     * Creates new individuals for evolution stored in some file.
     *
     * @param fileName name of the file with stored individual
     * @return new individuals nedeed for population-based algorithms to run
     */
    public static Individual createNewInstance(String fileName) {
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP) {
            return new BPPIndividual(fileName);
        }
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.CVRP) {
            return new CVRPIndividual(fileName);
        }
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP_GHH) {
            return new BPP_GHH_Individual(fileName);
        }

        System.out.println(MethodName.getCurrentMethodName() + " ERROR - Bad optimization problem set!");
        System.exit(1);
        return null;
    }

    /**
     * Creates new individuals for evolution.
     *
     * @param b dumb boolean parameter
     * @return new individuals nedeed for population-based algorithms to run
     */
    public static Individual createNewInstance(boolean b) {
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP) {
            return new BPPIndividual(b);
        }
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.CVRP) {
            return new CVRPIndividual(b);
        }
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP_GHH) {
            return new BPP_GHH_Individual(b);
        }

        System.out.println(MethodName.getCurrentMethodName() + " ERROR - Bad optimization problem set!");
        System.exit(1);
        return null;
    }

    /**
     * Calculates instance fitness value.
     *
     * @param instanceID actual instance ID
     * @return fitness of the solution obtained on instace
     */
    public abstract double calcInstanceFitness(int instanceID);

    /**
     * Assign fitness value obtained by the node on the actual instance.
     *
     * @param instanceID actual instance ID
     * @param node       actual node index
     * @return fitness value of the node on the actual instace
     */
    public abstract double calcNodeInstanceFitness(int instanceID, int node);

    /**
     * Calc instance fitness for current node.
     *
     * @param node node ID
     * @return total fitness of actual node
     */
    public abstract double calcNodeTotalFitness(int node);

    /**
     * Calculates nodes total fitness
     */
    public abstract void calcNodesTotalFitness();

    /**
     * Calculates and stores fitness value of individual. It is functional value
     * of the best point in simplex.
     *
     * @return fitness value at the end of execution.
     */
    public abstract double calcPopulationFitness();

    /**
     * Prints information about individual.
     */
    public void printIndividual() {
        for (int n = 0; n < this.nodes.length; n++) {
            System.out.print(n + " | " + this.nodes[n].name + " | ");
            for (int ch = 0; ch < this.nodes[n].arity; ch++) {
                System.out.print(this.nodes[n].childrenNodes[ch].id + " ");
            }
            System.out.print("| ");
            for (int p = 0; p < this.nodes[n].parents.size(); p++) {
                System.out.print(this.nodes[n].parents.get(p).id + " ");
            }
            System.out.println("| " + this.nodes[n].nodeFitness);
        }
        //System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions");
    }

    /**
     * Prints infix tree rooted in the node n.
     *
     * @param n root of the tree to be printed
     */
    public void printInfixTree(Node n) {
        if (n.arity == 2) {
            System.out.print(n.toStringPart1());
            printInfixTree(n.childrenNodes[0]);
            System.out.print(n.toStringMiddlePart());
            printInfixTree(n.childrenNodes[1]);
            System.out.print(n.toStringPart2());
        }
        if (n.arity == 1) {
            System.out.print(n.toStringPart1());
            printInfixTree(n.childrenNodes[0]);
            System.out.print(n.toStringPart2());
        }
        if (n.arity == 0) {
            System.out.print(n.toString());
        }
    }

    /**
     * String of the infix tree.
     *
     * @param n actual node
     * @return infix tree String
     */
    public String infixTreeToString(Node n) {
        String s = "";
        if (n.arity == 2) {
            s += n.toStringPart1();
            s += infixTreeToString(n.childrenNodes[0]);
            s += n.toStringMiddlePart();
            s += infixTreeToString(n.childrenNodes[1]);
            s += n.toStringPart2();
        }
        if (n.arity == 1) {
            s += n.toStringPart1();
            s += infixTreeToString(n.childrenNodes[0]);
            s += n.toStringPart2();
        }
        if (n.arity == 0) {
            s += n.toString();
        }
        return s;

    }

    /**
     * Make fitness actual nodes creating the constant values.
     */
    public void markConstants() {
        // Find constant trees.
        SNGP_HH_RL.FIND_CONSTANT_TREES = true;
        double[] firstValue = new double[Settings.POPULATION_SIZE];
        double actualValue;
        for (Node n : this.nodes) {
            n.returnsConstant = true;
        }
        for (int i = 0; i < 20; i++) {
            // Generate random values for terminal features.
            for (int n = 0; n < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE; n++) {
                this.nodes[n].randomValue = Math.random() * 10.0 - 5.0;
            }
            for (int n = 0; n < Settings.POPULATION_SIZE; n++) {

                if (this.nodes[n].isFitnessActual) {
                    continue;
                }
                if (!this.nodes[n].returnsConstant) {
                    continue;
                }

                // Feature terminals are not constants.
                if (n < Population.doubleTerminals.length - Settings.CONSTANTS_SIZE) {
                    this.nodes[n].returnsConstant = false;
                    continue;
                }
                // Skip constants.
                if (n >= Population.doubleTerminals.length - Settings.CONSTANTS_SIZE && n < Population.doubleTerminals.length) {
                    continue;
                }
                if (i == 0) {
                    firstValue[n] = this.nodes[n].execute_node(Population.newIndividual);
                } else {
                    actualValue = this.nodes[n].execute_node(Population.newIndividual);
                    //System.out.println(n.id + " | Actual " + actualValue  + " | First " + firstValue);
                    if (actualValue != firstValue[n]) {
                        this.nodes[n].returnsConstant = false;
                        continue;
                    }
                }
            }

        }
        SNGP_HH_RL.FIND_CONSTANT_TREES = false;
    }

    /**
     * Set best predecessor (including this node) to all out of date nodes.
     */
    public void setBestParents() {

        Node thisNode;
        for (int n = Settings.POPULATION_SIZE - 1; n >= 0; n--) {

            thisNode = this.nodes[n];
            thisNode.bestParent = thisNode;

            if(Settings.FITNESS_MINIMIZATION) {
                for (Node p : thisNode.parents) {
                    if (thisNode.bestParent.nodeFitness < p.bestParent.nodeFitness) {
                        thisNode.bestParent = p.bestParent;
                    }
                }
            }
            else {
                for (Node p : thisNode.parents) {
                    if (thisNode.bestParent.nodeFitness > p.bestParent.nodeFitness) {
                        thisNode.bestParent = p.bestParent;
                    }
                }
            }
        }
    }

    /**
     * Extracts nodes from the tree rooted in the node n.
     *
     * @param n root of the tree
     * @return ArrayList of the nodes contained in the tree
     */
    public ArrayList<Node> getTreeNodes(Node n) {

        for (Node nn : this.nodes) {
            nn.opened = false;
        }
        ArrayList<Node> tree = new ArrayList();
        Node actual;
        ArrayList<Node> queue = new ArrayList();
        queue.add(n);
        while (!queue.isEmpty()) {
            actual = queue.get(0);
            actual.opened = true;
            tree.add(actual);
            queue.remove(0);
            if (actual.arity > 0) {
                for (Node ch : actual.childrenNodes) {
                    if (!ch.opened) {
                        queue.add(ch);
                        ch.opened = true;
                    }
                }
            }
        }

        return tree;
    }

    /**
     * Return node with the best fitness value.
     *
     * @return node with the best fitness value
     */
    public Node getBestNode() {
        Node bestNode = this.nodes[Population.doubleTerminals.length];
        for (int n = Population.doubleTerminals.length; n < Settings.POPULATION_SIZE; n++) {
            if(Settings.FITNESS_MINIMIZATION) {
                if (this.nodes[n].nodeFitness < bestNode.nodeFitness) {
                    bestNode = this.nodes[n];
                }
            }
            else {
                if (this.nodes[n].nodeFitness > bestNode.nodeFitness) {
                    bestNode = this.nodes[n];
                }
            }
        }

        return bestNode;
    }

    /**
     * Return node with the best fitness value.
     *
     * @return node with the best fitness value
     */
    public int getBestNodeID() {
        Node bestNode = this.nodes[Population.doubleTerminals.length];
        int bestID = Population.doubleTerminals.length;
        for (int n = Population.doubleTerminals.length; n < Settings.POPULATION_SIZE; n++) {
            if(Settings.FITNESS_MINIMIZATION) {
                if (this.nodes[n].nodeFitness < bestNode.nodeFitness) {
                    bestNode = this.nodes[n];
                    bestID = n;
                }
            }
            else {
                if (this.nodes[n].nodeFitness > bestNode.nodeFitness) {
                    bestNode = this.nodes[n];
                    bestID = n;
                }
            }
        }

        //System.out.println(MethodName.getCurrentMethodName() + " Best ID " + bestID + " with fitness " + this.nodes[bestID].nodeFitness);
        return bestID;
    }

    /**
     * Method for checking consistency of the individual.
     *
     * @return true if connections are correct
     */
    public boolean checkIndividual() {
        boolean correct = true;
        for (Node n : this.nodes) {
            if (n.arity != 0) {
                for (Node ch : n.childrenNodes) {
                    if (ch.id >= n.id) {
                        System.out.println("ERROR - Node " + n.id + " has child " + ch.id + " with higher id!");
                        correct = false;
                    }
                }
            }
            for (Node p : n.parents) {
                if (p.id <= n.id) {
                    System.out.println("ERROR - Node " + n.id + " has  parent " + p.id + " with lower id!");
                    correct = false;
                }
            }
        }
        return correct;
    }

    /**
     * Saves individual structure into the file.
     */
    public void saveIndividual() {
        try {
            //System.out.println(MethodName.getCurrentMethodName() + " " + SNGP_HH_RL.optimizationProblem.outputDir + "Ind_" + SNGP_HH_RL.timeStamp + ".txt");

            //PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_IND.txt");
            PrintWriter pout = new PrintWriter("RESULTS\\" + SNGP_HH_RL.timeStampString  + "_IND.txt");
            pout.write("PROBLEM : " + SNGP_HH_RL.optimizationProblem.getClass().getSimpleName() + "\r\n");
            pout.write("NAME : " + this.individualName + "\r\n");
            pout.write("FITNESS : " + this.bestFitness + "\r\n");
            pout.write("BEST_NODE : " + this.getBestNodeID() + "\r\n");
            pout.write("GENERATION : " + this.startingGeneration + "\r\n");
            pout.write("NODES : " + Settings.POPULATION_SIZE + "\r\n");
            pout.write("TERMINALS : " + Population.doubleTerminals.length + "\r\n");
            pout.write("CONSTANTS : " + Settings.CONSTANTS_SIZE + "\r\n");
            pout.write("NODES_SECTION\r\n");
            for (int n = 0; n < this.nodes.length; n++) {
                pout.write(n + ":" + this.nodes[n].name);
                if (this.nodes[n].name.equals("Constant")) pout.write("_" + this.nodes[n].value);
                pout.write(":");
                for (int ch = 0; ch < this.nodes[n].arity; ch++) {
                    pout.write(this.nodes[n].childrenNodes[ch].id + "");
                    if (ch < this.nodes[n].arity - 1) pout.write(" ");
                }
                pout.write(":");
                for (int p = 0; p < this.nodes[n].parents.size(); p++) {
                    pout.write(this.nodes[n].parents.get(p).id + "");
                    if (p < this.nodes[n].parents.size() - 1) pout.write(" ");
                }
                pout.write(":" + this.nodes[n].nodeFitness + "\r\n");
            }
            pout.close();
        } catch (FileNotFoundException FNFE) {

        }

    }

    /**
     * Saves best individual structure (only nodes in the tree rooted from getBestNode()).
     */
    public void saveBestIndividual() {
        // TODO
        //--- ArrayList of nodes in best individual tree.
        ArrayList<Integer> nodesIDs = new ArrayList();
        //--- Queue for BFS algorithm.
        ArrayList<Node> queue = new ArrayList();

        //--- Find all nodes below the root.
        Node n = this.getBestNode();
        queue.add(n);

        //--- BFS algorithm.
        while(!queue.isEmpty()) {
            n = queue.get(0); //--- Pop.
            queue.remove(0); //--- Delete top from the queue.

            nodesIDs.add(n.id); //--- Add id of the poped node to the ArrayList.
            //--- Skip terminal nodes.
            if(n.arity == 0) {
                continue;
            }
            //--- Add children of the actual node to the queue.
            for(Node ch : n.childrenNodes) {
                queue.add(ch);
            }
        }

        //--- Saving procedure.
        try {
            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_IND_BEST.txt");
            pout.write("PROBLEM : " + SNGP_HH_RL.optimizationProblem.getClass().getSimpleName() + "\r\n");
            pout.write("NAME : " + this.individualName + "\r\n");
            pout.write("FITNESS : " + this.bestFitness + "\r\n");
            pout.write("BEST_NODE : " + this.getBestNodeID() + "\r\n");
            pout.write("GENERATION : " + this.startingGeneration + "\r\n");
            pout.write("NODES : " + Settings.POPULATION_SIZE + "\r\n");
            pout.write("TERMINALS : " + Population.doubleTerminals.length + "\r\n");
            pout.write("CONSTANTS : " + Settings.CONSTANTS_SIZE + "\r\n");
            pout.write("NODES_SECTION\r\n");

            //--- Store nodes information if it is in the ArrayList.
            for(int i = 0; i < Settings.POPULATION_SIZE; i++) {
                if(nodesIDs.contains(i)) {
                    pout.write(i + ":" + this.nodes[i].name);
                    if (this.nodes[i].name.equals("Constant")) pout.write("_" + this.nodes[i].value);
                    pout.write(":");
                    for (int ch = 0; ch < this.nodes[i].arity; ch++) {
                        pout.write(this.nodes[i].childrenNodes[ch].id + "");
                        if (ch < this.nodes[i].arity - 1) pout.write(" ");
                    }
                    pout.write(":");
                    for (int p = 0; p < this.nodes[i].parents.size(); p++) {
                        pout.write(this.nodes[i].parents.get(p).id + "");
                        if (p < this.nodes[i].parents.size() - 1) pout.write(" ");
                    }
                    pout.write(":" + this.nodes[i].nodeFitness + "\r\n");
                }
            }
            //--- Close the PrintWriter.
            pout.close();
        }
        catch(FileNotFoundException FNFE) {
            FNFE.printStackTrace();
        }
    }

    /**
     * Saves best individual in infix form.
     */
    public void saveBestInfixIndividual() {
        try {
            PrintWriter pout = new PrintWriter(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_IND_INFIX.txt");
            pout.write(this.nodes[this.getBestNodeID()].nodeFitness + "\r\n");
            pout.write(this.infixTreeToString(this.getBestNode()));
            pout.close();
        }
        catch(FileNotFoundException FNFE) {
            FNFE.printStackTrace();
        }
    }

    /**
     * Loads individual from the file.
     * @param path path the the file
     */
    public void loadIndividual(String path) {
        File file;
        String line;
        int TERMINALS = -1;

        file = LoadedFile.getFile(path);

        // Start loading evolution.
        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);
            try {
                try {
                    String nodes[];
                    line = br.readLine();
                    //--- First part of the individual file with some basic information.
                    while (!line.equals("NODES_SECTION")) {
                        nodes = line.split(" ");
                        switch (nodes[0]) {
                            case "ACTIONS":
                                // TODO
                                break;
                            case "BEST_NODE":
                                this.bestNodeID = Integer.parseInt(nodes[2]);
                                break;
                            case "COMMENT":
                                this.comment = nodes[2];
                                break;
                            case "CONSTANTS":
                                Settings.CONSTANTS_SIZE = Integer.parseInt(nodes[2]);
                                break;
                            case "FITNESS":
                                this.bestFitness = Double.parseDouble(nodes[2]);
                                break;
                            case "GENERATION":
                                this.startingGeneration = Integer.parseInt(nodes[2]);
                                break;
                            case "HEURISTICS":
                                // TODO
                                break;
                            case "NAME":
                                if(nodes.length > 2) {
                                    this.individualName = nodes[2];
                                }
                                else {
                                    this.individualName = file.getName().substring(0, file.getName().length()-4);
                                }
                                break;
                            case "NODES":
                                this.nodes = new Node[Integer.parseInt(nodes[2])];
                                Settings.POPULATION_SIZE = this.nodes.length;
                                break;
                            case "PROBLEM":
                                // TODO
                                break;
                            case "TERMINALS":
                                TERMINALS = Integer.parseInt(nodes[2]);
                                break;
                            default:
                                System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Unknown individual property (" + nodes[0] + ") in the file " + path + "!");
                                System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Expecting line in form \"PROPERTY : VALUE\" or line with \"NODES_SECTION\"!");
                                System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Current line: " + line);
                                System.exit(1);
                        }
                        line = br.readLine();
                    }

                    //--- Load non-constant terminals.
                    for (int t = 0; t < TERMINALS - Settings.CONSTANTS_SIZE; t++) {
                        line = br.readLine();
                        nodes = line.split(":");
                        this.nodes[t] = Node.getNodeByName(nodes[1], Population.doubleTerminals);
                        this.nodes[t].id = t;
                        this.nodes[t].nodeFitness = Double.parseDouble(nodes[nodes.length - 1]);
                    }
                    //--- Load constants.
                    for (int t = TERMINALS - Settings.CONSTANTS_SIZE; t < TERMINALS; t++) {
                        line = br.readLine();
                        nodes = line.split(":");
                        this.nodes[t] = new Constant();
                        this.nodes[t].id = t;
                        this.nodes[t].nodeValue = this.nodes[t].value = Double.parseDouble(nodes[1].split("_")[1]);
                        this.nodes[t].nodeFitness = Double.parseDouble(nodes[nodes.length - 1]);
                    }
                    //--- Load functions.
                    for (int f = TERMINALS; f < Settings.POPULATION_SIZE; f++) {
                        line = br.readLine();
                        nodes = line.split(":");
                        this.nodes[f] = Node.getNodeByName(nodes[1], Population.doubleFunctions);
                        this.nodes[f].id = f;
                        this.nodes[f].nodeFitness = Double.parseDouble(nodes[nodes.length - 1]);
                        //--- Assign children and parents.
                        String children[] = nodes[2].split(" ");
                        for (int ch = 0; ch < this.nodes[f].childrenNodes.length; ch++) {
                            this.nodes[f].childrenNodes[ch] = this.nodes[Integer.parseInt(children[ch])];
                            this.nodes[f].childrenNodes[ch].parents.add(this.nodes[f]);
                        }
                    }
                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        //this.nodes[this.bestNodeID].isFitnessActual = false;
        System.out.println("\033[32m" + "INDIVIDUAL: " + "Individual " + path + " successfully loaded!" + "\033[0m");
    }

    /**
     * Executes original heuristics for all training instances.
     *
     * @param print indicates if information will be printed
     */
    public void executeLowLevelHeuristics(boolean print) {
    }

    /**
     * Executes all nodes in individual on all instances.
     */
    public void executeIndividual1Thread() {
        //--- For all instances.
        for (int inst = 0; inst < Problem.INSTANCES.length; inst++) {
            //--- For all nodes.
            for (int node = Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES; node < Settings.POPULATION_SIZE; node++) {
                executeIndividual1Thread(inst, node);
            }
        }
    }

    /**
     * Execute individual's nodes on specific instance
     *
     * @param instanceID ID of the instance
     */
    public void executeIndividual1Thread(int instanceID) {
        //--- Execute individual's nodes on specific instance
        for (int node = Settings.POPULATION_SIZE - Settings.NUMBER_OF_EXECUTED_NODES; node < Settings.POPULATION_SIZE; node++) {
            executeIndividual1Thread(instanceID, node);
        }
    }

    /**
     * Executes individual's specific node on specific instances.
     *
     * @param instanceID ID of the executed instance
     * @param nodeID     ID of the executed node
     */
    public abstract void executeIndividual1Thread(int instanceID, int nodeID);

    /**
     * Creates Vector object with all terminal values stored.
     * @return Vector object
     */
    public Vector createVOI() {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
        return null;
    }

    /**
     * Restarts value for the current individual.
     *
     * @param instanceID ID of the instance
     */
    public abstract void restartValues(int instanceID);

    /**
     * Clonning method.
     *
     * @return clone of the individual
     */
    @Override
    public abstract Individual clone();

    /**
     * Compares two individuals.
     *
     * @param o second individual.
     * @return 1 if this is greater, -1 if second individual is greater, 0 when
     * both are equal.
     */
    @Override
    public int compareTo(Object o) {
        Individual n = (Individual) o;
        if(Settings.FITNESS_MINIMIZATION) {
            if (this.averageFitness < n.averageFitness) {
                return -1;
            }
            if (n.averageFitness < this.averageFitness) {
                return 1;
            }
        }
        else {
            if (this.averageFitness > n.averageFitness) {
                return -1;
            }
            if (n.averageFitness > this.averageFitness) {
                return 1;
            }
        }
        return 0;
    }
}
