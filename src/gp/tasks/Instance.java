package gp.tasks;

import heuristics.Heuristic;
import sngp.MethodName;
import sngp.Settings;

import java.util.HashMap;

/**
 * Created by Fanda on 14. 3. 2016.
 */
public abstract class Instance implements Cloneable {

    /**
     * Name of the instnace.
     */
    public String name = "";

    /**
     * Size of the instance.
     */
    public int size;

    /**
     * Best fitness value obtained by a single low level heuristic.
     */
    public double bestHeuristicFitness = Double.MAX_VALUE;

    /**
     * Best known solution fitness.
     */
    public double theoreticalOptimumFitness = 0.0;
    public double theoreticalOptimumValue = 0.0;

    /**
     * Subgroup where this instance belong.
     */
    public String instanceSubgroup = "";
    /**
     * Restarts whole instance.
     */
    public void resetInstance() {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
    }

    /**
     * Prints information about the instance.
     */
    public void printInstance() {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
    }

    /**
     * Prints solutions and information related to it.
     */
    public void printSolution() {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
    }

    /**
     * Checks solution for errors.
     */
    public boolean checkSolution() {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
        return true;
    }

    /**
     * Clonning method
     *
     * @return deep clone of the instance.
     */
    @Override
    public abstract Instance clone();
}
