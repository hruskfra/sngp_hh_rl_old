package gp.tasks.tsp;

import gp.tasks.Instance;
import gp.tasks.cvrp.CVRPInstance;
import sngp.MethodName;
import sngp.Vector;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by fhruska on 07.02.2017.
 */
public class TSPInstance extends Instance {

    /**
     * Positions of cities.
     */
    public Vector[] cities;

    /**
     * Symmetric distances of TSP problem.
     */
    public double[][] distances;

    /**
     * ArrayList of route.
     */
    public ArrayList<Integer> route = new ArrayList<>();

    /**
     * Constructor of CVRP instance.
     *
     * @param size size of instance
     */
    public TSPInstance(int size) {
        this.size = size;
        this.cities = new Vector[size];
        this.distances = new double[size][size];
    }

    /**
     * Restarts whole instance.
     */
    @Override
    public void resetInstance() {
        this.route = new ArrayList<>(); //--- Delete route.
    }

    /**
     * Creates copy of the solution.
     *
     * @param a actual solution of the instance
     * @return ArrayList of route
     */
    public ArrayList<Integer> copySolution(ArrayList<Integer> a) {
        ArrayList<Integer> tempSolution = new ArrayList();
        for (int r = 0; r < a.size(); r++) {
            tempSolution.add(r, a.get(r));
        }
        return tempSolution;
    }

    /**
     * Gets array of unassigned cities.
     *
     * @return array of unassigned cities
     */
    public int[] getUnassignedCities() {

        int freeCities[];
        int count = this.size - this.route.size();

        //--- Allocate array and store ids for free cities.
        freeCities = new int[count];
        count = 0;
        boolean found;
        for(int actualCity = 0; actualCity < this.size; actualCity++) {
            found = false;
            for (int i = 0; i < this.route.size(); i++) {
                if (this.route.get(i) == actualCity) {
                    found = true;
                    break;
                }
            }
            if(!found) {
                freeCities[count++] = actualCity;
            }
        }

        return freeCities;
    }

    /**
     * Calculates number of unassigned cities.
     *
     * @return number of unassigned cities.
     */
    public int getNumberOfUnassignedCities() {
        return this.size - this.route.size();
    }

    /**
     * Finds furthest unassigned city.
     *
     * @param freeCities array of unassigned cities
     * @return index of the furthest unassigned city from the depot
     */
    public int getFurthestUnassignedCityFromDepot(int[] freeCities) {

        int id = 0;
        double fdist = this.distances[0][freeCities[0]];
        for (int i = 1; i < freeCities.length; i++) {
            if (this.distances[0][freeCities[i]] > fdist) {
                fdist = this.distances[0][freeCities[i]];
                id = i;
            }
        }
        return id;
    }

    /**
     * Prints instances parameters.
     */
    @Override
    public void printInstance() {
        System.out.println("===");
        System.out.println("Name: " + this.name);
        System.out.println("#Cities: " + this.cities.length);
        System.out.println();
    }

    /**
     * Prints actual solution.
     */
    @Override
    public void printSolution() {
        System.out.println("Instance: " + this.name);
        for (int i = 0; i < this.route.size(); i++) {
            System.out.print(i + ") " + this.route.get(i).toString());
        }
        if (this.route.isEmpty()) {
            System.out.println();
        }
    }

    /**
     * Checks solution for errors.
     *
     * @return trueif solution is without errors.
     */
    @Override
    public boolean checkSolution() {
        boolean correct = true;
        //--- Some cities are unassigned.
        if(getUnassignedCities().length != 0) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Cities " + Arrays.toString(getUnassignedCities())+ " are unassigned!.");
            correct = false;
        }
        //--- Check if some city is not included more than once.
        int[] citiesCounter = new int[this.size];
        Arrays.fill(citiesCounter, 0);
        for (int i = 0; i < this.route.size(); i++) {
                citiesCounter[this.route.get(i)]++;
        }
        //--- Check if there are all ones in the city counter array.
        for(int i =0; i < citiesCounter.length; i++) {
            if(citiesCounter[i] > 1) {
                correct = false;
                System.err.println(MethodName.getCurrentMethodName() + " - ERROR - City " + (i) + " is included more than once (" + citiesCounter[i] +  ") in the solution!");
            }
        }
        if(!correct) {
            this.printSolution();
            System.exit(1);
        }

        return correct;
    }

    /**
     * Creates clone of the CVRP instance.
     *
     * @return clone of the CVRP instance
     */
    public TSPInstance clone() {

        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Missing implementation of cloning method!");
        System.exit(1);

        TSPInstance c = new TSPInstance(this.size);
        c.name = this.name;

        c.cities = new Vector[this.cities.length];
        for(int i = 0; i < this.cities.length; i++) {
            c.cities[i] = this.cities[i].clone();
        }

        c.distances = new double[this.distances.length][this.distances[0].length];
        for(int i = 0; i < this.distances.length; i++) {
            for(int j = 0; j < this.distances[0].length; j++) {
                c.distances[i][j]= this.distances[i][j];
            }
        }

        return this;
    }
}
