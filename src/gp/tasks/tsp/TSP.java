/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.tasks.tsp;

import gp.tasks.Problem;
import gp.tasks.cvrp.CVRPInstance;
import loaders.tsp.LoadedInstance;
import heuristics.Heuristic;
import heuristics.cvrp.*;
import sngp.*;


import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author fanda
 */
public class TSP extends Problem {

    /**
     * Indicates if CVRP is symmetrical.
     */
    public static boolean SYMMETRICAL = true;

    /**
     * Constructor.
     */
    public TSP() {
        instancesTrainingDir = "INSTANCES/" + this.getClass().getSimpleName() + "/Training/";
        instancesTestingDir = "INSTANCES/" + this.getClass().getSimpleName() + "/Testing/";
        //--- Check if directory exists and create new one if it is not true.
        if(!new File("RESULTS\\" + this.getClass().getSimpleName() + "\\").isDirectory()) {
            new File ("RESULTS\\" + this.getClass().getSimpleName() + "\\").mkdir();
            System.out.println("OPTIMIZATION PROBLEM: Directory RESULTS\\" + this.getClass().getSimpleName() + "\\ created!");
        }
        outputDir = "RESULTS/" + this.getClass().getSimpleName() + "/";
    }

    /**
     * Initializes low-lew heuristics for actual optimization problem.
     * @return Array of loaded heuristics.
     */
    @Override
    public Heuristic[] initHeuristics() {
        ArrayList<Heuristic> llhs = new ArrayList();

        llhs.add(new ClarkeWright());
        llhs.add(new Kilby());
        llhs.add(new MoleJameson());

        Heuristic[] h = new Heuristic[llhs.size()];
        for (int i = 0; i < h.length; i++) {
            h[i] = llhs.get(i);
        }

        Problem.NUMBER_OF_LLHS = h.length;

        return h;
    }

    /**
     * Initializes actions used in HH for actual optimization problem.
     * @return Array of loaded heuristics.
     */
    @Override
    public Heuristic[] initActions() {
        ArrayList<Heuristic> ac = new ArrayList();
        ac.add(new ClarkeWright());
        ac.add(new Kilby());
        ac.add(new MoleJameson());

        Heuristic[] h = new Heuristic[ac.size()];
        for (int i = 0; i < h.length; i++) {
            h[i] = ac.get(i);
        }

        Problem.NUMBER_OF_ACTIONS = h.length;

        return h;
    }

    /**
     * Loads instances in the directory from method parameters.
     * @param dir directory with instances.
     */
    @Override
    public void loadInstances(String dir) {
        /**
         * Set all instances in directory.
         */
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        int counter = 0;
        String n;
        //--- Count instances according its file extension.
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {

                n = listOfFiles[i].getName().substring(listOfFiles[i].getName().length() - 3, listOfFiles[i].getName().length());
                if (n.equals("tsp") || n.equals("TSP") || n.equals("txt") || n.equals("TXT")) counter++;
            }
        }
        //--- Store paths to instances.
        instancesPaths = new String[counter];
        counter = 0;
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                n = listOfFiles[i].getName().substring(listOfFiles[i].getName().length() - 3, listOfFiles[i].getName().length());
                if (n.equals("tsp") || n.equals("TSP") || n.equals("txt") || n.equals("TXT")) instancesPaths[counter++] = listOfFiles[i].getName();
            }
        }

        //--- Load all instances.
        counter = 0;
        Problem.NUMBER_OF_INSTANCES = listOfFiles.length;
        Problem.INSTANCES = new TSPInstance[listOfFiles.length];
        for(String sinst : instancesPaths) {
            Problem.INSTANCES[counter] = loadInstance(dir + "/" + sinst, true);
            counter++;
        }

        System.out.println("INSTANCE: " + Problem.NUMBER_OF_INSTANCES + " instances successfully loaded!");
    }

    /**
     * Loads TSP instance from the file.
     *
     * @param path path to the file
     * @param print @code tru if information about loading process will be printed
     * @return TSPInstance object
     */
    public TSPInstance loadInstanceOldMethod(String path, boolean print) {

        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Missing Implementation!");
        System.exit(1);

        TSPInstance instance = new TSPInstance(1);
        File file;
        String line;
        int lineCounter = 0;    // Var for better exporting of bugs in file format.

        boolean exists = new File(path).exists();

        // File exists?
        if (exists) {
            // Then prepare for loading.
            file = new File(path);
            if (file.isDirectory()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Input file path " + path + " leads only to directory.");
                System.exit(1);

            }
            if (print) {
                System.out.println("INSTANCE: Found file " + path + ". Loading instance...");
            }
        } else {
            // Else make a new populations.
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Cannot find file " + path + ".");
            System.exit(1);
            return null;
        }


        // Start loading evolution.
        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);
            try {
                try {

                    String nodes[];

                    //---
                    line = br.readLine();
                    lineCounter++;

                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        if (print) {
            System.out.println("INSTANCE: " + "Instance successfully loaded!" + "\033[0m");
        }
        return instance;
    }

    /**
     * Loads instance from the file using TSPLIB4J library.
     *
     * @param path path to the file
     * @param print @code true if information about progress will be print
     *
     * @return object of the TSPInstance
     */
    @Override
    public TSPInstance loadInstance(String path, boolean print) {

        try {
            return getInstanceFromLoader(new LoadedInstance(new File(path)));
        }
        catch(IOException IOE) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - File " + path + " does not exist!");
            System.exit(1);
        };

        System.exit(1);
        return null;
    }

    /**
     * Loads instance using TSPLIB4J library and transforms it to only necessary structure used in SNGP.
     *
     * @param loadedInst instance loaded using TSPLIB4J
     *
     * @return TSPInstance object used in SNGP optimization process
     */
    public static TSPInstance getInstanceFromLoader(LoadedInstance loadedInst) {
        TSPInstance inst = new TSPInstance(loadedInst.getDimension());

        inst.distances = loadedInst.getDistanceMatrix();
        inst.name = loadedInst.getName();

        return inst;
    }
}
