package gp.tasks;

import gp.individuals.Individual;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPP_GHH;
import gp.tasks.cvrp.CVRP;
import gp.tasks.tsp.TSP;
import gp.tasks.tsptw.TSPTW;
import heuristics.Heuristic;
import sngp.MethodName;
import sngp.Settings;

import java.util.ArrayList;

/**
 * Created by Fanda on 15. 3. 2016.
 */
public class Problem {
    /**
     * Number of loaded instances.
     */
    public static int NUMBER_OF_INSTANCES = -1;

    /**
     * Array of loaded instances.
     */
    public static Instance[] INSTANCES;

    /**
     * Actual instance ID.
     */
    public static int INSTANCE_ID = -1;

    /**
     * Number of low level heuristics.
     */
    public static int NUMBER_OF_LLHS = 0;

    /**
     * Number of actions for the SNGP and RL.
     */
    public static int NUMBER_OF_ACTIONS = 0;

    /**
     * Files with instance and individual.
     */
    public static String[] instancesPaths;
    public static String instancesTrainingDir;
    public static String instancesTestingDir;

    /**
     * Output dir paths
     */
    public static String outputDir = "RESULTS/";

    /**
     * Evolved heuristics from the previous step.
     */
    public static ArrayList<Individual> evolvedHeuristics = new ArrayList();

    /**
     * Possible heuristics for the BBP.
     */
    public static Heuristic[] llhs;

    /**
     * Possible actions based on heuristics.
     */
    public static Heuristic[] actions;

    /**
     * Subgroups that divide instances into smaller groups.
     */
    public static String[] instanceSubgroups;

    /**
     * Creates object referenced for the optimization problem.
     */
    public static Problem createProblem() {
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP) {
            return new BPP();
        } else if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.CVRP) {
            return new CVRP();
        } else if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP_GHH) {
            return new BPP_GHH();
        } else if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.TSP) {
            return new TSP();
        } else if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.TSPTW) {
            return new TSPTW();
        } else {
            System.err.println(MethodName.getCurrentMethodName() + " ERROR - There is no problem such as " + Settings.OPTIMIZATION_PROBLEM.toString() + ".");
            System.exit(1);
            return null;
        }
    }

    /**
     * Initializes low-lew heuristics for actual optimization problem.
     *
     * @return Array of loaded heuristics.
     */
    public Heuristic[] initHeuristics() {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
        System.exit(1);
        return null;
    }

    /**
     * Initializes actions used in HH for actual optimization problem.
     *
     * @return Array of loaded heuristics.
     */
    public Heuristic[] initActions() {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
        System.exit(1);
        return null;
    }

    /**
     * Set all instances in directory.
     */
    public void getInstancesPaths() {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
    }

    /**
     * Picks best fitness values among all low level heuristics on every instance and stores it.
     */
    public void getBestLowLevelHeuristicFitnessValues() {
        double bestInstFitness;
        Heuristic.AVG_BEST_LLH_FITNESS = 0;
        //--- For all instances.
        for (int inst = 0; inst < Problem.NUMBER_OF_INSTANCES; inst++) {
            //--- Initialize border value according to fitness mini/maximization.
            if (Settings.FITNESS_MINIMIZATION) {
                bestInstFitness = Double.MAX_VALUE;
            } else {
                bestInstFitness = -Double.MAX_VALUE;
            }

            //--- Store best fitness values according to fitness mini/maximization.
            for (int llh = 0; llh < llhs.length; llh++) {

                if (Settings.FITNESS_MINIMIZATION) {
                    if (llhs[llh].instancesFitness[inst] < bestInstFitness) {
                        bestInstFitness = llhs[llh].instancesFitness[inst];
                    }
                } else {
                    if (llhs[llh].instancesFitness[inst] > bestInstFitness) {
                        bestInstFitness = llhs[llh].instancesFitness[inst];
                    }
                }
            }
            //--- Store best fitness for actual instance.
            INSTANCES[inst].bestHeuristicFitness = bestInstFitness;

            // System.out.println(MethodName.getCurrentMethodName() + " - Instance: " + inst + " | Best Finess: " + INSTANCES[inst].bestHeuristicFitness);

            Heuristic.AVG_BEST_LLH_FITNESS += bestInstFitness;
        }

        //--- Calculate average LLHs best fitness across all instances.
        Heuristic.AVG_BEST_LLH_FITNESS /= (double) (Problem.NUMBER_OF_INSTANCES);
    }

    /**
     * Automatically generates subgroups of instances.
     */
    public void generateInstancesSubgroups() {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
        System.exit(-1);
    }

    /**
     * Loads instances in the directory from method parameters.
     *
     * @param dir directory with instances.
     */
    public void loadInstances(String dir) {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
    }

    /**
     * Loads instances in the directory from method parameters.
     *
     * @param dir directory with instances.
     */
    public Instance loadInstance(String dir, boolean print) {
        System.err.println(MethodName.getCurrentMethodName() + " WARNING - Missing implementation! Override this method in correct child to perform some actions.");
        return null;
    }

    /**
     * Compares two fitness values and return the better one.
     *
     * @param newValue    new fitness value
     * @param actualValue actual best fitness value
     * @return better fitness value according to min/maximization criterion settings
     */
    public static double getBetterFitness(double newValue, double actualValue) {
        if (Settings.FITNESS_MINIMIZATION) {
            if (newValue < actualValue) return newValue;
            else return actualValue;
        } else {
            if (newValue > actualValue) return newValue;
            else return actualValue;
        }
    }

    /**
     * Returns boolean value indicating if the first argument is better fitness than the second.
     *
     * @param newValue new fitness value
     * @param refValue best fitness value known
     * @return true if newValue is better tahn refValue according to min/maximization criterion settings.
     */
    public static boolean isBetterFitness(double newValue, double refValue) {
        if (Settings.FITNESS_MINIMIZATION) {
            if (newValue < refValue) return true;
            else return false;
        } else {
            if (newValue > refValue) return true;
            else return false;
        }
    }
}
