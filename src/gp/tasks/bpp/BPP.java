package gp.tasks.bpp;

import gp.individuals.BPP_GHH_Individual;
import gp.tasks.Problem;
import heuristics.Heuristic;
import heuristics.bpp.*;
import sngp.MethodName;
import sngp.SNGP_HH_RL;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Fanda on 11. 3. 2016.
 */
public class BPP extends Problem {

    /**
     * Sorting items according to their weight.
     */
    public enum sorting {
        decreasing,
        increasing
    }
    public static sorting ITEMS_SORTING = sorting.decreasing;

    /**
     * Print information about heuristic progress?
     */
    public static boolean PRINT_LLHS_PROGRESS = false;

    /**
     * Evolved heuristics from the previous step.
     */
    //public static ArrayList<BPP_GHH_Individual> evolvedHeuristics = new ArrayList();

    /**
     * Constructor.
     */
    public BPP() {
        instancesTrainingDir = "INSTANCES/BPP/Training/";
        instancesTestingDir = "INSTANCES/BPP/Testing/";
        //--- Check if directory exists and create new one if it is not true.
        if(!new File("RESULTS\\" + this.getClass().getSimpleName() + "\\").isDirectory()) {
            new File ("RESULTS\\" + this.getClass().getSimpleName() + "\\").mkdir();
            System.out.println("OPTIMIZATION PROBLEM: Directory RESULTS\\" + this.getClass().getSimpleName() + "\\ created!");
        }
        outputDir = "RESULTS/" + this.getClass().getSimpleName() + "/";

        File temp = new File(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_IND_BEST.txt");
        temp = new File(SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_IND_INFIX.txt");
        System.out.println("OPTIMIZATION PROBLEM: File " + SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_IND_INFIX.txt" + " created!");
        System.out.println("OPTIMIZATION PROBLEM: File " + SNGP_HH_RL.resultsDirPath + SNGP_HH_RL.timeStampString + "_IND_BEST.txt" + " created!");

    }

    /**
     * Loads instances in the directory from method parameters.
     * @param dir directory with instances.
     */
    @Override
    public void loadInstances(String dir) {
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        int counter = 0;
        String n;
        //--- Count instances according its file extension.
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {

                n = listOfFiles[i].getName().substring(listOfFiles[i].getName().length() - 3, listOfFiles[i].getName().length());
                if (n.equals("bpp") || n.equals("BPP") || n.equals("txt")) counter++;
            }
        }
        //--- Store paths to instances.
        instancesPaths = new String[counter];
        counter = 0;
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                n = listOfFiles[i].getName().substring(listOfFiles[i].getName().length() - 3, listOfFiles[i].getName().length());
                if (n.equals("bpp") || n.equals("BPP") || n.equals("txt")) instancesPaths[counter++] = listOfFiles[i].getName();
            }
        }

        //--- Load all instance.
        counter = 0;
        BPP.INSTANCES = new BPPInstance[listOfFiles.length];
        Problem.NUMBER_OF_INSTANCES = listOfFiles.length;
        for(String sinst : instancesPaths) {
            BPP.INSTANCES[counter++] = loadInstance(dir + "/" + sinst, false);
        }

        System.out.println("INSTANCE: " + Problem.NUMBER_OF_INSTANCES + " instances successfully loaded!");

        this.generateInstancesSubgroups();
    }

    /**
     * Loads instance from file in parameter.
     * @param fileName file with instance.
     * @return
     */
    @Override
    public BPPInstance loadInstance(String fileName, boolean print) {
        BPPInstance inst = new BPPInstance();
        File file;
        String line;
        int lineCounter = 0;    // Var for better exporting of bugs in file format.

        boolean exists = new File(fileName).exists();

        // File exists?
        if (exists) {
            // Then prepare for loading.
            file = new File(fileName);
            if (file.isDirectory()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Input file path " + fileName + " leads only to directory.");
                System.exit(1);

            }

            //System.out.println("INSTANCE: Loading instance " + fileName + " ...");

        } else {
            // Else make a new populations.
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Cannot find file " + fileName + ".");
            System.exit(1);
            return null;
        }


        // Start loading evolution.
        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);
            try {
                try {

                    String nodes[];

                    //--- Name of the instance file.
                    nodes = fileName.split("/");
                    inst.name = nodes[nodes.length-1];
                    //--- Number of items.
                    line = br.readLine();
                    inst.size = Integer.parseInt(line.trim());
                    inst.items = new BPPItem[inst.size];
                    lineCounter++;
                    //--- Bin capacity.
                    line = br.readLine();
                    inst.binCapacity = Integer.parseInt(line.trim());
                    lineCounter++;
                    //--- Items and its weight.
                    for (int i = 0; i < inst.size; i++) {
                        line = br.readLine();
                        inst.items[i] = new BPPItem(Integer.parseInt(line.trim()));
                        inst.items[i].itemID = i;
                        //--- Max weight for some features.
                        if(inst.items[i].weight > inst.maxItemWeight) {
                            inst.maxItemWeight = inst.items[i].weight;
                        }
                        lineCounter++;
                    }

                    //--- Calc theoretical optimum value and fitness.
                    double opt = 0.0;
                    double sum = 0.0;
                    for (int i = 0; i < inst.size; i++) {
                        sum += inst.items[i].weight;
                    }
                    opt = sum / inst.binCapacity;
                    inst.theoreticalOptimumValue = opt;
                    opt = (int)(opt)+1;
                    inst.theoreticalOptimumFitness = opt;

                    //System.out.println(MethodName.getCurrentMethodName() + " " + inst.theoreticalOptimumValue);


                    //--- There are some special cases for the global optima solution quality.
                    if(inst.name.length() >= 12 && inst.name.charAt(0)=='F' && inst.name.charAt(11)=='t') {
                        inst.theoreticalOptimumFitness = inst.size/3.0;
                        //System.out.println(MethodName.getCurrentMethodName() + " " + inst.name + " " + inst.theoreticalOptimumFitness);
                    }
                    else if(inst.name.length() >= 12 && inst.name.charAt(0)=='F' && inst.name.charAt(11)=='u') {
                        if(inst.name.equals("Falkenauer_u120_08.txt")) {
                            inst.theoreticalOptimumFitness++;
                        }
                        if(inst.name.equals("Falkenauer_u120_19.txt")) {
                            inst.theoreticalOptimumFitness++;
                        }
                        if(inst.name.equals("Falkenauer_u250_07.txt")) {
                            inst.theoreticalOptimumFitness++;
                        }
                        if(inst.name.equals("Falkenauer_u250_12.txt")) {
                            inst.theoreticalOptimumFitness++;
                        }
                        if(inst.name.equals("Falkenauer_u250_13.txt")) {
                            inst.theoreticalOptimumFitness++;
                        }
                    }
                    //System.out.println(MethodName.getCurrentMethodName() + " " + inst.name + " " + inst.theoreticalOptimumFitness);

                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        System.out.println("\033[32m" + "INSTANCE: Instance " + fileName + " successfully loaded!" + "\033[0m");

        return inst;
    }

    /**
     * Initializes low-lew heuristics for actual optimization problem.
     * @return Array of loaded heuristics.
     */
    @Override
    public Heuristic[] initHeuristics() {
        ArrayList<Heuristic> llhs = new ArrayList();
        /*llhs.add(new FirstFit());
        llhs.add(new BestFit());
        llhs.add(new Pack2LargestItems());
        llhs.add(new PackLargestItem());
        llhs.add(new PackSmallestItem());
        llhs.add(new PackUpTo2LargestItems());
        llhs.add(new PackUpTo3LargestItems());
        llhs.add(new PackUpTo5LargestItems());/**/

        llhs.add(new DJD());
        //llhs.add(new ADJD());
        llhs.add(new FirstFitDecreasingOrig());
        llhs.add(new BestFitDecreasingOrig());
        llhs.add(new SumOfSquares());/**/
        llhs.add(new WorstFit());
        //llhs.add(new BestFitDescending());
        //llhs.add(new DJD());
        //llhs.add(new DJT());
        //llhs.add(new FirstFitDescending());
        //llhs.add(new ADJD());
        //lhs.add(new SumOfSquares());

        /*llhs.add(new WorstFit());
        llhs.add(new FirstFitIncreasing());
        //llhs.add(new DJT());
        llhs.add(new FirstFit());
        llhs.add(new BestFit());/**/
        Heuristic[] h = new Heuristic[llhs.size()];
        for (int i = 0; i < h.length; i++) {
            h[i] = llhs.get(i);
        }

        Problem.NUMBER_OF_LLHS = h.length;

        return h;
    }
    
    /** 
     * Initializes actions used in HH for actual optimization problem.
     * @return Array of loaded heuristics.
     */
    @Override
    public Heuristic[] initActions() {
        ArrayList<Heuristic> ac = new ArrayList();
        //ac.add(new Pack2LargestItems());
        //ac.add(new PackLargestItem());
        //ac.add(new PackSmallestItem());
        /*ac.add(new PackUpTo2LargestItems());
        ac.add(new PackUpTo3LargestItems());/**/

        //ac.add(new DJT());
        ac.add(new FirstFitDecreasingOrig());
        ac.add(new DJD());
        ac.add(new BestFitDecreasingOrig());
        ac.add(new SumOfSquares());/**/
        ac.add(new WorstFit());
        //ac.add(new PackUpTo5LargestItems());

        Heuristic[] h = new Heuristic[ac.size()];
        for (int i = 0; i < h.length; i++) {
            h[i] = ac.get(i);
        }

        Problem.NUMBER_OF_ACTIONS = h.length;

        return h;
    }
    
    /**
     * Load evolved heuristics.
     */
    public static void loadEvolvedHeuristics() {
        //--- Load and execute test individuals.
        BPP_GHH_Individual testInd = new BPP_GHH_Individual();
        testInd.loadIndividual("RESULTS/BPP_GHH/3/Ind_LBFIGZ_N1C1W1.txt");
        testInd.EXECUTE_ONLY_BEST_NODE = true;
        evolvedHeuristics.add(testInd.clone());
        testInd.loadIndividual("RESULTS/BPP_GHH/3/Ind_LBFIGZ_N1C1W2.txt");
        testInd.EXECUTE_ONLY_BEST_NODE = true;
        evolvedHeuristics.add(testInd.clone());
        testInd.loadIndividual("RESULTS/BPP_GHH/3/Ind_LBFIGZ_N1C1W4.txt");
        testInd.EXECUTE_ONLY_BEST_NODE = true;
        evolvedHeuristics.add(testInd.clone());
        testInd.loadIndividual("RESULTS/BPP_GHH/3/Ind_LBFIGZ_N2C1W1.txt");
        testInd.EXECUTE_ONLY_BEST_NODE = true;
        evolvedHeuristics.add(testInd.clone());
        testInd.loadIndividual("RESULTS/BPP_GHH/3/Ind_LBFIGZ_N2C2W1.txt");
        testInd.EXECUTE_ONLY_BEST_NODE = true;
        evolvedHeuristics.add(testInd.clone());
        testInd.loadIndividual("RESULTS/BPP_GHH/3/Ind_LBFIGZ_N2C3W1.txt");
        testInd.EXECUTE_ONLY_BEST_NODE = true;
        evolvedHeuristics.add(testInd.clone());
    }

    /**
     * Automatically generates subgroups of instances.
     */
    @Override
    public void generateInstancesSubgroups() {
        ArrayList<String> groups = new ArrayList();
        for (int inst = 0; inst < Problem.NUMBER_OF_INSTANCES; inst++) {
            //--- NXCYWZ instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'N'
                    && Problem.INSTANCES[inst].name.charAt(2) == 'C'
                    && Problem.INSTANCES[inst].name.charAt(4) == 'W') {
                if (!groups.contains("Scholl_1")) {
                    groups.add("Scholl_1");
                }
                Problem.INSTANCES[inst].instanceSubgroup = "Scholl_1";
            }
            //--- Hard instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'H') {
                if (!groups.contains("HARD")) {
                    groups.add("HARD");
                }
                Problem.INSTANCES[inst].instanceSubgroup = "HARD";
            }
            //--- NXWYBZRA instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'N'
                    && Problem.INSTANCES[inst].name.charAt(2) == 'W'
                    && Problem.INSTANCES[inst].name.charAt(4) == 'B') {
                if (!groups.contains("Scholl_2")) {
                    groups.add("Scholl_2");
                }
                Problem.INSTANCES[inst].instanceSubgroup = "Scholl_2";
            }
            //--- FalkenauerU instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'F'
                    && Problem.INSTANCES[inst].name.charAt(11) == 'u') {
                if (!groups.contains("Falk_U")) {
                    groups.add("Falk_U");
                }
                Problem.INSTANCES[inst].instanceSubgroup = "Falk_U";
            }
            //--- FalkenauerT instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'F'
                    && Problem.INSTANCES[inst].name.charAt(11) == 't') {
                if (!groups.contains("Falk_T")) {
                    groups.add("Falk_T");
                }
                Problem.INSTANCES[inst].instanceSubgroup = "Falk_T";
            }
        }

        //--- Convert ArrayList to Array.
        Problem.instanceSubgroups = new String[groups.size()];
        for(int s = 0; s < groups.size(); s++) {
            Problem.instanceSubgroups[s] = groups.get(s);
        }
    }

    /**
     * Automatically generates subgroups of instances.
     * OLDER version with more detailed subgroups.
     */
    public void generateInstancesSubgroupsOLD() {
        ArrayList<String> groups = new ArrayList();
        for (int inst = 0; inst < Problem.NUMBER_OF_INSTANCES; inst++) {
            //--- NXCYWZ instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'N'
                    && Problem.INSTANCES[inst].name.charAt(2) == 'C'
                    && Problem.INSTANCES[inst].name.charAt(4) == 'W') {
                if (!groups.contains(Problem.INSTANCES[inst].name.substring(0, 6))) {
                    groups.add(Problem.INSTANCES[inst].name.substring(0, 6));
                }
                Problem.INSTANCES[inst].instanceSubgroup = Problem.INSTANCES[inst].name.substring(0, 6);
            }
            //--- Hard instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'H') {
                if (!groups.contains("HARD")) {
                    groups.add("HARD");
                }
                Problem.INSTANCES[inst].instanceSubgroup = "HARD";
            }
            //--- NXWYBZRA instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'N'
                    && Problem.INSTANCES[inst].name.charAt(2) == 'W'
                    && Problem.INSTANCES[inst].name.charAt(4) == 'B') {
                if (!groups.contains(Problem.INSTANCES[inst].name.substring(0, 6))) {
                    groups.add(Problem.INSTANCES[inst].name.substring(0, 6));
                }
                Problem.INSTANCES[inst].instanceSubgroup = Problem.INSTANCES[inst].name.substring(0, 6);
            }
            //--- FalkenauerU instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'F'
                    && Problem.INSTANCES[inst].name.charAt(11) == 'u') {
                if (!groups.contains("FalkU_" + Problem.INSTANCES[inst].size)) {
                    groups.add("FalkU_" + Problem.INSTANCES[inst].size);
                }
                Problem.INSTANCES[inst].instanceSubgroup = "FalkU_" + Problem.INSTANCES[inst].size;
            }
            //--- FalkenauerT instances.
            if (Problem.INSTANCES[inst].name.charAt(0) == 'F'
                    && Problem.INSTANCES[inst].name.charAt(11) == 't') {
                if (!groups.contains("FalkT_" + Problem.INSTANCES[inst].size)) {
                    groups.add("FalkT_" + Problem.INSTANCES[inst].size);
                }
                Problem.INSTANCES[inst].instanceSubgroup = "FalkT_" + Problem.INSTANCES[inst].size;
            }
        }

        //--- Convert ArrayList to Array.
        Problem.instanceSubgroups = new String[groups.size()];
        for(int s = 0; s < groups.size(); s++) {
            Problem.instanceSubgroups[s] = groups.get(s);
        }
    }
}
