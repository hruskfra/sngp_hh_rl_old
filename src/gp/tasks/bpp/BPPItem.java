package gp.tasks.bpp;

import sngp.MethodName;

/**
 * Created by Fanda on 11. 3. 2016.
 */
public class BPPItem implements Comparable, Cloneable{

    public double weight = -1;
    public int allocatedBin = -1;
    public int itemID = -1;

    public BPPItem() {

    }

    public BPPItem(double w) {
        this.weight =w;
    }

    @Override
    public BPPItem clone(){
        BPPItem it = new BPPItem();
        it.weight = this.weight;
        it.allocatedBin = this.allocatedBin;
        it.itemID = this.itemID;
        return it;
    }

    @Override
    public int compareTo(Object o) {
        BPPItem it = (BPPItem)(o);

        if(BPP.sorting.decreasing == BPP.ITEMS_SORTING) {
            if (this.weight > it.weight) {
                return -1;
            }
            else if(this.weight < it.weight) {
                return 1;
            }
            else return 0;
        }
        if(BPP.sorting.increasing == BPP.ITEMS_SORTING) {
            if (this.weight > it.weight) {
                return 1;
            }
            else if(this.weight < it.weight) {
                return -1;
            }
            else return 0;
        }
        System.out.println(MethodName.getCurrentMethodName() + " ERROR - Should not be here! Sorting type: " + BPP.ITEMS_SORTING + " " + BPP.sorting.decreasing + " " + (BPP.ITEMS_SORTING == BPP.sorting.decreasing) + " " + this.weight + " " + it.weight) ;
        return 0;
    }
}