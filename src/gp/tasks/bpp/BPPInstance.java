package gp.tasks.bpp;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import gp.tasks.Instance;
import gp.tasks.Problem;
import heuristics.Heuristic;
import sngp.*;
/**
 * Created by Fanda on 11. 3. 2016.
 */
public class BPPInstance extends Instance implements Cloneable {

    public double binCapacity = -1;
    public BPPItem[] items;
    public ArrayList<BPPBin> bins = new ArrayList();
    public int currentBin = -1;
    public double maxItemWeight = -1;
    public BPPItem actualItem = null;

    /**
     * Restarts whole instance.
     */
    @Override
    public void resetInstance() {
        this.bins = new ArrayList();
        for(int i = 0; i < this.items.length; i++) {
            items[i].allocatedBin = -1;
        }
    }

    /**
     * Creates and opens new bin.
     */
    public void createNewBin() {
        this.bins.add(this.bins.size(), new BPPBin(this.bins.size(), this.binCapacity));
        this.currentBin = this.bins.size()-1;
    }

    /**
     * Creates and opens new bin.
     * @param it item from the instance
     */
    public void createNewBin(BPPItem it) {
        this.bins.add(this.bins.size(), new BPPBin(this.bins.size(), this.binCapacity));
        this.bins.get(this.bins.size()-1).addItem(it);
        this.currentBin = this.bins.size()-1;
    }

    /**
     * Collects and returns all unassigned items in this instance.
     * @return ArrayList of unassigned items
     */
    public ArrayList<BPPItem> getUnpackedItems() {
        ArrayList<BPPItem> fi = new ArrayList();
        for(int i = 0; i < this.items.length; i++) {
            if(this.items[i].allocatedBin == -1) {
                fi.add(this.items[i]);
            }
        }
        /*if(fi.isEmpty()) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - No free items left!");
            System.out.println(MethodName.getCurrentMethodStack());
        }/**/
        return fi;
    }

    /**
     * Returns largest unpacked item.
     * @return
     */
    public BPPItem getLargestUnpackedItem() {
        ArrayList<BPPItem> fi = getUnpackedItems();
        if(fi.isEmpty()) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There are not any free items in the instance!");
            return null;
        }
        double maxWeight = 0.0;
        BPPItem it = null;
        for(int i = 0; i < fi.size(); i++) {
            if(fi.get(i).weight>maxWeight) {
                maxWeight = fi.get(i).weight;
                it = fi.get(i);
            }
        }
        return it;
    }

    /**
     * Returns smallest unpacked item.
     * @return
     */
    public BPPItem getSmallestUnpackedItem() {
        ArrayList<BPPItem> fi = getUnpackedItems();
        if(fi.isEmpty()) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There are not any free items in the instance!");
            return null;
        }
        double minWeight = Double.MAX_VALUE;
        BPPItem it = null;
        for(int i = 0; i < fi.size(); i++) {
            if(fi.get(i).weight < minWeight) {
                minWeight = fi.get(i).weight;
                it = fi.get(i);
            }
        }
        return it;
    }

    /**
     * Returns 2 largest unpacked item.
     * @return
     */
    public BPPItem[] get2LargesUnpackedItem() {
        ArrayList<BPPItem> fi = getUnpackedItems();
        if(fi.isEmpty()) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There are not any free items in the instance!");
            return null;
        }
        if(fi.size() < 2) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There in not enough items in the instance!");
            return null;
        }
        double maxWeight = 0.0;
        BPPItem[] it = new BPPItem[2];

        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);
        it[0] = fi.get(0);
        it[1] = fi.get(1);

        return it;
    }

    /**
     * Prints instances parameters.
     */
    @Override
    public void printInstance() {
        System.out.println("===\r\nName: " + this.name);
        System.out.println("Bin capacity: " + this.binCapacity);
        System.out.println("#Items: " + this.items.length);
        System.out.print("Weights: ");
        for(int i = 0; i < this.items.length; i++)  {
            System.out.print(this.items[i].weight + " ");
        }
        System.out.println();
    }

    /**
     * Prints final solution obtained on this instance.
     */
    @Override
    public void printSolution() {

        /*for(int b = 0; b < this.bins.size(); b++) {
            System.out.println("Bin: " + b);
            for(int it = 0 ; it < this.bins.get(b).allocatedItems.size(); it++) {
                System.out.println("Bin: " + b + " | Item: " + this.bins.get(b).allocatedItems.get(it) + " | Weight: " + this.items[this.bins.get(b).allocatedItems.get(it)].weight);
            }
        }/**/

        System.out.println("===\r\nName: " + this.name);
        for(int b = 0; b < this.bins.size(); b++) {
            System.out.print("Bin: " + b + " | Items: ");
            for(int it = 0;it < this.bins.get(b).allocatedItems.size(); it++) {
                System.out.print(this.bins.get(b).allocatedItems.get(it) + " (" + this.items[this.bins.get(b).allocatedItems.get(it)].weight +")");
                if(it < this.bins.get(b).allocatedItems.size()-1) System.out.print(", ");
            }
            System.out.println("| Fullness: " + (this.binCapacity - this.bins.get(b).freeSpace) + "/" + this.binCapacity);
        }

        System.out.println("Theoretical optimum: " + this.theoreticalOptimumFitness + " (" + theoreticalOptimumValue + ")");
        System.out.println();
    }

    /**
     * Checks the solution for errors.
     * @return true if solution is without errors
     */
    @Override
    public boolean checkSolution() {

        boolean correct = true;

        int itemsCounter[] = new int[this.items.length];
        Arrays.fill(itemsCounter, 0);

        for(int b = 0; b < this.bins.size(); b++) {
            for(int it = 0; it < this.bins.get(b).allocatedItems.size(); it++) {
                itemsCounter[this.bins.get(b).allocatedItems.get(it)]++;
            }
            if(this.bins.get(b).allocatedItems.isEmpty()) {
                System.err.println("ERROR - Bin " + b + " has not allocated any items!");
                correct = false;
            }
            if(this.bins.get(b).freeSpace == this.binCapacity) {
                System.err.println("ERROR - Bin " + b + " has free space equal to the bin capacity!");
                correct = false;
            }
            if(this.bins.get(b).freeSpace < 0) {
                System.err.println("ERROR - Bin " + b + " has allocated more weight than it should have (free space = " + this.bins.get(b).freeSpace + ")!");
                correct = false;
            }
        }
        for(int it = 0; it < this.items.length; it++) {
            if(itemsCounter[it] == 0) {
                System.err.println("ERROR - Item " + it + " was not added into the bin!");
                correct = false;
            }
            if(itemsCounter[it] > 1) {
                System.err.println("ERROR - Item " + it + " was added multiple times!");
                correct = false;
            }
            if(this.items[it].allocatedBin == -1) {
                System.err.println("ERROR - Item " + it + " has not allocated any bin!");
                correct = false;
            }
        }
        if(!correct) {
            this.printSolution();
            System.exit(1);
        }

        return correct;

    }
    
    /**
     * Deep clone of the instance.
     * @return
     */
    @Override
    public BPPInstance clone() {
        BPPInstance inst = new BPPInstance();
        inst.size = this.size;
        inst.binCapacity = this.binCapacity;
        inst.currentBin = this.currentBin;
        inst.name = this.name;
        inst.theoreticalOptimumFitness = this.theoreticalOptimumFitness;
        inst.theoreticalOptimumValue = this.theoreticalOptimumValue;

        inst.items = new BPPItem[this.items.length];
        for (int i = 0; i < inst.items.length; i++) {
            inst.items[i] = this.items[i].clone();
        }
        inst.bins = new ArrayList();
        for (int i = 0; i < this.bins.size(); i++) {
            inst.bins.add(i, this.bins.get(i).clone());
        }
        return inst;
    }
}
