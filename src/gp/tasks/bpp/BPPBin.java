package gp.tasks.bpp;

import java.util.ArrayList;
import sngp.*;

/**
 * Created by Fanda on 11. 3. 2016.
 */
public class BPPBin  implements Cloneable{

    public int binID = -1;
    public double freeSpace = -1;
    public double binCapacity = -1;
    public ArrayList<Integer> allocatedItems = new ArrayList();

    public BPPBin() {}

    public BPPBin(int id, double c) {
        this.binID = id;
        this.binCapacity = c;
        this.freeSpace = this.binCapacity;
    }

    public BPPBin clone() {
        BPPBin b = new BPPBin(this.binID, this.binCapacity);
        b.binID = this.binID;
        b.freeSpace = this.freeSpace;
        b.allocatedItems = new ArrayList();
        for(int i = 0; i < this.allocatedItems.size(); i++) {
            b.allocatedItems.add(i, this.allocatedItems.get(i));
        }
        return b;
    }

    /**
     * Places item into the bin if it is possible.
     * @param it item to be placed
     */
    public void addItem(BPPItem it) {
        //--- Check if item it is not already assigned here or in another bin.
        if (!this.canBePlaced(it)) {
            System.out.println(MethodName.getCurrentMethodName() + " ERROR occuured while placing item " + it.itemID + " to the bin " + this.binID + ": \r\n" +
                    "This item is already assigned in this bin (ID: " + this.binID + ")! \r\n" +
                    "This item has already assigned another bin (ID: " + it.allocatedBin + ")\"\r\n" +
                    "This item has larger weight than the bin's remaining free space (" + it.weight + " > " + this.freeSpace + ")!");

            return;
        }
        //--- Assign item it to this bin and adjust free capacity.
        this.allocatedItems.add(it.itemID);
        it.allocatedBin = this.binID;
        this.freeSpace -= it.weight;

        if (BPP.PRINT_LLHS_PROGRESS) {
            System.out.println(MethodName.getCurrentMethodName() + " Item " + it.itemID + " with weight " + it.weight + " has been placed in the bin " + this.binID + " (Free space: " + this.freeSpace + ")");
        }
    }

    public boolean canBePlaced(BPPItem it) {
        //--- Check if item it is not already assigned here or in another bin.
        if(this.allocatedItems.contains(it)) {
            return false;
        }
        if(it.allocatedBin != -1) {
            return false;
        }
        //--- Check if item is not too large for the rest of the bin capacity.
        if(it.weight > this.freeSpace) {
            return false;
        }
        return true;
    }
}
