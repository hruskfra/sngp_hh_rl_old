/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.tasks.cvrp;

import gp.tasks.Problem;
import loaders.tsp.LoadedInstance;
import loaders.tsp.VehicleRoutingTable;
import heuristics.Heuristic;
import heuristics.cvrp.*;
import sngp.*;


import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author fanda
 */
public class CVRP extends Problem {
    
    /**
     * Indicates if CVRP is symmetrical.
     */
    public static boolean SYMMETRICAL = true;

    /**
     * Constructor.
     */
    public CVRP() {
        instancesTrainingDir = "INSTANCES/" + this.getClass().getSimpleName() + "/Training/";
        instancesTestingDir = "INSTANCES/" + this.getClass().getSimpleName() + "/Testing/";
        //--- Check if directory exists and create new one if it is not true.
        if(!new File("RESULTS\\" + this.getClass().getSimpleName() + "\\").isDirectory()) {
            new File ("RESULTS\\" + this.getClass().getSimpleName() + "\\").mkdir();
            System.out.println("OPTIMIZATION PROBLEM: Directory RESULTS\\" + this.getClass().getSimpleName() + "\\ created!");
        }
        outputDir = "RESULTS/" + this.getClass().getSimpleName() + "/";
    }

    /**
     * Initializes low-lew heuristics for actual optimization problem.
     * @return Array of loaded heuristics.
     */
    @Override
    public Heuristic[] initHeuristics() {
        ArrayList<Heuristic> llhs = new ArrayList();
        
        llhs.add(new ClarkeWright());
        llhs.add(new Kilby());
        llhs.add(new MoleJameson());
        
        Heuristic[] h = new Heuristic[llhs.size()];
        for (int i = 0; i < h.length; i++) {
            h[i] = llhs.get(i);
        }

        Problem.NUMBER_OF_LLHS = h.length;

        return h;
    }
    
    /** 
     * Initializes actions used in HH for actual optimization problem.
     * @return Array of loaded heuristics.
     */
    @Override
    public Heuristic[] initActions() {
        ArrayList<Heuristic> ac = new ArrayList();
        ac.add(new ClarkeWright());
        ac.add(new Kilby());
        ac.add(new MoleJameson());
        ac.add(new AlgCW1());
        ac.add(new AlgCW3());
        ac.add(new AlgCW7());

        Heuristic[] h = new Heuristic[ac.size()];
        for (int i = 0; i < h.length; i++) {
            h[i] = ac.get(i);
        }

        Problem.NUMBER_OF_ACTIONS = h.length;

        return h;
    }
    
    /**
     * Loads instances in the directory from method parameters.
     * @param dir directory with instances.
     */
    @Override
    public void loadInstances(String dir) {
        /**
         * Set all instances in directory.
         */
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        int counter = 0;
        String n;
        //--- Count instances according its file extension.
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {

                n = listOfFiles[i].getName().substring(listOfFiles[i].getName().length() - 3, listOfFiles[i].getName().length());
                if (n.equals("vrp") || n.equals("VRP")) counter++;
            }
        }
        //--- Store paths to instances.
        instancesPaths = new String[counter];
        counter = 0;
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                n = listOfFiles[i].getName().substring(listOfFiles[i].getName().length() - 3, listOfFiles[i].getName().length());
                if (n.equals("vrp") || n.equals("VRP")) instancesPaths[counter++] = listOfFiles[i].getName();
            }
        }

        //--- Load all instances.
        counter = 0;
        Problem.NUMBER_OF_INSTANCES = listOfFiles.length;
        Problem.INSTANCES = new CVRPInstance[listOfFiles.length];
        for(String sinst : instancesPaths) {
            Problem.INSTANCES[counter] = loadInstance(dir + "/" + sinst, true);
            counter++;
        }
        System.out.println("INSTANCE: " + Problem.NUMBER_OF_INSTANCES + " instances successfully loaded!");
    }

    /**
     * Loads instance from the file.
     *
     * @param path path to the file
     * @param print @code true if information about progress will be print
     *
     * @return object of the CVRPInstance
     */
    public CVRPInstance loadInstanceOldMethod(String path, boolean print) {

        CVRPInstance instance = new CVRPInstance(1);
        File file;
        String line;
        int lineCounter = 0;    // Var for better exporting of bugs in file format.

        boolean exists = new File(path).exists();

        // File exists?
        if (exists) {
            // Then prepare for loading.
            file = new File(path);
            if (file.isDirectory()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Input file path " + path + " leads only to directory.");
                System.exit(1);

            }
            if (print) {
                System.out.println("INSTANCE: Found file " + path + ". Loading instance...");
            }
        } else {
            // Else make a new populations.
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Cannot find file " + path + ".");
            System.exit(1);
            return null;
        }


        // Start loading evolution.
        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);
            try {
                try {

                    String nodes[];

                    //
                    line = br.readLine();
                    lineCounter++;
                    //
                    line = br.readLine();
                    lineCounter++;
                    //
                    line = br.readLine();
                    lineCounter++;
                    //
                    line = br.readLine();
                    lineCounter++;

                    //System.out.println(i + ") " + fitness + "(" + bestIndividualIndex + ")");
                    //System.out.println(i + ") " + line);
                    if (line.isEmpty()) {
                        System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Bad format of input file. Unexpected new empty line at line " + (lineCounter - 1) + ".");
                        System.exit(1);
                    }

                    //--- Split strings in line.
                    nodes = line.split(" ");
                    instance = new CVRPInstance(Integer.parseInt(nodes[2]));
                    String[] n = path.split("/");
                    instance.name = n[n.length-1];
                    if (print) {
                        System.out.println("INSTANCE: Instance " + instance.name + " size set to " + instance.size);
                    }

                    //---
                    line = br.readLine();
                    lineCounter++;
                    String distCalc = "";
                    nodes = line.split(" ");
                    distCalc = nodes[2];
                    if (!distCalc.equals("EUC_2D")) {
                        return null;
                    }

                    //---
                    line = br.readLine();
                    lineCounter++;
                    nodes = line.split(" ");
                    instance.vehicleCapacity = Double.parseDouble(nodes[2]);
                    if (print) {
                        System.out.println("INSTANCE: Vehicle capacity set to " + instance.vehicleCapacity);
                    }
                    //---  Node coordinates.
                    line = br.readLine();
                    lineCounter++;

                    //--- Nodes positions.
                    int numNodes = instance.size;
                    double nodesPos[][] = new double[numNodes][2];
                    instance.distances = new double[numNodes][numNodes];
                    instance.cities = new Vector[numNodes];
                    instance.demands = new double[numNodes];

                    for (int i = 0; i < numNodes; i++) {
                        //System.out.println(line);
                        line = br.readLine();
                        StringTokenizer st = new StringTokenizer(line);
                        String s = st.nextToken();
                        s = st.nextToken();
                        nodesPos[i][0] = Double.parseDouble(s);
                        s = st.nextToken();
                        nodesPos[i][1] = Double.parseDouble(s);
                        // Store city.
                        double[] v = {nodesPos[i][0], nodesPos[i][1]};
                        instance.cities[i] = new Vector(v);
                    }

                    // Calculate distances.
                    for (int i = 0; i < numNodes; i++) {
                        for (int j = i; j < numNodes; j++) {
                            if (i == j) {
                                instance.distances[i][j] = 0.0;
                                continue;
                            }
                            instance.distances[i][j] = instance.distances[j][i] = Math.sqrt(
                                    Math.abs(nodesPos[i][0] - nodesPos[j][0]) * Math.abs(nodesPos[i][0] - nodesPos[j][0]) +
                                            Math.abs(nodesPos[i][1] - nodesPos[j][1]) * Math.abs(nodesPos[i][1] - nodesPos[j][1])
                            );
                        }
                    }

                    line = br.readLine();
                    String[] s = line.split(" ");
                    if (!s[0].equals("DEMAND_SECTION")) {
                        System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Bad format of input file. Expected DEMAND_SECTION String.");
                        System.exit(1);
                    }

                    for (int i = 0; i < numNodes; i++) {
                        line = br.readLine();
                        StringTokenizer st = new StringTokenizer(line);
                        String ss = st.nextToken();
                        ss = st.nextToken();
                        instance.demands[i] = Double.parseDouble(ss);
                        instance.sumDemands += instance.demands[i];
                    }

                    //--- Calculate sum of all demands.
                    instance.sumDemands = instance.calcDemandsSum();
                    instance.sumUnassignedDemands = instance.calcUnassignedDemandsSum();
                    //--- Calculate some distance statistics.
                    instance.maxDistanceBetweenCities = instance.calcMaxDistanceBetweenCities();
                    instance.maxDistanceBetweenUnassignedCities = instance.calcMaxDistanceBetweenUnassignedCities();
                    instance.minDistanceBetweenCities = instance.calcMinDistanceBetweenCities();
                    instance.minDistanceBetweenUnassignedCities = instance.calcMinDistanceBetweenUnassignedCities();
                    instance.maxDistanceFromDepot = instance.calcMaxDistanceFromDepot();
                    instance.maxDistanceUnassignedFromDepot = instance.calcMaxDistanceUnassignedFromDepot();
                    instance.minDistanceFromDepot = instance.calcMinDistanceFromDepot();
                    instance.minDistanceUnassignedFromDepot = instance.calcMinDistanceUnassignedFromDepot();
                    instance.worstSolutionDistance = instance.calcWorstSolutionDistance();
                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        if (print) {
            System.out.println("INSTANCE: " + "Instance successfully loaded!" + "\033[0m");
        }
        return instance;
    }

    /**
     * Loads instance from the file using TSPLIB4J library.
     *
     * @param path path to the file
     * @param print @code true if information about progress will be print
     *
     * @return object of the CVRPInstance
     */
    @Override
    public CVRPInstance loadInstance(String path, boolean print) {

        try {
            return getInstanceFromLoader(new LoadedInstance(new File(path)));
        }
        catch(IOException IOE) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - File " + path + " does not exist!");
            System.exit(1);
        };

        System.exit(1);
        return null;
    }

    /**
     * Loads instance using TSPLIB4J library and transforms it to only necessary structure used in SNGP.
     *
     * @param loadedInst instance loaded using TSPLIB4J
     *
     * @return CVRPInstance object used in SNGP optimization process
     */
    public static CVRPInstance getInstanceFromLoader(LoadedInstance loadedInst) {
        CVRPInstance inst = new CVRPInstance(loadedInst.getDimension());

        inst.vehicleCapacity = loadedInst.getCapacity();
        inst.distances = loadedInst.getDistanceMatrix();
        inst.name = loadedInst.getName();

        VehicleRoutingTable vrt = loadedInst.getVehicleRoutingTable();
        inst.demands = vrt.getDemenadsArray();

        //--- Calculate sum of all demands.
        inst.sumDemands = inst.calcDemandsSum();
        inst.sumUnassignedDemands = inst.calcUnassignedDemandsSum();
        //--- Calculate some distance statistics.
        inst.maxDistanceBetweenCities = inst.calcMaxDistanceBetweenCities();
        inst.maxDistanceBetweenUnassignedCities = inst.calcMaxDistanceBetweenUnassignedCities();
        inst.minDistanceBetweenCities = inst.calcMinDistanceBetweenCities();
        inst.minDistanceBetweenUnassignedCities = inst.calcMinDistanceBetweenUnassignedCities();
        inst.maxDistanceFromDepot = inst.calcMaxDistanceFromDepot();
        inst.maxDistanceUnassignedFromDepot = inst.calcMaxDistanceUnassignedFromDepot();
        inst.minDistanceFromDepot = inst.calcMinDistanceFromDepot();
        inst.minDistanceUnassignedFromDepot = inst.calcMinDistanceUnassignedFromDepot();
        inst.worstSolutionDistance = inst.calcWorstSolutionDistance();

        return inst;
    }
}
