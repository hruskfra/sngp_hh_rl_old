/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.tasks.cvrp;

import gp.Population;
import sngp.*;

import java.util.ArrayList;

/**
 * @author Fanda
 */
public class CVRPThread extends Thread implements Runnable {

    /**
     * Indicates which population it should execute.
     */
    public static boolean PARENTS = true;
    /**
     * ID of current thread.
     */
    public int threadID;
    /**
     * Number of current individual for execution.
     */
    public int individualNumber;
    /**
     * Individual numbers.
     */
    public ArrayList<Integer> assignedIndividuals = new ArrayList();
    /**
     * If it can be redistributed
     */
    public static boolean CAN_BE_REDISTRIBUTED = true;
    public boolean WAIT_FOR_MUTEX = false;
    public boolean WAIT_FOR_THREADS = false;
    public static boolean CRITICAL_SECTION = false;

    public long startThTime = System.currentTimeMillis();
    public long actualThTime = System.currentTimeMillis();

    /**
     * States of the thread.
     */
    public enum STATE {

        WAIT, RECOMBINE, EXECUTE;
    }

    public volatile STATE THREAD_STATE = STATE.WAIT;

    /**
     * Constructor.
     */
    public CVRPThread(int id) {
        this.threadID = id;
        this.individualNumber = id;

        //System.out.println("Thread " + this.threadID + ": CREATED.");
    }

    public void restartThread(int id) {
        this.threadID = id;
        this.individualNumber = id;
    }

    @Override
    public String toString() {
        String s = "Thread: " + this.threadID + " | Assigned: ";
        for (int ind : assignedIndividuals) {
            s += ind + " ";
        }
        return s;
    }

    /**
     * Redistributes individuals numbers into all allocated threads.
     *
     * @param t              Threads
     * @param runningThreads number of threads
     * @param newRun         indicates if this method should allocate whole population or reallocate remaining numbers.
     */
    public static synchronized void redistributeIndividuals(CVRPThread[] t, int runningThreads, boolean newRun) {
        // New round, assign individuals to ArrayList.
        if (newRun) {
            for (int th = 0; th < t.length; th++) {
                t[th].assignedIndividuals = new ArrayList();
            }
            for (int i = 0; i < Settings.POPULATION_SIZE; i += runningThreads) {

                if (i >= Settings.POPULATION_SIZE) {
                    break;
                }
                for (int th = 0; th < runningThreads; th++) {
                    if(i+th == 0 && Population.generationsCounter > 0) {
                        continue;
                    }
                    if (i + th < Settings.POPULATION_SIZE) {
                        t[th].assignedIndividuals.add(i + th);
                    }
                    //System.out.println(MethodName.getCurrentMethodName() + " " + t[th].threadID + " added " + (i+th));
                }
            }
            return;
        }
        if (!newRun) {
            CVRPThread.CRITICAL_SECTION = true; // Lock the critical section.

            int[] inds = new int[t.length];
            int[] newSizes = new int[t.length];
            int oldSum = 0;
            int newSum = 0;
            int perThread = 0;
            int rest = 0;
            ArrayList<Integer> freeInds = new ArrayList();
            for (int th = 0; th < t.length; th++) {
                inds[th] = t[th].assignedIndividuals.size();
                oldSum += inds[th];
            }
            perThread = oldSum / t.length;
            rest = oldSum - (perThread * t.length);
            for (int i = 0; i < t.length; i++) {
                //System.out.println(MethodName.getCurrentMethodName() + "\r\nBefore\r\n" + t[i].toString());
                newSizes[i] = perThread;
                if (i < rest) {
                    newSizes[i]++;
                }
            }
                                /*System.out.println(MethodName.getCurrentMethodName());
                                for(int pp : newSizes) {
								System.out.print(pp + " ");
								}
								System.out.println();/**/

            //System.out.println(MethodName.getCurrentMethodName() + " perThread " + perThread);
            // Do not redistribute if there are not enough individuals to execute.
            if (perThread <= 1) {
                CVRPThread.CAN_BE_REDISTRIBUTED = false;
                CVRPThread.CRITICAL_SECTION = false; // Leave the critical section!
                return;
            }

            // Remove all individuals in above average size threads.
            for (int i = 0; i < t.length; i++) {
                if (t[i].assignedIndividuals.size() > newSizes[i]) {
                    int s = t[i].assignedIndividuals.size();
                    for (int j = newSizes[i]; j < s; j++) {
                        freeInds.add(t[i].assignedIndividuals.get(j));
                    }
                    for (int j = newSizes[i]; j < s; j++) {
                        t[i].assignedIndividuals.remove(newSizes[i]);
                    }
                }
            }
            //System.out.println(MethodName.getCurrentMethodName() + " " + freeInds.toString());
            // Assign new individuals to new threads.
            for (int i = 0; i < t.length; i++) {
                // Skip all full threads
                if (t[i].assignedIndividuals.size() >= newSizes[i]) {
                    //System.out.println(MethodName.getCurrentMethodName() + "\r\nAfter\r\n" + t[i].toString());
                    newSum += t[i].assignedIndividuals.size();
                    continue;
                }
                // Fil the free threads.
                for (int j = t[i].assignedIndividuals.size(); j < newSizes[i]; j++) {
                    t[i].assignedIndividuals.add(freeInds.get(0));
                    freeInds.remove(0);
                }
                //System.out.println(MethodName.getCurrentMethodName() + "\r\nAfter\r\n" + t[i].toString());
                newSum += t[i].assignedIndividuals.size();
            }

            if (!freeInds.isEmpty()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There are some free individuals after redistribution!");
                System.out.println(MethodName.getCurrentMethodName() + ": Free individuals - " + freeInds.toString());
                System.exit(1);
            }
            if (newSum != oldSum) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - There is another count of individuals in all threads after redistribution! (" + oldSum + " != " + newSum + ") ");
                System.exit(1);
            }

            CVRPThread.CRITICAL_SECTION = false; // Leave critical section.
        }
    }

    /**
     * Override method.
     */
    @Override
    public void run() {

        int s;
        while (true) {
            /*if (THREAD_STATE == STATE.EXECUTE) {
                System.out.println(MethodName.getCurrentMethodName() + " +THR " + this.threadID + " " + THREAD_STATE.toString());
            }/**/

            /*if (System.currentTimeMillis() - actualThTime > 10E2) {
                System.out.println(MethodName.getCurrentMethodName() + " ++THR " + this.threadID + " " + THREAD_STATE.toString());
                actualThTime = System.currentTimeMillis();
            }/**/

            if (THREAD_STATE == STATE.WAIT) {
                // Waiting loop.
            } /*else if (THREAD_STATE == STATE.RECOMBINE) {
                int p1, p2;
                int o1, o2;
                ArrayList<Node>[] a = new ArrayList[2];
                for (int pop = 0; pop < this.assignedIndividuals.size(); pop += 2) {
                    // Last even individual in ArrayList, mutate it.
                    if (pop == this.assignedIndividuals.size() - 1) {
                        o1 = this.assignedIndividuals.get(pop);
                        p1 = Selection.tournamentSelection2(Population.parents, -1);

                        // Create new mutated individual.
                        if (Settings.SUBTREE_MUTATION) {
                            a[0] = Mutation.subTreeMutation(Population.parents[p1].tree);
                        }
                        if (Settings.NODE_REPLACEMENT_MUTATION) {
                            a[0] = Mutation.nodeReplacementMutation(Population.parents[p1].tree);
                        }
                        if (Settings.HOIST_MUTATION) {
                            a[0] = Mutation.hoistMutation(Population.parents[p1].tree);
                        }
                        // Store new individuals.
                        Population.offsprings[o1].tree = a[0];
                        Population.offsprings[o1].startingGeneration = Population.generationsCounter - 1;
                        // Connect its nodes.
                        Population.offsprings[o1].connectChildren(0, 0);
                        Population.offsprings[o1].connectParents(Population.offsprings[o1].tree.get(0));
                        Population.offsprings[o1].getMaxDepth();
                        Population.offsprings[o1].setNodeTypes();
                        //CVRP_GP_CW.mutation(Population.offsprings[p1]);
                    }
                    // Select two offsprings to recombination.
                    else {
                        o1 = this.assignedIndividuals.get(pop);
                        o2 = this.assignedIndividuals.get(pop + 1);
                        p1 = Selection.tournamentSelection2(Population.parents, -1);
                        p2 = Selection.tournamentSelection2(Population.parents, p1);
                        if (Math.random() < Settings.CROSSOVER_PROB) {
                            // Apply crossover operator.
                            if (Settings.COMMON_CROSSOVER) a = Crossover.crossover(p1, p2);
                            if (Settings.WEIGHTED_CROSSOVER) a = Crossover.weightedCrossover(p1, p2);
                            if (Settings.ONE_POINT_CROSSOVER) {
                                a = Crossover.onePointCrossover(p1, p2);
                                if (a[0].size() == 0) {
                                    System.out.println(MethodName.getCurrentMethodName() + ": Clonning");
                                    System.exit(1);
                                }
                            }
                            if (Settings.UNIFORM_CROSSOVER) a = Crossover.uniformCrossover(p1, p2);

                            //System.err.print("C");
                            // CVRP_PORTFOLIOS.onePointCrossover2(CVRP_PORTFOLIOS.offsprings[p1], CVRP_PORTFOLIOS.offsprings[p2]);
                            //onePointCrossover2(offsprings[pop + 1], parents[i1]);

                        } // Mutation.
                        else {
                            // Create new mutated individual.
                            if (Settings.SUBTREE_MUTATION) {
                                a[0] = Mutation.subTreeMutation(Population.parents[p1].tree);
                                a[1] = Mutation.subTreeMutation(Population.parents[p2].tree);
                            }
                            if (Settings.NODE_REPLACEMENT_MUTATION) {
                                a[0] = Mutation.nodeReplacementMutation(Population.parents[p1].tree);
                                a[1] = Mutation.nodeReplacementMutation(Population.parents[p2].tree);
                            }
                            if (Settings.HOIST_MUTATION) {
                                a[0] = Mutation.hoistMutation(Population.parents[p1].tree);
                                a[1] = Mutation.hoistMutation(Population.parents[p2].tree);
                            }
                        }
                        // Store new individuals.
                        Population.offsprings[o1].tree = a[0];
                        Population.offsprings[o1].startingGeneration = Population.generationsCounter - 1;
                        // Connect its nodes.
                        Population.offsprings[o1].connectChildren(0, 0);
                        Population.offsprings[o1].connectParents(Population.offsprings[o1].tree.get(0));
                        Population.offsprings[o1].getMaxDepth();
                        Population.offsprings[o1].setNodeTypes();
                        // Store new individuas.
                        Population.offsprings[o2].tree = a[1];
                        Population.offsprings[o2].startingGeneration = Population.generationsCounter - 1;
                        // Connect its nodes.
                        Population.offsprings[o2].connectChildren(0, 0);
                        Population.offsprings[o2].connectParents(Population.offsprings[o2].tree.get(0));
                        Population.offsprings[o2].getMaxDepth();
                        Population.offsprings[o2].setNodeTypes();
                    }
                }
                CVRP_GP_CW.increaseThreadsDone();
                this.THREAD_STATE = STATE.WAIT;
            }/**/ /*else if (THREAD_STATE == STATE.EXECUTE) {
                s = this.assignedIndividuals.size();
                //System.out.println(MethodName.getCurrentMethodName() + " Thread " + this.threadID + " size: " + s);
                if (PARENTS) {
                    // Assign individuals.
                    //redistributeIndividuals(CVRP_PORTFOLIOS.threads, CVRP_PORTFOLIOS.parents, true);
                    for (int i = 0; i < s; i++) {
                        int ind = this.assignedIndividuals.get(0);
                        this.assignedIndividuals.remove(0);
                        Population.population[ind].executeIndividual(TSP.INSTANCE_ID);

                        while (TSPThread.CRITICAL_SECTION) {
                            // Waiting loop for execution of critical section.
                        }
                        // Check if thread has somethong to execute and if not end it.
                        if (this.assignedIndividuals.isEmpty()) {
                            //System.out.println(MethodName.getCurrentMethodName() + " Thread " + this.threadID + " FINISHED!");
                            break;
                        }
                    }
                } else {
                    // Assign individuals.
                    //redistributeIndividuals(CVRP_PORTFOLIOS.threads, CVRP_PORTFOLIOS.offsprings, true);
                    // Execute individual.
                    for (int i = 0; i < s; i++) {
                        int ind = this.assignedIndividuals.get(0);
                        this.assignedIndividuals.remove(0);
                        Population.offsprings[ind].executeIndividual(TSP.INSTANCE_ID);
                        //System.out.println(MethodName.getCurrentMethodName() + " " + this.toString());
                        while (TSPThread.CRITICAL_SECTION) {
                            // Waiting loop for execution of critical section.
                        }
                        // Check if thread has somethong to execute and if not end it.
                        if (this.assignedIndividuals.isEmpty()) {
                            //System.out.println(MethodName.getCurrentMethodName() + " Thread " + this.threadID + " FINISHED!");
                            break;
                        }
                    }
                }

                //CVRP_PORTFOLIOS.increaseThreadsDone();
                if (CAN_BE_REDISTRIBUTED) {
                    redistributeIndividuals(CVRP_GP_CW.threads, CVRP_GP_CW.threads.length, false);
                } else if (!CAN_BE_REDISTRIBUTED) {
                    //System.out.println(MethodName.getCurrentMethodName() + " Thread " + this.threadID + " FINISHED!");
                    CVRP_GP_CW.increaseThreadsDone();
                    this.THREAD_STATE = STATE.WAIT;
                }
                // Empty thread.
                if (this.assignedIndividuals.isEmpty() && CAN_BE_REDISTRIBUTED) {
                    //System.out.println(MethodName.getCurrentMethodName() + " Thread " + this.threadID + " FINISHED!");
                    CVRP_GP_CW.increaseThreadsDone();
                    this.THREAD_STATE = STATE.WAIT;
                }
            }/**/
        }
    }
}
