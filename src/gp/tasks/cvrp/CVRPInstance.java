/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.tasks.cvrp;

import gp.tasks.Instance;

import java.util.ArrayList;
import java.util.Arrays;

import sngp.*;

/**
 * @author fanda
 */
public class CVRPInstance extends Instance {

    /**
     * Positions of cities.
     */
    public Vector[] cities;

    /**
     * Symmetric distances of TSP problem.
     */
    public double[][] distances;

    /**
     * Capacities of all towns.
     */
    public double[] demands;

    /**
     * Vehicle capacity
     */
    public double vehicleCapacity = 100.0;

    /**
     * ArrayList of routes.
     */
    public ArrayList<ArrayList<Integer>> routes = new ArrayList<>();

    /**
     * City's corresponding route.
     */
    public int[] cityOnRoute;

    /**
     * Maximal distance between cities.
     */
    public double maxDistanceBetweenCities = -1.0;

    /**
     * Maximal distance between unassigned cities.
     */
    public double maxDistanceBetweenUnassignedCities = -1.0;

    /**
     * Maximal distance depot-city
     */
    public double maxDistanceFromDepot = -1.0;

    /**
     * Maximal distance between depot and unassigned cities.
     */
    public double maxDistanceUnassignedFromDepot = -1.0;

    /**
     * Smallest difference between demands in particular instance.
     */
    public double minimalDemandDifference = Double.POSITIVE_INFINITY;

    /**
     * Distance between nearest cities in the instance.
     */
    public double minDistanceBetweenCities = Double.POSITIVE_INFINITY;

    /**
     * Distance between nearest unassigned cities in the instance.
     */
    public double minDistanceBetweenUnassignedCities = Double.POSITIVE_INFINITY;

    /**
     * Distance between depot and one of the city.
     */
    public double minDistanceFromDepot = Double.POSITIVE_INFINITY;

    /**
     * Minimal distance between depot and unassigned cities.
     */
    public double minDistanceUnassignedFromDepot = Double.POSITIVE_INFINITY;

    /**
     * Worst solution (depot-town-depot) distance.
     */
    public double worstSolutionDistance = 0.0;

    /**
     * Sum of all demands.
     */
    public double sumDemands = 0.0;

    /**
     * Sum of the demands of all unassigned cities.
     */
    public double sumUnassignedDemands = 0.0;

    /**
     * Constructor of CVRP instance.
     *
     * @param size size of instance
     */
    public CVRPInstance(int size) {
        this.size = size;
        this.cities = new Vector[size];
        this.distances = new double[size][size];
        this.demands = new double[size];

        this.cityOnRoute = new int[this.size];
        Arrays.fill(this.cityOnRoute, -1);
    }

    /**
     * Restarts whole instance.
     */
    @Override
    public void resetInstance() {
        this.routes = new ArrayList<>(); //--- Delete route.
        this.cityOnRoute = new int[this.size];
        Arrays.fill(this.cityOnRoute, -1); //--- Delete city-route references.
    }

    /**
     * Creates new route.
     *
     * @param r ID of the route
     * @param c city
     */
    public void createRoute(int r, int c) {
        this.routes.add(r, new ArrayList<Integer>());
        this.routes.get(r).add(0);
        this.routes.get(r).add(c);
        this.routes.get(r).add(0);
        this.cityOnRoute[c] = r;
    }

    /**
     * Return capacity of the route.
     *
     * @param id index of the route
     * @return route capacity
     */
    public double getRouteCapacity(int id) {

        double cap = 0.0;
        if (this.routes.size() == 0) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Accessing route with bad id (" + id + "/" + this.routes.size() + ")");
            return 0.0;
        }
        for (int c = 0; c < this.routes.get(id).size(); c++) {
            cap += this.demands[this.routes.get(id).get(c)];
        }
        return cap;
    }

    /**
     * Return capacity of the specific route.
     *
     * @param route ArrayList of cities on the route
     * @return sum of demends on the route
     */
    public double getRouteCapacity(ArrayList<Integer> route) {

        double cap = 0.0;
        for (int city = 0; city < route.size(); city++) {
            cap += this.demands[route.get(city)];
        }
        return cap;
    }

    /**
     * Joins two route.
     *
     * @param c1 first city
     * @param c2 second city
     * @return below zero parameter means that route weren't joined.
     */
    public int joinRoutes(int c1, int c2) {

        //System.out.println("route size " + this.route.get(r1).size());
        //System.out.println(MethodName.getCurrentMethodName() + " " + this.cityOnRoute.length + " " + this.instanceExecutedID);
        int r1 = this.cityOnRoute[c1];
        int r2 = this.cityOnRoute[c2];

        ArrayList<Integer> tRoute1 = new ArrayList<>();
        ArrayList<Integer> tRoute2 = new ArrayList<>();

        //--- City c1 doesn't have assigned any route - create new one.
        if (r1 == -1) {
            //--- Create temporally route.
            tRoute1.add(0);
            tRoute1.add(c1);
            tRoute1.add(0);
        } else {
            //--- Else store actual route with city c1.
            for (int i = 0; i < this.routes.get(r1).size(); i++) {
                tRoute1.add(i, this.routes.get(r1).get(i));
            }
        }

        //--- City c2 doesn't have assigned any route - create new one.
        if (r2 == -1) {
            //--- Create temporally route.
            tRoute2.add(0);
            tRoute2.add(c2);
            tRoute2.add(0);
        } else {
            //--- Else store actual route with city c2.
            for (int i = 0; i < this.routes.get(r2).size(); i++) {
                tRoute2.add(i, this.routes.get(r2).get(i));
            }
        }

        //--- Nodes are in the same route and cannot be joined.
        if (r1 >= 0 && r1 == r2) {
            //System.out.println(MethodName.getCurrentMethodName() + " Cannot join the same ways!");
            return -1;
        }

        //--- Starts and endings of the route.
        int start1 = -1, start2 = -1;
        int ending1 = -1, ending2 = -1;

        if (tRoute1.get(1) == c1) {
            start1 = c1;
        }
        if (tRoute1.get(tRoute1.size() - 2) == c1) {
            ending1 = c1;
        }
        if (tRoute2.get(1) == c2) {
            start2 = c2;
        }
        if (tRoute2.get(tRoute2.size() - 2) == c2) {
            ending2 = c2;
        }

        //--- Any of these nodes are not at the edge of route -> cannot join the route.
        if ((start1 == -1 && ending1 == -1) || (start2 == -1 && ending2 == -1)) {
            //System.out.println(MethodName.getCurrentMethodName() + " Cannot join route " + temp1.toString() + " and " + temp2.toString() + ". One of selected nodes is not at the edge of the route!");
            return -2;
        }

        //--- Check if total route capacity does not exceed vehicle limits.
        //--- Calculates capacity of the new joined route.
        double totalCap = getRouteCapacity(tRoute1) + getRouteCapacity(tRoute2);

        //--- Total capacity exceeds vehicle limits.
        if (totalCap > this.vehicleCapacity) {
            //--- Create new route with the city if it is not assigned.
            if (this.cityOnRoute[c1] == -1) {
                this.createRoute(this.routes.size(), c1);
            }
            //--- Create new route with the city if it is not assigned.
            if (this.cityOnRoute[c2] == -1) {
                this.createRoute(this.routes.size(), c2);
            }
            return -3;
        }

        //--- Find new route to store.        
        int storeRouteID = -1; //--- Index of new route in route ArrayList.
        //--- First city is already assigned, will work with its route.
        if (r1 != -1) {
            storeRouteID = r1;
            if (r2 != -1) {
                //--- Delete second route.
                this.routes.remove(r2);
                //--- If route r2 has lower ID, it is necessery to adjust the id of route in the ArrayList and decrease it by 1.
                if (r2 < r1) {
                    storeRouteID--;
                }
            }
        } //--- Second city is already assigned (and first one not) assigned.
        else if (r2 != -1) {
            storeRouteID = r2;
        }
        //--- Both cities are unassigned, create new route.
        if (storeRouteID == -1) {
            this.routes.add(this.routes.size(), new ArrayList<Integer>());
            storeRouteID = this.routes.size() - 1;
        }

        //System.out.println(MethodName.getCurrentMethodName() + " BEFORE JOIN 1 (" + c1 + ") - " + temp1.toString());
        //System.out.println(MethodName.getCurrentMethodName() + " BEFORE JOIN 2 (" + c2 + ") - " + temp2.toString());

        //--- JOINING PROCEDURE. Check all four possibilities for joining route.
        //--- X---- ----X
        if (start1 != -1 && ending2 != -1) {
            //System.out.println(MethodName.getCurrentMethodName() + " X---- ----X");
            int end = tRoute2.size() - 1;
            //--- Copy the first route behind the second one.
            for (int i = 1; i < tRoute1.size() - 1; i++) {
                tRoute2.add(end, tRoute1.get(i));
                end++;
            }

            //--- Store route in route ArrayList.
            //--- Delete route stored on storeRouteID index.
            this.routes.set(storeRouteID, new ArrayList<Integer>());
            //--- Copy new route to the storeRouteID index.
            for (int i = 0; i < tRoute2.size(); i++) {
                this.routes.get(storeRouteID).add(i, tRoute2.get(i));
                this.cityOnRoute[tRoute2.get(i)] = storeRouteID;
            }
            //this.routestToString();
            return 1;
        } //--- ----X X----
        else if (start2 != -1 && ending1 != -1) {
            //System.out.println(MethodName.getCurrentMethodName() + " ----X X----");
            int end = tRoute1.size() - 1;
            //--- Copy second route behind the first one.
            for (int i = 1; i < tRoute2.size() - 1; i++) {
                tRoute1.add(end, tRoute2.get(i));
                end++;
            }

            //--- Store route in route ArrayList.
            //--- Delete route stored on storeRouteID index.
            this.routes.set(storeRouteID, new ArrayList<Integer>());
            //--- Copy new route to the storeRouteID index.
            for (int i = 0; i < tRoute1.size(); i++) {
                this.routes.get(storeRouteID).add(i, tRoute1.get(i));
                this.cityOnRoute[tRoute1.get(i)] = storeRouteID;
            }
            //this.routestToString();
            return 2;
        }
        //--- Symmetrical version of CVRP.
        else if (CVRP.SYMMETRICAL) {
            //--- X---- X----
            if (start1 != -1 && start2 != -1) {
                //System.out.println(MethodName.getCurrentMethodName() + " X---- X----");
                //--- Copy reversed first route before the second one.
                for (int i = 1; i < tRoute1.size() - 1; i++) {
                    tRoute2.add(1, tRoute1.get(i));
                }
                //--- Store route in route ArrayList.
                //--- Delete route stored on storeRouteID index.
                this.routes.set(storeRouteID, new ArrayList<Integer>());
                //--- Copy new route to the storeRouteID index.
                for (int i = 0; i < tRoute2.size(); i++) {
                    this.routes.get(storeRouteID).add(i, tRoute2.get(i));
                    this.cityOnRoute[tRoute2.get(i)] = storeRouteID;
                }
                //this.routestToString();
                return 3;
            } //--- ----X ----X
            else if (ending1 != -1 && ending2 != -1) {
                //System.out.println(MethodName.getCurrentMethodName() + " ----X ----X");
                //--- Copy reversed second route behind the first one.
                int end = tRoute1.size() - 1;
                for (int i = 1; i < tRoute2.size() - 1; i++) {
                    tRoute1.add(end, tRoute2.get(i));
                }
                //--- Store route in route ArrayList.
                //--- Delete route stored on storeRouteID index.
                this.routes.set(storeRouteID, new ArrayList<Integer>());
                //--- Copy new route to the storeRouteID index. 
                for (int i = 0; i < tRoute1.size(); i++) {
                    this.routes.get(storeRouteID).add(i, tRoute1.get(i));
                    this.cityOnRoute[tRoute1.get(i)] = storeRouteID;
                }
                //this.routestToString();
                return 4;
            }
        }

        //System.out.println(MethodName.getCurrentMethodName() + " Cannot join route " + temp1.toString() + " and " + temp2.toString() + "!");
        return -4;
    }

    /**
     * Creates copy of the solution.
     *
     * @param a actual solution of the instance
     * @return ArrayList of route
     */
    public ArrayList<ArrayList<Integer>> copySolution(ArrayList<ArrayList<Integer>> a) {
        ArrayList<ArrayList<Integer>> tempSolution = new ArrayList<>();
        for (int r = 0; r < a.size(); r++) {
            tempSolution.add(r, new ArrayList<Integer>());
            for (int t = 0; t < a.get(r).size(); t++) {
                tempSolution.get(r).add(t, a.get(r).get(t));
            }
        }
        return tempSolution;
    }

    /**
     * Gets array of unassigned cities.
     *
     * @return array of unassigned cities
     */
    public int[] getUnassignedCities() {

        int freeCities[];
        int count = 0;

        //--- Calculate number of unclassified cities.
        for (int i = 1; i < this.cities.length; i++) {
            if (this.cityOnRoute[i] == -1) {
                count++;
            }
        }

        //--- Allocate array and store ids for free cities.
        freeCities = new int[count];
        count = 0;
        for (int i = 1; i < this.cityOnRoute.length; i++) {
            if (this.cityOnRoute[i] == -1) {
                freeCities[count++] = i;
            }
        }

        return freeCities;
    }

    /**
     * Calculates number of unassigned cities.
     *
     * @return number of unassigned cities.
     */
    public int getNumberOfUnassignedCities() {
        int count = 0;
        // Do not count depot.
        for (int i = 1; i < this.cityOnRoute.length; i++) {
            if (this.cityOnRoute[i] == -1) {
                count++;
            }
        }
        return count;
    }

    /**
     * Finds furthest unassigned city.
     *
     * @param freeCities array of unassigned cities
     * @return index of the furthest unassigned city from the depot
     */
    public int getFurthestUnassignedCityFromDepot(int[] freeCities) {

        int id = 0;
        double fdist = this.distances[0][freeCities[0]];
        for (int i = 1; i < freeCities.length; i++) {
            if (this.distances[0][freeCities[i]] > fdist) {
                fdist = this.distances[0][freeCities[i]];
                id = i;
            }
        }
        return id;
    }

    /**
     * Sets correct number of route for cities.
     */
    public void setCitiesOnRoutes() {
        for (int i = 0; i < this.cityOnRoute.length; i++) {
            this.cityOnRoute[i] = -1;
        }
        for (int i = 0; i < this.routes.size(); i++) {
            for (int j = 0; j < this.routes.get(i).size(); j++) {
                if (this.routes.get(i).get(j) > 0) {
                    this.cityOnRoute[this.routes.get(i).get(j)] = i;
                }
            }
        }
    }

    /**
     * Calculates and returns maximal distance between all cities.
     *
     * @return maximal distance between all cities
     */
    public double calcMaxDistanceBetweenCities() {
        this.maxDistanceBetweenCities = 0.0;
        for (int i = 1; i < size; i++) {
            for (int j = 1; j < size; j++) {
                if (i == j) continue;
                if (this.distances[i][j] > this.maxDistanceBetweenCities && this.distances[i][j] != Double.POSITIVE_INFINITY) {
                    this.maxDistanceBetweenCities = this.distances[i][j];
                }
            }
        }
        return this.maxDistanceBetweenCities;
    }

    /**
     * Calculates and returns maximal distance between unassigned cities.
     *
     * @return maximal distance between unassigned cities
     */
    public double calcMaxDistanceBetweenUnassignedCities() {
        this.maxDistanceBetweenCities = 0.0;
        int[] uc = this.getUnassignedCities();
        for (int i = 0; i < uc.length; i++) {
            for (int j = 0; j < uc.length; j++) {
                if (i == j) continue;
                if (this.distances[uc[i]][uc[j]] > this.maxDistanceBetweenUnassignedCities && this.distances[uc[i]][uc[j]] != Double.POSITIVE_INFINITY) {
                    this.maxDistanceBetweenUnassignedCities = this.distances[uc[i]][uc[j]];
                }
            }
        }
        return this.maxDistanceBetweenCities;
    }

    /**
     * Calculates and returns minimal distance between all cities.
     *
     * @return minimal distance between all cities
     */
    public double calcMinDistanceBetweenCities() {
        this.minDistanceBetweenCities = Double.POSITIVE_INFINITY;
        for (int i = 1; i < size; i++) {
            for (int j = 1; j < size; j++) {
                if (i == j) continue;
                if (this.distances[i][j] < this.minDistanceBetweenCities && this.distances[i][j] != Double.POSITIVE_INFINITY) {
                    this.minDistanceBetweenCities = this.distances[i][j];
                }
            }
        }
        return this.minDistanceBetweenCities;
    }

    /**
     * Calculates and returns minimal distance between unassigned cities.
     *
     * @return minimal distance between unassigned cities
     */
    public double calcMinDistanceBetweenUnassignedCities() {
        this.minDistanceBetweenUnassignedCities = Double.POSITIVE_INFINITY;
        int[] uc = this.getUnassignedCities();
        for (int i = 0; i < uc.length; i++) {
            for (int j = 0; j < uc.length; j++) {
                if (i == j) continue;
                if (this.distances[uc[i]][uc[j]] < this.minDistanceBetweenUnassignedCities && this.distances[uc[i]][uc[j]] != Double.POSITIVE_INFINITY) {
                    this.minDistanceBetweenUnassignedCities = this.distances[uc[i]][uc[j]];
                }
            }
        }
        return this.minDistanceBetweenUnassignedCities;
    }

    /**
     * Calculates and returns maximal distance between city and depot in the instance.
     *
     * @return maximal distance between city and depot in the instance
     */
    public double calcMaxDistanceFromDepot() {
        this.maxDistanceFromDepot = 0.0;
        for (int i = 1; i < size; i++) {
            if (this.distances[i][0] > this.maxDistanceFromDepot && this.distances[i][0] != Double.POSITIVE_INFINITY) {
                this.maxDistanceFromDepot = this.distances[i][0];
            }
            if (this.distances[0][i] > this.maxDistanceFromDepot && this.distances[0][i] != Double.POSITIVE_INFINITY) {
                this.maxDistanceFromDepot = this.distances[0][i];
            }
        }
        return maxDistanceFromDepot;
    }

    /**
     * Calculates and returns maximal distance between unassigned city and depot in the instance.
     *
     * @return maximal distance between unassigned city and depot in the instance
     */
    public double calcMaxDistanceUnassignedFromDepot() {
        this.maxDistanceUnassignedFromDepot = 0.0;
        int[] uc = this.getUnassignedCities();
        for (int i = 0; i < uc.length; i++) {
            if (this.distances[uc[i]][0] > this.maxDistanceUnassignedFromDepot && this.distances[uc[i]][0] != Double.POSITIVE_INFINITY) {
                this.maxDistanceUnassignedFromDepot = this.distances[uc[i]][0];
            }
            if (this.distances[0][uc[i]] > this.maxDistanceUnassignedFromDepot && this.distances[0][uc[i]] != Double.POSITIVE_INFINITY) {
                this.maxDistanceUnassignedFromDepot = this.distances[0][uc[i]];
            }
        }
        return this.maxDistanceUnassignedFromDepot;
    }

    /**
     * Calculates and returns minimal distance between city and depot in the instance.
     *
     * @return minimal distance between city and depot in the instance
     */
    public double calcMinDistanceFromDepot() {
        this.minDistanceFromDepot = Double.POSITIVE_INFINITY;
        for (int i = 1; i < size; i++) {
            if (this.distances[i][0] < this.minDistanceFromDepot && this.distances[i][0] != Double.POSITIVE_INFINITY) {
                this.minDistanceFromDepot = this.distances[i][0];
            }
            if (this.distances[0][i] < this.minDistanceFromDepot && this.distances[0][i] != Double.POSITIVE_INFINITY) {
                this.minDistanceFromDepot = this.distances[0][i];
            }
        }
        return minDistanceFromDepot;
    }

    /**
     * Calculates and returns minimal distance between unassigned city and depot in the instance.
     *
     * @return minimal distance between unassigned city and depot in the instance
     */
    public double calcMinDistanceUnassignedFromDepot() {
        this.minDistanceUnassignedFromDepot = Double.POSITIVE_INFINITY;
        int[] uc = this.getUnassignedCities();
        for (int i = 0; i < uc.length; i++) {
            if (this.distances[uc[i]][0] < this.minDistanceUnassignedFromDepot && this.distances[uc[i]][0] != Double.POSITIVE_INFINITY) {
                this.minDistanceUnassignedFromDepot = this.distances[uc[i]][0];
            }
            if (this.distances[0][uc[i]] < this.minDistanceUnassignedFromDepot && this.distances[0][uc[i]] != Double.POSITIVE_INFINITY) {
                this.minDistanceUnassignedFromDepot = this.distances[0][uc[i]];
            }
        }
        return this.minDistanceUnassignedFromDepot;
    }

    /**
     * Calculates and returns distance of the possible worst solution consist of (@code size) - 1
     * routes from the depot to the each city.
     *
     * @return distance of the possible worst solution
     */
    public double calcWorstSolutionDistance() {
        this.worstSolutionDistance = 0.0;
        for (int i = 1; i < size; i++) {
            this.worstSolutionDistance += 2 * this.distances[0][i];
        }
        return worstSolutionDistance;
    }

    /**
     * Calculate and returns sum of all demands.
     *
     * @return returns sum of all demands
     */
    public double calcDemandsSum() {
        this.sumDemands = 0.0;
        for (int i = 0; i < size; i++) {
            this.sumDemands += this.demands[i];
        }
        return sumDemands;
    }

    /**
     * Calculate and returns sum of all demands of unassigned cities.
     *
     * @return returns sum of all demands of unassigned cities
     */
    public double calcUnassignedDemandsSum() {
        this.sumUnassignedDemands = 0.0;
        int[] uc = this.getUnassignedCities();
        for (int i = 0; i < uc.length; i++) {
            this.sumUnassignedDemands += this.demands[uc[i]];
        }
        return sumUnassignedDemands;
    }

    /**
     * Prints instances parameters.
     */
    @Override
    public void printInstance() {
        System.out.println("===\r\nName: " + this.name);
        System.out.println("Vehicle capacity: " + this.vehicleCapacity);
        System.out.println("#Cities: " + this.cities.length);
        System.out.print("Demands: ");
        for (int i = 0; i < this.demands.length; i++) {
            System.out.print(this.demands[i] + " ");
        }
        System.out.println();
    }

    /**
     * Prints actual solution.
     */
    @Override
    public void printSolution() {
        System.out.println("Instance: " + this.name);
        for (int i = 0; i < this.routes.size(); i++) {
            System.out.print(i + ") " + this.routes.get(i).toString());
            System.out.println(" = " + this.getRouteCapacity(i) + " / " + this.vehicleCapacity);
        }
        if (this.routes.isEmpty()) {
            System.out.println();
        }
    }

    /**
     * Checks solution for errors.
     *
     * @return trueif solution is without errors.
     */
    @Override
    public boolean checkSolution() {
        boolean correct = true;
        //--- Some cities are unassigned.
        if (getUnassignedCities().length != 0) {
            System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Cities " + Arrays.toString(getUnassignedCities()) + " are unassigned!.");
            correct = false;
        }
        //--- Check if sum of demands on particulary route doesn't exceeds vehicle capacity limits.
        for (int i = 0; i < this.routes.size(); i++) {
            if (this.getRouteCapacity(i) > this.vehicleCapacity) {
                correct = false;
                System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Route " + i + " exceeds vehicle capacity (" + this.getRouteCapacity(i) + "/" + this.vehicleCapacity + ")!");
            }
        }
        //--- Check if some city is not included more than once.
        int[] citiesCounter = new int[this.size - 1];
        Arrays.fill(citiesCounter, 0);
        for (int i = 0; i < this.routes.size(); i++) {
            for (int j = 0; j < this.routes.get(i).size(); j++) {
                //--- Skip the depot.
                if (this.routes.get(i).get(j) == 0) {
                    continue;
                }
                citiesCounter[this.routes.get(i).get(j) - 1]++;
            }
        }
        //--- Check if there are all ones in the city counter array.
        for (int i = 0; i < citiesCounter.length; i++) {
            if (citiesCounter[i] > 1) {
                correct = false;
                System.err.println(MethodName.getCurrentMethodName() + " - ERROR - City " + (i + 1) + " is included more than once (" + citiesCounter[i] + ") in the solution!");
            }
        }
        if (!correct) {
            this.printSolution();
            System.exit(1);
        }

        return correct;
    }

    /**
     * Creates clone of the CVRP instance.
     *
     * @return clone of the CVRP instance
     */
    public CVRPInstance clone() {

        System.err.println(MethodName.getCurrentMethodName() + " - ERROR - Missing implementation of clonning method!");
        System.exit(1);

        CVRPInstance c = new CVRPInstance(this.size);
        c.name = this.name;
        c.demands = new double[this.demands.length];
        for (int i = 0; i < this.demands.length; i++) {
            c.demands[i] = this.demands[i];
        }
        c.sumDemands = this.sumDemands;

        c.cities = new Vector[this.cities.length];
        for (int i = 0; i < this.cities.length; i++) {
            c.cities[i] = this.cities[i].clone();
        }

        c.distances = new double[this.distances.length][this.distances[0].length];
        for (int i = 0; i < this.distances.length; i++) {
            for (int j = 0; j < this.distances[0].length; j++) {
                c.distances[i][j] = this.distances[i][j];
            }
        }

        c.vehicleCapacity = this.vehicleCapacity;

        c.maxDistanceBetweenCities = this.maxDistanceBetweenCities;
        c.maxDistanceBetweenUnassignedCities = this.maxDistanceBetweenUnassignedCities;
        c.maxDistanceFromDepot = this.maxDistanceFromDepot;
        c.maxDistanceUnassignedFromDepot = this.maxDistanceUnassignedFromDepot;

        c.minDistanceBetweenCities = this.minDistanceBetweenCities;
        c.minDistanceBetweenUnassignedCities = this.minDistanceBetweenUnassignedCities;
        c.minDistanceFromDepot = this.minDistanceFromDepot;
        c.minDistanceUnassignedFromDepot = this.minDistanceUnassignedFromDepot;

        c.sumDemands = this.sumDemands;
        c.sumUnassignedDemands = this.sumUnassignedDemands;

        c.worstSolutionDistance = this.worstSolutionDistance;
        c.bestHeuristicFitness = this.bestHeuristicFitness;

        c.cityOnRoute = new int[this.cityOnRoute.length];
        Arrays.fill(cityOnRoute, -1);
        c.routes = this.copySolution(this.routes);
        c.setCitiesOnRoutes();

        return this;
    }


}
