package gp.tasks.tsptw;

import gp.tasks.Instance;
import sngp.MethodName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Fanda on 4.3.2017.
 */
public class TSPTWInstance extends Instance {

    /**
     * Map of prizes for each day.
     */
    public HashMap<String, HashMap<String, ArrayList<Integer>>> map = new HashMap();

    /**
     * ArrayList of routes.
     */
    public ArrayList<ArrayList<Integer>> routes = new ArrayList<>();

    /**
     * City's corresponding route.
     */
    public int[] cityOnRoute;

    /**
     * Depot.
     */
    public String startingCity = "";

    /**
     * Length of the trip.
     */
    public int instanceLenth = 0;

    /**
     * Actaul day of the trip.
     */
    public int actualDay = 0;

    /**
     * Restarts whole instance.
     */
    @Override
    public void resetInstance() {
        this.routes = new ArrayList<>(); //--- Delete route.
        this.cityOnRoute = new int[this.size];
        Arrays.fill(this.cityOnRoute, -1); //--- Delete city-route references.
    }

    /**
     * Clonning method.
     * @return clone of this instance
     */
    public TSPTWInstance clone() {
        System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Not implemented!");
        System.exit(1);
        return this;
    }
}
