/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.tasks.tsptw;

import gp.tasks.Problem;
import gp.tasks.cvrp.CVRPInstance;
import gp.tasks.tsptw.TSPTWInstance;
import loaders.tsp.LoadedInstance;
import heuristics.Heuristic;
import heuristics.cvrp.*;
import sngp.*;


import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author fanda
 */
public class TSPTW extends Problem {


    /**
     * Constructor.
     */
    public TSPTW() {
        instancesTrainingDir = "INSTANCES/" + this.getClass().getSimpleName() + "/Training/";
        instancesTestingDir = "INSTANCES/" + this.getClass().getSimpleName() + "/Testing/";
        //--- Check if directory exists and create new one if it is not true.
        if(!new File("RESULTS\\" + this.getClass().getSimpleName() + "\\").isDirectory()) {
            new File ("RESULTS\\" + this.getClass().getSimpleName() + "\\").mkdir();
            System.out.println("OPTIMIZATION PROBLEM: Directory RESULTS\\" + this.getClass().getSimpleName() + "\\ created!");
        }
        outputDir = "RESULTS/" + this.getClass().getSimpleName() + "/";
    }

    /**
     * Initializes low-lew heuristics for actual optimization problem.
     * @return Array of loaded heuristics.
     */
    @Override
    public Heuristic[] initHeuristics() {
        ArrayList<Heuristic> llhs = new ArrayList();

        llhs.add(new ClarkeWright());
        llhs.add(new Kilby());
        llhs.add(new MoleJameson());

        Heuristic[] h = new Heuristic[llhs.size()];
        for (int i = 0; i < h.length; i++) {
            h[i] = llhs.get(i);
        }

        Problem.NUMBER_OF_LLHS = h.length;

        return h;
    }

    /**
     * Initializes actions used in HH for actual optimization problem.
     * @return Array of loaded heuristics.
     */
    @Override
    public Heuristic[] initActions() {
        ArrayList<Heuristic> ac = new ArrayList();
        ac.add(new ClarkeWright());
        ac.add(new Kilby());
        ac.add(new MoleJameson());
        ac.add(new AlgCW1());
        ac.add(new AlgCW3());
        ac.add(new AlgCW7());

        Heuristic[] h = new Heuristic[ac.size()];
        for (int i = 0; i < h.length; i++) {
            h[i] = ac.get(i);
        }

        Problem.NUMBER_OF_ACTIONS = h.length;

        return h;
    }

    /**
     * Loads instances in the directory from method parameters.
     * @param dir directory with instances.
     */
    @Override
    public void loadInstances(String dir) {
        /**
         * Set all instances in directory.
         */
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        int counter = 0;
        String n;
        //--- Count instances according its file extension.
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {

                n = listOfFiles[i].getName().substring(listOfFiles[i].getName().length() - 3, listOfFiles[i].getName().length());
                if (n.equals("tsp") || n.equals("TSP") || n.equals("txt") || n.equals("TXT")) counter++;
            }
        }
        //--- Store paths to instances.
        instancesPaths = new String[counter];
        counter = 0;
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                n = listOfFiles[i].getName().substring(listOfFiles[i].getName().length() - 3, listOfFiles[i].getName().length());
                if (n.equals("tsp") || n.equals("TSP") || n.equals("txt") || n.equals("TXT")) instancesPaths[counter++] = listOfFiles[i].getName();
            }
        }

        //--- Load all instances.
        counter = 0;
        Problem.NUMBER_OF_INSTANCES = listOfFiles.length;
        Problem.INSTANCES = new TSPTWInstance[listOfFiles.length];
        for(String sinst : instancesPaths) {
            Problem.INSTANCES[counter] = loadInstance(dir + "/" + sinst, true);
            counter++;
        }

        System.out.println("INSTANCE: " + Problem.NUMBER_OF_INSTANCES + " instances successfully loaded!");
    }

    /**
     * Loads instance from the file using TSPLIB4J library.
     *
     * @param path path to the file
     * @param print @code true if information about progress will be print
     *
     * @return object of the TSPInstance
     */
    @Override
    public TSPTWInstance loadInstance(String path, boolean print) {

        TSPTWInstance instance = new TSPTWInstance();
        File file;
        String line;
        int lineCounter = 0;    // Var for better exporting of bugs in file format.

        boolean exists = new File(path).exists();

        //--- File exists?
        if (exists) {
            //--- Then prepare for loading.
            file = new File(path);
            if (file.isDirectory()) {
                System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Input file path " + path + " leads only to directory.");
                System.exit(1);

            }
            if (print) {
                System.out.println("INSTANCE: Found file " + path + ". Loading instance...");
            }
        } else {
            //--- Else make a new populations.
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Cannot find file " + path + ".");
            System.exit(1);
            return null;
        }


        //--- Start loading evolution.
        try {
            FileInputStream input = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(isr);
            try {
                try {

                    String nodes[];

                    //--- Load starting city.
                    line = br.readLine();
                    instance.startingCity = line;
                    instance.size = 0;
                    while(!line.isEmpty()) {
                        line = br.readLine();
                        nodes = line.split(" ");
                        if(Integer.parseInt(nodes[2]) > instance.instanceLenth) {
                            instance.instanceLenth = Integer.parseInt(nodes[2]);
                        }
                        if(!instance.map.containsKey(nodes[0])) {
                            instance.size++;
                            instance.map.put(nodes[0], new HashMap());
                        }
                        if(!instance.map.get(nodes[0]).containsKey(nodes[1])) {
                            instance.map.get(nodes[0]).put(nodes[1], new ArrayList<Integer>());
                        }
                        try {
                            while(instance.map.get(nodes[0]).get(nodes[1]).size() < Integer.parseInt(nodes[2])) {
                                instance.map.get(nodes[0]).get(nodes[1]).add(instance.map.get(nodes[0]).get(nodes[1]).size(), -1);
                            }
                            instance.map.get(nodes[0]).get(nodes[1]).add(Integer.parseInt(nodes[2]), Integer.parseInt(nodes[3]));
                        }catch(IndexOutOfBoundsException IAOOBE) {
                            System.err.println(line);
                            System.err.println(instance.map.get(nodes[0]).get(nodes[1]).size());
                            IAOOBE.printStackTrace();

                            System.exit(1);
                        }
                    }

                } catch (IOException IOE) {
                }
            } catch (NullPointerException NPE) {
            }
        } catch (FileNotFoundException FNFE) {
        }

        ArrayList<String> queue = new ArrayList();
        queue.add(instance.startingCity);
        int qSize;
        for (int day = instance.instanceLenth; day >= 0; day--) {
            qSize = queue.size();
            //System.out.println(queue.toString());
            for (String city1 : instance.map.keySet()) {

                for (int c2 = 0; c2 < qSize; c2++) {
                    String city2 = queue.get(c2);
                    if(city1.equals(city2)) {
                        continue;
                    }
                    //--- Fill short ArrayLists.
                    try {
                    while(instance.map.get(city1).get(city2).size() < instance.instanceLenth + 1) {

                            instance.map.get(city1).get(city2).add(instance.map.get(city1).get(city2).size() - 1, -1);

                    }
                    }catch(NullPointerException NPE) {
                        System.err.println(city1 + " " + city2);
                        NPE.printStackTrace();
                        System.exit(1);
                    }
                   // System.out.println(city1 + "  " + city2 + " " + instance.map.get(city1).get(city2).toString() + " " +instance.map.get(city1).get(city2).size() );
                    if(day == instance.instanceLenth) {
                        if (instance.map.get(city1).get(city2).get(day) == -1) {
                            queue.add(queue.size(), city1);
                            System.out.println("Added " + city1 + " in day " + day);
                        }
                    }
                    else {
                        instance.map.get(city1).get(city2).set(day, -1);
                        System.out.println("Removed " + city1 + " - " + city2 + " in day " + day);
                    }
                }

            }
            for (int c2 = 0; c2 < qSize; c2++) {
                queue.remove(0);
            }
        }
        if (print) {
            System.out.println("INSTANCE: " + "Instance successfully loaded!" + "\033[0m");
        }
        return instance;
    }
}
