/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.tasks.tsptw;

/**
 *
 * @author Fanda
 */
public class Savings implements Comparable, Cloneable {

    /**
     * First city.
     */
    public String firstCity = "'";

    /**
     * Second city.
     */
    public String secondCity = "";

    /**
     * Savings.
     */
    public double savings = 0.0;

    /**
     *
     * @param c1
     * @param c2
     * @param s
     */
    public Savings(String c1, String c2, double s) {
        this.firstCity = c1;
        this.secondCity = c2;
        this.savings = s;
    }

    /**
     * Prints actual savings.
     */
    public void printSavings() {
        System.out.println("(" + this.firstCity + "," + this.secondCity +") = " + this.savings);
    }

    /**
     * Compares two objects of this class.
     * @param o another savings to compare
     * @return comparison number.
     */
    @Override
    public int compareTo(Object o) {
        Savings f = (Savings)(o);
        if(this.savings < f.savings) return 1;
        if(f.savings < this.savings) return -1;
        else return 0;
    }

    /**
     * Clone method
     */
    public Savings clone() {
        Savings s = new Savings(this.firstCity, this.secondCity, this.savings);
        return s;
    }
}
