package gp.terminals.bpp_ghh;

import gp.Node;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import gp.tasks.Instance;

import java.util.ArrayList;

/**
 * Created by Fanda on 12. 4. 2016.
 */
public class WeightSumUnpackedItems extends Node{
    /**
     * Constructor of node.
     */
    public WeightSumUnpackedItems() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public WeightSumUnpackedItems clone() {
        return new WeightSumUnpackedItems();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);
        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        double sum = 0.0;
        for(BPPItem it : fi) {
            sum+= it.weight;
        }
        return sum;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
