/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.bpp_ghh;

import gp.Node;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.Instance;

/**
 *
 * @author Fanda
 */
public class BinFreeSpaceWithItem extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public BinFreeSpaceWithItem() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public BinFreeSpaceWithItem clone() {
        return new BinFreeSpaceWithItem();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);
        if(inst.currentBin == -1) {
            return inst.binCapacity - inst.actualItem.weight;
        }
        return inst.bins.get(inst.currentBin).freeSpace - inst.actualItem.weight;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
