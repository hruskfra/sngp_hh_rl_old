/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.bpp_ghh;

import gp.Node;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.Instance;

/**
 *
 * @author Fanda
 */
public class ItemWeight extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public ItemWeight() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public ItemWeight clone() {
        return new ItemWeight();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);
        return inst.actualItem.weight;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
