/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals;

import gp.individuals.Individual;
import gp.*;

/**
 *
 * @author Fanda
 */
public class Zero extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public Zero() {
        this.arity = 0;
        this.value = 0;
        this.nodeValue = 0.0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
        this.name = this.getClass().getSimpleName();
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public Zero clone() {
        return new Zero();
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Individual i) {
        this.nodeValue = 0.0;
        return 0.0;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
