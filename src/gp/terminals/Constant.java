/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals;

import gp.*;
import gp.individuals.Individual;
import sngp.Settings;

/**
 *
 * @author Fanda
 */
public class Constant extends Node implements Cloneable{

    /**
     * Just range of the value of this constant centered around zero.
     * For example constant can has value for -10 to +10 when this variable is set to 20.
     */
    public static int value_interval_range = 20;

    /**
     * Constructor of node.
     */
    public Constant() {
        this.arity = 0;
        this.value = Math.random() * Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
        this.name = this.getClass().getSimpleName();
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public Constant clone() {
        Constant c = new Constant();
        c.value = this.value;
        return c;
    }

    /**
     * Execution method
     * @param i actual individual.
     * @return this node's value.
     */
    @Override
    public  double execute_node(Individual i) {
        this.nodeValue = this.value;
        return this.value;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return "CONST_" + this.value + "";
    }
    
}
