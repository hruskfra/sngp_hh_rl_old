
package gp.terminals;

import gp.Node;
import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.Instance;

/**
 * Created by Fanda on 14. 3. 2016.
 */
public class HeuristicID extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public HeuristicID() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
        this.value = 1;
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public HeuristicID clone() {
        return new HeuristicID();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        return this.value;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}

