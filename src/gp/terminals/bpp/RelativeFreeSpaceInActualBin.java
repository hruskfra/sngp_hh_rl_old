/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.bpp;

import gp.tasks.bpp.BPPInstance;
import gp.*;
import gp.tasks.Instance;

/**
 *
 * @author Fanda
 */
public class RelativeFreeSpaceInActualBin extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public RelativeFreeSpaceInActualBin() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public RelativeFreeSpaceInActualBin clone() {
        return new RelativeFreeSpaceInActualBin();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);
        //System.out.println("REL FREE SPACE TERMINAL EXEC METHOD! " + inst.name + " " + inst.bins.get(inst.currentBin).freeSpace + " " + inst.bins.get(inst.currentBin).freeSpace / inst.binCapacity);
        //System.out.println(MethodName.getCurrentMethodName() + " " + inst.bins.get(inst.currentBin).freeSpace + " " + inst.binCapacity);inst.printSolution();
        return inst.bins.get(inst.currentBin).freeSpace / inst.binCapacity;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
