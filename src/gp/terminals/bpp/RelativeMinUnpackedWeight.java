/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.bpp;

import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPInstance;
import gp.*;
import gp.tasks.bpp.BPPItem;
import gp.tasks.Instance;
import sngp.MethodName;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Fanda
 */
public class RelativeMinUnpackedWeight extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public RelativeMinUnpackedWeight() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public RelativeMinUnpackedWeight clone() {
        return new RelativeMinUnpackedWeight();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);

        ArrayList<BPPItem> fi  = inst.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;


        if(fi.size() == 0) return 0;
        try {
            Collections.sort(fi);
        }
        catch (IllegalArgumentException IAE) {
            System.out.println(MethodName.getCurrentMethodName() + " #Free items: " + fi.size());
            IAE.printStackTrace();
            System.exit(1);
        }
        return (fi.get(fi.size()-1).weight) / inst.binCapacity;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
