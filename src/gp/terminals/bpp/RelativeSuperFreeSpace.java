/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.bpp;

import gp.tasks.bpp.BPPBin;
import gp.tasks.bpp.BPPInstance;
import gp.*;
import gp.tasks.Instance;

/**
 * @author Fanda
 */
public class RelativeSuperFreeSpace extends Node implements Cloneable {

    /**
     * Constructor of node.
     */
    public RelativeSuperFreeSpace() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    @Override
    public RelativeSuperFreeSpace clone() {
        return new RelativeSuperFreeSpace();
    }

    /**
     * Execution method.
     * Returns free space
     *
     *
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance) (i);
        double tOpt = inst.theoreticalOptimumValue % 1.0;
        //System.out.println(MethodName.getCurrentMethodName() + " tOptimum " + inst.theoreticalOptimumValue + " rest " + tOpt);
        tOpt = 1.0 - tOpt;
        for (BPPBin b : inst.bins) {
            if (b.freeSpace != inst.binCapacity) {
                tOpt -= (b.freeSpace/inst.binCapacity);
                if (tOpt < 0.0) {
                    tOpt += 1.0;
                }
            }
        }
        return tOpt;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
