/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.bpp;

import gp.tasks.bpp.BPPInstance;
import gp.*;
import gp.tasks.Instance;

/**
 *
 * @author Fanda
 */
public class RelativeNumberOfPackedItems extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public RelativeNumberOfPackedItems() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public RelativeNumberOfPackedItems clone() {
        return new RelativeNumberOfPackedItems();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);

        double unpacked = inst.getUnpackedItems().size();
        return (double)(inst.items.length - unpacked) / (double)inst.items.length;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
