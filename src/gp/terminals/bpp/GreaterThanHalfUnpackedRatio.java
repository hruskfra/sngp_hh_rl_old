package gp.terminals.bpp;

import gp.Node;
import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;
import sngp.MethodName;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 14. 3. 2016.
 */
public class GreaterThanHalfUnpackedRatio extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public GreaterThanHalfUnpackedRatio() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public GreaterThanHalfUnpackedRatio clone() {
        return new GreaterThanHalfUnpackedRatio();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);

        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;

        //--- Check if there are any items left (should not occur! Only for VOI).
        if(fi.isEmpty()) return 0.0;

        try {
            Collections.sort(fi);
        }
        catch (IllegalArgumentException IAE) {
            System.out.println(MethodName.getCurrentMethodName() + " #Free items: " + fi.size());
            IAE.printStackTrace();
            System.exit(1);
        }
        int counter = 0;
        for(BPPItem it : fi) {
            if(it.weight > inst.binCapacity/2.0) {
                counter++;
            }
        }
        return ((double)counter/(double)fi.size());
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}

