package gp.terminals.bpp;

        import gp.Node;
        import gp.tasks.bpp.BPPInstance;
        import gp.tasks.Instance;

/**
 * Created by Fanda on 14. 3. 2016.
 */
public class RelativeBinsUsedComparedToOptimum extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public RelativeBinsUsedComparedToOptimum() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public RelativeBinsUsedComparedToOptimum clone() {
        return new RelativeBinsUsedComparedToOptimum();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);

        return (double)(inst.bins.size()) / (double)(inst.theoreticalOptimumFitness);
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}

