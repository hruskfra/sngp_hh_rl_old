
package gp.terminals.bpp;

        import gp.Node;
        import gp.tasks.bpp.BPPBin;
        import gp.tasks.bpp.BPPInstance;
        import gp.tasks.Instance;

/**
 * Created by Fanda on 14. 3. 2016.
 */
public class RelativeAverageBinFullness extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public RelativeAverageBinFullness() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public RelativeAverageBinFullness clone() {
        return new RelativeAverageBinFullness();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);

        double meanFullness = 0.0;

        for(BPPBin b : inst.bins) {
            meanFullness += (b.binCapacity - b.freeSpace) / b.binCapacity;
        }

        return meanFullness/inst.bins.size();
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}

