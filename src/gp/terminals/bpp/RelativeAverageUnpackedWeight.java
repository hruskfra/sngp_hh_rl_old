/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.bpp;

import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPInstance;
import gp.*;
import gp.tasks.bpp.BPPItem;
import gp.tasks.Instance;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Fanda
 */
public class RelativeAverageUnpackedWeight extends Node implements Cloneable {

    /**
     * Constructor of node.
     */
    public RelativeAverageUnpackedWeight() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    @Override
    public RelativeAverageUnpackedWeight clone() {
        return new RelativeAverageUnpackedWeight();
    }

    /**
     * Execution method.
     *
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance) (i);

        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        double avg = 0.0;
        for(BPPItem it : fi) {
            avg += it.weight;
        }
        avg /= (double)(fi.size());
        return avg;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
