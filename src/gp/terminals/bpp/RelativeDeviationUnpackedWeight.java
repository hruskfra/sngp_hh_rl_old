/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.bpp;

import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPInstance;
import gp.*;
import gp.tasks.bpp.BPPItem;
import gp.tasks.Instance;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Fanda
 */
public class RelativeDeviationUnpackedWeight extends Node implements Cloneable {

    /**
     * Constructor of node.
     */
    public RelativeDeviationUnpackedWeight() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    @Override
    public RelativeDeviationUnpackedWeight clone() {
        return new RelativeDeviationUnpackedWeight();
    }

    /**
     * Execution method.
     *
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance) (i);

        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        //--- Check if there are any items left (should not occur! Only for VOI).
        if(fi.isEmpty()) return 0.0;

        //--- Calculate average weight.
        double avg = 0.0;
        for(BPPItem it : fi) {
            avg += it.weight/inst.binCapacity;
        }
        avg /= (double)(fi.size());

        double dev = 0.0; //--- Deviation

        //--- Sum of the squares of deviations from the average weight.
        for(BPPItem it : fi) {
            dev += Math.pow(it.weight/inst.binCapacity - avg ,2.0);
        }
        //--- Divide it by the number of items.
        dev /= (double)(fi.size());
        //--- Make square root from the variation.
        dev = Math.sqrt(dev);
        return dev;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
