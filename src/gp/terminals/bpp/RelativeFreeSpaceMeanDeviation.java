package gp.terminals.bpp;

        import gp.Node;
        import gp.tasks.bpp.BPPBin;
        import gp.tasks.bpp.BPPInstance;
        import gp.tasks.Instance;

/**
 * Created by Fanda on 14. 3. 2016.
 */
public class RelativeFreeSpaceMeanDeviation extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public RelativeFreeSpaceMeanDeviation() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public RelativeFreeSpaceMeanDeviation clone() {
        return new RelativeFreeSpaceMeanDeviation();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);
        double meanDev =0.0;
        double meanFS = 0.0;
        for(BPPBin b : inst.bins) {
            meanFS += (b.freeSpace/b.binCapacity);
        }
        meanFS /= inst.bins.size();

        for(BPPBin b : inst.bins) {
            meanDev += Math.abs((b.freeSpace/b.binCapacity) - meanFS);
        }
        meanDev /= inst.bins.size();
        return meanDev;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}

