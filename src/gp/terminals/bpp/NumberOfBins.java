package gp.terminals.bpp;

import gp.tasks.bpp.BPPInstance;
import gp.tasks.Instance;
import gp.Node;

/**
 * Created by Fanda on 12. 4. 2016.
 */
public class NumberOfBins extends Node{
    /**
     * Constructor of node.
     */
    public NumberOfBins() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public NumberOfBins clone() {
        return new NumberOfBins();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);
        return inst.bins.size();
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
