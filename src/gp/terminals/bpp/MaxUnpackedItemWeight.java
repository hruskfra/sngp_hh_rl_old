package gp.terminals.bpp;

import gp.Node;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.Instance;

/**
 * Created by Fanda on 12. 4. 2016.
 */
public class MaxUnpackedItemWeight  extends Node {

    /**
     * Constructor of node.
     */
    public MaxUnpackedItemWeight() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public MaxUnpackedItemWeight clone() {
        return new MaxUnpackedItemWeight();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);
        if(inst.getUnpackedItems().size()==0) {
            return 0.0;

        }
        return inst.getLargestUnpackedItem().weight;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
