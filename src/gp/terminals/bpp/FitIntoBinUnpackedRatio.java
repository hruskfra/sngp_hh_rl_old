package gp.terminals.bpp;

import gp.Node;
import gp.tasks.*;
import gp.tasks.bpp.BPP;
import gp.tasks.bpp.BPPInstance;
import gp.tasks.bpp.BPPItem;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Fanda on 14. 3. 2016.
 */
public class FitIntoBinUnpackedRatio extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public FitIntoBinUnpackedRatio() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public FitIntoBinUnpackedRatio clone() {
        return new FitIntoBinUnpackedRatio();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);

        ArrayList<BPPItem> fi = inst.getUnpackedItems();
        BPP.ITEMS_SORTING = BPP.sorting.decreasing;
        Collections.sort(fi);

        //--- Check if there are any items left (should not occur! Only for VOI).
        if(fi.isEmpty()) return 0.0;

        int counter = 0;
        for(BPPItem it : fi) {
            if(it.weight < inst.bins.get(inst.bins.size()-1).freeSpace) {
                counter++;
            }
        }
        return ((double)counter/(double)fi.size());
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}

