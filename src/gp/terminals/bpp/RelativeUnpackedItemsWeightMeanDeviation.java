
package gp.terminals.bpp;

        import gp.Node;
        import gp.tasks.bpp.BPPInstance;
        import gp.tasks.bpp.BPPItem;
        import gp.tasks.Instance;

        import java.util.ArrayList;

/**
 * Created by Fanda on 14. 3. 2016.
 */
public class RelativeUnpackedItemsWeightMeanDeviation extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public RelativeUnpackedItemsWeightMeanDeviation() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public RelativeUnpackedItemsWeightMeanDeviation clone() {
        return new RelativeUnpackedItemsWeightMeanDeviation();
    }

    /**
     * Execution method.
     * @param i instance in actual state
     * @return double value of this node.
     */
    public double execute_node(Instance i) {

        BPPInstance inst = (BPPInstance)(i);

        ArrayList<BPPItem> fi = inst.getUnpackedItems();

        double meanDev =0.0;
        double meanIW = 0.0;

        for(BPPItem it : fi) {
            meanIW += (it.weight/inst.maxItemWeight);
        }
        meanIW /= inst.items.length;

        for(BPPItem it : fi) {
            meanDev += Math.abs((it.weight/inst.maxItemWeight) - meanIW);
        }
        meanDev /= inst.items.length;
        return meanDev;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}

