/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals;

import gp.*;
import gp.individuals.Individual;

/**
 *
 * @author Fanda
 */
public class One extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public One() {
        this.arity = 0;
        this.value = 1.0;
        this.nodeValue = 1.0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
        this.name = this.getClass().getSimpleName();
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public One clone() {
        return new One();
    }
    /**
     * Execution method.
     * @param i executed individual.
     * @return double value of this node.
     */
    @Override
    public double execute_node(Individual i) {
        return 1.0;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
