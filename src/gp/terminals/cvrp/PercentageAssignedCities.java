/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 *
 * @author Fanda
 */
public class PercentageAssignedCities extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public PercentageAssignedCities() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public PercentageAssignedCities clone() {
        return new PercentageAssignedCities();
    }

    /**
     * Execution method.
     * @param t dump parameter.
     * @return double value of this node.
     */
    @Override
    public double execute_node(Individual t) {
        CVRPIndividual tt = (CVRPIndividual)(t);
        CVRPInstance cc = (CVRPInstance)tt.instance;
        
        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        double res = 0.0;
        for(int route = 0; route < cc.routes.size(); route++) {
            for(int town = 0; town < cc.routes.get(route).size(); town++) {
                if(cc.routes.get(route).get(town) != 0) {
                    res++;
                }
            }
        }
        res /= (double)(CVRP.INSTANCES[t.instanceExecutedID].size-1);
        res *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue =  res;
        return res;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
