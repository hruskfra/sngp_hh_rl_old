/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 *
 * @author Fanda
 */
public class PercentageMinRouteLength extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public PercentageMinRouteLength() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public PercentageMinRouteLength clone() {
        return new PercentageMinRouteLength();
    }

    /**
     * Execution method.
     * @param tt actual individual in execution process.
     * @return double value of this node.
     */
    public double execute_node(Individual tt) {

        CVRPIndividual t = (CVRPIndividual)(tt);
        CVRPInstance cinst = (CVRPInstance)CVRP.INSTANCES[t.instanceExecutedID];
        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }


        double res = Double.MAX_VALUE;
        double dist;
        double temp;
        for(int route = 0; route < cinst.routes.size(); route++) {
            temp = 0.0;
            for(int town = 0; town < cinst.routes.get(route).size()-1; town++) {
                dist = cinst.distances[cinst.routes.get(route).get(town)][cinst.routes.get(route).get(town+1)];
                temp += dist / cinst.maxDistanceBetweenCities;
            }
            if(res > temp) {
                res = temp;
            }
        }
        if(cinst.routes.isEmpty()) {
            res = 0.0;
        }

        res *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue = res;
        return res;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
