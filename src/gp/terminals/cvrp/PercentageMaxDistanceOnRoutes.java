/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 *
 * @author Fanda
 */
public class PercentageMaxDistanceOnRoutes extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public PercentageMaxDistanceOnRoutes() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public PercentageMaxDistanceOnRoutes clone() {
        return new PercentageMaxDistanceOnRoutes();
    }

    /**
     * Execution method.
     * @param tt actual individual in execution process.
     * @return double value of this node.
     */
    public double execute_node(Individual tt) {

        CVRPIndividual t = (CVRPIndividual)(tt);
        CVRPInstance cc = (CVRPInstance) CVRP.INSTANCES[tt.instanceExecutedID];

        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        double res = 0.0;
        for(int r = 0; r < cc.routes.size(); r++) {
            for(int c = 0; c < cc.routes.get(r).size()-1; c++) {
                if(res < cc.distances[cc.routes.get(r).get(c)][cc.routes.get(r).get(c+1)]){
                    res = cc.distances[cc.routes.get(r).get(c)][cc.routes.get(r).get(c+1)];
                }
            }
        }

        res /= cc.maxDistanceBetweenCities;
        res *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue = res;

        return res;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
