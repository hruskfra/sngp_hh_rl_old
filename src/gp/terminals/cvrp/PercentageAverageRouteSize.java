/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 * @author Fanda
 */
public class PercentageAverageRouteSize extends Node implements Cloneable {

    /**
     * Constructor of node.
     */
    public PercentageAverageRouteSize() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    @Override
    public PercentageAverageRouteSize clone() {
        return new PercentageAverageRouteSize();
    }

    /**
     * Execution method.
     * @param tt actual individual in execution process.
     * @return double value of this node.
     */
    public double execute_node(Individual tt) {

        CVRPIndividual t = (CVRPIndividual)(tt);
        CVRPInstance cc = (CVRPInstance)tt.instance;

        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        double res = 0.0;
        for (int route = 0; route < cc.routes.size(); route++) {
            res += cc.routes.get(route).size()-2;

        }
        res /= (double) (cc.routes.size());
        res /= (double) (cc.size-1);
        if(Double.isNaN(res)) res = 0.0;
        if(cc.routes.isEmpty()) res = 0.0;

        res *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue = res;
        return res;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
