/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.SNGP_HH_RL;
import sngp.Settings;
import gp.individuals.CVRPIndividual;
import java.awt.geom.Line2D;
import java.util.ArrayList;


/**
 * @author Fanda
 */
public class PercentageIntersectingLines extends Node implements Cloneable {

    /**
     * Constructor of node.
     */
    public PercentageIntersectingLines() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    @Override
    public PercentageIntersectingLines clone() {
        return new PercentageIntersectingLines();
    }

    /**
     * Execution method.
     * @param tt actual individual in execution process.
     * @return double value of this node.
     */
    public double execute_node(Individual tt) {

        CVRPIndividual t = (CVRPIndividual)(tt);
        CVRPInstance cinst = (CVRPInstance)CVRP.INSTANCES[t.instanceExecutedID];

        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        double res = 0;
        float cx1, cx2, cy1, cy2;
        for (ArrayList<Integer> r : cinst.routes) {
            //for(Integer t1 : r) {
            for (int t11 = 0; t11 < r.size() - 2; t11++) {
                int t1 = r.get(t11);
                int t2 = r.get(t11 + 1);
                for (int t22 = 1; t22 < r.size() - 1; t22++) {

                    int t3 = r.get(t22);
                    int t4 = r.get(t22 + 1);
                    cx1 = (float) cinst.cities[t1].values[0];
                    cy1 = (float) cinst.cities[t1].values[1];
                    cx2 = (float) cinst.cities[t2].values[0];
                    cy2 = (float) cinst.cities[t2].values[1];
                    Line2D line1 = new Line2D.Float(cx1, cy1, cx2, cy2);
                    cx1 = (float) cinst.cities[t3].values[0];
                    cy1 = (float) cinst.cities[t3].values[1];
                    cx2 = (float) cinst.cities[t4].values[0];
                    cy2 = (float) cinst.cities[t4].values[1];
                    Line2D line2 = new Line2D.Float(cx1, cy1, cx2, cy2);
                    if (line2.intersectsLine(line1)) {
                        res++;
                    }
                }
            }
        }

        res /= (double)((CVRP.INSTANCES[t.instanceExecutedID].size-1)*(CVRP.INSTANCES[t.instanceExecutedID].size-1));
        if(res > 1.0) res = 1.0; // Cut the outlayed values
        res *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue = res;
        return res;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
