/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 *
 * @author Fanda
 */
public class PercentageAverageDistanceOnRoutes extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public PercentageAverageDistanceOnRoutes() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public PercentageAverageDistanceOnRoutes clone() {
        return new PercentageAverageDistanceOnRoutes();
    }

    /**
     * Execution method.
     * @param t dump parameter.
     * @return double value of this node.
     */
    public double execute_node(Individual t) {

        CVRPIndividual tt = (CVRPIndividual)(t);
        CVRPInstance cinst = (CVRPInstance)CVRP.INSTANCES[t.instanceExecutedID];

        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        double res = 0.0;
        double count = 0.0;
        double dist;
        for(int r = 0; r < cinst.routes.size(); r++) {
            for(int c = 0; c < cinst.routes.get(r).size()-1; c++) {
                dist = cinst.distances[cinst.routes.get(r).get(c)][cinst.routes.get(r).get(c+1)];
                res += dist / cinst.maxDistanceBetweenCities;
                count++;
            }
        }
        res /= count;
        if(Double.isNaN(res)) res = 0.0;

        res *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue = res;
        return res;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
