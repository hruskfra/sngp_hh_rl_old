/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 *
 * @author Fanda
 */
public class PercentageRemainingDemands extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public PercentageRemainingDemands() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public PercentageRemainingDemands clone() {
        return new PercentageRemainingDemands();
    }

    /**
     * Execution method.
     * @param tt actual individual in execution process.
     * @return double value of this node.
     */
    public double execute_node(Individual tt) {

        CVRPIndividual t = (CVRPIndividual)(tt);
        CVRPInstance c = (CVRPInstance) CVRP.INSTANCES[t.instanceExecutedID];

        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        int[] freeCities = c.getUnassignedCities();
        double sum = 0.0;

        for(int i = 0 ; i < freeCities.length; i++) {
            sum += c.demands[freeCities[i]];
        }
        sum /= (c.sumDemands);
        sum *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue =  sum;
        return sum;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
