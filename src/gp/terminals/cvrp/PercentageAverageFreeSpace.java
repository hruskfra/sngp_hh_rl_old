/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 * @author Fanda
 */
public class PercentageAverageFreeSpace extends Node implements Cloneable {

    /**
     * Constructor of node.
     */
    public PercentageAverageFreeSpace() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     *
     * @return clone of this object.
     */
    @Override
    public PercentageAverageFreeSpace clone() {
        return new PercentageAverageFreeSpace();
    }

    /**
     * Execution method.
     * @param tt actual individual in execution process.
     * @return double value of this node.
     */
    public double execute_node(Individual tt) {

        CVRPIndividual t = (CVRPIndividual)(tt);
        CVRPInstance cinst = (CVRPInstance)CVRP.INSTANCES[t.instanceExecutedID];

        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        double freeSpace = 0.0;
        double cap;
        for(int r = 0; r < cinst.routes.size(); r++) {
            cap = 0.0;
            for(int c = 0; c < cinst.routes.get(r).size(); c++) {
                cap += cinst.demands[cinst.routes.get(r).get(c)];
            }
            freeSpace += cinst.vehicleCapacity-cap;
        }
        freeSpace /= (double)(cinst.routes.size());
        freeSpace /= (double)(cinst.vehicleCapacity);
        if(cinst.routes.isEmpty()) freeSpace = 0.0;
        if(Double.isNaN(freeSpace)) freeSpace = 0.0;

        freeSpace *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue = freeSpace;
        return freeSpace;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
