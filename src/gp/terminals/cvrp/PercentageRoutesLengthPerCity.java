/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 *
 * @author Fanda
 */
public class PercentageRoutesLengthPerCity extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public PercentageRoutesLengthPerCity() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public PercentageRoutesLengthPerCity clone() {
        return new PercentageRoutesLengthPerCity();
    }

    /**
     * Execution method.
     * @param tt actual individual in execution process.
     * @return double value of this node.
     */
    public double execute_node(Individual tt) {

        CVRPIndividual t = (CVRPIndividual)(tt);
        CVRPInstance c = (CVRPInstance) CVRP.INSTANCES[t.instanceExecutedID];

        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        double res;
        double tempDist = 0.0;
        double tempSize = 0.0;
        for(int route = 0; route < c.routes.size(); route++) {
            for(int town = 0; town < c.routes.get(route).size()-1; town++) {
                tempDist +=  c.distances[c.routes.get(route).get(town)][c.routes.get(route).get(town+1)];
            }
            tempSize += c.routes.get(route).size()-2;
        }
        if(c.routes.isEmpty()) {
            res = 0.0;
        }
        else {
            res = tempDist / tempSize;
            res /= c.worstSolutionDistance;
        }

        res *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue = res;
        return res;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
