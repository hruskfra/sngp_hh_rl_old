/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.MethodName;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 *
 * @author Fanda
 */
public class PercentageAverageDistanceAssignedFromDepot extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public PercentageAverageDistanceAssignedFromDepot() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public PercentageAverageDistanceAssignedFromDepot clone() {
        return new PercentageAverageDistanceAssignedFromDepot();
    }

    /**
     * Execution method.
     * @param t dump parameter.
     * @return double value of this node.
     */
    public double execute_node(Individual t) {

        CVRPIndividual tt = (CVRPIndividual)(t);
        CVRPInstance cc = (CVRPInstance) CVRP.INSTANCES[tt.instanceExecutedID];

        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        double res = 0.0;
        double count = 0.0;
        for(int r = 0; r < cc.routes.size(); r++) {
            for(int c = 0; c < cc.routes.get(r).size(); c++) {
                res += (cc.distances[0][cc.routes.get(r).get(c)])/ cc.maxDistanceFromDepot;
                if(cc.routes.get(r).get(c) != 0) {
                    count++;
                }
            }
        }
        res /= count;
        if(Double.isNaN(res)) res = 0.0;
        //--- Check if there is some unassigned city with distance defined.
        if(res == Double.POSITIVE_INFINITY) {
            System.err.println(MethodName.getCurrentMethodName() + " - WARNING - Node " + this.getClass().getSimpleName() + " returns INFINITY value!");
        }

        res *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;
        this.nodeValue = res;
        return res;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
