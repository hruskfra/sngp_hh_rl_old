/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp.terminals.cvrp;

import gp.*;
import gp.individuals.CVRPIndividual;
import gp.individuals.Individual;
import gp.tasks.cvrp.CVRP;
import gp.tasks.cvrp.CVRPInstance;
import sngp.MethodName;
import sngp.SNGP_HH_RL;
import sngp.Settings;

/**
 *
 * @author Fanda
 */
public class PercentageMinDistanceUnassignedFromDepot extends Node implements Cloneable{

    /**
     * Constructor of node.
     */
    public PercentageMinDistanceUnassignedFromDepot() {
        this.name = this.getClass().getSimpleName();
        this.arity = 0;
        this.returnType = Double.class;
        this.argumentTypes = new Class[this.arity][0];
    }

    /**
     * Shallow copy of object
     * @return clone of this object.
     */
    @Override
    public PercentageMinDistanceUnassignedFromDepot clone() {
        return new PercentageMinDistanceUnassignedFromDepot();
    }

    /**
     * Execution method.
     * @param tt actual individual in execution process.
     * @return double value of this node.
     */
    public double execute_node(Individual tt) {

        CVRPIndividual t = (CVRPIndividual)(tt);
        CVRPInstance cinst = (CVRPInstance)CVRP.INSTANCES[t.instanceExecutedID];

        if(SNGP_HH_RL.FIND_CONSTANT_TREES) {
            return this.randomValue;
        }

        double res = Double.POSITIVE_INFINITY;
        double temp;
        int[] freeCities = cinst.getUnassignedCities();
        for(int c : freeCities) {
            temp = cinst.distances[0][c];
            if(temp < res) {
                res = temp;
            }
        }
        //--- Complete instance.
        if(freeCities.length == 0) {
            res = 0.0;
        }
        //--- Check if there is some unassigned city with distance defined.
        if(res == Double.POSITIVE_INFINITY) {
            System.err.println(MethodName.getCurrentMethodName() + " - WARNING - Node " + this.getClass().getSimpleName() + " returns INFINITY value!");
        }
        res /= cinst.maxDistanceFromDepot;
        res *= Settings.TERMINAL_MULTIPLICATION_COEFICIENT;

        this.nodeValue = res ;
        return res;
    }

    /**
     * @return String representation of this node.
     */
    @Override
    public String toString() {
        return this.name;
    }

}
