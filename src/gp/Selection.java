/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp;

import sngp.MethodName;
import sngp.Settings;

import java.util.*;

import gp.individuals.*;

/**
 * @author Fanda
 */
public class Selection {

    /**
     * Selects node at random from the individual.
     *
     * @param ind population of nodes
     * @return randomly chosen individual's index
     */
    public static int randomSelection(Individual ind) {
        return (int) (Math.random() * (Settings.POPULATION_SIZE - Population.doubleTerminals.length)) + Population.doubleTerminals.length;
    }

    /**
     * Tournament selection.
     *
     * @param ind SNGP individual
     * @return index of the selected node
     */
    public static int tournamentSelection(Individual ind) {
        Random generator = new Random(System.currentTimeMillis());
        int best = generator.nextInt(Settings.POPULATION_SIZE - Population.doubleTerminals.length) + Population.doubleTerminals.length;
        int candidate;
        for (int i = 0; i < Settings.TOURNAMENT_SIZE; i++) {
            candidate = generator.nextInt(Settings.POPULATION_SIZE - Population.doubleTerminals.length) + Population.doubleTerminals.length;
            if (ind.nodes[best].nodeFitness > ind.nodes[candidate].nodeFitness) {
                best = candidate;
            }
        }
        return best;
    }

    /**
     * Selection method for evolution. Classic tournament selection with variable
     * number of individuals in tournament.
     *
     * @param ind population which from individuals are chosen.
     * @return index of winner individual.
     */
    public static int depthwiseTournamentSelection(Individual ind) {

        // First choose node at random.
        int rnd = (int) (Math.random() * (Settings.POPULATION_SIZE - Population.doubleTerminals.length) + Population.doubleTerminals.length);

        // DFS to find all parents among roots with randomly chosen node.
        ArrayList<Node> queue = new ArrayList();
        Node bestRoot = null;
        queue.add(ind.nodes[rnd]);
        Node actual;

        while (!queue.isEmpty()) {
            actual = queue.get(0);
            queue.remove(0);
            /*if(actual.parents.isEmpty()) {
                if(bestRoot == null || actual.nodeFitness < bestRoot.nodeFitness) {
                    bestRoot = actual;
                    continue;
                }
            }/**/
            if (bestRoot == null || actual.nodeFitness < bestRoot.nodeFitness) {
                bestRoot = actual;
            }
            for (Node p : actual.parents) {
                if (!queue.contains(p)) {
                    queue.add(p);
                }
            }
        }

        // BFS to store the tree and set its node's depth.
        ArrayList<Node> tree = new ArrayList();
        queue = new ArrayList();

        bestRoot.depth = 0;
        int actDepth = -1;
        queue.add(bestRoot);
        while (!queue.isEmpty()) {
            actDepth++;
            int qSize = queue.size();
            // Take all nodes in actual depth.
            for (int i = 0; i < qSize; i++) {
                actual = queue.get(0);
                queue.remove(0);
                // Terminals are unimportant.
                if (actual.arity == 0) {
                    continue;
                }
                // Add node to the tree.
                tree.add(actual);
                actual.depth = actDepth;

                for (Node ch : actual.childrenNodes) {
                    if (tree.contains(ch)) {
                        continue;
                    }
                    if (ch.arity == 0) {
                        continue;
                    }
                    if (queue.contains(ch)) {
                        continue;
                    }
                    // Add children nodes to the end of the queue to simulate stack.
                    queue.add(queue.size(), ch);
                }
            }
        }

        /*ind.printIndividual();
        System.out.println(MethodName.getCurrentMethodName() + " START " + rnd + " ---- root " + bestRoot.id);
        System.out.print(MethodName.getCurrentMethodName() + " tree ");
        for(Node t : tree) {
            System.out.print(t.id + "(" + t.depth + ") ");
        }
        System.out.println();/**/

        actual = tree.get((int) (Math.random() * tree.size()));
        for (int i = 1; i < Settings.TOURNAMENT_SIZE; i++) {
            bestRoot = tree.get((int) (Math.random() * tree.size()));
            if (bestRoot.depth > actual.depth) {
                actual = bestRoot;
            }
        }

        return actual.id;
    }

    /**
     * Fast tournament selection with best predecessor known.
     *
     * @param ind individual in which will tournament be performed
     * @return id of the selected node
     */
    public static int fasterTournamentSelection(Individual ind) {

        // First choose node at random.
        int rnd = (int) (Math.random() * (Settings.POPULATION_SIZE - Population.doubleTerminals.length) + Population.doubleTerminals.length);

        // Then get its best parent.
        Node bestRoot = ind.nodes[rnd].bestParent;
        bestRoot.depth = 0;

        // Get depth of the best tree.
        // BFS to store the tree and set its node's depth.
        ArrayList<Node> tree = new ArrayList();
        ArrayList<Node> queue = new ArrayList();
        Node actual;

        int actDepth = -1;
        queue.add(bestRoot);
        while (!queue.isEmpty()) {
            actDepth++;
            int qSize = queue.size();
            // Take all nodes in actual depth.
            for (int i = 0; i < qSize; i++) {
                actual = queue.get(0);
                queue.remove(0);
                // Terminals are unimportant.
                if (actual.arity == 0) {
                    continue;
                }
                // Add node to the tree.
                tree.add(actual);
                actual.depth = actDepth;

                for (Node ch : actual.childrenNodes) {
                    if (tree.contains(ch)) {
                        continue;
                    }
                    if (ch.arity == 0) {
                        continue;
                    }
                    if (queue.contains(ch)) {
                        continue;
                    }
                    // Add children nodes to the end of the queue to simulate stack.
                    queue.add(queue.size(), ch);
                }
            }
        }

        /*if( (System.currentTimeMillis() - actTime) > 10) {
            System.out.println("tree(" + (System.currentTimeMillis() - actTime) + ")[" + tree.size() + "],");
            actTime = System.currentTimeMillis();
        }/**/

        actual = tree.get((int) (Math.random() * tree.size()));
        for (int i = 1; i < Settings.TOURNAMENT_SIZE; i++) {
            bestRoot = tree.get((int) (Math.random() * tree.size()));
            if (bestRoot.depth > actual.depth) {
                actual = bestRoot;
            }
        }


        return actual.id;
    }
}
