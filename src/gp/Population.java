package gp;

import gp.terminals.HeuristicID;
import gp.terminals.bpp_ghh.*;
import gp.terminals.bpp_ghh.BinCapacity;
import gp.terminals.bpp_ghh.BinFreeSpace;
import gp.terminals.bpp_ghh.NumberOfBins;
import gp.terminals.cvrp.*;
import gp.terminals.bpp.*;
import gp.terminals.*;
import gp.functions.*;
import gp.terminals.bpp.RelativeFreeSpaceInActualBin;
import gp.terminals.bpp.RelativeHighestWeightOfUnpackedItem;
import gp.terminals.bpp.RelativeNumberOfPackedItems;
import gp.terminals.img.X;
import gp.terminals.img.Y;
import sngp.MethodName;
import sngp.SNGP_HH_RL;
import sngp.Settings;
import gp.individuals.Individual;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * Created by Fanda on 3. 9. 2015.
 */
public class Population {

    public static Individual bestIndividual;
    public static Individual actualIndividual;
    public static Individual newIndividual;
    public static Individual testIndividual;

    /**
     * Population of the nodes.
     */
    public static Node[] population;

    /**
     * Constants.
     */
    public static Node[] constants;

    /**
     * Array of terminal nodes.
     */
    public static Node[] doubleTerminals;

    /**
     * Array of function nodes.
     */
    public static Node[] doubleFunctions;

    /**
     * Generations counter
     */
    public static int generationsCounter = 0;

    /**
     * Initialize set of nodes used in GP.
     */
    public static void initGenotype(boolean print) {

        //--- Generate empty arrays.
        doubleTerminals = new Node[0];
        doubleFunctions = new Node[0];

        //--- Creates terminals with return type double.
        ArrayList<Node> temp = new ArrayList<Node>(); // Temporally ArrayList.
        //--- Set of terminals.

        //--- CVRP
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.CVRP) {
            temp.add(new HeuristicID());
            temp.add(new PercentageAssignedCities());
            temp.add(new PercentageMaxFreeSpace());
            temp.add(new PercentageAverageFreeSpace());
            temp.add(new PercentageMaxDistanceUnassignedFromDepot());
            temp.add(new PercentageAverageDistanceUnassignedFromDepot());
            temp.add(new PercentageRoutesLength());
            temp.add(new PercentageIntersectingLines());
            temp.add(new PercentageNumberOfRoutes());
            temp.add(new PercentageRemainingDemands());
        }

        //---BPP
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP) {
            /*temp.add(new RelativeFreeSpaceInActualBin());
            temp.add(new RelativeHighestWeightOfUnpackedItem());
            temp.add(new RelativeNumberOfPackedItems());
            //temp.add(new RelativeUnpackedItemsWeightMeanDeviation());
            temp.add(new RelativeFreeSpaceMeanDeviation());/**/

            /*temp.add(new RelativeAverageBinFullness());
            temp.add(new RelativeFreeSpaceStandardDeviation());
            temp.add(new RelativePackedItemsWeightMeanDeviation());
            temp.add(new RelativePackedItemsWeightStandardDeviation());
            temp.add(new RelativeUnpackedItemsWeightStandardDeviation());
            temp.add(new RelativeBinsUsedComparedToOptimum());/**/

            temp.add(new HeuristicID());
            temp.add(new RelativeFreeSpaceInActualBin());
            temp.add(new RelativeMinUnpackedWeight());
            temp.add(new RelativeMaxUnpackedWeight());
            temp.add(new RelativeMeanUnpackedWeight());
            temp.add(new RelativeDeviationUnpackedWeight());
            temp.add(new GreaterThanHalfUnpackedRatio());
            temp.add(new FitIntoBinUnpackedRatio());/**/

            //temp.add(new RelativeSuperFreeSpace());
            //temp.add(new TheoreticalOneBinLeft());

            /*temp.add(new BinCapacity());
            temp.add(new BinFreeSpace());
            temp.add(new MaxUnpackedItemWeight());
            temp.add(new MinUnpackedItemWeight());/**/


        }
        if (Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP_GHH) {

            temp.add(new BinCapacity());
            temp.add(new BinFreeSpace());
            temp.add(new BinFreeSpaceWithItem());
            temp.add(new BinOccupiedSpace());
            temp.add(new ItemWeight());
            //temp.add(new MostEmptyBinWithSomeItem());
            temp.add(new NextItemWeight());
            temp.add(new WeightSumUnpackedItems());
            //temp.add(new SmallestFreeSpace());

        }

        if(Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.IMG) {
            temp.add(new X());
            temp.add(new Y());
            temp.add(new One());
        }

            //temp.add(new Zero());
        //temp.add(new One());
        //--- Constants.
        for (int c = 0; c < Settings.CONSTANTS_SIZE; c++) {
            temp.add(new Constant());
            temp.get(temp.size() - 1).value = Math.random() * (Settings.CONSTNANT_MAX_VALUE-Settings.CONSTNANT_MIN_VALUE) - Settings.CONSTNANT_MIN_VALUE;
            //--- First constant is always zero.
            if (c == 0) {
                temp.get(temp.size() - 1).value = 0.0;
            }
            //--- Second constant is always one.
            if (c == 1) {
                temp.get(temp.size() - 1).value = 1.0;
            }
        }

        // Create array of double-terminals.
        doubleTerminals = new Node[temp.size()];
        for (int i = 0; i < doubleTerminals.length; i++) {
            doubleTerminals[i] = temp.get(i).clone();
        }


        // Creates functions with return type double.
        temp = new ArrayList<Node>();

        //--- BPP.
        /*temp.add(new Add());
        temp.add(new Sub());
        temp.add(new Mul());
        temp.add(new Div());
        temp.add(new Sqrt());
        temp.add(new Pow2());/**/

        //--- IMG.
        temp.add(new Add());
        temp.add(new Sub());
        temp.add(new Mul());
        temp.add(new Div());
        temp.add(new Sqrt());
        temp.add(new Pow2());
        temp.add(new Pow3());
        temp.add(new Sin());
        temp.add(new Log10());
        temp.add(new Max());
        temp.add(new Min());
        temp.add(new Less());
        temp.add(new Greater());

        //temp.add(new Pow3());
        //temp.add(new Exp());
        //temp.add(new Log10());
        //temp.add(new Sigmoid());
        //temp.add(new Tanh());
        //temp.add(new Sin());
        //temp.add(new Cos());

        //temp.add(new UnaryMinus());
        /*temp.add(new Max());
        temp.add(new Min());
        temp.add(new Less());
        temp.add(new Greater());/**/

        doubleFunctions = new Node[temp.size()];
        for (int i = 0; i < doubleFunctions.length; i++) {
            doubleFunctions[i] = temp.get(i).clone();
        }


        if (print) {
            System.out.println("SNGP: Number of terminal nodes set to " + (doubleTerminals.length));
            System.out.print("SNGP: Terminal nodes: ");
            for(int t = 0; t < doubleTerminals.length; t++) {
                System.out.print(doubleTerminals[t].getClass().getSimpleName());
                if(doubleTerminals[t].getClass() == Constant.class) {
                    break;
                }
                if(t != doubleTerminals.length-1) System.out.print(", ");
            }
            System.out.println();
            System.out.println("SNGP: Number of functional nodes set to " + (doubleFunctions.length));
            System.out.print("SNGP: Terminal nodes: ");
            for(int t = 0; t < doubleFunctions.length; t++) {
                System.out.print(doubleFunctions[t].getClass().getSimpleName());
                if(t != doubleFunctions.length-1) System.out.print(", ");
            }
            System.out.println();
        }
        if (doubleTerminals.length >= Settings.POPULATION_SIZE) {
            System.err.println(MethodName.getCurrentMethodName() + ": WARNING - Cannot create proper population! The number of terminals (" + doubleTerminals.length + ") is greater than the size of the population (" + Settings.POPULATION_SIZE + ")! ");
            System.exit(1);
        }
    }

    /**
     * Reinitialize constants for the new experiment.
     */
    public static void reinitializeConstants() {
        for (int i = 0; i < Settings.CONSTANTS_SIZE; i++) {
            if (i == 0) {
                Population.doubleTerminals[Population.doubleTerminals.length - Settings.CONSTANTS_SIZE + i].value = 0.0;
                continue;
            }
            if (i == 1) {
                Population.doubleTerminals[Population.doubleTerminals.length - Settings.CONSTANTS_SIZE + i].value = 1.0;
                continue;
            }
            Population.doubleTerminals[Population.doubleTerminals.length - Settings.CONSTANTS_SIZE + i].value = Math.random();
        }
    }

    /**
     * Initializes the population.
     */
    public static void initPopulation() {
        actualIndividual = Individual.createNewInstance();
        newIndividual = actualIndividual.clone();
        bestIndividual = actualIndividual.clone();
    }

    /**
     * Returns String representation of all nodes in the genotype.
     * @ return String representation of the genotype
     */
    public static String genotypeToString() {
        String s = "GENOTYPE:\r\n";
        s += "Terminals: ";
        for(int t = 0 ;t < doubleTerminals.length; t++) {
            s += doubleTerminals[t].getClass().getSimpleName() + " ";
        }
        s += "\r\nFunctions: ";
        for(int f = 0; f < doubleFunctions.length; f++) {
            s += doubleFunctions[f].getClass().getSimpleName() + " ";
        }
        s += "\r\n\r\n";
        return s;
    }
}
