/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp;

import gp.tasks.Problem;
import sngp.MethodName;
import sngp.SNGP_HH_RL;
import sngp.Settings;
import gp.individuals.*;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Fanda
 */
public class Mutation {

    /**
     * Mutates node in certain position of the tree.
     */
    public static void nodeReplacementMutation(Individual ind) {

        //System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Not implemented!");
        // Return individual.
        //Individual retI;

        // Id of the mutated node in the individual.
        //int rndNode = (int)(Math.random()*(Settings.POPULATION_SIZE-Population.doubleTerminals.length) + Population.doubleTerminals.length);
        int rndNode = -1;
        if (Settings.SELECTION == Settings.Selection.depthwise) {
            //mutNode = Selection.tournamentSelection(ind);
            rndNode = Selection.fasterTournamentSelection(ind);
        }
        if (Settings.SELECTION == Settings.Selection.random) {
            rndNode = Selection.randomSelection(ind);
        }

        int rndFunc = (int) (Math.random() * Population.doubleFunctions.length);

        //System.out.println(MethodName.getCurrentMethodName() + " Node " + rndNode + " | origArity " + ind.nodes[rndNode].arity + " | newArity " + Population.doubleFunctions[rndFunc].arity);
        // If the node has the same arity as an old one, change only its function.
        if (ind.nodes[rndNode].arity == Population.doubleFunctions[rndFunc].arity) {
            int[] childrenIDs = new int[ind.nodes[rndNode].arity];
            int[] parentsIDs = new int[ind.nodes[rndNode].parents.size()];
            ArrayList<Integer>[] childrenInParent = new ArrayList[ind.nodes[rndNode].parents.size()]; // node - parent, children - node

            // Store children ids.
            for (int child = 0; child < ind.nodes[rndNode].arity; child++) {
                // Save children id.
                childrenIDs[child] = ind.nodes[rndNode].childrenNodes[child].id;
                // Delete link from children.
                ind.nodes[rndNode].childrenNodes[child].parents.remove(ind.nodes[rndNode]);
            }
            // Store parents ids.
            for (int parent = 0; parent < ind.nodes[rndNode].parents.size(); parent++) {
                childrenInParent[parent] = new ArrayList();
                parentsIDs[parent] = ind.nodes[rndNode].parents.get(parent).id;
                // Find reference on the mutated point in a list of children nodes of the parent of this node to future replacing.
                for (int ch = 0; ch < ind.nodes[rndNode].parents.get(parent).arity; ch++) {
                    if (ind.nodes[rndNode].parents.get(parent).childrenNodes[ch].id == rndNode) {
                        childrenInParent[parent].add(ch);
                        //break;
                    }
                }
            }

            ind.nodes[rndNode] = Population.doubleFunctions[rndFunc].clone();
            ind.nodes[rndNode].id = rndNode;

            // Set original children nodes.
            for (int child = 0; child < ind.nodes[rndNode].arity; child++) {
                ind.nodes[rndNode].childrenNodes[child] = ind.nodes[childrenIDs[child]];
                ind.nodes[childrenIDs[child]].parents.add(ind.nodes[rndNode]);
            }
            // Set original parents nodes.
            for (int parent = 0; parent < ind.nodes[rndNode].parents.size(); parent++) {
                ind.nodes[rndNode].parents.add(ind.nodes[parentsIDs[parent]]);
                for (int ch = 0; ch < childrenInParent[parent].size(); ch++) {
                    ind.nodes[parentsIDs[parent]].childrenNodes[childrenInParent[parent].get(ch)] = ind.nodes[rndNode];
                }
            }

        } else {
            // Delete this node as a parent in other nodes.
            for (int child = 0; child < ind.nodes[rndNode].childrenNodes.length; child++) {
                ind.nodes[rndNode].childrenNodes[child].parents.remove(ind.nodes[rndNode]);
            }
            int[] parentsIDs = new int[ind.nodes[rndNode].parents.size()];
            ArrayList<Integer>[] childrenInParent = new ArrayList[ind.nodes[rndNode].parents.size()]; // node - parent, children - node
            // Store parents ids.
            for (int parent = 0; parent < ind.nodes[rndNode].parents.size(); parent++) {
                childrenInParent[parent] = new ArrayList();
                parentsIDs[parent] = ind.nodes[rndNode].parents.get(parent).id;
                // Find reference on the mutated point in a list of children nodes of the parent of this node to future replacing.
                for (int ch = 0; ch < ind.nodes[rndNode].parents.get(parent).arity; ch++) {
                    if (ind.nodes[rndNode].parents.get(parent).childrenNodes[ch].id == rndNode) {
                        childrenInParent[parent].add(ch);
                        break;
                    }
                }
            }
            ind.nodes[rndNode] = Population.doubleFunctions[rndFunc].clone();
            ind.nodes[rndNode].id = rndNode;

            // Set new randomly chosen children nodes.
            for (int child = 0; child < ind.nodes[rndNode].arity; child++) {
                int rnd = (int) (Math.random() * rndNode);
                ind.nodes[rndNode].childrenNodes[child] = ind.nodes[rnd];
                ind.nodes[rnd].parents.add(ind.nodes[rndNode]);
            }


            // Set original parents nodes.
            for (int parent = 0; parent < ind.nodes[rndNode].parents.size(); parent++) {
                ind.nodes[rndNode].parents.add(ind.nodes[parentsIDs[parent]]);
                for (int ch = 0; ch < childrenInParent[parent].size(); ch++) {
                    ind.nodes[parentsIDs[parent]].childrenNodes[childrenInParent[parent].get(ch)] = ind.nodes[rndNode];
                }
            }
        }

        // Spread out-of-date signal to all parents of switched nodes.
        ArrayList<Node> queue = new ArrayList();
        queue.add(ind.nodes[rndNode]);

        //queue.add(ind.nodes[mutChildID]);
        //queue.add(ind.nodes[newChild]);
        Node actual;
        while (!queue.isEmpty()) {
            actual = queue.get(0);
            actual.isFitnessActual = false;
            queue.remove(0);
            for (Node p : actual.parents) {
                if(p.isFitnessActual) {
                    queue.add(queue.size(), p);
                }
            }
            //queue.addAll(actual.parents.stream().collect(Collectors.toList()));
        }
    }

    /**
     * Changes the link between nodes.
     */
    public static void linkChangeMutation(Individual ind, boolean mutTree) {

        //System.out.println(MethodName.getCurrentMethodName() + " 0");
        //int mutNode = (int)(Math.random()*(Settings.POPULATION_SIZE-Population.doubleTerminals.length) + Population.doubleTerminals.length);
        int mutNode = -1;
        //--- Mutate node on the active tree.
        if(mutTree) {
            int treeNum = Problem.NUMBER_OF_LLHS; //SNGP_HH_RL.algs.size()-1;
            int rndTree = (int)(Math.random() * treeNum);
            rndTree += (Settings.POPULATION_SIZE-treeNum);
            ArrayList<Node> tree = ind.getTreeNodes(ind.nodes[rndTree]);
            //System.out.println(MethodName.getCurrentMethodName() + " 1");
            // Remove terminals.
            for(int t = 0; t < tree.size(); t++) {
                if(tree.get(t).arity == 0)  {
                    tree.remove(t);
                    t--;
                }
            }
            //System.out.println(MethodName.getCurrentMethodName() + " 2");
            // Random node.
            mutNode = tree.get((int)(Math.random() * tree.size())).id;
        }
        else {
            //System.out.println(MethodName.getCurrentMethodName() + " 3");
            if (Settings.SELECTION == Settings.Selection.depthwise) {
                //mutNode = Selection.tournamentSelection(ind);
                mutNode = Selection.fasterTournamentSelection(ind);
            }
            if (Settings.SELECTION == Settings.Selection.random) {
                mutNode = Selection.randomSelection(ind);
            }
        }

        //System.out.println(MethodName.getCurrentMethodName() + " 4");
        int mutChild = (int) (Math.random() * ind.nodes[mutNode].childrenNodes.length);
        int newChild;// = (int) (Math.random() * mutNode);

        // Decide if function or terminal will be the end of the new link.
        double rnd = Math.random();
        // If the mutation node os the first function, mutate link to the terminal.
        if(mutNode == Population.doubleTerminals.length) {
            rnd = -2.0;
        }
        if(rnd < Settings.LINK_TO_TERMINAL_PROB) {
            //System.out.println(MethodName.getCurrentMethodName() + " 5");
            // If it is terminal, decide if it will be variable (feature) or constant including 0 and 1 (that is that +2 here).
            if(Math.random() < Settings.LINK_TO_VARIABLE_PROB) {
                newChild = (int)(Math.random() * (Population.doubleTerminals.length - (Settings.CONSTANTS_SIZE)));
            }
            // It is constant.
            else {
                newChild = (int)(Math.random() *(Settings.CONSTANTS_SIZE) + (Population.doubleTerminals.length - (Settings.CONSTANTS_SIZE)));
            }
        }
        else {
           // System.out.println(MethodName.getCurrentMethodName() + " 6");
            newChild = (int)(Math.random() * (mutNode - Population.doubleTerminals.length) + Population.doubleTerminals.length);
        }

        //System.out.println(MethodName.getCurrentMethodName() + " 7");
        // Same nodes.
        if (newChild == ind.nodes[mutNode].childrenNodes[mutChild].id) {
            return;
        }

        //System.out.println(MethodName.getCurrentMethodName() + " 8");
        // Remove this node as parent.
        ind.nodes[mutNode].childrenNodes[mutChild].parents.remove(ind.nodes[mutNode]);
        ind.nodes[mutNode].childrenNodes[mutChild] = ind.nodes[newChild];
        ind.nodes[newChild].parents.add(ind.nodes[mutNode]);

        // Spread out-of-date signal to all parents of switched nodes.
        ArrayList<Node> queue = new ArrayList();
        queue.add(ind.nodes[mutNode]);
        Node actual;

        //System.out.println(MethodName.getCurrentMethodName() + " 9");
        while (!queue.isEmpty()) {
            actual = queue.get(0);
            actual.isFitnessActual = false;
            //System.out.println(MethodName.getCurrentMethodName() + "NODE: " + actual.id + " is DISABLED from " + mutNode);

            queue.remove(0);
            for (Node p : actual.parents) {
                if (p.isFitnessActual) {
                    queue.add(p);
                }
            }
        }

        //System.out.println(MethodName.getCurrentMethodName() + " 10");
    }

    /**
     * Changes the link between nodes.
     */
    public static void certainLinkMutation(Individual ind, int mutNode) {

        int mutChild = (int) (Math.random() * ind.nodes[mutNode].childrenNodes.length);
        int newChild;// = (int) (Math.random() * mutNode);

        // Link to the non constant tree.
        if(!ind.nodes[mutNode].childrenNodes[mutChild].returnsConstant) {
            return;
        }

        int counter = 0;
        do {
            newChild = (int) (Math.random() * (mutNode - Population.doubleTerminals.length) + Population.doubleTerminals.length);
            counter++;
            if(counter > 30) break;
        } while(!ind.nodes[newChild].returnsConstant);

        // Same nodes.
        if (newChild == ind.nodes[mutNode].childrenNodes[mutChild].id) {
            return;
        }


        // Remove this node as parent.
        ind.nodes[mutNode].childrenNodes[mutChild].parents.remove(ind.nodes[mutNode]);
        ind.nodes[mutNode].childrenNodes[mutChild] = ind.nodes[newChild];
        ind.nodes[newChild].parents.add(ind.nodes[mutNode]);

        // Spread out-of-date signal to all parents of switched nodes.
        ArrayList<Node> queue = new ArrayList();
        queue.add(ind.nodes[mutNode]);
        Node actual;
        while (!queue.isEmpty()) {
            actual = queue.get(0);
            actual.isFitnessActual = false;
            //System.out.println(MethodName.getCurrentMethodName() + "NODE: " + actual.id + " is DISABLED from " + mutNode);

            queue.remove(0);
            for (Node p : actual.parents) {
                if (p.isFitnessActual) {
                    queue.add(p);
                }
            }
        }
    }

    /**
     * Mutates constant value.
     */
    public static void constantMutation(Individual ind) {

        //System.out.println(MethodName.getCurrentMethodName() + " CONT MUT");
        // If there are no constants to mutate, exit this method.
        if(Settings.CONSTANTS_SIZE == 0) {
            return;
        }
        int mutConst = (int)(Math.random() * Settings.CONSTANTS_SIZE) + (Population.doubleTerminals.length - Settings.CONSTANTS_SIZE);

        // Do not mutate zero and one.
        if(mutConst == Population.doubleTerminals.length - Settings.CONSTANTS_SIZE || mutConst == Population.doubleTerminals.length - Settings.CONSTANTS_SIZE + 1) {
            return;
        }
        if(!ind.nodes[mutConst].name.equals("Constant")) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Not a constant node (" + mutConst +")!");
            System.exit(1);
        }
        ind.nodes[mutConst].value = Math.random() * (Settings.CONSTNANT_MAX_VALUE-Settings.CONSTNANT_MIN_VALUE) - Settings.CONSTNANT_MIN_VALUE;

        // Spread out-of-date signal to all parents of switched nodes.
        ArrayList<Node> queue = new ArrayList();
        queue.add(ind.nodes[mutConst]);
        Node actual;
        while (!queue.isEmpty()) {
            actual = queue.get(0);
            actual.isFitnessActual = false;
            //System.out.println(MethodName.getCurrentMethodName() + "NODE: " + actual.id + " is DISABLED from " + mutNode);

            queue.remove(0);
            for (Node p : actual.parents) {
                if (p.isFitnessActual) {
                    queue.add(p);
                }
            }
        }
    }

    /**
     * Moves function nodes contained in the best tree to the left side of the individual. Nodes of the best tree
     * transcript the original nodes.
     *
     * @param origInd
     */
    public static Individual originalMoveBestLeft(Individual origInd) {

        Individual newInd = origInd.clone();

        double bestF = Double.MAX_VALUE;
        int bestID = -1;
        for (int i = 0; i < Settings.POPULATION_SIZE; i++) {
            if (origInd.nodes[i].nodeFitness < bestF) {
                bestF = origInd.nodes[i].nodeFitness;
                bestID = i;
            }
        }

        if (bestID < Population.doubleTerminals.length) {
            return origInd;
        }

        /*System.out.println(MethodName.getCurrentMethodName());
        ind.printIndividual();
        System.out.println(MethodName.getCurrentMethodName() + " BEST ID " + bestID);/**/

        ArrayList<Integer> bestNodes = new ArrayList();
        ArrayList<Node> queue = new ArrayList();
        queue.add(origInd.nodes[bestID]);
        Node actual;
        // DFS to get whole tree rooted in the bestID node.
        while (!queue.isEmpty()) {
            actual = queue.get(0);
            // Add function to the queue.
            if (actual.arity != 0 && !bestNodes.contains(actual.id)) {
                bestNodes.add(actual.id);
            }
            queue.remove(0);

            if (actual.arity == 0) {
                continue;
            }
            for (int a = 0; a < actual.childrenNodes.length; a++) {
                // Do not add doubled nodes.
                if (queue.contains(actual.childrenNodes[a])) {
                    continue;
                }
                // Do not add terminal nodes.
                if (actual.childrenNodes[a].arity == 0) {
                    continue;
                }
                // Do not add nodes already placed in bestNodes array list.
                if (bestNodes.contains(actual.childrenNodes[a].id)) {
                    continue;
                }

                // Add this children
                queue.add(actual.childrenNodes[a]);

            }
        }


        Collections.sort(bestNodes);
        //System.out.println(MethodName.getCurrentMethodName() + " " + bestNodes.toString());

        // Reassign nodes from the best tree.
        int placePointer = Population.doubleTerminals.length;
        for (int n = 0; n < bestNodes.size(); n++) {
            // Replace node at the left side.
            newInd.nodes[placePointer++] = newInd.nodes[bestNodes.get(n)];

        }
        // Remove old nodes.
        for (int n = 0; n < bestNodes.size(); n++) {
            // Delete node from its original position, if the position is different.
            if (bestNodes.get(n) >= Population.doubleTerminals.length + bestNodes.size()) {
                newInd.nodes[bestNodes.get(n)] = null;
            }
        }

        // Fill the gaps from the best tree in the rest of the individual with the deep clones.
        for (int i = Population.doubleTerminals.length + bestNodes.size(); i < Settings.POPULATION_SIZE; i++) {
            // Gap, create clone and assign it proper children.
            if (newInd.nodes[i] == null) {
                // Create copy of the node.
                newInd.nodes[i] = origInd.nodes[i].clone();
                newInd.nodes[i].nodeFitness = origInd.nodes[i].nodeFitness;
                // Assign proper children.
                for (int ch = 0; ch < origInd.nodes[i].arity; ch++) {
                    // If children in original individual is out of shrinked part of the population...
                    if (origInd.nodes[i].childrenNodes[ch].id >= bestNodes.size() + Population.doubleTerminals.length) {
                        newInd.nodes[i].childrenNodes[ch] = newInd.nodes[origInd.nodes[i].childrenNodes[ch].id];
                    } else {
                        int childID = origInd.nodes[i].childrenNodes[ch].id;
                        int listIndex = -1;
                        for (int b = 0; b < bestNodes.size(); b++) {
                            if (bestNodes.get(b) == childID) {
                                listIndex = b;
                                break;
                            }
                        }
                        int positionIndex = Population.doubleTerminals.length + listIndex;
                        if (listIndex == -1) {
                            positionIndex = origInd.nodes[i].childrenNodes[ch].id;
                        }
                        newInd.nodes[i].childrenNodes[ch] = newInd.nodes[positionIndex];
                        //newInd.nodes[positionIndex].parents.add(newInd.nodes[i]);
                    }

                }
            }
            // Copy ids from original individual.
            else {
                for (int ch = 0; ch < newInd.nodes[i].arity; ch++) {
                    newInd.nodes[i].childrenNodes[ch] = newInd.nodes[origInd.nodes[i].childrenNodes[ch].id];
                }
            }
        }

        for (int i = 0; i < Settings.POPULATION_SIZE; i++) {
            newInd.nodes[i].id = i;
        }

        // Clear parent nodes.
        for (Node n : newInd.nodes) {
            n.parents = new ArrayList();

            if (n.id >= Population.doubleTerminals.length + bestNodes.size()) {
                n.isFitnessActual = false;
            } else {
                n.isFitnessActual = true;
            }

            if (n.arity == 0) {
                continue;
            }
            for (Node ch : n.childrenNodes) {
                ch.parents.add(n);
            }
        }

        if (!newInd.checkIndividual()) {
            System.exit(1);
        }

        return newInd;
    }

    /**
     * Moves function nodes contained in the best tree to the left side of the individual. Nodes of the best tree
     * transcript the original nodes.
     *
     * @param origInd
     */
    public static Individual originalMoveBestRight(Individual origInd) {

        Individual newInd = origInd.clone();

        //long actTime = System.currentTimeMillis();

        double bestF = Double.MAX_VALUE;
        int bestID = -1;
        for (int i = 0; i < Settings.POPULATION_SIZE; i++) {
            if (origInd.nodes[i].nodeFitness < bestF) {
                bestF = origInd.nodes[i].nodeFitness;
                bestID = i;
            }
        }

        if (bestID < Population.doubleTerminals.length) {
            return origInd;
        }

        if(bestID == Settings.POPULATION_SIZE-1) {return origInd;}

        /*System.out.print("best(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/


        ArrayList<Integer> bestNodes = new ArrayList();
        ArrayList<Node> queue = new ArrayList();
        queue.add(origInd.nodes[bestID]);
        Node actual;
        // DFS to get whole tree rooted in the bestID node.
        while (!queue.isEmpty()) {
            actual = queue.get(0);
            // Add function to the queue.
            //if (actual.arity != 0 && !bestNodes.contains(actual.id)) {
            if(actual.arity != 0) {
                bestNodes.add(actual.id);
            }

            queue.remove(0);

            if (actual.arity == 0) {
                continue;
            }/**/
            for (int a = 0; a < actual.childrenNodes.length; a++) {
                // Do not add doubled nodes.
                if (queue.contains(actual.childrenNodes[a])) {
                    continue;
                }
                // Do not add terminal nodes.
                if (actual.childrenNodes[a].arity == 0) {
                    continue;
                }
                // Do not add nodes already placed in bestNodes array list.
                if (bestNodes.contains(actual.childrenNodes[a].id)) {
                    continue;
                }
                // Add this node to the queue.
                //if (!bestNodes.contains(actual.childrenNodes[a].id)) {
                queue.add(actual.childrenNodes[a]);
                //}

            }
        }

        Collections.sort(bestNodes);

        /*if(print) {
            System.out.println("\r\n" + MethodName.getCurrentMethodName() + "------------------------------------------------");
            origInd.printColoredIndividual(bestNodes);
        }/**/
        /*System.out.print("tree(" + ((System.currentTimeMillis() - actTime)) + ")[" + +bestNodes.size() + "],");
        actTime = System.currentTimeMillis();/**/

        // Reassign nodes from the best tree.
        int placePointer = Settings.POPULATION_SIZE - 1;
        for (int n = bestNodes.size() - 1; n >= 0; n--) {
            // Replace node at the left side.
            newInd.nodes[placePointer--] = newInd.nodes[bestNodes.get(n)];

        }

        /*System.out.print("replace(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        // Remove old nodes.
        for (int n = 0; n < bestNodes.size(); n++) {
            // Delete node from its original position, if the position is different than shrinked tree.
            if (bestNodes.get(n) < Settings.POPULATION_SIZE - bestNodes.size()) {
                newInd.nodes[bestNodes.get(n)] = null;
            }
        }

        /*System.out.print("delete(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        // Fill the gaps from the best tree in the rest of the individual with the deep clones.
        for (int i = Population.doubleTerminals.length; i < Settings.POPULATION_SIZE - bestNodes.size(); i++) {
            // Gap, create clone and assign it proper children.
            if (newInd.nodes[i] == null) {
                // Create copy of the node.
                newInd.nodes[i] = origInd.nodes[i].clone();
                newInd.nodes[i].nodeFitness = origInd.nodes[i].nodeFitness;
                // Assign proper children.
                for (int ch = 0; ch < origInd.nodes[i].arity; ch++) {
                    // Node only shows to the left and not to the best tree so copy references.
                    int childID = origInd.nodes[i].childrenNodes[ch].id;
                    newInd.nodes[i].childrenNodes[ch] = newInd.nodes[childID];
                    //newInd.nodes[childID].parents.add(newInd.nodes[i]);
                }
            }
            // Copy ids from original individual.
            else {
                for (int ch = 0; ch < newInd.nodes[i].arity; ch++) {
                    newInd.nodes[i].childrenNodes[ch] = newInd.nodes[origInd.nodes[i].childrenNodes[ch].id];
                }
            }
        }

        /*System.out.print("clone(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        for (int i = 0; i < Settings.POPULATION_SIZE; i++) {
            newInd.nodes[i].id = i;
        }

        // Clear parent nodes.
        for (Node n : newInd.nodes) {
            n.parents = new ArrayList();

            if (n.id >= Population.doubleTerminals.length && n.id < Settings.POPULATION_SIZE - bestNodes.size()) {
                n.isFitnessActual = false;
            } else {
                n.isFitnessActual = true;
            }

            if (n.arity == 0) {
                continue;
            }
            for (Node ch : n.childrenNodes) {
                ch.parents.add(n);
            }
        }


        /*System.out.print("parents(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        if (!newInd.checkIndividual()) {
            System.exit(1);
        }

        /*if(print) {
            int bnsize = bestNodes.size();
            bestNodes = new ArrayList();
            for (int i = 0; i < bnsize; i++) {
                bestNodes.add(Settings.POPULATION_SIZE - 1 - i);
            }
            newInd.printColoredIndividual(bestNodes);
        }/**/
        /*System.out.println("check(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/
        return newInd;
    }

    /**
     * Moves function nodes contained in the best tree to the left side of the individual. Nodes of the best tree
     * transcript the original nodes.
     *
     * @param origInd
     */
    public static Individual originalMoveBestLeft2(Individual origInd) {

        Individual newInd = origInd.clone();

        double bestF = Double.MAX_VALUE;
        int bestID = -1;
        for (int i = 0; i < Settings.POPULATION_SIZE; i++) {
            if (origInd.nodes[i].nodeFitness < bestF) {
                bestF = origInd.nodes[i].nodeFitness;
                bestID = i;
            }
        }

        if (bestID < Population.doubleTerminals.length) {
            return origInd;
        }

        /*System.out.println(MethodName.getCurrentMethodName());
        ind.printIndividual();
        System.out.println(MethodName.getCurrentMethodName() + " BEST ID " + bestID);/**/

        ArrayList<Integer> bestNodes = new ArrayList();
        ArrayList<Node> queue = new ArrayList();
        queue.add(origInd.nodes[bestID]);
        Node actual;
        // DFS to get whole tree rooted in the bestID node.
        while (!queue.isEmpty()) {
            actual = queue.get(0);
            // Add function to the queue.
            if (actual.arity != 0 && !bestNodes.contains(actual.id)) {
                bestNodes.add(actual.id);
            }
            queue.remove(0);

            if (actual.arity == 0) {
                continue;
            }
            for (int a = 0; a < actual.childrenNodes.length; a++) {
                // Do not add doubled nodes.
                if (queue.contains(actual.childrenNodes[a])) {
                    continue;
                }
                // Do not add terminal nodes.
                if (actual.childrenNodes[a].arity == 0) {
                    continue;
                }
                // Do not add nodes already placed in bestNodes array list.
                if (bestNodes.contains(actual.childrenNodes[a].id)) {
                    continue;
                }
                // Add this children
                queue.add(actual.childrenNodes[a]);
            }
        }

        Collections.sort(bestNodes);

        // Reassign nodes from the best tree.
        int placePointer = Population.doubleTerminals.length;
        for (int n = 0; n < bestNodes.size(); n++) {
            // Replace node at the left side.
            newInd.nodes[placePointer++] = newInd.nodes[bestNodes.get(n)];

        }
        // Remove old nodes.
        for (int n = 0; n < bestNodes.size(); n++) {
            // Delete node from its original position, if the position is different.
            if (bestNodes.get(n) >= Population.doubleTerminals.length + bestNodes.size()) {
                newInd.nodes[bestNodes.get(n)] = null;
            }
        }

        // Fill the gaps from the best tree in the rest of the individual with the deep clones.
        for (int i = Population.doubleTerminals.length + bestNodes.size(); i < Settings.POPULATION_SIZE; i++) {
            // Gap, create clone and assign it proper children.
            if (newInd.nodes[i] == null) {
                // Create copy of the node.
                newInd.nodes[i] = origInd.nodes[i].clone();
                newInd.nodes[i].nodeFitness = origInd.nodes[i].nodeFitness;
                newInd.nodes[i].isFitnessActual = false;

                 // Assign proper children.
                for (int ch = 0; ch < origInd.nodes[i].arity; ch++) {
                    // If children in original individual is out of shrinked part of the population...
                    if (origInd.nodes[i].childrenNodes[ch].id >= bestNodes.size() + Population.doubleTerminals.length) {
                        newInd.nodes[i].childrenNodes[ch] = newInd.nodes[origInd.nodes[i].childrenNodes[ch].id];
                    } else {
                        int childID = origInd.nodes[i].childrenNodes[ch].id;
                        int listIndex = -1;
                        for (int b = 0; b < bestNodes.size(); b++) {
                            if (bestNodes.get(b) == childID) {
                                listIndex = b;
                                break;
                            }
                        }
                        int positionIndex = Population.doubleTerminals.length + listIndex;
                        if (listIndex == -1) {
                            positionIndex = origInd.nodes[i].childrenNodes[ch].id;
                        }
                        newInd.nodes[i].childrenNodes[ch] = newInd.nodes[positionIndex];
                        //newInd.nodes[positionIndex].parents.add(newInd.nodes[i]);
                    }

                }
            }
            // Copy ids from original individual.
            else {
                for (int ch = 0; ch < newInd.nodes[i].arity; ch++) {
                    newInd.nodes[i].childrenNodes[ch] = newInd.nodes[origInd.nodes[i].childrenNodes[ch].id];
                }
            }
        }

        for (int i = 0; i < Settings.POPULATION_SIZE; i++) {
            newInd.nodes[i].id = i;
        }

        // Clear parent nodes.
        for (Node n : newInd.nodes) {
            n.parents = new ArrayList();

            if (n.id >= Population.doubleTerminals.length + bestNodes.size()) {
                n.isFitnessActual = false;
            } else {
                n.isFitnessActual = true;
            }

            if (n.arity == 0) {
                continue;
            }
            for (Node ch : n.childrenNodes) {
                ch.parents.add(n);
            }
        }

        if (!newInd.checkIndividual()) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Bad individual structure!");
            System.exit(1);
        }

        return newInd;
    }

    /**
     * Moves function nodes contained in the best tree to the left side of the individual. Nodes of the best tree
     * transcript the original nodes.
     *
     * @param origInd
     */
    public static Individual originalMoveBestRight2(Individual origInd) {

        Individual newInd = origInd.clone();

        //long actTime = System.currentTimeMillis();

        double bestF = Double.MAX_VALUE;
        int bestID = -1;
        for (int i = 0; i < Settings.POPULATION_SIZE; i++) {
            if (origInd.nodes[i].nodeFitness < bestF) {
                bestF = origInd.nodes[i].nodeFitness;
                bestID = i;
            }
        }

        if (bestID < Population.doubleTerminals.length) {
            return origInd;
        }

        /*System.out.print("best(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        /*System.out.println(MethodName.getCurrentMethodName());
        ind.printIndividual();
        System.out.println(MethodName.getCurrentMethodName() + " BEST ID " + bestID);/**/

        ArrayList<Integer> bestNodes = new ArrayList();
        ArrayList<Node> queue = new ArrayList();
        queue.add(origInd.nodes[bestID]);
        Node actual;
        // DFS to get whole tree rooted in the bestID node.
        while (!queue.isEmpty()) {
            actual = queue.get(0);
            // Add function to the queue.
            if (actual.arity != 0 && !bestNodes.contains(actual.id)) {
                bestNodes.add(actual.id);
            }
            queue.remove(0);

            if (actual.arity == 0) {
                continue;
            }/**/
            for (int a = 0; a < actual.childrenNodes.length; a++) {
                // Do not add doubled nodes.
                if (queue.contains(actual.childrenNodes[a])) {
                    continue;
                }
                // Do not add terminal nodes.
                if (actual.childrenNodes[a].arity == 0) {
                    continue;
                }
                // Do not add nodes already placed in bestNodes array list.
                if (bestNodes.contains(actual.childrenNodes[a].id)) {
                    continue;
                }
                // Add this node to the queue.
                //if (!bestNodes.contains(actual.childrenNodes[a].id)) {
                queue.add(actual.childrenNodes[a]);
                //}

            }
        }

        Collections.sort(bestNodes);

        /*System.out.print("tree(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        // Reassign nodes from the best tree.
        int placePointer = Settings.POPULATION_SIZE - 1;
        for (int n = bestNodes.size() - 1; n >= 0; n--) {
            // Replace node at the left side.
            newInd.nodes[placePointer--] = newInd.nodes[bestNodes.get(n)];

        }

        /*System.out.print("replace(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        // Remove old nodes.
        for (int n = 0; n < bestNodes.size(); n++) {
            // Delete node from its original position, if the position is different than shrinked tree.
            if (bestNodes.get(n) < Settings.POPULATION_SIZE - bestNodes.size()) {
                newInd.nodes[bestNodes.get(n)] = null;
            }
        }

        /*System.out.print("delete(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        // Fill the gaps from the best tree in the rest of the individual with the deep clones.
        for (int i = Population.doubleTerminals.length; i < Settings.POPULATION_SIZE - bestNodes.size(); i++) {
            // Gap, create clone and assign it proper children.
            if (newInd.nodes[i] == null) {
                // Create copy of the node.
                newInd.nodes[i] = origInd.nodes[i].clone();
                newInd.nodes[i].nodeFitness = origInd.nodes[i].nodeFitness;
                newInd.nodes[i].isFitnessActual = false;
                // Assign proper children.
                for (int ch = 0; ch < origInd.nodes[i].arity; ch++) {
                    // Node only shows to the left and not to the best tree so copy references.
                    int childID = origInd.nodes[i].childrenNodes[ch].id;
                    newInd.nodes[i].childrenNodes[ch] = newInd.nodes[childID];
                    //newInd.nodes[childID].parents.add(newInd.nodes[i]);
                }
            }
            // Copy ids from original individual.
            else {
                for (int ch = 0; ch < newInd.nodes[i].arity; ch++) {
                    newInd.nodes[i].childrenNodes[ch] = newInd.nodes[origInd.nodes[i].childrenNodes[ch].id];
                }
            }
        }

        /*System.out.print("clone(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        for (int i = 0; i < Settings.POPULATION_SIZE; i++) {
            newInd.nodes[i].id = i;
        }

        // Clear parent nodes.
        for (Node n : newInd.nodes) {
            n.parents = new ArrayList();

            if (n.id >= Population.doubleTerminals.length && n.id < Settings.POPULATION_SIZE - bestNodes.size()) {
                n.isFitnessActual = false;
            } else {
                n.isFitnessActual = true;
            }

            if (n.arity == 0) {
                continue;
            }
            for (Node ch : n.childrenNodes) {
                ch.parents.add(n);
            }
        }

        /*System.out.print("parents(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/

        if (!newInd.checkIndividual()) {
            System.out.println(MethodName.getCurrentMethodName() + ": ERROR - Bad individual structure!");
            System.exit(1);
        }

        /*System.out.println("check(" +  ((System.currentTimeMillis()-actTime)) + "),");
        actTime = System.currentTimeMillis();/**/
        return newInd;
    }
}
