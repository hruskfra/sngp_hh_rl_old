/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gp;

import gp.individuals.Individual;
import gp.tasks.Instance;
import gp.tasks.Problem;
import sngp.MethodName;

import java.util.ArrayList;

/**
 * @author Fanda
 */
public abstract class Node {

    /**
     * id of the node.
     */
    public int id;

    /**
     * Number of arguments of the node.
     */
    public int arity;

    /**
     * Depth of the node.
     */
    public int depth = 0;

    /**
     * Name of the node.
     */
    public String name;

    /**
     * Value of terminal node.
     */
    public double value;

    /**
     * Array of children nodes.
     */
    public Node[] childrenNodes;

    /**
     * Parents of the node.
     */
    public ArrayList<Node> parents = new ArrayList();

    /**
     * Return type of node.
     */
    public Class returnType;

    /**
     * Return types of its arguments.
     */
    public Class[][] argumentTypes;

    /**
     * Fitness of the node.
     */
    public double nodeValue = 0.0;

    /**
     * Fitness of this node.
     */
    public double nodeFitness = Double.MAX_VALUE;

    /**
     * Fitness values for this node and for all instances.
     */
    public double[] nodeInstanceFitness = new double[Problem.NUMBER_OF_INSTANCES];

    /**
     * Best predecessor of the node.
     */
    public Node bestParent = this;

    /**
     * Indicates if this node value is actual.
     */
    public boolean isFitnessActual = false;

    /**
     * Indicates if tree rooted here creates constant or not.
     */
    public boolean returnsConstant = false;

    /**
     * Randomly generated value returned by this node.
     */
    public double randomValue = 0.0;

    /**
     * Indicates if the tree of this node is fixed and cannot be changed.
     */
    public boolean fixedNode = false;

    /**
     * Values of the nodes during execution
     */
    public int nodeOccurences = 0;

    /**
     * Opened node during BFS/DFS algorithms.
     */
    public boolean opened = false;

    /**
     * @return String used in infix representation.
     */
    public String toStringPart1() {
        return "";
    }

    /**
     * @return String used in infix representation.
     */
    public String toStringPart2() {
        return "";
    }

    /**
     * @return String used in infix representation.
     */
    public String toStringMiddlePart() {
        return "ERROR_" + this.name;
    }

    /**
     * Abstract method for execution of nodes. Rewritten in every type of node.
     */
    public double execute_node() {
        return execute_node();
    }

    /**
     * Method for execution method together with actual instance state.
     *
     * @param inst
     * @return
     */
    public double execute_node(Instance inst) {
        return -1.0;
    }

    /**
     * Abstract method for executing certain node in tree.
     *
     * @param i individual where some method should be done
     * @return soma double value
     */
    public double execute_node(Individual i) {
        return -1.0;
    }

    /**
     * Another execution method with parameter which mean some index depends on kind of node.
     *
     * @return execution result of this node.
     */
    public double execute_node(Individual t, int i, int j) {
        System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Should not execute this abstract method!");
        System.exit(1);
        return -2.0;
    }

    /**
     * Another execution method with parameter which mean some index depends on kind of node.
     *
     * @return execution result of this node.
     */
    public double execute_node(Individual t, int i, int j, int k) {
        System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Should not execute this abstract method!");
        System.exit(1);
        return -1.0;
    }

    /**
     * Calculates occurences of the node in the tree.
     */
    public void findNodeOccurencesInTheTree() {

        if (this.opened) {
            return;
        }
        if (this.arity == 0) {
            this.nodeOccurences++;
            return;
        } else {
            this.opened = true;
            for (Node ch : this.childrenNodes) {
                ch.findNodeOccurencesInTheTree();
            }
            this.nodeOccurences++;
            return;
        }
    }

    /**
     * Returns node which maps with the input String and it is in input array.
     * @param nodeName  name of the class of the node
     * @param nodes array of nodes where to find
     * @return corresponding node, if exists.
     */
    public static Node getNodeByName(String nodeName, Node[] nodes) {
        Node n = null;
        for (int c = 0; c < nodes.length; c++) {
            //System.out.println(nodes[1] + " " + Population.doubleTerminals.length);
            if (nodeName.equals(nodes[c].getClass().getSimpleName())) {
                n = nodes[c].clone();
                break;
            }
        }
        //--- Proper class not found.
        if (n == null) {
            System.err.println(MethodName.getCurrentMethodName() + ": ERROR - Bad function node! (" + nodes[1] + ")!");
            System.exit(1);
        }
        return n;
    }

    /**
     * Shallow copy of the node.
     *
     * @return Shallow copy of the node.
     */
    @Override
    public abstract Node clone();

}
