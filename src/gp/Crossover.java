package gp;

import gp.individuals.Individual;
import gp.tasks.bpp.BPP;
import sngp.Settings;

/**
 * Created by Fanda on 18-Apr-16.
 */
public class Crossover {

    /**
     * One point crossover.
     * This crossover takes two individuals (only if individuals are represented by the specific root node at the od of
     * the genome, like AlgorithmNode) and switches their roots.
     */
    public static void onePointCrossover(Individual ind) {

        int startIndex = -1, endIndex = -1;
        int p1, p2;
        int ch1, ch2;
        if(Settings.OPTIMIZATION_PROBLEM == Settings.OptimizationProblem.BPP) {
            startIndex = Settings.POPULATION_SIZE - BPP.actions.length*Settings.INDIVIDUALS_ROOTED_AT_THE_END;
            endIndex = Settings.POPULATION_SIZE;
        }

        //--- Choose two parents at random.
        p1 = (int)(Math.random()*(endIndex - startIndex) + startIndex);
        p2 = p1;
        //--- Make sure that parents will be different.
        while (p2 == p1) {
            p2 = (int)(Math.random()*(endIndex - startIndex) + startIndex);
        }

        //System.out.println(MethodName.getCurrentMethodName() + " p1 " + p1 + " p2 " + p2);

        //--- Store IDs of children of selected parents.
        ch1 = ind.nodes[p1].childrenNodes[0].id;
        ch2 = ind.nodes[p2].childrenNodes[0].id;

        //--- Check for possible loops created by crossover.
        if(ch1 >= p2 || ch2 >= p1) {
            return;
        }

        //--- Delete parent reference in their children.
        ind.nodes[ch2].parents.remove(ind.nodes[p2]);
        ind.nodes[ch1].parents.remove(ind.nodes[p1]);

        //--- Switch the links leading from the parent nodes.
        ind.nodes[p1].childrenNodes[0] = ind.nodes[ch2];
        ind.nodes[ch2].parents.add(ind.nodes[p1]);
        ind.nodes[p2].childrenNodes[0] = ind.nodes[ch1];
        ind.nodes[ch1].parents.add(ind.nodes[p2]);

        //System.out.println(MethodName.getCurrentMethodName() + " p1 " + p1 + " ch1 " + ind.nodes[p1].childrenNodes[0].id);
        //System.out.println(MethodName.getCurrentMethodName() + " p2 " + p2 + " ch2 " + ind.nodes[p2].childrenNodes[0].id);
    }
}
