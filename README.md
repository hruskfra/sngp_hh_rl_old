SNGP_HH_RL java library.

Library which can be used for automated generating low-level heuristic for variable optimization problems based on Single Node Genetic Programming.
Structure of the code allows easy incorporation of the new opimization problems. 

Predefined optimization problems:
- 1D-Bin-Packing
- Capacitated Vehicle Routing Problem
- Travelling Salesman Problem (work in progress)
- Travelling Salesman Problem with Time Windows (work in progress)

